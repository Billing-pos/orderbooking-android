package com.example.sheetalchauhan.orderbookingapplication.activities.product;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.SubCategoryAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.product.SubCagetoryData;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategoryActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_next)
    TextView tv_next;
    @BindView(R.id.rv_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.tv_address)
    TextView tv_address;
    DBAdapter dbAdapter;
    String address, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        ButterKnife.bind(this);
        tv_name.setText(R.string.subproduct_list);
        tv_next.setOnClickListener(this);
        back.setOnClickListener(this);
        address = AppUtils.readStringFromPref(SubCategoryActivity.this, "address");
        String addressHindi = AppUtils.readStringFromPref(SubCategoryActivity.this, "address2");
        phone = AppUtils.readStringFromPref(SubCategoryActivity.this, "phone");

        if (AppUtils.readStringFromPref(SubCategoryActivity.this, "changeLanguage").equalsIgnoreCase("english")) {
            tv_address.setText(address + "\n" + phone);
        } else {
            tv_address.setText(addressHindi + "\n" + phone);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(SubCategoryActivity.this, 2);
        recyclerView.setLayoutManager(layoutManager);

        dbAdapter = DBAdapter.getInstance(SubCategoryActivity.this);
        dbAdapter.open();
        List<SubCagetoryData> productDataList = dbAdapter.getSubCategory();
        dbAdapter.close();
        SubCategoryAdapter prodcutAdapter = new SubCategoryAdapter(SubCategoryActivity.this, productDataList);
        recyclerView.setAdapter(prodcutAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.tv_next:
                Intent intent = new Intent(SubCategoryActivity.this, EquantityActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
        }

    }
}
