package com.example.sheetalchauhan.orderbookingapplication.phase_II.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentResponseModel {

    @SerializedName("error")
    private String error;
    @SerializedName("msg")
    private List<String> msg;
    @SerializedName("data")
    private List<CommentDataResponseModel> data;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<String> getMsg() {
        return msg;
    }

    public void setMsg(List<String> msg) {
        this.msg = msg;
    }

    public List<CommentDataResponseModel> getData() {
        return data;
    }

    public void setData(List<CommentDataResponseModel> data) {
        this.data = data;
    }
}