package com.example.sheetalchauhan.orderbookingapplication.phase_II.utitlity;


import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.AreaFragment;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.ui.main.PlaceholderFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class SetTabLayout {

    private static SetTabLayout instance = null;

    public static SetTabLayout getInstance() {
        if (instance == null)
            instance = new SetTabLayout();
        return instance;
    }

    public void setTabView(Context context, final List<String> areaList, final FragmentManager fm, TabLayout tabs, final String recent, String all) {
        final ArrayList<String> list = (ArrayList<String>) areaList;

        TabLayout.Tab firstTab = tabs.newTab();
        firstTab.setText(context.getResources().getString(R.string.recent) + " " + getHindi(context, recent));
        tabs.addTab(firstTab, true);
        setFragment(1, fm, PlaceholderFragment.newInstance(recent, "area"));


        TabLayout.Tab secondTab = tabs.newTab();
        secondTab.setText(context.getResources().getString(R.string.all) + " " + getHindi(context, recent));
        tabs.addTab(secondTab);

        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0)
                    setFragment(1, fm, PlaceholderFragment.newInstance(recent, "area"));
                else
                    setFragment(1, fm, AreaFragment.newInstance(list, recent, "area"));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setFragment(int id, FragmentManager fm, Fragment fragment) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout, fragment);
        ft.commit();
    }

    private String getHindi(Context context, String title) {

        if (title.equalsIgnoreCase("area")) {
            title = context.getResources().getString(R.string.area);

        } else if (title.equalsIgnoreCase("sector")) {
            title = context.getResources().getString(R.string.sector);

        } else if (title.equalsIgnoreCase("apartment")) {
            title = context.getResources().getString(R.string.apartment);

        } else if (title.equalsIgnoreCase("block")) {
            title = context.getResources().getString(R.string.block);

        } else if (title.equalsIgnoreCase("house")) {
            title = context.getResources().getString(R.string.house);

        } else if (title.equalsIgnoreCase("floor")) {
            title = context.getResources().getString(R.string.floor);

        }
        return title;
    }

}
