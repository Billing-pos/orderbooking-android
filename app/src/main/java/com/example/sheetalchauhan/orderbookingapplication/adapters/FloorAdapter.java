package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.AddressListActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.PhoneListActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.FullAddressActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class FloorAdapter extends RecyclerView.Adapter<FloorAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<String> addressListList;
    int mCheckedPosition;
    FloorAdapter.ViewHolder viewHolder1;
    int flag;
    String lastAddress = null, newAddress = null, phone = "";
    DBAdapter dbAdapter;
    List<CustomergetAddress> list_newaddress = new ArrayList<>();
    boolean isAddressWithoutNumber = false;

    public FloorAdapter(Context context, List<String> addressListList) {
        this.context = context;
        this.addressListList = addressListList;
        lastAddress = AppUtils.readStringFromPref(context, "address");
        dbAdapter = DBAdapter.getInstance(context);
    }

    @Override
    public FloorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.area_layout, parent, false);
        viewHolder1 = new FloorAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final FloorAdapter.ViewHolder holder, final int position) {

        phone = AppUtils.readStringFromPref(context, "phone");
        String floor = "";

        if (Pattern.matches(".f", addressListList.get(position))) {
            floor = addressListList.get(position);

        } else if (Pattern.matches("..f", addressListList.get(position))) {
            floor = addressListList.get(position);

        } else if (Pattern.matches("...f", addressListList.get(position))) {
            floor = addressListList.get(position);

        } else {
            floor = addressListList.get(position) + "f";
        }

        holder.name.setText(floor);
        final String finalFloor = floor;
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hide_layout = AppUtils.readStringFromPref(context, "hide");

                if (hide_layout.contains("hide")) {

                    AppUtils.writeStringToPref(context, "floor", finalFloor);
                    AppUtils.writeStringToPref(context, "floor2", finalFloor);
                    String area = AppUtils.readStringFromPref(context, "area");
                    String phone = AppUtils.readStringFromPref(context, "phone");
                    String sector = AppUtils.readStringFromPref(context, "sector");
                    String apartment = AppUtils.readStringFromPref(context, "apartment");
                    String block = AppUtils.readStringFromPref(context, "block");
                    String house = AppUtils.readStringFromPref(context, "houseno");
                    String floor = AppUtils.readStringFromPref(context, "floor");
                    String strAddress = area + "," + sector + "," + apartment + "," + block + "/" + house + "," + floor;

                    AppUtils.writeStringToPref(context, "address", strAddress);
                    AppUtils.writeStringToPref(context, "address2", strAddress);
                    Intent intent = new Intent(context, PhoneListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    context.startActivity(intent);

                } else {

                    String temp = AppUtils.readStringFromPref(context, "AddressAdding");

                    if (temp.contains("update")) {
                        AppUtils.writeStringToPref(context, "floor", holder.name.getText().toString().replaceAll("f", ""));
                        checkNewAddressExist();
                        dbAdapter.open();

                    } else {
                        AppUtils.writeStringToPref(context, "floor", holder.name.getText().toString().replaceAll("f", ""));
                        Intent intent = new Intent(context, FullAddressActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    private void checkNewAddressExist() {
        String area = AppUtils.readStringFromPref(context, "area");
        String sector = AppUtils.readStringFromPref(context, "sector");
        String apartment = AppUtils.readStringFromPref(context, "apartment");
        String block = AppUtils.readStringFromPref(context, "block");
        String house = AppUtils.readStringFromPref(context, "houseno");
        String floor = AppUtils.readStringFromPref(context, "floor");
        String newAddress = area + "," + sector + "," + apartment + "," + block + "/" + house + "," + floor + "f";
        AppUtils.writeStringToPref(context, "companyname", "Firm");

        list_newaddress = dbAdapter.getExistingNumber(phone, newAddress);
        try {

            if (!lastAddress.equalsIgnoreCase(newAddress)) {
                if (!checkAddressHaveMobile()) {
                    for (int i = 0; i < list_newaddress.size(); i++) {

                        if (list_newaddress.get(0).getmobile().contains(phone) || list_newaddress.get(0).getmobile2().equalsIgnoreCase(phone) ||
                                list_newaddress.get(0).getmobile3().equalsIgnoreCase(phone) || list_newaddress.get(0).getmobile4().equalsIgnoreCase(phone) ||
                                list_newaddress.get(0).getmobile5().equalsIgnoreCase(phone)) {

                            Toast.makeText(context, R.string.mobile_already_reg_address, Toast.LENGTH_SHORT).show();

                        } else {

                            if (list_newaddress.get(0).getmobile().isEmpty()) {
                                updateMobile(list_newaddress.get(0).getId(), "mobile", phone);

                            } else if (list_newaddress.get(0).getmobile2().isEmpty()) {
                                updateMobile(list_newaddress.get(0).getId(), "mobile2", phone);

                            } else if (list_newaddress.get(0).getmobile3().isEmpty()) {
                                updateMobile(list_newaddress.get(0).getId(), "mobile3", phone);

                            } else if (list_newaddress.get(0).getmobile4().isEmpty()) {
                                updateMobile(list_newaddress.get(0).getId(), "mobile4", phone);

                            } else if (list_newaddress.get(0).getmobile5().isEmpty()) {
                                updateMobile(list_newaddress.get(0).getId(), "mobile5", phone);

                            } else {
                                Toast.makeText(context, "5 Mobile already registered with this address", Toast.LENGTH_SHORT).show();
                            }
                        }


                    }
                } else {

                    updateMobile(list_newaddress.get(0).getId(), "mobile", phone);
                }
            } else {
                Toast.makeText(context, R.string.same_address, Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            Toast.makeText(context, "An error occured ...", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateMobile(int tableId, String columnName, String phone) {
        int id = 0;
        id = tableId;

        if (id > 0) {
            Long rowId = dbAdapter.updateExistingAddress(tableId + "", columnName, phone);

            if (rowId == -1) {
                Toast.makeText(context, "Address is not Updated !", Toast.LENGTH_LONG).show();

            } else {
                checkLastMobilePositionToDelete();
            }
        } else {
            Toast.makeText(context, "something weng wrong ...", Toast.LENGTH_SHORT).show();

        }
    }

    private void checkLastMobilePositionToDelete() {
        List<CustomergetAddress> list_oldAddress = new ArrayList<>();
        list_oldAddress = dbAdapter.getExistingNumber(phone, lastAddress);

        String mobile = "", mobile2 = "", mobile3 = "", mobile4 = "", mobile5 = "";

        try {

            mobile = list_oldAddress.get(0).getmobile();
            mobile2 = list_oldAddress.get(0).getmobile2();
            mobile3 = list_oldAddress.get(0).getmobile3();
            mobile4 = list_oldAddress.get(0).getmobile4();
            mobile5 = list_oldAddress.get(0).getmobile5();

            if (phone.equalsIgnoreCase(mobile)) {
                removePhone("mobile", AppUtils.readStringFromPref(context, "addressid"));

            } else if (phone.equalsIgnoreCase(mobile2)) {
                removePhone("mobile2", AppUtils.readStringFromPref(context, "addressid"));

            } else if (phone.equalsIgnoreCase(mobile3)) {
                removePhone("mobile3", AppUtils.readStringFromPref(context, "addressid"));


            } else if (phone.equalsIgnoreCase(mobile4)) {
                removePhone("mobile4", AppUtils.readStringFromPref(context, "addressid"));


            } else if (phone.equalsIgnoreCase(mobile5)) {
                removePhone("mobile5", AppUtils.readStringFromPref(context, "addressid"));

            }

        } catch (Exception e) {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
        }

    }

    private void removePhone(String columnName, String tableId) {
        try {
            if (Integer.parseInt(tableId) > 0) {

                Long rowId = dbAdapter.removeUserPhone(columnName, Integer.parseInt(tableId));

                if (rowId == -1) {
                    Toast.makeText(context, "Phone is not removed", Toast.LENGTH_LONG).show();

                } else {
                    context.startActivity(new Intent(context, AddressListActivity.class));
                    ((Activity) context).finish();
                }

            } else {
                Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkAddressHaveMobile() {
        String mobile1 = (list_newaddress.get(0).getmobile()) == null ? "" : list_newaddress.get(0).getmobile();
        String mobile2 = list_newaddress.get(0).getmobile2();
        String mobile3 = list_newaddress.get(0).getmobile3();
        String mobile4 = list_newaddress.get(0).getmobile4();
        String mobile5 = list_newaddress.get(0).getmobile5();

        if (!mobile1.equalsIgnoreCase("")) {
            return false;
        }
        if (!mobile2.equalsIgnoreCase("")) {
            return false;
        }
        if (!mobile3.equalsIgnoreCase("")) {
            return false;
        }
        if (!mobile4.equalsIgnoreCase("")) {
            return false;
        }
        if (!mobile5.equalsIgnoreCase("")) {
            return false;
        } else {
            return true;
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;


        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.tv_area);

        }
    }

}