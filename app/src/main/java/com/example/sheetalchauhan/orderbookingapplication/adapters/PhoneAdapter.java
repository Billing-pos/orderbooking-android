package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Customer;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneAdapter extends RecyclerView.Adapter<PhoneAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<Customer> addressListList;
    int mCheckedPosition;
    Customer addressList;
    PhoneAdapter.ViewHolder viewHolder1;
    String flag, address1;
    DBAdapter dbAdapter;


    public PhoneAdapter(Context context1, List<Customer> addressListList, String flag, String address) {
        context = context1;
        this.addressListList = addressListList;
        this.flag = flag;
        this.address1 = address;

        dbAdapter = DBAdapter.getInstance(context);

    }

    // to validate phone number and telephone
    public static boolean isValidMobile(String s) {
        Pattern p = Pattern.compile("(0/91)?[5-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    public static boolean isValidTelephone(String s) {
        Pattern p = Pattern.compile("[2-9][0-9]{7}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    @Override
    public PhoneAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.address_item, parent, false);
        viewHolder1 = new PhoneAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final PhoneAdapter.ViewHolder holder, final int position) {

        addressList = addressListList.get(position);
        if (flag.equals("0")) {
            holder.ratio.setVisibility(View.GONE);
            holder.textview.setVisibility(View.VISIBLE);
            holder.ll_edit_delete.setVisibility(View.VISIBLE);
            holder.et_address.setText(addressList.getMobile());
        }

        holder.addressid.setText(String.valueOf(addressListList.get(position).getId()));
        holder.name.setText(addressList.getMobile());
        holder.deleteadress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                final Dialog dialog = new Dialog(context);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.dialog);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                dialog.setTitle("");
//                TextView text = (TextView) dialog.findViewById(R.id.textDialog);
//                text.setText("Are You sure for delete Address");
//                dialog.show();
//
//                Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
//                Button submit = (Button) dialog.findViewById(R.id.submit);
//                declineButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//
//                submit.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                        deleteAddress(holder.addressid.getText().toString().trim());
//                    }
//                });
            }
        });

        holder.ratio.setOnCheckedChangeListener(null);
        if (mCheckedPosition == 0) {
            AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());
            AppUtils.writeStringToPref(context, "phone", holder.name.getText().toString());
        }
        holder.ratio.setChecked(position == mCheckedPosition);
        holder.ratio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCheckedPosition = position;
                notifyDataSetChanged();
                AppUtils.writeStringToPref(context, "phone", holder.name.getText().toString());
                AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());

            }
        });


        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheckedPosition = position;
                notifyDataSetChanged();
                AppUtils.writeStringToPref(context, "phone", holder.name.getText().toString());
                AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());

            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.et_address.setVisibility(View.VISIBLE);
                holder.img_address_submit.setVisibility(View.VISIBLE);
                holder.name.setVisibility(View.GONE);
                holder.edit.setVisibility(View.GONE);
                holder.et_address.setVisibility(View.VISIBLE);

            }
        });

        holder.img_address_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phonestr = holder.et_address.getText().toString();
                if (phonestr.isEmpty()) {
                    holder.et_address.setError("Please Enter Phone No");
                    holder.et_address.requestFocus();
                    return;

                } else if (!isValidMobile(phonestr) && !isValidTelephone(phonestr)) {
                    holder.et_address.setError("invalid mobile !");
                    holder.et_address.requestFocus();
                    return;

                } else {
                    String strAddress = addressList.getMobile();
                    dbAdapter.open();
                    int addressCheck = dbAdapter.getCustomerAddressList(address1);

                    if (addressCheck > 0) {

                        Customer customer = new Customer();
                        customer.setAddress(strAddress);
                        customer.setMobile(phonestr);
                        customer.setFirm("");
                        customer.setFlag("1");

                        int id2 = addressList.getId();
                        int id = addressListList.get(position).getId();
                        String orderId = addressList.getOrderId();

                        long rowId = dbAdapter.updateCustomerMobile("" + id2, phonestr, orderId, "", "");
                        if (rowId == -1) {
                            Toast.makeText(context, R.string.update_phone_success_not, Toast.LENGTH_SHORT).show();

                        } else {

                            holder.et_address.setVisibility(View.GONE);
                            holder.img_address_submit.setVisibility(View.GONE);
                            holder.name.setText(holder.et_address.getText().toString());
                            holder.name.setVisibility(View.VISIBLE);
                            holder.edit.setVisibility(View.VISIBLE);

                            Toast.makeText(context, R.string.update_phone_success, Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        Customer customer = new Customer();
                        customer.setAddress(strAddress);
                        customer.setMobile(phonestr);
                        customer.setFirm("");
                        customer.setFlag("1");

                        int id2 = addressList.getId();
                        int id = addressListList.get(position).getId();
                        String orderId = addressList.getOrderId();

                        long rowId = dbAdapter.updateCustomerMobile("" + id2, phonestr, orderId, "", "");
                        if (rowId == -1) {
                            Toast.makeText(context, R.string.update_phone_success_not, Toast.LENGTH_SHORT).show();

                        } else {

                            holder.et_address.setVisibility(View.GONE);
                            holder.img_address_submit.setVisibility(View.GONE);
                            holder.name.setText(holder.et_address.getText().toString());
                            holder.name.setVisibility(View.VISIBLE);
                            holder.edit.setVisibility(View.VISIBLE);

                            Toast.makeText(context, R.string.update_phone_success, Toast.LENGTH_SHORT).show();

                        }
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, addressid, textview;
        public ImageView deleteadress, edit, img_address_submit;
        public RadioButton ratio;
        public EditText et_address;
        public LinearLayout ll_item;
        public RelativeLayout ll_edit_delete;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.address);
//            deleteadress = (ImageView) v.findViewById(R.id.deleteadress);
            addressid = (TextView) v.findViewById(R.id.addressid);
            ratio = (RadioButton) v.findViewById(R.id.ratio);
            edit = (ImageView) v.findViewById(R.id.edit);
            img_address_submit = (ImageView) v.findViewById(R.id.img_address_submit);
            et_address = (EditText) v.findViewById(R.id.et_address);
            textview = (TextView) v.findViewById(R.id.textview);
            ll_item = (LinearLayout) v.findViewById(R.id.ll_item);
            ll_edit_delete = (RelativeLayout) v.findViewById(R.id.ll_edit_delete);
        }
    }


}