package com.example.sheetalchauhan.orderbookingapplication.model.passcode;

import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ResponsePojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passcoderesponse extends ResponsePojo {
    @SerializedName("data")
    @Expose

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
