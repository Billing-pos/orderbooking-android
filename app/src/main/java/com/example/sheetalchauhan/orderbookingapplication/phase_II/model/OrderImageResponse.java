package com.example.sheetalchauhan.orderbookingapplication.phase_II.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderImageResponse {

    @SerializedName("error")
    @Expose
    private boolean error;

    @SerializedName("msg")
    @Expose
    private List<String> listString;

    @SerializedName("page")
    @Expose
    private String page;

    @SerializedName("data")
    @Expose
    private List<HindiAddressModel> addressModelList;


    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<String> getListString() {
        return listString;
    }

    public void setListString(List<String> listString) {
        this.listString = listString;
    }

    public List<HindiAddressModel> getAddressModelList() {
        return addressModelList;
    }

    public void setAddressModelList(List<HindiAddressModel> addressModelList) {
        this.addressModelList = addressModelList;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
