package com.example.sheetalchauhan.orderbookingapplication.phase_iii.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderRequestModel implements Serializable {

    @SerializedName("order_id")
    @Expose
    String order_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
