package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;

import java.util.List;

public class FirmFilterAdapter extends RecyclerView.Adapter<FirmFilterAdapter.ViewHolder> {
    Context context;
    View view1;
    List<Firm> addressListList;
    int mCheckedPosition;
    Firm firm;
    FirmFilterAdapter.ViewHolder viewHolder1;
    Clickonfirm listener;


    public FirmFilterAdapter(Context context1, List<Firm> addressListList, Clickonfirm listener) {
        context = context1;
        this.addressListList = addressListList;
        this.listener = listener;
    }

    @Override
    public FirmFilterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.filter_firm, parent, false);
        viewHolder1 = new FirmFilterAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final FirmFilterAdapter.ViewHolder holder, final int position) {

        if (addressListList == null || addressListList.isEmpty())
            return;
        holder.btn_firmId.setText(context.getResources().getString(R.string.firm_name) + "       : " + addressListList.get(position).getName()
                + "\n" + context.getResources().getString(R.string.firm_mobile) + "    : " + addressListList.get(position).getMobile()
                + "\n" + context.getResources().getString(R.string.firm_address) + " : " + addressListList.get(position).getAddress());

        holder.btn_firmId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickOnButton(addressListList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }


    public interface Clickonfirm {
        void clickOnButton(Firm model);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public Button btn_firmName, btn_firmId;

        public ViewHolder(View v) {
            super(v);
            btn_firmName = v.findViewById(R.id.btn_firmName);
            btn_firmId = v.findViewById(R.id.btn_firmId);
        }
    }

}
