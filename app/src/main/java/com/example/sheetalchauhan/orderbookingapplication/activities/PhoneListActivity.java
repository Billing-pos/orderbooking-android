package com.example.sheetalchauhan.orderbookingapplication.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.order.CustomerOrdersActivity;
import com.example.sheetalchauhan.orderbookingapplication.adapters.PhoneAdapter2;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppClass;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhoneListActivity extends AppCompatActivity implements View.OnClickListener, PhoneAdapter2.HideView2 {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_comment)
    TextView tv_comment;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_tags)
    TextView tv_tags;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.bt_shopping)
    Button bt_shopping;
    @BindView(R.id.ll_layout)
    LinearLayout ll_layout;
    @BindView(R.id.tv_choose)
    TextView tv_choose;
    String address, addressid;
    DBAdapter dbAdapter;
    String flag, tableId;
    PhoneAdapter2 addressAdapter;
    String phoneCount = null, tags;
    Context context;

    List<CustomergetAddress> customerList = new ArrayList<>();
    List<String> listPhone = new ArrayList<>();
    String address2 = "", aid = "", address1 = null;
    String add[];
    String add2[];
    String colno = "";
    String mobile = "";
    boolean isOnlyAddNumber = false;
    private String userType = null, contact_name = null, cartage = null, comments = null;
    private String calculation_weight;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_list);
        context = this;
        ButterKnife.bind(this);
        dbAdapter = DBAdapter.getInstance(this);
        back.setOnClickListener(this);
        bt_shopping.setOnClickListener(this);
        flag = AppUtils.readStringFromPref(PhoneListActivity.this, "flag");

        if (flag != null) {

            if (flag.equals("0")) {
                ll_layout.setVisibility(View.GONE);
                bt_shopping.setVisibility(View.GONE);
                tv_choose.setVisibility(View.GONE);
                tv_name.setText("" + AppUtils.readStringFromPref(PhoneListActivity.this, "address"));

            } else {
                tv_name.setText(R.string.phone_data);
                ll_layout.setVisibility(View.VISIBLE);
            }
        }

        address = AppUtils.readStringFromPref(PhoneListActivity.this, "address");
        address1 = address.replaceAll(", ", ",");

        ll_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                phoneCount = "" + addressAdapter.getItemCount();

                Log.i("phonecount", phoneCount);
                if (Integer.parseInt(phoneCount) >= 10) {
                    Toast.makeText(PhoneListActivity.this, "You have entered maximum phone number.", Toast.LENGTH_SHORT).show();

                } else {

                    add = address1.split(",");
                    add2 = add[3].split("/");
                    AppUtils.writeStringToPref(PhoneListActivity.this, "area", add[0]);
                    AppUtils.writeStringToPref(PhoneListActivity.this, "sector", add[1]);
                    AppUtils.writeStringToPref(PhoneListActivity.this, "apartment", add[2]);
                    AppUtils.writeStringToPref(PhoneListActivity.this, "block", add2[0]);
                    AppUtils.writeStringToPref(PhoneListActivity.this, "houseno", add2[1]);
                    AppUtils.writeStringToPref(PhoneListActivity.this, "floor", add[4]);
                    AppUtils.writeStringToPref(PhoneListActivity.this, "address", address1);
                    AppUtils.writeStringToPref(PhoneListActivity.this, "address2", address1);


                    Intent intent = new Intent(PhoneListActivity.this, AddressPhoneActivity.class);
                    AppUtils.writeStringToPref(PhoneListActivity.this, "AddressAdding", "phone");
                    intent.putExtra("add_address", address);
                    intent.putExtra("phoneCount", phoneCount);
                    intent.putExtra("tableId", tableId);
                    intent.putExtra("addNumberNew", isOnlyAddNumber);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
            }
        });
        if (getIntent().getBooleanExtra("addPhoneNew", false)) {
            tv_name.setText(R.string.add_new_phone);
            bt_shopping.setVisibility(View.GONE);
            isOnlyAddNumber = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialiseData();
    }

    private void initialiseData() {
        customerList.clear();
        listPhone.clear();
        dbAdapter.open();
        customerList = dbAdapter.getAllPhoneList(address1);
        if (customerList.size() > 0) {

            try {
                if (customerList != null && customerList.size() > 0) {
                    contact_name = customerList.get(0).getContact_name();
                    userType = customerList.get(0).getType();
                    cartage = customerList.get(0).getCartage();
                    comments = customerList.get(0).getComments();
                    calculation_weight = customerList.get(0).getCalculation_weight();

                    if (comments != null && !comments.isEmpty())
                        tv_comment.setText(getResources().getString(R.string.customer_comment) + " : " + comments);
                }

                try {
                    if (calculation_weight != null && !calculation_weight.isEmpty() && !calculation_weight.equalsIgnoreCase("null"))
                        AppUtils.writeStringToPref(context, "calculation_weight", calculation_weight);
                } catch (Exception e) {
                    AppUtils.writeStringToPref(context, "calculation_weight", "20");
                    e.printStackTrace();
                }

                if (userType != null) {
                    AppUtils.writeStringToPref(context, "userType", userType);
                    AppUtils.writeStringToPref(context, "category_id", userType);
                }

                if (cartage != null && !cartage.isEmpty() && !cartage.equalsIgnoreCase("null"))
                    AppUtils.writeStringToPref(context, "cartage", cartage);

                if (contact_name != null)
                    AppUtils.writeStringToPref(context, "userName", contact_name);

                String mobile6 = (customerList.get(0).getMobile6() != null) ? customerList.get(0).getMobile6() : "";
                String mobile7 = (customerList.get(0).getMobile7() != null) ? customerList.get(0).getMobile7() : "";
                String mobile8 = (customerList.get(0).getMobile8() != null) ? customerList.get(0).getMobile8() : "";
                String mobile9 = (customerList.get(0).getMobile9() != null) ? customerList.get(0).getMobile9() : "";
                String mobile10 = (customerList.get(0).getMobile10() != null) ? customerList.get(0).getMobile10() : "";

                tableId = customerList.get(0).getId() + "";

                mobile = (customerList.get(0).getMobile() == null) ? "" : customerList.get(0).getMobile();

                if (!mobile.contains("") || !mobile.isEmpty()) {
                    listPhone.add(customerList.get(0).getmobile());

                } else {
                    AppUtils.writeStringToPref(this, "colNo", "1");

                }
                tags = customerList.get(0).getTags();

                if (tags.contains("green")) {
                    tv_tags.setBackgroundColor(context.getResources().getColor(R.color.colorgreen));

                } else if (tags.contains("red")) {
                    tv_tags.setBackgroundColor(context.getResources().getColor(R.color.colorred));

                } else if (tags.contains("yellow")) {
                    tv_tags.setBackgroundColor(context.getResources().getColor(R.color.coloryellow));

                } else {
//            tv_tags.setBackgroundColor(context.getResources().getColor(R.color.colorred));
                }

                customerList.get(0).getmobile2();
                if (!customerList.get(0).getmobile2().isEmpty()) {
                    listPhone.add(customerList.get(0).getmobile2());

                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "2");
                    }
                }

                if (!customerList.get(0).getmobile3().contains("") || !customerList.get(0).getmobile3().isEmpty()) {
                    listPhone.add(customerList.get(0).getmobile3());

                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "3");
                    }
                }

                if (!customerList.get(0).getmobile4().contains("") || !customerList.get(0).getmobile4().isEmpty()) {
                    listPhone.add(customerList.get(0).getmobile4());

                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "4");
                    }
                }

                if (!customerList.get(0).getmobile5().contains("") || !customerList.get(0).getmobile5().isEmpty()) {
                    listPhone.add(customerList.get(0).getmobile5());
                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "5");
                    }
                }
                if (!mobile6.contains("") || !mobile6.isEmpty()) {
                    listPhone.add(mobile6);
                    Log.i("mobile6", String.valueOf(listPhone));
                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "6");
                    }
                }
                if (!mobile7.contains("") || !mobile7.isEmpty()) {
                    listPhone.add(mobile7);
                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "7");
                    }
                }
                if (!mobile8.contains("") || !mobile8.isEmpty()) {
                    listPhone.add(mobile8);
                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "8");
                    }
                }
                if (!mobile9.contains("") || !mobile9.isEmpty()) {
                    listPhone.add(mobile9);
                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "9");
                    }
                }

                if (!mobile10.contains("") || !mobile10.isEmpty()) {
                    listPhone.add(mobile10);
                } else {
                    colno = AppUtils.readStringFromPref(this, "colNo");
                    if (colno == "") {
                        AppUtils.writeStringToPref(this, "colNo", "10");
                    }
                }
            } catch (Exception e) {
                Log.i("errrorrr", e.getMessage());
                Toast.makeText(context, "Error to repeat order ...", Toast.LENGTH_SHORT).show();
            }
        } else {

            ll_layout.setVisibility(View.GONE);
            tv_choose.setVisibility(View.GONE);
        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PhoneListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(linearLayoutManager);


        if (customerList.size() > 0) {
            Log.i("phone_data_size", "--" + listPhone.size());
            addressAdapter = new PhoneAdapter2(PhoneListActivity.this, customerList, flag, address, listPhone, tableId);
            recyclerview.setAdapter(addressAdapter);
            addressAdapter.notifyDataSetChanged();

        } else {
            bt_shopping.setVisibility(View.GONE);
            Toast.makeText(PhoneListActivity.this, "No Record Found !", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_shopping:
                AppUtils.writeStringToPref(PhoneListActivity.this, "addressid", aid);
                addressid = AppUtils.readStringFromPref(PhoneListActivity.this, "addressid");

                // remove as per shahil he want to continue order wihthout phone

//                if (!addressid.equals("") || !addressid.isEmpty()) {

                AppUtils.writeStringToPref(PhoneListActivity.this, "phone", address2);
                AppUtils.writeStringToPref(PhoneListActivity.this, "addressid", aid);
                AppUtils.writeStringToPref(PhoneListActivity.this, "address", address1);
                AppUtils.writeStringToPref(PhoneListActivity.this, "address2", address1);
                AppUtils.writeStringToPref(PhoneListActivity.this, "AddressAdding", "normal");


                add = address1.split(",");
                add2 = add[3].split("/");
                AppUtils.writeStringToPref(PhoneListActivity.this, "area", add[0]);
                AppUtils.writeStringToPref(PhoneListActivity.this, "sector", add[1]);
                AppUtils.writeStringToPref(PhoneListActivity.this, "apartment", add[2]);
                AppUtils.writeStringToPref(PhoneListActivity.this, "block", add2[0]);
                AppUtils.writeStringToPref(PhoneListActivity.this, "houseno", add2[1]);
                AppUtils.writeStringToPref(PhoneListActivity.this, "floor", add[4]);

                Intent intent = new Intent();
                if (AppClass.isOrderFirst) {
                    AppClass.isOrderFirst = false;
                    finish();

                } else {
                    intent = new Intent(PhoneListActivity.this, CustomerOrdersActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }


//                } else {
//                    Toast.makeText(PhoneListActivity.this, "Please Add / Select Phone", Toast.LENGTH_SHORT).show();
//
//                }
                break;
        }

    }

    @Override
    public void hideView(String address, String id) {
//        addressAdapter.notifyDataSetChanged();
        address2 = address;
        aid = id;
    }

}