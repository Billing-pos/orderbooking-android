package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.FloorActivity;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

public class HouseNoAdapter extends RecyclerView.Adapter<HouseNoAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<String> addressListList;
    int mCheckedPosition;
    HouseNoAdapter.ViewHolder viewHolder1;
    int flag;


    public HouseNoAdapter(Context context, List<String> addressListList) {
        this.context = context;
        this.addressListList = addressListList;


    }

    @Override
    public HouseNoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.area_layout, parent, false);
        viewHolder1 = new HouseNoAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final HouseNoAdapter.ViewHolder holder, final int position) {


        holder.name.setText(addressListList.get(position));
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "houseno", holder.name.getText().toString());
                AppUtils.writeStringToPref(context, "houseno2", holder.name.getText().toString());
                Intent intent = new Intent(context, FloorActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;


        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.tv_area);

        }
    }


}

