package com.example.sheetalchauhan.orderbookingapplication.phase_iii.model;

import java.util.ArrayList;

public class ResponseSearchProduct {

    private boolean error;

    private String msg;

    private ArrayList<SearchProductModel> data;


    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<SearchProductModel> getData() {
        return data;
    }

    public void setData(ArrayList<SearchProductModel> data) {
        this.data = data;
    }
}
