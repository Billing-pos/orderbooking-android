package com.example.sheetalchauhan.orderbookingapplication;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.sheetalchauhan.orderbookingapplication.activities.AddCustomerActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.ChangeReceiptFormat;
import com.example.sheetalchauhan.orderbookingapplication.activities.ManageTag.AllTag;
import com.example.sheetalchauhan.orderbookingapplication.activities.PrintBluetoothActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.TotalDataActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.EditCustomerPasswordActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.navigationsactivity.RecieptFormatActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.order.OrderInfoActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.product.AddProductActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.ReceiptTypeModel;
import com.example.sheetalchauhan.orderbookingapplication.model.ReceiptTypeResponse;
import com.example.sheetalchauhan.orderbookingapplication.model.ResponsePojoisSelect;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomerResponse;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.FirmData;
import com.example.sheetalchauhan.orderbookingapplication.model.isFirmSelected;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Order;
import com.example.sheetalchauhan.orderbookingapplication.model.ordersync.OrderProductSync;
import com.example.sheetalchauhan.orderbookingapplication.model.ordersync.OrderSyncData;
import com.example.sheetalchauhan.orderbookingapplication.model.passcode.PasscodeRequestModel;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.CustomerResponseDto;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductData;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductResponseDto;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ResponsePojo;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.SubCategoryResponse;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.WholesaleDataResponse;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.WholesaleResponse;
import com.example.sheetalchauhan.orderbookingapplication.netwowk.NetworkStatus;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.Alarm;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.Cartridge;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.Change_Language;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.OrderWithImage;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CommentDataResponseModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CommentResponseModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CustomerUpsyncAddress;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.HindiAddressModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.OrderImageResponse;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppClass;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.HandleError;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int TIME_OUT = 1500;
    @BindView(R.id.bt_order_info)
    LinearLayout ll_order_info;
    @BindView(R.id.bt_history)
    LinearLayout ll_history;
    @BindView(R.id.bt_order)
    Button bt_order;
    @BindView(R.id.bt_downsync)
    Button bt_downsync;
    @BindView(R.id.bt_upsync)
    Button bt_upsync;
    @BindView(R.id.ll_layout_sync)
    LinearLayout ll_layout_sync;
    @BindView(R.id.spn_downsync)
    Spinner spn_downsync;
    DBAdapter dbAdapter;
    List<CustomerUpsyncAddress> sendCartList = null;
    List<Cart> cartList = null;
    List<Order> orderList = null;
    List<OrderSyncData> orderSyncDataList = null;
    DrawerLayout drawer;
    NavigationView navigationView;
    int size = 0;
    String imeiNumber = null;
    private int page = 1;
    private int page_size = 0;
    private int orderpage = 1, imagepage = 1;
    private int image_pagesize = 0;
    private List<Firm> frimList = new ArrayList<>();
    private ProgressDialog progressCustomer, progress7Customer, progress7Image, progressUpsyncOrder, progressWholesale, progressUpsyncCustomer;
    private Context context;

    private void startDownsyncFromLastCustomer(Boolean isNavClick) {
        if (isNavClick) {
            orderpage = 1;
            page = 1;
            AppUtils.writeStringToPref(context, "order_page", "1");

        } else {
            // check last inserted when connection break
            String tempPageno = AppUtils.readStringFromPref(context, "customer_page");
            if (tempPageno != null && !tempPageno.isEmpty()) {
                String serverData = AppUtils.readStringFromPref(context, "total_customer");
                if (serverData != null && !serverData.isEmpty()) {
                    if (page > 1)
                        page = (Integer.parseInt(tempPageno) - 1);
                    else
                        page = 1;
                }
            }
        }


        // customer list all
        progressCustomer = new ProgressDialog(MainActivity.this);
        progressCustomer.setMessage(getResources().getString(R.string.pw_all_customer));
        progressCustomer.setCancelable(false);
        progressCustomer.show();
        new AsyncTaskForAllCustomer().execute("customer-list-all");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbAdapter = DBAdapter.getInstance(MainActivity.this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        deviceId();
        databaseBackup();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //......................Spn_code..........................//
        String arr[] = {getResources().getString(R.string.select_downsync_),
                getResources().getString(R.string.seven_days_downsync),
                getResources().getString(R.string.all_downsnyc)};
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.custom_spinner, arr);
        spn_downsync.setAdapter(adapter);

        spn_downsync.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
                switch (i) {
                    case 1:
                        // getting data for 7 days
                        if (NetworkStatus.getConnectivity(MainActivity.this)) {
                            progress7Customer = new ProgressDialog(MainActivity.this);
                            progress7Customer.setMessage(getResources().getString(R.string.pw_7customer));
                            progress7Customer.setCancelable(false);
                            progress7Customer.show();
                            new AsyncTaskForGet7Customer().execute("customer");

                            // getting common api orderimage, product, subcategory, firm etc.
                            commonApiFor7andAllDays();
                        }
                        break;

                    case 2:
                        if (NetworkStatus.getConnectivity(MainActivity.this)) {
                            startDownsyncFromLastCustomer(false);
//                          getting common api orderimage, product, subcategory, firm etc.
                            commonApiFor7andAllDays();
                        } else {
                            Toast.makeText(MainActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        //..........................End...........................//
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        findViewById(R.id.bt_customer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                size = dbAdapter.getFirmList().size();
                String firmName = (AppUtils.readStringFromPref(getApplicationContext(), "firmname") != null) ? AppUtils.readStringFromPref(getApplicationContext(), "firmname") : "";

                if (firmName == "") {
                    showAlertPopup();

                } else {
                    if (firmDeselect(context)) {
                        AppUtils.writeStringToPref(MainActivity.this, "flag", "0");
                        Intent intent = new Intent(MainActivity.this, AddProductActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                    } else
                        showAlertPopup();
                }
            }
        });

        bt_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                size = dbAdapter.getFirmList().size();
                String firmName = (AppUtils.readStringFromPref(getApplicationContext(), "firmname") != null) ? AppUtils.readStringFromPref(getApplicationContext(), "firmname") : "";

                if (firmName == "") {
                    showAlertPopup();
                } else {
                    AppUtils.writeStringToPref(MainActivity.this, "is_invoice", null);
                    Intent intent = new Intent(MainActivity.this, OrderWithImage.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
            }
        });

        ll_order_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                size = dbAdapter.getFirmList().size();
                String firmName = (AppUtils.readStringFromPref(getApplicationContext(), "firmname") != null) ? AppUtils.readStringFromPref(getApplicationContext(), "firmname") : "";

                if (firmName == "") {
                    showAlertPopup();

                } else {
                    if (firmDeselect(context)) {
                        AppUtils.writeStringToPref(MainActivity.this, "is_invoice", null);
                        Intent intent = new Intent(MainActivity.this, OrderWithImage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                    } else
                        showAlertPopup();
                }
            }
        });

        ll_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppClass.isOrderFirst = false;

                size = dbAdapter.getFirmList().size();
                String firmName = (AppUtils.readStringFromPref(getApplicationContext(), "firmname") != null) ? AppUtils.readStringFromPref(getApplicationContext(), "firmname") : "";

                if (firmName == "") {
                    showAlertPopup();
                } else {
                    if (firmDeselect(context)) {

                        AppUtils.writeStringToPref(MainActivity.this, "flag", "1");
                        Intent intent = new Intent(MainActivity.this, AddCustomerActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                    } else
                        showAlertPopup();
                }
            }
        });


        bt_upsync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (NetworkStatus.getConnectivity(context)) {
                    // calling for upsync customer data
                    orderSyncDataList = new ArrayList<OrderSyncData>();
                    progressUpsyncCustomer = new ProgressDialog(MainActivity.this);
                    progressUpsyncCustomer.setMessage(getResources().getString(R.string.pw_up_customer));
                    progressUpsyncCustomer.setCancelable(false);
                    progressUpsyncCustomer.show();
                    upsyncCustomerData();

                    // calling for upsync customer-order data
                    progressUpsyncOrder = new ProgressDialog(MainActivity.this);
                    progressUpsyncOrder.setMessage(getResources().getString(R.string.pw_up_order));
                    progressUpsyncOrder.setCancelable(false);
                    progressUpsyncOrder.show();
                    upsyncOrderData();

                }
            }
        });

    }

    private void setOrderPageValue() {
        AppUtils.writeStringToPref(context, "order_page", "1");
    }

    private void setCustomerPageValue() {
        AppUtils.writeStringToPref(context, "customer_page", "1");
    }

    private void upsyncOrderData() {
        if (NetworkStatus.getConnectivity(context)) {

            orderList = dbAdapter.getOrderSyncDataBatch40();
            if (orderList.size() > 0) {

                for (int i = 0; i < orderList.size(); i++) {

                    List<OrderProductSync> orderProductSyncList = new ArrayList<OrderProductSync>();
                    OrderSyncData orderSyncData = new OrderSyncData();
                    orderSyncData.setOrderid(orderList.get(i).getOrderId() != null ? orderList.get(i).getOrderId() : "");
                    orderSyncData.setAddress(orderList.get(i).getAddress() != null ? orderList.get(i).getAddress() : "");
                    orderSyncData.setMobile((!orderList.get(i).getPhone().equals("")) ? orderList.get(i).getPhone() : "-");
                    orderSyncData.setComment_id((!orderList.get(i).getComment_id().equals("null")) ? orderList.get(i).getComment_id() : "");
                    orderSyncData.setCustom_comment((orderList.get(i).getCustom_comment() != null) ? orderList.get(i).getCustom_comment() : "");
                    orderSyncData.setInvalid((orderList.get(i).getInvalid() != null) ? orderList.get(i).getInvalid() : "");
                    orderSyncData.setMobile2((orderList.get(i).getMobile2() != null) ? orderList.get(i).getMobile2() : "");
                    orderSyncData.setFirm((orderList.get(i).getMobile2() != null) ? orderList.get(i).getFirm() : "");
                    orderSyncData.setTotal_amount((orderList.get(i).getMobile2() != null) ? orderList.get(i).getTotal_amount() : "");
                    orderSyncData.setReceipt_type((orderList.get(i).getReceipt_type() != null) ? orderList.get(i).getReceipt_type() : "");
                    orderSyncData.setStatus(orderList.get(i).getStatus());
                    cartList = dbAdapter.getOrderProduct(orderList.get(i).getOrderId());

                    if (cartList.size() > 0) {

                        String finalOrderId = orderList.get(i).getOrderId().substring(0, orderList.get(i).getOrderId().length() - 5);

                        for (int j = 0; j < cartList.size(); j++) {
                            OrderProductSync orderProductSync = new OrderProductSync();
                            orderProductSync.setOrderid(finalOrderId);
                            orderProductSync.setProductId(cartList.get(j).getProductid() != null ? cartList.get(j).getProductid() : "");
                            orderProductSync.setProductName(cartList.get(j).getProductname() != null ? cartList.get(j).getProductname() : "");
                            orderProductSync.setQty(cartList.get(j).getQuantity() != null ? cartList.get(j).getQuantity() : "");
                            orderProductSync.setPrice(cartList.get(j).getPrice() != null ? cartList.get(j).getPrice() : "");
                            orderProductSyncList.add(orderProductSync);
                            Log.i("cart_value1", "-" + orderProductSync.getProductName() + "--" + orderProductSync.getProductId() + "--" + orderProductSync.getPrice() + "--" + orderProductSync.getQty());
                        }
                    }

                    orderSyncData.setOrderProductSyncList(orderProductSyncList);
                    orderSyncDataList.add(orderSyncData);
                }

                new UpsyncForSendOrderData().execute("send-order-data");
                setOrderPageValue();

            } else {
                if (progressUpsyncOrder != null)
                    progressUpsyncOrder.dismiss();
                Toast.makeText(MainActivity.this, R.string.no_orderfound, Toast.LENGTH_SHORT).show();
            }
        } else {
            if (progressUpsyncOrder != null)
                progressUpsyncOrder.dismiss();
        }
    }

    private void upsyncCustomerData() {
        if (NetworkStatus.getConnectivity(context)) {
            sendCartList = new ArrayList<CustomerUpsyncAddress>();
            sendCartList = dbAdapter.getNewCustomerBatch50();

            if (sendCartList.size() > 0) {
                new UpsyncForSendCustomerData().execute("send-customer-data");
                setCustomerPageValue();

            } else {
                Toast.makeText(MainActivity.this, R.string.no_customer_data_found, Toast.LENGTH_SHORT).show();
                if (progressUpsyncCustomer != null)
                    progressUpsyncCustomer.dismiss();
            }
        } else {
            if (progressUpsyncCustomer != null)
                progressUpsyncCustomer.dismiss();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        dbAdapter.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }

        dbAdapter.open();
        clearPref();
        try {
            dbAdapter.deleteCart();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.are_you_sure_you_want_to_exit)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                    }
                })

                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_firm) {
            Intent intent = new Intent(MainActivity.this, ChangeReceiptFormat.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.nav_receipt) {
            Intent intent = new Intent(MainActivity.this, RecieptFormatActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.nav_printer) {
            AppUtils.writeStringToPref(this, "navigation_side", "1");
            Intent intent = new Intent(MainActivity.this, PrintBluetoothActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.nav_password) {
            Intent intent = new Intent(MainActivity.this, EditCustomerPasswordActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.nav_invoice) {
            AppUtils.writeStringToPref(this, "is_invoice", "1");
            Intent intent = new Intent(MainActivity.this, OrderInfoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.nav_tab) {
            Intent intent = new Intent(MainActivity.this, AllTag.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.OrderWithImage) {
            Intent intent = new Intent(MainActivity.this, OrderWithImage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.nav_total_data) {
            Intent intent = new Intent(MainActivity.this, TotalDataActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.alarm_all) {
            Intent intent = new Intent(MainActivity.this, Alarm.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();

        } else if (id == R.id.menu_cartridge) {
            Intent intent = new Intent(MainActivity.this, Cartridge.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.menu_switch_language) {
            Intent intent = new Intent(MainActivity.this, Change_Language.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();

        } else if (id == R.id.addnewphone) {
            Intent intent = new Intent(MainActivity.this, AddNewPhoneActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (id == R.id.downsync_all) {

            if (NetworkStatus.getConnectivity(MainActivity.this)) {
                startDownsyncFromLastCustomer(true);
//              // getting common api orderimage, product, subcategory, firm etc.
                commonApiFor7andAllDays();
            }
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void clearPref() {
        AppUtils.writeStringToPref(context, "area", "");
        AppUtils.writeStringToPref(context, "area2", "");
        AppUtils.writeStringToPref(context, "sector", "");
        AppUtils.writeStringToPref(context, "sector2", "");
        AppUtils.writeStringToPref(context, "apartment", "");
        AppUtils.writeStringToPref(context, "apartment2", "");
        AppUtils.writeStringToPref(context, "block", "");
        AppUtils.writeStringToPref(context, "block2", "");
        AppUtils.writeStringToPref(context, "houseno", "");
        AppUtils.writeStringToPref(context, "houseno2", "");
        AppUtils.writeStringToPref(context, "floor", "");
        AppUtils.writeStringToPref(context, "floor2", "");

        AppUtils.writeStringToPref(context, "kothi", "");
        AppUtils.writeStringToPref(context, "gali", "");
        AppUtils.writeStringToPref(context, "lift", "");
        AppUtils.writeStringToPref(context, "extra", "");

        AppUtils.writeStringToPref(context, "phone", "");
        AppUtils.writeStringToPref(context, "address", "");
        AppUtils.writeStringToPref(context, "address2", "");
        AppUtils.writeStringToPref(context, "addressid", "");
        AppUtils.writeStringToPref(context, "newaddress", "");
        AppUtils.writeStringToPref(context, "AddressAdding", "");
        // IInd phase
        AppUtils.writeStringToPref(context, "userType", "");
        AppUtils.writeStringToPref(context, "userName", "");
        AppUtils.writeStringToPref(context, "userName", "");
        AppUtils.writeStringToPref(context, "customerName", null);
        AppUtils.writeStringToPref(context, "second_phone", null);
        AppUtils.writeStringToPref(context, "isCheckedBox", "false");
        // for edit cart
        AppUtils.writeIntToPref(context, "commentId", 0);
        AppUtils.writeStringToPref(context, "customComment", "");
        AppUtils.writeStringToPref(context, "orderId", "");
        AppUtils.writeStringToPref(context, "secondPhone", "");
        AppUtils.writeStringToPref(context, "editCart", "false");
        AppUtils.writeStringToPref(context, "userName", "");
        AppUtils.writeStringToPref(context, "cartage", "");
        AppUtils.writeStringToPref(context, "category_id", "");
        AppUtils.writeStringToPref(context, "calculation_weight", "");

        AppUtils.writeIntToPref(context, "paid_amount_already", 0);
        AppUtils.writeBoolToPref(context, "isAdvancePaid", false);


    }

    //...........................................................................................//
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    //...........................................................................................//
    public void isIMEIPHONEREAD() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 102);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant
                return;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case 102: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! do the
                    // calendar task you need to do.

                    String deviceUniqueIdentifier = null;

                    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != tm) {
                        deviceUniqueIdentifier = tm.getDeviceId();
                    }
                    if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
                        deviceUniqueIdentifier = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    }

                    AppUtils.writeStringToPref(MainActivity.this, "deviceUniqueIdentifier", deviceUniqueIdentifier);
                    databaseBackup();

                } else {
                    Toast.makeText(this, "Please enable this permission to continue", Toast.LENGTH_SHORT).show();
                    isIMEIPHONEREAD();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }

    public void databaseBackup() {
        if (isStoragePermissionGranted()) {

            try {
                final String inFileName = "/data/data/com.example.sheetalchauhan.orderbookingapplication/databases/Order_Booking1.db";
                File dbFile = new File(inFileName);
                FileInputStream fis = new FileInputStream(dbFile);

                String outFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
                OutputStream output = new FileOutputStream(outFileName);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    output.write(buffer, 0, length);
                }
                output.flush();
                output.close();
                fis.close();
            } catch (Exception e) {
//                Log.i("STORAGE", "ERROR THROUGH");
            }
        }
    }
    //...........................................................................................//

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapter.close();
    }


    void showAlertPopup() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.firm_selected_or_not);
        TextView no = (TextView) dialog.findViewById(R.id.txt_no);
        TextView yes = (TextView) dialog.findViewById(R.id.txt_yes);
        // if button is clicked, close the custom dialog
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (size > 0) {
                    Intent intent = new Intent(MainActivity.this, RecieptFormatActivity.class);
                    startActivity(intent);
                }
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void deviceId() {
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        imeiNumber = android_id;
    }

    private void callCustomerPageAgain() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                page++;
                if (page <= page_size) {

                    if (NetworkStatus.getConnectivity(MainActivity.this)) {
                        new AsyncTaskForAllCustomer().execute("customer-list-all");
                    } else {
                        progressCustomer.dismiss();
                        Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                    AppUtils.writeStringToPref(context, "customer_page", "" + page);

                } else {
                    AppUtils.writeStringToPref(context, "customer_page", "2");
                    dbAdapter.deleteCustomerTable();
                    dbAdapter.deleteCustomerTableNew();
                    progressCustomer.dismiss();
                    Toast.makeText(MainActivity.this, "Customer Downsync completed", Toast.LENGTH_SHORT).show();
                }
            }
        }, TIME_OUT);

    }

    private void callOrderImagePageAgain() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                imagepage++;
                if (imagepage <= image_pagesize) {
                    if (NetworkStatus.getConnectivity(MainActivity.this)) {
                        new AsyncTaskGettingAllAddress().execute("getting-alladdress");
                    } else
                        progress7Image.dismiss();
                } else {
                    progress7Image.dismiss();
                    Toast.makeText(MainActivity.this, R.string.order_image_downsync_completed, Toast.LENGTH_LONG).show();
                }
            }
        }, TIME_OUT);
    }

    private void commonApiFor7andAllDays() {
        if (NetworkStatus.getConnectivity(MainActivity.this)) {
            // getting order images
            progress7Image = new ProgressDialog(MainActivity.this);
            progress7Image.setMessage(getResources().getString(R.string.hindi_address));
            progress7Image.setCancelable(false);
            progress7Image.show();
            new AsyncTaskGettingAllAddress().execute("getting-alladdress");

            // getting wholesaler customer data


            progressWholesale = new ProgressDialog(context);
            progressWholesale.setMessage(getResources().getString(R.string.pw_wholesalerprice));
            progressWholesale.setCancelable(false);
            progressWholesale.show();
            new AsynTaskWholeSalePrice().execute("wholesale-price");
            // firm list
            new AsyncTaskForFirmList().execute("firm-list");
            // comment list
            new AsyncTaskForComment().execute("comment-list");
            // ReceiptType list
            new AsyncTaskForReceiptType().execute("ReceiptType-list");
            // product list
            new AsyncTaskForProductList().execute("product-list");
            // product's subcategory list
            new AsyncTaskForSubcategoryList().execute("sub-cat-list");
            // checking firm is selected or not.
            new AsyncTaskRunner().execute("isselected-firm");

        }
    }

    public boolean firmDeselect(Context context) {
        boolean isFirmSelected = false;
        String firmselected = AppUtils.readStringFromPref(context, "firmname");
        if (firmselected != null && firmselected != "") {
            SimpleDateFormat format = new SimpleDateFormat("HH");
            String date = format.format(new Date());

            SimpleDateFormat format3 = new SimpleDateFormat("yyyyMMdd");
            String dailyDate = format3.format(new Date());

            if (Integer.parseInt(date) >= 10) {

                String lastDayDate = AppUtils.readStringFromPref(context, "lastDayDate");
                if (lastDayDate != null && (Integer.parseInt(lastDayDate) == Integer.parseInt(dailyDate))) {
                    Log.i("firm_logout_date_match", "current " + dailyDate + ", lastDate = " + lastDayDate);
                    isFirmSelected = true;
                } else {
                    checkFirmToDeselect(context);
                    isFirmSelected = false;
                }
            } else {
                isFirmSelected = true;
            }
        }
        return isFirmSelected;
    }

    private void checkFirmToDeselect(Context context) {
        if (NetworkStatus.getConnectivity2(context)) {
            PasscodeRequestModel model = new PasscodeRequestModel();
            model.setDeviceId(imeiNumber);
            DBAdapter dbAdapter = DBAdapter.getInstance(context);
            dbAdapter.open();
            String currentFirm = AppUtils.readStringFromPref(context, "firmselected");
            if (currentFirm != null && !currentFirm.isEmpty()) {

                model.setFirmCode(String.valueOf(dbAdapter.getFirmList().get(Integer.parseInt(currentFirm))));

                Call<ResponsePojo> call = RetrofitHandler.getInstance().getApi().sendDeSelectedFirm(model);
                call.enqueue(new Callback<ResponsePojo>() {
                    @Override
                    public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                        clearFirmData(context);
                        Log.i("firm_logout", "have internet firmlogut");
                    }

                    @Override
                    public void onFailure(Call<ResponsePojo> call, Throwable t) {
                        Log.i("firm_logout", ": firm lougout onfailure");
                        clearFirmData(context);
                    }
                });
            } else {
                clearFirmData(context);
            }

        } else {
            Log.i("firm_logout", ": no internet firm lougout condition");
            clearFirmData(context);
        }
    }

    private void clearFirmData(Context context) {
        AppUtils.writeStringToPref(context, "firmname", "");
        AppUtils.writeStringToPref(context, "firmaddress", "");
        AppUtils.writeStringToPref(context, "firmmobile", "");
        AppUtils.writeStringToPref(context, "firmcode", "");
        AppUtils.writeStringToPref(context, "firmselected", "");
        AppUtils.writeStringToPref(context, "firmselected", "");
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        private String resp = "response";

        @Override
        protected String doInBackground(String... params) {
            isFirmSelected model = new isFirmSelected();
            model.setDeviceId("" + imeiNumber);
            Call<ResponsePojoisSelect> call = RetrofitHandler.getInstance().getApi().sendisSelectedFirm(model);
            call.enqueue(new Callback<ResponsePojoisSelect>() {
                @Override
                public void onResponse(Call<ResponsePojoisSelect> call, Response<ResponsePojoisSelect> response) {

                    AppUtils.writeBoolToPref(MainActivity.this, "isFirstTimeInstall", true);
                    ResponsePojoisSelect responseDto = response.body();

                    if (response.isSuccessful()) {

                        if (responseDto.isError() == false) {
                            String code = null;
                            code = responseDto.getcode();

                            try {
                                if (code != null) {
                                    AppUtils.writeStringToPref(MainActivity.this, "firmcode", responseDto.getcode());
                                    frimList = dbAdapter.getFirmName(responseDto.getcode());
                                    AppUtils.writeStringToPref(MainActivity.this, "firmname", frimList.get(0).getName());
                                    AppUtils.writeStringToPref(MainActivity.this, "firmaddress", frimList.get(0).getAddress());
                                    AppUtils.writeStringToPref(MainActivity.this, "firmmobile", frimList.get(0).getMobile());
                                }
                            } catch (Exception e) {
//                                Toast.makeText(MainActivity.this, "Please select firm", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponsePojoisSelect> call, Throwable t) {
                    HandleError.handleTimeoutError(context, t, "920");
                }
            });

            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }


        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

    private class AsyncTaskForAllCustomer extends AsyncTask<String, String, String> {
        private String resp = "response";

        @Override
        protected String doInBackground(String... params) {
            Call<CustomerResponse> call = RetrofitHandler.getInstance().getApi().getCustomerListAll("" + page, "" + BuildConfig.VERSION_CODE);
            call.enqueue(new Callback<CustomerResponse>() {
                @Override
                public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                    CustomerResponse responseDto = response.body();

                    if (response.isSuccessful()) {

                        if (responseDto.isError() == false) {
                            page_size = Integer.parseInt(responseDto.getPage());
                            AppUtils.writeStringToPref(context, "total_customer", "" + Integer.parseInt(responseDto.getCount()));

                            List<CustomergetAddress> list = responseDto.getCustomerResponseDataList();
                            if (list.size() > 0) {
                                for (int i = 0; i < list.size(); i++) {

                                    CustomergetAddress customer = new CustomergetAddress();
                                    customer.setId(list.get(i).getId());
                                    customer.setFirm(list.get(i).getFirm());
                                    customer.setmobile(list.get(i).getMobile() != null ? list.get(i).getMobile().replace("-", "").trim() : "");
                                    customer.setmobile2(list.get(i).getMobile2() != null ? list.get(i).getMobile2().replace("-", "").trim() : "");
                                    customer.setmobile3(list.get(i).getMobile3() != null ? list.get(i).getMobile3().replace("-", "").trim() : "");
                                    customer.setmobile4(list.get(i).getMobile4() != null ? list.get(i).getMobile4().replace("-", "").trim() : "");
                                    customer.setmobile5(list.get(i).getMobile5() != null ? list.get(i).getMobile5().replace("-", "").trim() : "");
                                    customer.setMobile6(list.get(i).getMobile6() != null ? list.get(i).getMobile6().replace("-", "").trim() : "");
                                    customer.setMobile7(list.get(i).getMobile7() != null ? list.get(i).getMobile7().replace("-", "").trim() : "");
                                    customer.setMobile8(list.get(i).getMobile8() != null ? list.get(i).getMobile8().replace("-", "").trim() : "");
                                    customer.setMobile9(list.get(i).getMobile9() != null ? list.get(i).getMobile9().replace("-", "").trim() : "");
                                    customer.setMobile10(list.get(i).getMobile10() != null ? list.get(i).getMobile10().replace("-", "").trim() : "");
                                    customer.setContact_name(list.get(i).getContact_name() != null ? list.get(i).getContact_name().replace("-", "").trim() : "");
                                    customer.setHouseno(list.get(i).getHouseno());
                                    customer.setFloor(list.get(i).getFloor());
                                    customer.setBlock(list.get(i).getBlock());
                                    customer.setApartment(list.get(i).getApartment());
                                    customer.setSector(list.get(i).getSector());
                                    customer.setArea(list.get(i).getArea());
                                    customer.setCartage_discount(list.get(i).getCartage_discount());
                                    customer.setRetail_discount(list.get(i).getRetail_discount());
                                    customer.setTags_new(list.get(i).getTags_new());
                                    customer.setOrderid(list.get(i).getOrderid());
                                    customer.setAddress(list.get(i).getAddress());
                                    customer.setFlag("0");

                                    customer.setAddress_one(list.get(i).getAddress_one());
                                    customer.setAddress_three(list.get(i).getAddress_three());
                                    customer.setTags(list.get(i).getTags());
                                    customer.setComments(list.get(i).getComments());
                                    customer.setCartage(list.get(i).getCartage());
                                    customer.setCalculation_weight(list.get(i).getCalculation_weight());
                                    customer.setDelivery_time(list.get(i).getDelivery_time());
                                    customer.setType(list.get(i).getType());

                                    customer.setKothi_flate(list.get(i).getKothi_flate());
                                    customer.setGali_no(list.get(i).getGali_no());
                                    customer.setLift(list.get(i).getLift());
                                    customer.setExtra(list.get(i).getExtra());

                                    dbAdapter.insertCustomerData1(customer);

                                }
                                callCustomerPageAgain();
                                dbAdapter.close();
                            } else {
                                Toast.makeText(MainActivity.this, R.string.no_data_found, Toast.LENGTH_SHORT).show();
                                progressCustomer.dismiss();
                            }

                        } else {
                            progressCustomer.dismiss();
                            Toast.makeText(MainActivity.this, "" + responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        progressCustomer.dismiss();
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CustomerResponse> call, Throwable t) {
                    progressCustomer.dismiss();
                    HandleError.handleTimeoutError(context, t, "1028");
                }
            });

            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }


        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

    private class AsyncTaskGettingAllAddress extends AsyncTask<String, String, String> {
        private String resp = "response";

        @Override
        protected String doInBackground(String... params) {
            Call<OrderImageResponse> call = RetrofitHandler.getInstance().getApi().getAllHindiAddress("" + imagepage);
            call.enqueue(new Callback<OrderImageResponse>() {
                @Override
                public void onResponse(Call<OrderImageResponse> call, Response<OrderImageResponse> response) {
                    OrderImageResponse responseDto = response.body();
                    if (response.isSuccessful()) {
                        if (responseDto.isError() == false && responseDto.getPage() != null) {

                            if (responseDto.getAddressModelList() != null && responseDto.getAddressModelList().size() > 0) {
                                image_pagesize = Integer.parseInt(responseDto.getPage());

                                for (int i = 0; i < responseDto.getAddressModelList().size(); i++) {
                                    HindiAddressModel model = new HindiAddressModel();
                                    String column = responseDto.getAddressModelList().get(i).getColumn_name() != null ? responseDto.getAddressModelList().get(i).getColumn_name() : "";
                                    String english_name = responseDto.getAddressModelList().get(i).getEnglish_name() != null ? responseDto.getAddressModelList().get(i).getEnglish_name() : "";
                                    String hindi_name = responseDto.getAddressModelList().get(i).getHindi_name() != null ? responseDto.getAddressModelList().get(i).getHindi_name() : "";

                                    model.setColumn_name(column);
                                    model.setEnglish_name(english_name.replace("'", ""));
                                    model.setHindi_name(hindi_name);
                                    long row_id = dbAdapter.insertHindiAddress(model);
                                    if (row_id == -1) {
                                        Toast.makeText(MainActivity.this, R.string.order_image_not_saved, Toast.LENGTH_LONG).show();
                                    }
                                }
                                callOrderImagePageAgain();

                            } else {
                                Toast.makeText(MainActivity.this, R.string.order_image_is_empty, Toast.LENGTH_LONG).show();
                                progress7Image.dismiss();
                            }

                        } else {
                            progress7Image.dismiss();
                            Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        progress7Image.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<OrderImageResponse> call, Throwable t) {
                    try {
//                        if (t.toString().equalsIgnoreCase("com.google.gson.JsonSyntaxException: java.lang.IllegalStateException: Expected BEGIN_ARRAY but was BEGIN_OBJECT at line 1 column 48 path $.data")){
//                            callOrderImagePageAgain();
//                        } else {
                        Log.i("address_if_is_1144", "-" + t.toString());
                        progress7Image.dismiss();
//                            HandleError.handleTimeoutError(context, t, "1146");
                    } catch (Exception e) {
                        HandleError.handleTimeoutError(context, t, "1146");
//                        progress7Image.dismiss();
                    }
                }
            });
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class AsyncTaskForComment extends AsyncTask<String, String, String> {
        private String resp = "comment-list";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(MainActivity.this)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Call<CommentResponseModel> call;
                if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english"))
                    call = RetrofitHandler.getInstance().getApi().getAllComment("1", "en");
                else
                    call = RetrofitHandler.getInstance().getApi().getAllComment("1", "hi");
                call.enqueue(new Callback<CommentResponseModel>() {
                    @Override
                    public void onResponse(Call<CommentResponseModel> call, Response<CommentResponseModel> response) {

                        CommentResponseModel commentResponseModel = response.body();
                        if (commentResponseModel != null) {

                            try {
                                dbAdapter.deleteCommentTable();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            for (int i = 0; i < commentResponseModel.getData().size(); i++) {
                                CommentDataResponseModel model = commentResponseModel.getData().get(i);
                                long rowid = dbAdapter.insertComment(model);
                                if (rowid > -1) {
                                    Log.i("isDateinserted2", "yes" + rowid);
                                } else {
                                    Log.i("isDateinserted2", "no" + rowid);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommentResponseModel> call, Throwable t) {
                        HandleError.handleTimeoutError(context, t, "1297");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class AsyncTaskForReceiptType extends AsyncTask<String, String, String> {
        private String resp = "comment-list";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(MainActivity.this)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Call<ReceiptTypeResponse> call = RetrofitHandler.getInstance().getApi().getReceiptType("" + BuildConfig.VERSION_CODE);
                call.enqueue(new Callback<ReceiptTypeResponse>() {
                    @Override
                    public void onResponse(Call<ReceiptTypeResponse> call, Response<ReceiptTypeResponse> response) {

                        if (!response.body().getError()) {

                            List<ReceiptTypeModel> list = response.body().getData();

                            if (null != list && !list.isEmpty()) {

                                try {
                                    dbAdapter.deletReceiptType();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                for (int i = 0; i < list.size(); i++) {
                                    ReceiptTypeModel model = list.get(i);
                                    long rowid = dbAdapter.insertReceiptType(model.getId(), model.getType());
                                    if (rowid > -1) {
                                        Log.i("isDateinserted2", "receipt_yes==" + rowid + model.getType());
                                    } else {
                                        Log.i("isDateinserted2", "receipt_no==" + rowid + model.getType());
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ReceiptTypeResponse> call, Throwable t) {
                        HandleError.handleTimeoutError(context, t, "1236");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class AsyncTaskForProductList extends AsyncTask<String, String, String> {
        private String resp = "product-list";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(context)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                String language = AppUtils.readStringFromPref(context, "changeLanguage");
                Call<ProductResponseDto> call = RetrofitHandler.getInstance().getApi().getProdcutList("1", language);
                call.enqueue(new Callback<ProductResponseDto>() {
                    @Override
                    public void onResponse(Call<ProductResponseDto> call, Response<ProductResponseDto> response) {
                        ProductResponseDto responseDto = response.body();

                        if (response.isSuccessful()) {
                            if (responseDto.isError() == false) {

                                dbAdapter.deleteProduct();

                                if (responseDto.getProductDataList().size() > 0) {
                                    for (int i = 0; i < responseDto.getProductDataList().size(); i++) {
                                        ProductData productData = new ProductData();
                                        productData.setProduct_id(responseDto.getProductDataList().get(i).getProduct_id());
                                        productData.setPrice(responseDto.getProductDataList().get(i).getPrice());
                                        productData.setProduct_name(responseDto.getProductDataList().get(i).getProduct_name());
                                        productData.setSubcategory(responseDto.getProductDataList().get(i).getSubcategory());
                                        productData.setWholesale_price(responseDto.getProductDataList().get(i).getWholesale_price());
                                        Log.i("user_prod", "" + productData.getProduct_name() + "ret " + productData.getPrice() + " wh " + productData.getWholesale_price());
                                        long rowid = dbAdapter.insertProduct(productData);
                                    }
//                                    dialog.dismiss();
                                }
                            } else {
                                Toast.makeText(context, responseDto.getMsg().get(0), Toast.LENGTH_LONG).show();
                            }

                        } else {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductResponseDto> call, Throwable t) {
                        HandleError.handleTimeoutError(context, t, "1363");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class AsyncTaskForSubcategoryList extends AsyncTask<String, String, String> {
        private String resp = "sub-cat-list";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(MainActivity.this)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Call<SubCategoryResponse> call;
                if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english"))
                    call = RetrofitHandler.getInstance().getApi().getSubCategory("1", "en");
                else
                    call = RetrofitHandler.getInstance().getApi().getSubCategory("1", "hi");
                call.enqueue(new Callback<SubCategoryResponse>() {
                    @Override
                    public void onResponse(Call<SubCategoryResponse> call, Response<SubCategoryResponse> response) {
                        SubCategoryResponse responseDto = response.body();

                        if (response.isSuccessful()) {
                            if (responseDto.isError() == false) {


                                dbAdapter.deleteSubCategory();
                                for (int i = 0; i < responseDto.getProductSubCategoryData().size(); i++) {
//                                    Log.i("user_subcate", responseDto.getProductSubCategoryData().get(i).getSubcategory_name());
                                    long row_id = dbAdapter.insertSubCategory(responseDto.getProductSubCategoryData().get(i));
                                }
                            } else {
                                Toast.makeText(MainActivity.this, "" + responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SubCategoryResponse> call, Throwable t) {
                        HandleError.handleTimeoutError(context, t, "1421");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    // getting wholesaler data
    private class AsynTaskWholeSalePrice extends AsyncTask<String, String, String> {
        private String response = "wholesale-price";

        @Override
        protected String doInBackground(String... strings) {
            if (!NetworkStatus.getConnectivity(MainActivity.this)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                        progressWholesale.dismiss();
                    }
                });
            } else {

                Call<WholesaleResponse> call = RetrofitHandler.getInstance().getApi().getWholesalePrice();
                call.enqueue(new Callback<WholesaleResponse>() {
                    @Override
                    public void onResponse(Call<WholesaleResponse> call, Response<WholesaleResponse> response) {
                        WholesaleResponse wholesaleResponse = response.body();
                        if (response.isSuccessful()) {
                            if (wholesaleResponse.isError() == false) {
                                dbAdapter.open();
                                List<WholesaleDataResponse> list = wholesaleResponse.getWholesaleDataResponses();
                                if (list.size() > 0) {
                                    for (int i = 0; i < list.size(); i++) {
                                        final WholesaleDataResponse wholesaleDataResponse = list.get(i);
                                        dbAdapter.insertWholeSalePrice(wholesaleDataResponse);
                                    }
                                    progressWholesale.dismiss();
                                } else {
                                    progressWholesale.dismiss();
                                }
                            } else {
                                progressWholesale.dismiss();
                            }
                        } else {
                            progressWholesale.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<WholesaleResponse> call, Throwable t) {
                        Log.i("Onerrrr", t.getMessage());
                        progressWholesale.dismiss();
                    }
                });
            }
            return response;
        }
    }

    private class AsyncTaskForFirmList extends AsyncTask<String, String, String> {
        private String resp = "response";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(MainActivity.this)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Call<FirmData> call;
                if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english"))
                    call = RetrofitHandler.getInstance().getApi().getFirmList("1", "en");
                else
                    call = RetrofitHandler.getInstance().getApi().getFirmList("1", "hi");

                call.enqueue(new Callback<FirmData>() {
                    @Override
                    public void onResponse(Call<FirmData> call, Response<FirmData> response) {
                        FirmData responseDto = response.body();

                        if (response.isSuccessful()) {
                            if (responseDto.isError() == false) {

                                dbAdapter.deleteFirm();

                                for (int i = 0; i < responseDto.getFirmList().size(); i++) {
                                    long row_id = dbAdapter.insertFirmData(responseDto.getFirmList().get(i));
                                }

                            } else {
                                Toast.makeText(MainActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<FirmData> call, Throwable t) {
                        HandleError.handleTimeoutError(context, t, "1480");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    // downsync for last 7 days
    private class AsyncTaskForGet7Customer extends AsyncTask<String, String, String> {
        private String resp = "order-list";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(MainActivity.this)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                        progress7Customer.dismiss();
                    }
                });

            } else {
                Call<CustomerResponseDto> call = RetrofitHandler.getInstance().getApi().getCustomerList("" + BuildConfig.VERSION_CODE);
                call.enqueue(new Callback<CustomerResponseDto>() {
                    @Override
                    public void onResponse(Call<CustomerResponseDto> call, Response<CustomerResponseDto> response) {
                        CustomerResponseDto responseDto = response.body();
                        if (response.isSuccessful()) {
                            if (responseDto.isError() == false) {
                                List<CustomergetAddress> list = responseDto.getCustomerResponseDataList();
                                if (list.size() > 0) {
                                    for (int i = 0; i < list.size(); i++) {
                                        CustomergetAddress customer = new CustomergetAddress();
                                        customer.setId(list.get(i).getId());
                                        customer.setOrderid(list.get(i).getOrderid());
                                        customer.setFirm(list.get(i).getFirm());
                                        customer.setmobile(list.get(i).getMobile() != null ? list.get(i).getMobile().replace("-", "").trim() : "");
                                        customer.setmobile2(list.get(i).getMobile2() != null ? list.get(i).getMobile2().replace("-", "").trim() : "");
                                        customer.setmobile3(list.get(i).getMobile3() != null ? list.get(i).getMobile3().replace("-", "").trim() : "");
                                        customer.setmobile4(list.get(i).getMobile4() != null ? list.get(i).getMobile4().replace("-", "").trim() : "");
                                        customer.setmobile5(list.get(i).getMobile5() != null ? list.get(i).getMobile5().replace("-", "").trim() : "");
                                        customer.setMobile6(list.get(i).getMobile6() != null ? list.get(i).getMobile6().replace("-", "").trim() : "");
                                        customer.setMobile7(list.get(i).getMobile7() != null ? list.get(i).getMobile7().replace("-", "").trim() : "");
                                        customer.setMobile8(list.get(i).getMobile8() != null ? list.get(i).getMobile8().replace("-", "").trim() : "");
                                        customer.setMobile9(list.get(i).getMobile9() != null ? list.get(i).getMobile9().replace("-", "").trim() : "");
                                        customer.setMobile10(list.get(i).getMobile10() != null ? list.get(i).getMobile10().replace("-", "").trim() : "");
                                        customer.setContact_name(list.get(i).getContact_name() != null ? list.get(i).getContact_name().replace("-", "").trim() : "");
                                        customer.setHouseno(list.get(i).getHouseno());
                                        customer.setFloor(list.get(i).getFloor());
                                        customer.setBlock(list.get(i).getBlock());
                                        customer.setApartment(list.get(i).getApartment());
                                        customer.setSector(list.get(i).getSector());
                                        customer.setArea(list.get(i).getArea());
                                        customer.setAddress(list.get(i).getAddress());
                                        customer.setAddress_one(list.get(i).getAddress_one());
                                        customer.setAddress_three(list.get(i).getAddress_three());
                                        customer.setTags(list.get(i).getTags());
                                        customer.setComments(list.get(i).getComments());
                                        customer.setCartage(list.get(i).getCartage());
                                        customer.setDelivery_time(list.get(i).getDelivery_time());
                                        customer.setType(list.get(i).getType());
                                        customer.setTags_new(list.get(i).getTags_new());
                                        customer.setCalculation_weight(list.get(i).getCalculation_weight());
                                        customer.setCartage_discount(list.get(i).getCartage_discount());
                                        customer.setRetail_discount(list.get(i).getRetail_discount());

                                        customer.setKothi_flate(list.get(i).getKothi_flate());
                                        customer.setGali_no(list.get(i).getGali_no());
                                        customer.setLift(list.get(i).getLift());
                                        customer.setExtra(list.get(i).getExtra());

                                        customer.setFlag("0");

                                        dbAdapter.insertCustomerIntoOldTable(customer);

                                        if (i == (list.size() - 1)) {
                                            Toast.makeText(MainActivity.this, R.string.customer_downsync_completed, Toast.LENGTH_SHORT).show();
                                            progress7Customer.dismiss();
                                        }
                                    }
                                } else {
                                    Toast.makeText(MainActivity.this, R.string.no_data_found, Toast.LENGTH_SHORT).show();
                                    progress7Customer.dismiss();
                                }

                            } else
                                progress7Customer.dismiss();

                        } else {
                            progress7Customer.dismiss();
                            Toast.makeText(MainActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CustomerResponseDto> call, Throwable t) {
                        progress7Customer.dismiss();
                        HandleError.handleTimeoutError(context, t, "1691");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    // for upsync
    private class UpsyncForSendCustomerData extends AsyncTask<String, String, String> {
        private String resp = "send-customer-data";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(MainActivity.this)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        progressUpsyncCustomer.dismiss();
                        Toast.makeText(MainActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Call<ResponsePojo> call = RetrofitHandler.getInstance().getApi().sendCustomerData("" + BuildConfig.VERSION_CODE, sendCartList);
                call.enqueue(new Callback<ResponsePojo>() {
                    @Override
                    public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                        ResponsePojo responseDto = response.body();
                        if (response.isSuccessful()) {

                            if (responseDto.isError() == false) {

                                dbAdapter.open();
                                dbAdapter.updateCustomerBatch(sendCartList);

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (dbAdapter.getNewCustomerBatch50().size() > 0) {
                                            upsyncCustomerData();

                                        } else {
                                            progressUpsyncCustomer.dismiss();
                                            Toast.makeText(MainActivity.this, R.string.no_customer_found, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }, 1500);

                            } else {
                                /*user error*/
                                progressUpsyncCustomer.dismiss();
                            }
                        } else {
                            /*developer error*/
                            progressUpsyncCustomer.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsePojo> call, Throwable t) {
                        progressUpsyncCustomer.dismiss();
                        HandleError.handleTimeoutError(context, t, "1756");
                    }
                });
            }

            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class UpsyncForSendOrderData extends AsyncTask<String, String, String> {
        private String resp = "order-data";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(MainActivity.this)) {

                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        progressUpsyncOrder.dismiss();
                        Toast.makeText(MainActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {

                if (orderSyncDataList.size() == 0) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            progressUpsyncOrder.dismiss();
                        }
                    });


                } else {

                    Call<ResponsePojo> call = RetrofitHandler.getInstance().getApi().upsyncOrderDataForP3("1", orderSyncDataList);
                    call.enqueue(new Callback<ResponsePojo>() {
                        @Override
                        public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                            ResponsePojo responseDto = response.body();
                            if (response.isSuccessful()) {

                                if (responseDto.isError() == false) {




                                    String jsonObject = response.body().getNext_invoice_no().getNext_invoice_no();
                                    Toast.makeText(MainActivity.this, "=="+jsonObject, Toast.LENGTH_SHORT).show();




                                    dbAdapter.open();
                                    dbAdapter.deleteOrder(orderList);
                                    dbAdapter.deleteSubOrder(orderSyncDataList);

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (dbAdapter.getOrderSyncDataBatch40().size() > 0) {
                                                upsyncOrderData();

                                            } else {
                                                progressUpsyncOrder.dismiss();
                                                Toast.makeText(MainActivity.this, R.string.no_order_found, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }, 1500);


                                } else {
                                    Toast.makeText(MainActivity.this, "" + responseDto.getMsg().get(0), Toast.LENGTH_SHORT).show();
                                    /*user error*/
                                    progressUpsyncOrder.dismiss();
                                }
                            } else {
                                /*developer error*/
//                                Toast.makeText(MainActivity.this, "internal server error order upsyncing", Toast.LENGTH_SHORT).show();
                                progressUpsyncOrder.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponsePojo> call, Throwable t) {
                            progressUpsyncOrder.dismiss();
                            HandleError.handleTimeoutError(context, t, "1844");
                        }
                    });
                }
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }
}

