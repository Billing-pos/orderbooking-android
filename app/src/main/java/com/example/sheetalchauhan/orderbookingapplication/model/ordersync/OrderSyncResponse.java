package com.example.sheetalchauhan.orderbookingapplication.model.ordersync;

import java.util.List;

public class OrderSyncResponse {
    private List<OrderSyncData> orderSyncDataList;

    public List<OrderSyncData> getOrderSyncDataList() {
        return orderSyncDataList;
    }

    public void setOrderSyncDataList(List<OrderSyncData> orderSyncDataList) {
        this.orderSyncDataList = orderSyncDataList;
    }
}
