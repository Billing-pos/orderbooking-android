package com.example.sheetalchauhan.orderbookingapplication.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

public class ChangeReceiptFormat extends AppCompatActivity {

    LinearLayout ll_first_r;
    RelativeLayout ll_second_r, ll_third_r, ll_fourth_r;
    RadioButton rb_1, rb_2, rb_3, rb_4;
    Context context;
    ImageView back;
    String format = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_receipt_format);
        context = this;
        setId();

        format = AppUtils.readStringFromPref(context, "format");

        if (format == null) {
            AppUtils.writeStringToPref(context, "format", "first");
            rb_1.setChecked(true);
        }


        if (AppUtils.readStringFromPref(context, "format").contains("first")) {
            rb_1.setChecked(true);
        } else if (AppUtils.readStringFromPref(context, "format").contains("second")) {
            rb_2.setChecked(true);
        } else if (AppUtils.readStringFromPref(context, "format").contains("third")) {
            rb_3.setChecked(true);
        } else if (AppUtils.readStringFromPref(context, "format").contains("fourth")) {
            rb_4.setChecked(true);
        } else {
            rb_1.setChecked(true);
        }

    }

    private void setId() {
        ll_first_r = findViewById(R.id.ll_first_r);
        ll_second_r = findViewById(R.id.ll_second_r);
        ll_third_r = findViewById(R.id.ll_third_r);
        ll_fourth_r = findViewById(R.id.ll_fourth_r);

        rb_1 = findViewById(R.id.rb_1);
        rb_2 = findViewById(R.id.rb_2);
        rb_3 = findViewById(R.id.rb_3);
        rb_4 = findViewById(R.id.rb_4);
        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ll_first_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "format", "first");
            }
        });

        ll_second_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "format", "second");
            }
        });

        ll_third_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "format", "third");

            }
        });

        ll_fourth_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "format", "fourth");
            }
        });

        rb_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "format", "first");
                rb_2.setChecked(false);
                rb_3.setChecked(false);
                rb_4.setChecked(false);
            }
        });

        rb_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "format", "second");
                rb_1.setChecked(false);
                rb_3.setChecked(false);
                rb_4.setChecked(false);
            }
        });

        rb_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "format", "third");
                rb_1.setChecked(false);
                rb_2.setChecked(false);
                rb_4.setChecked(false);
            }
        });
        rb_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "format", "fourth");
                rb_1.setChecked(false);
                rb_2.setChecked(false);
                rb_3.setChecked(false);
            }
        });

    }
}
