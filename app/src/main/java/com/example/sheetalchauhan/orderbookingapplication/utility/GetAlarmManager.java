package com.example.sheetalchauhan.orderbookingapplication.utility;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import com.example.sheetalchauhan.orderbookingapplication.breceiver.AlarmReceiver;
import com.example.sheetalchauhan.orderbookingapplication.breceiver.Constants;

public class GetAlarmManager {

    private static GetAlarmManager instance = null;
    private static AlarmManager alarmManager = null;
    private static Context mContext;
    private static PendingIntent pendingIntent;
    private static Uri mRingtone = null;


    public static void setRingtone(Uri ringtone) {
        mRingtone = ringtone;
    }

    public static Uri getRingtone(Context context) {
        if (mRingtone == null) {
            mRingtone = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM);
        }
        return mRingtone;
    }

    public static GetAlarmManager getInstance(Context context) {
        if (instance == null) {
            instance = new GetAlarmManager();
        }
        mContext = context;
        return instance;
    }

    public static AlarmManager getAlarmManager(Context context) {
        if (alarmManager == null) {
            alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        }
        return alarmManager;
    }

    public static Context getContext(Context context) {
        if (mContext == null) {
            mContext = context;
        }
        return mContext;
    }

    public static PendingIntent getPendingIntent(Context context, int alarmId, Intent intent) {
        if (pendingIntent == null) {
            pendingIntent = PendingIntent.getBroadcast(context, alarmId, intent, 0);
        }
        return pendingIntent;
    }

    public void setAlarmManager(long alarmTime, int alarmId, String orderids, String date, String time, String text) {
        alarmManager = getAlarmManager(mContext);
        Intent intent = new Intent(mContext, AlarmReceiver.class);
        intent.putExtra("intentType", Constants.ADD_INTENT);
        intent.putExtra("PendingId", alarmId);
        intent.putExtra("DATE", date);
        intent.putExtra("TIME", time);
        intent.putExtra("TODO", text);
        intent.putExtra("alarmId", alarmId);
        intent.putExtra("orderids", orderids);


        PendingIntent alarmIntent = GetAlarmManager.getPendingIntent(mContext, alarmId, intent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime, HandleError.getSnoozingTime(mContext), alarmIntent);

        } else {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime, HandleError.getSnoozingTime(mContext), alarmIntent);
        }
    }
}
