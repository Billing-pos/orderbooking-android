package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.MainActivity;
import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.ItemSwipeCallback2;
import com.example.sheetalchauhan.orderbookingapplication.adapters.AlarmAdapter;
import com.example.sheetalchauhan.orderbookingapplication.breceiver.AlarmReceiver;
import com.example.sheetalchauhan.orderbookingapplication.breceiver.Constants;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.AlarmModel;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.Constant;
import com.example.sheetalchauhan.orderbookingapplication.utility.GetAlarmManager;
import com.example.sheetalchauhan.orderbookingapplication.utility.HandleError;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Alarm extends AppCompatActivity implements AlarmAdapter.StopAlarm {
    private static final int RINGOTNE_CODE = 1099;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.logout)
    ImageView settingAlarm;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    DBAdapter dbAdapter;
    List<AlarmModel> alarmModelList = new ArrayList<>();
    AlarmAdapter adapter;
    private Context context;
    private Uri ringtone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        ButterKnife.bind(this);
        settingAlarm.setVisibility(View.VISIBLE);
        tv_name.setText(R.string.alarm_list);
        context = this;

        if (getIntent().getStringExtra(Constant.ORDER_ID) != null) {
            String orderId = getIntent().getStringExtra(Constant.ORDER_ID);
            String alarmId = "" + getIntent().getIntExtra(Constant.ALARM_ID, 0);

            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("intentType", Constants.OFF_INTENT);
            intent.putExtra("AlarmId", alarmId);
            intent.putExtra("TODO", "text");
            intent.putExtra("orderids", "orderids");
            sendBroadcast(intent);
            stopAlarm(orderId, alarmId);
        }


        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();
        alarmModelList = dbAdapter.getAlarmList();
        dbAdapter.close();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new AlarmAdapter(this, alarmModelList);
        recyclerview.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemSwipeCallback2(this, adapter));
        itemTouchHelper.attachToRecyclerView(recyclerview);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToMainActivity();
            }
        });

        settingAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.layout_alarm_settings);

                TextView tv_ringtonename = dialog.findViewById(R.id.tv_ringtonename);
                Switch sw_silent = dialog.findViewById(R.id.sw_silent);
                Switch sw_vibrate = dialog.findViewById(R.id.sw_vibrate);
                EditText et_snoozetime = dialog.findViewById(R.id.et_snoozetime);
                Button bt_snoozetime = dialog.findViewById(R.id.bt_snoozetime);

                et_snoozetime.setText("" + (HandleError.getSnoozingTime(context) / 60000));
                bt_snoozetime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int value = Integer.parseInt(et_snoozetime.getText().toString());
                        HandleError.setSnoozingTime(context, value);
                        Toast.makeText(context, R.string.submit, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

                if (AppUtils.readStringFromPref(context, "vibrateMode") != null) {
                    if (AppUtils.readStringFromPref(context, "vibrateMode").equalsIgnoreCase("true"))
                        sw_vibrate.setChecked(true);
                    else
                        sw_vibrate.setChecked(false);
                }

                if (AppUtils.readStringFromPref(context, "silentMode") != null) {
                    if (AppUtils.readStringFromPref(context, "silentMode").equalsIgnoreCase("true"))
                        sw_silent.setChecked(true);
                    else
                        sw_silent.setChecked(false);
                }

                sw_silent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (sw_silent.isChecked())
                            AppUtils.writeStringToPref(context, "silentMode", "true");

                        else
                            AppUtils.writeStringToPref(context, "silentMode", "false");
                    }
                });

                sw_vibrate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (sw_vibrate.isChecked())
                            AppUtils.writeStringToPref(context, "vibrateMode", "true");

                        else
                            AppUtils.writeStringToPref(context, "vibrateMode", "false");
                    }
                });

                tv_ringtonename.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri ringtone = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM);
                        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, ringtone);
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, ringtone);
                        startActivityForResult(intent, RINGOTNE_CODE);
                    }
                });


                dialog.show();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RINGOTNE_CODE:
                    ringtone = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                    if (ringtone != null) {
                        GetAlarmManager.setRingtone(ringtone);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void stopCurrentAlarm(AlarmModel model) {
        stopAlarm(model.getOrderId(), model.getAlarmId());
    }

    private void stopAlarm(String orderId, String alarmId) {

        DBAdapter dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();
        dbAdapter.updateAlarm(orderId, "2");
        dbAdapter.close();

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        try {
            Log.i("aaaalarm_id_blo", ": try");

            int requetCode = Integer.parseInt(alarmId);
            AlarmManager alarmManager = GetAlarmManager.getAlarmManager(context);
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("alarmId", requetCode);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requetCode, intent, PendingIntent.FLAG_NO_CREATE);
            alarmManager.cancel(pendingIntent);
            startActivity(intent);

        } catch (Exception e) {
            Log.i("aaaalarm_id_blo", ": catch");
            e.printStackTrace();
        }


        alarmModelList = dbAdapter.getAlarmList();
        adapter = new AlarmAdapter(this, alarmModelList);
        recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishSwipeAlarm(AlarmModel model) {
        try {
            Log.i("alarm_id", ":" + model.getAlarmId());
            dbAdapter.deleteAlarmData(model.getAlarmId());
            alarmModelList.remove(model);

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();

            try {
                int requetCode = Integer.parseInt(model.getAlarmId());
                AlarmManager alarmManager = GetAlarmManager.getAlarmManager(context);
                Intent intent = new Intent(context, AlarmReceiver.class);
                intent.putExtra("alarmId", requetCode);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requetCode, intent, PendingIntent.FLAG_NO_CREATE);
                alarmManager.cancel(pendingIntent);

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        sendToMainActivity();
    }

    private void sendToMainActivity() {
        startActivity(new Intent(context, MainActivity.class));
        finish();
    }


}
