package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.FirmFilterAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterDataActivity extends AppCompatActivity implements FirmFilterAdapter.Clickonfirm {

    @BindView(R.id.spn_month)
    Spinner spn_month;
    @BindView(R.id.spinner_year)
    Spinner spinner_year;
    @BindView(R.id.rl_spinner_day)
    Button rl_spinner_day;
    @BindView(R.id.tv_to)
    TextView tv_to;
    @BindView(R.id.tv_from)
    TextView tv_from;
    @BindView(R.id.et_dateto)
    EditText et_Second;
    @BindView(R.id.et_datefrom)
    EditText et_first;
    @BindView(R.id.ll_byMonth)
    LinearLayout ll_byMonth;
    @BindView(R.id.ll_month_year)
    LinearLayout ll_month_year;
    @BindView(R.id.ll_fordate)
    LinearLayout ll_fordate;
    @BindView(R.id.bt_dateSubmit)
    Button bt_dateSubmit;
    @BindView(R.id.btn_byReceipt)
    Button btn_byReceipt;
    @BindView(R.id.btn_byDate)
    Button btn_byDate;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.btn_byFirm)
    Button btn_byFirm;
    @BindView(R.id.btn_byDay)
    Button btn_byDay;
    @BindView(R.id.btn_byMonth)
    Button btn_byMonth;
    @BindView(R.id.tv_filter)
    TextView tv_filter;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.rv_recyclerview)
    RecyclerView recyclerview;
    DBAdapter dbAdapter;
    List<Firm> firmList = new ArrayList<>();
    private Context context;
    private Boolean isShowCalender = false;
    private String yearEt, mntEt, dayEt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_data);
        context = this;
        ButterKnife.bind(this);
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();
        firmList = dbAdapter.getFirmList();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layoutManager);

        if (firmList == null)
            return;

        FirmFilterAdapter firmAdapter = new FirmFilterAdapter(this, firmList, this);
        recyclerview.setAdapter(firmAdapter);

        btn_byFirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setText(btn_byFirm.getText().toString());
                recyclerview.setVisibility(View.VISIBLE);
                ll_fordate.setVisibility(View.GONE);
                ll_byMonth.setVisibility(View.GONE);
                rl_spinner_day.setVisibility(View.GONE);
            }
        });
        btn_byDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setText(btn_byDate.getText().toString());
                recyclerview.setVisibility(View.GONE);
                ll_byMonth.setVisibility(View.GONE);
                rl_spinner_day.setVisibility(View.GONE);
                ll_fordate.setVisibility(View.VISIBLE);
                et_first.setHint("eg: YYYY-MM-DD");
                et_Second.setHint("eg: YYYY-MM-DD");
                et_first.setText("");
                et_Second.setText("");
                tv_from.setText(R.string.from_date);
                tv_to.setText(R.string.to_date);


                isShowCalender = true;
                et_first.setFocusable(false);
                et_Second.setFocusable(false);

            }
        });
        btn_byReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setText(btn_byReceipt.getText().toString());
                recyclerview.setVisibility(View.GONE);
                ll_byMonth.setVisibility(View.GONE);
                rl_spinner_day.setVisibility(View.GONE);
                ll_fordate.setVisibility(View.VISIBLE);
                et_first.setHint("eg: MAC1");
                et_Second.setHint("eg: MAC10");
                et_first.setText("");
                et_Second.setText("");
                tv_from.setText(R.string.from_receipt);
                tv_to.setText(R.string.to_receipt);

                isShowCalender = false;
                et_first.setFocusable(true);
                et_Second.setFocusable(true);

            }
        });

        btn_byMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setText(btn_byMonth.getText().toString());
                recyclerview.setVisibility(View.GONE);
                ll_fordate.setVisibility(View.GONE);
                rl_spinner_day.setVisibility(View.GONE);
                ll_byMonth.setVisibility(View.VISIBLE);
                rl_spinner_day.setVisibility(View.GONE);
                ll_month_year.setVisibility(View.VISIBLE);
            }
        });

        btn_byDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setText(btn_byDay.getText().toString());
                recyclerview.setVisibility(View.GONE);
                ll_fordate.setVisibility(View.GONE);
                ll_month_year.setVisibility(View.GONE);
                ll_byMonth.setVisibility(View.VISIBLE);
                rl_spinner_day.setVisibility(View.VISIBLE);
            }
        });

        rl_spinner_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar2();
            }
        });


        findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_first.getText().toString().isEmpty() || et_Second.getText().toString().isEmpty()) {
                    Toast.makeText(context, R.string.please_enter_proper, Toast.LENGTH_SHORT).show();
                } else {

                    if (tv_from.getText().toString().equalsIgnoreCase("From Receipt"))
                        sendToOrderImage("Receipt", et_first.getText().toString(), et_Second.getText().toString());
                    else
                        sendToOrderImage("Date", et_first.getText().toString(), et_Second.getText().toString());
                }
            }
        });

        et_first.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_Second.setFilters(new InputFilter[]{new InputFilter.AllCaps()});


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_name.setText(R.string.filter);


        /////// user friendly date selection
        et_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar(true);
            }
        });
        et_Second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar(false);
            }
        });
    }


    private void setText(String text) {
        tv_filter.setText("Filter - " + text);
    }

    @Override
    public void clickOnButton(Firm model) {
        Log.i("firmnaame", "-" + model.getFirmcode());
        Intent intent = new Intent(context, OrderWithImage.class);
        intent.putExtra("FirmId", model.getFirmcode());
        startActivity(intent);
        finish();
    }

    private void sendToOrderImage(String use, String form, String to) {
        Intent intent = new Intent(context, OrderWithImage.class);
        intent.putExtra("from", form);
        intent.putExtra("to", to);
        intent.putExtra("use", use);
        startActivity(intent);
        finish();
    }

    private void monthWise(String month, String year) {
        Intent intent = new Intent(context, OrderWithImage.class);
        intent.putExtra("month", month);
        intent.putExtra("year", year);
        startActivity(intent);
        finish();
    }

    private void monthWise(String day, String month, String year) {
        Intent intent = new Intent(context, OrderWithImage.class);
        intent.putExtra("day", day);
        intent.putExtra("month", month);
        intent.putExtra("year", year);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        bt_dateSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl_spinner_day.getVisibility() == View.VISIBLE) {

                    if (rl_spinner_day.getText().toString().isEmpty()) {
                        Toast.makeText(context, "Please Choose Date", Toast.LENGTH_SHORT).show();
                    } else {
                        monthWise(dayEt, mntEt, yearEt);
                    }

                } else {
                    monthWise(mntEt, yearEt);
                }
            }
        });


        final ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getYear());
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_year.setAdapter(yearAdapter);
        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!getYear().isEmpty()) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    yearEt = getYear().get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getMonth());
        monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_month.setAdapter(monthAdapter);
        spn_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!getMonth().isEmpty()) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    if (position <= 9) {
                        mntEt = "0" + (position + 1);
                    } else {
                        mntEt = "" + (position + 1);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public List<String> getDay() {
        final List<String> day = new ArrayList<>();
        day.add("1");
        day.add("2");
        day.add("3");
        day.add("4");
        day.add("5");
        day.add("6");
        day.add("7");
        day.add("8");
        day.add("9");
        day.add("10");
        day.add("11");
        day.add("12");
        day.add("13");
        day.add("14");
        day.add("15");
        day.add("16");
        day.add("17");
        day.add("18");
        day.add("19");
        day.add("20");
        day.add("21");
        day.add("22");
        day.add("23");
        day.add("24");
        day.add("25");
        day.add("26");
        day.add("27");
        day.add("28");
        day.add("29");
        day.add("30");
        day.add("31");
        return day;
    }

    public List<String> getMonth() {
        final List<String> month = new ArrayList<>();
        month.add("January");
        month.add("February");
        month.add("March");
        month.add("April");
        month.add("May");
        month.add("June");
        month.add("July");
        month.add("August");
        month.add("September");
        month.add("October");
        month.add("November");
        month.add("December");
        return month;
    }

    public List<String> getYear() {
        final List<String> year = new ArrayList<>();
        year.add("2020");
        year.add("2019");
        year.add("2018");
        year.add("2017");
        return year;
    }

    private void showCalendar(Boolean isTrue) {
        if (isShowCalender) {
            try {
                Calendar calendar = Calendar.getInstance();
                final int date = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month + 1;

                        String tempDay = "", tempMonth = "";
                        if (dayOfMonth < 10) {
                            tempDay = "0" + dayOfMonth;
                        } else {
                            tempDay = "" + dayOfMonth;
                        }

                        if (month < 10) {
                            tempMonth = "0" + month;
                        } else {
                            tempMonth = "" + month;

                        }

                        if (isTrue)
                            et_first.setText("" + year + "-" + tempMonth + "-" + tempDay);
                        else
                            et_Second.setText("" + year + "-" + tempMonth + "-" + tempDay);
                    }
                }, year, month, date);
                datePickerDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showCalendar2() {
        try {
            Calendar calendar = Calendar.getInstance();
            final int date = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    month = month + 1;

                    String tempDay = "", tempMonth = "";
                    if (dayOfMonth < 10) {
                        tempDay = "0" + dayOfMonth;
                    } else {
                        tempDay = "" + dayOfMonth;
                    }

                    if (month < 10) {
                        tempMonth = "0" + month;
                    } else {
                        tempMonth = "" + month;
                    }
                    dayEt = "" + tempDay;
                    mntEt = "" + tempMonth;
                    yearEt = "" + year;

                    rl_spinner_day.setText("" + year + "-" + tempMonth + "-" + tempDay);
                }
            }, year, month, date);
            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
