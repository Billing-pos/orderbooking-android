package com.example.sheetalchauhan.orderbookingapplication.activities.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderInfoActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.bt_search)
    Button bt_search;
    @BindView(R.id.bt_invoice_submit)
    Button bt_invoice_submit;
    @BindView(R.id.et_invoice)
    AutoCompleteTextView et_invoice;
    @BindView(R.id.et_address)
    AutoCompleteTextView et_address;
    @BindView(R.id.bt_address_search)
    Button bt_address_search;
    @BindView(R.id.tv_address_error)
    TextView tv_address_error;
    @BindView(R.id.tv_invoice_error)
    TextView tv_invoice_error;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.img_close1)
    ImageView img_close1;
    @BindView(R.id.lnr_invoice_number)
    LinearLayout lnr_invoice_number;
    @BindView(R.id.txt_invoice_number)
    TextView txt_invoice_number;
    @BindView(R.id.lnr_address_search)
    LinearLayout lnr_address_search;
    @BindView(R.id.ll_nextInvoice)
    LinearLayout ll_nextInvoice;
    @BindView(R.id.et_invoice_next)
    EditText et_invoice_next;


    List<String> customerList;
    List<String> orderIdList;
    DBAdapter dbAdapter;
    String is_invoice = null;
    String next_invoice = "";
    String firmcode = "";
    private Context context;
    private List list = new ArrayList<>();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_info);
        ButterKnife.bind(this);
        context = this;
        tv_name.setText(R.string.order_information);
        back.setOnClickListener(this);
        bt_search.setOnClickListener(this);
        bt_address_search.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        is_invoice = AppUtils.readStringFromPref(this, "is_invoice");

        try {
            if (is_invoice == null) {
                customerList = dbAdapter.getAllPhoneList1New();
                orderIdList = dbAdapter.getOrderIdlist();

            } else {


                firmcode = (AppUtils.readStringFromPref(OrderInfoActivity.this, "firmcode") == null) ? "" : AppUtils.readStringFromPref(OrderInfoActivity.this, "firmcode");
//.................................................................................................//

                Log.i("firm_code", "fi-" + firmcode);
                if (firmcode != "") {


                    if (AppUtils.readStringFromPref(context, "next_invoice") != null && !AppUtils.readStringFromPref(context, "next_invoice").isEmpty()) {

                        if (dbAdapter.isOrderExist(AppUtils.readStringFromPref(context, "next_invoice"))) {
                            next_invoice = AppUtils.readStringFromPref(context, "next_invoice");
                        } else {
                            AppUtils.writeStringToPref(context, "next_invoice", "");
                            setOrderId();
                        }

                    } else {
                        setOrderId();
                    }


                } else {
                    Toast.makeText(context, R.string.please_select_firm, Toast.LENGTH_SHORT).show();
                }
            }
            //customerList = dbAdapter.getAllPhoneList1New();
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, customerList);
            et_address.setAdapter(adapter);
            et_address.setThreshold(1);

            //orderIdList = dbAdapter.getOrderIdlist();
            ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, orderIdList);
            et_invoice.setAdapter(adapter2);
            et_invoice.setThreshold(1);
            img_close.setOnClickListener(this);
            img_close1.setOnClickListener(this);


            if (is_invoice != null) {
                lnr_address_search.setVisibility(View.GONE);
                lnr_invoice_number.setVisibility(View.VISIBLE);
                ll_nextInvoice.setVisibility(View.VISIBLE);
                if (next_invoice == "") {
                    txt_invoice_number.setText(getResources().getString(R.string.firm_not_selected));
                } else {
                    txt_invoice_number.setText(getResources().getString(R.string.next_invoice_number) + " - " + next_invoice);
                }
                AppUtils.writeStringToPref(this, "is_invoice", "");
            } else {

                lnr_address_search.setVisibility(View.VISIBLE);
                lnr_invoice_number.setVisibility(View.GONE);
                ll_nextInvoice.setVisibility(View.GONE);
            }
            et_invoice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position >= 0) {
                        et_invoice.setClickable(false);
                        img_close1.setVisibility(View.VISIBLE);
                        et_invoice.setEnabled(false);
                    }
                }
            });

            et_address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position >= 0) {
                        et_address.setClickable(false);
                        img_close.setVisibility(View.VISIBLE);
                        et_address.setEnabled(false);
                    }
                }
            });

            et_address.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (et_address.getText().toString().isEmpty()) {
                        img_close.setVisibility(View.GONE);
                        tv_address_error.setVisibility(View.GONE);
                    } else {
                        img_close.setVisibility(View.VISIBLE);
                        tv_address_error.setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            et_invoice.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (et_invoice.getText().toString().isEmpty()) {
                        img_close1.setVisibility(View.GONE);
                        tv_invoice_error.setVisibility(View.GONE);
                    } else {
                        img_close1.setVisibility(View.VISIBLE);
                        tv_invoice_error.setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

        } catch (Exception e) {
//            Toast.makeText(this, "Error while create order ...", Toast.LENGTH_SHORT).show();
        }

        bt_invoice_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String invoice = et_invoice_next.getText().toString().trim();
                if (invoice.isEmpty()) {
                    Toast.makeText(context, R.string.please_enter_invoice, Toast.LENGTH_SHORT).show();

                } else if (!invoice.contains(AppUtils.readStringFromPref(context, "firmcode"))) {
                    Toast.makeText(context, R.string.please_enter_valid_invoice, Toast.LENGTH_SHORT).show();

                } else if (invoice.equalsIgnoreCase(AppUtils.readStringFromPref(context, "firmcode"))) {
                    Toast.makeText(context, R.string.please_enter_valid_invoice, Toast.LENGTH_SHORT).show();

                } else if (next_invoice != null && invoice.equalsIgnoreCase(next_invoice)) {
                    Toast.makeText(context, R.string.current_invoice_is_same, Toast.LENGTH_SHORT).show();

                } else if (!dbAdapter.isOrderExist(invoice)) {
                    Toast.makeText(context, R.string.invoice_number_is_exist, Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, R.string.invoice_changed, Toast.LENGTH_SHORT).show();
                    AppUtils.writeStringToPref(context, "next_invoice", invoice);
                    finish();
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_search:
                if (et_invoice.getText().toString().isEmpty()) {
                    tv_invoice_error.setVisibility(View.VISIBLE);
                    tv_invoice_error.setText("Please Enter Invoice No.");
                    et_invoice.requestFocus();
                    return;
                } else {
                    AppUtils.writeStringToPref(OrderInfoActivity.this, "invoiceno", et_invoice.getText().toString());
                    Intent intent = new Intent(OrderInfoActivity.this, OrderInfoDetailsActivity.class);
                    intent.putExtra("through", "invoice");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;

            case R.id.bt_address_search:
                if (et_address.getText().toString().isEmpty()) {
                    tv_address_error.setVisibility(View.VISIBLE);
                    tv_address_error.setText(R.string.please_enter_address_);
                    et_address.requestFocus();
                    return;

                } else if (!isValidAddress(et_address.getText().toString())) {
                    img_close.setVisibility(View.VISIBLE);
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(OrderInfoActivity.this, "orderAddress", et_address.getText().toString());
                    Intent intent = new Intent(OrderInfoActivity.this, AddressOrderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    return;

                } else {
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(OrderInfoActivity.this, "orderAddress", et_address.getText().toString().replaceAll(", ", ","));
                    Intent intent = new Intent(OrderInfoActivity.this, OrderInfoDetailsActivity.class);
                    intent.putExtra("through", "address");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;

            case R.id.img_close1:
                et_invoice.setText("");
                et_invoice.requestFocus();
                img_close1.setVisibility(View.GONE);
                et_invoice.setEnabled(true);
                break;

            case R.id.img_close:
                et_address.setText("");
                et_address.requestFocus();
                img_close.setVisibility(View.GONE);
                et_address.setEnabled(true);
                break;
        }
    }

    // to validate existing address
    public boolean isValidAddress(String s) {
        boolean isValidAddress = false;

        for (int i = 0; i < customerList.size(); i++) {
            if (customerList.contains(s)) {

                isValidAddress = true;
                break;
            } else {
                isValidAddress = false;
            }

        }

        return isValidAddress;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (is_invoice != null) {
            orderIdList = dbAdapter.getOrderIdlist();
        }
    }

    private void setOrderId() {
        list = dbAdapter.LastOrder(firmcode);

        if (list.size() > 0) {
            int localdOrderId = (Integer.parseInt(list.get(list.size() - 1).toString()) + 1);
            next_invoice = firmcode + "" + localdOrderId;

            if (!dbAdapter.isOrderExist(next_invoice)) {
                // order id is exist in database
                localdOrderId++;
                next_invoice = firmcode + "" + localdOrderId;

                for (int i = 0; i < list.size(); i++) {

                    if (dbAdapter.isOrderExist(next_invoice)) {
                        return;
                    } else {
                        localdOrderId++;
                        next_invoice = firmcode + "" + localdOrderId;
                    }
                }
            }

        } else {
            next_invoice = firmcode + (1);
        }
    }
}
