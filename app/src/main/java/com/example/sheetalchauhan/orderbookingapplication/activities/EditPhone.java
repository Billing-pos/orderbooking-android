package com.example.sheetalchauhan.orderbookingapplication.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditPhone extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.bt_proceed)
    Button bt_proceed;
    String phonestr;
    DBAdapter dbAdapter;
    Context context;
    String tableId = null, column = null, mobile = null;

    public static boolean isValidTelephone(String s) {
        Pattern p = Pattern.compile("[2-9][0-9]{7}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    // to validate phone number
    public static boolean isValid(String s) {
        Pattern p = Pattern.compile("(0/91)?[5-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_phone);
        ButterKnife.bind(this);
        tv_name.setText(R.string.update_phone_number);
        back.setOnClickListener(this);
        context = this;
        bt_proceed.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(context);

        tableId = getIntent().getStringExtra("tableId");
        column = getIntent().getStringExtra("column");
        mobile = getIntent().getStringExtra("mobile");

        if (mobile != null) {
            et_phone.setText("" + mobile);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        et_phone.setError(null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_proceed:
                phonestr = et_phone.getText().toString();
                if (phonestr.isEmpty()) {
                    et_phone.setError(getResources().getString(R.string.enter_phone_no));
                    et_phone.requestFocus();
                    return;

                } else if (!isValid(phonestr) && !isValidTelephone(phonestr)) {
                    et_phone.setError(getResources().getString(R.string.invalid_phone));
                    et_phone.requestFocus();
                    return;

                } else {

                    long rowId = dbAdapter.updateCustomerMobile(tableId, column, et_phone.getText().toString(), "", "");
//                    long rowId = dbAdapter.updateCustomerMobile(phonestr, id, orderId);
                    if (rowId == -1) {
                        Toast.makeText(context, R.string.update_phone_success_not, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, R.string.update_phone_success, Toast.LENGTH_SHORT).show();
                        finish();

                    }
                }
                break;
        }
    }

}
