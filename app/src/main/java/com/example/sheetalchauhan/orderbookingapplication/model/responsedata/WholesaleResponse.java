package com.example.sheetalchauhan.orderbookingapplication.model.responsedata;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WholesaleResponse {

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private List<WholesaleDataResponse> wholesaleDataResponses;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<WholesaleDataResponse> getWholesaleDataResponses() {
        return wholesaleDataResponses;
    }

    public void setWholesaleDataResponses(List<WholesaleDataResponse> wholesaleDataResponses) {
        this.wholesaleDataResponses = wholesaleDataResponses;
    }

}
