package com.example.sheetalchauhan.orderbookingapplication.model.customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomergetAddress {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("order_id")
    @Expose
    private String orderid;

    @SerializedName("firm")
    @Expose
    private String firm;

    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("mobile2")
    @Expose
    private String mobile2;

    @SerializedName("mobile3")
    @Expose
    private String mobile3;

    @SerializedName("mobile4")
    @Expose
    private String mobile4;

    @SerializedName("mobile5")
    @Expose
    private String mobile5;

    @SerializedName("mobile6")
    @Expose
    private String mobile6;

    @SerializedName("mobile7")
    @Expose
    private String mobile7;

    @SerializedName("mobile8")
    @Expose
    private String mobile8;

    @SerializedName("mobile9")
    @Expose
    private String mobile9;

    @SerializedName("mobile10")
    @Expose
    private String mobile10;

    @SerializedName("contact_name")
    @Expose
    private String contact_name;

    @SerializedName("houseno")
    @Expose
    private String houseno;

    @SerializedName("floor")
    @Expose
    private String floor;

    @SerializedName("block")
    @Expose
    private String block;

    @SerializedName("apartment")
    @Expose
    private String apartment;

    @SerializedName("sector")
    @Expose
    private String sector;

    @SerializedName("area")
    @Expose
    private String area;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("address_one")
    @Expose
    private String address_one;

    @SerializedName("address_three")
    @Expose
    private String address_three;

    @SerializedName("flag")
    @Expose
    private String flag;

    @SerializedName("tags")
    @Expose
    private String tags;

    @SerializedName("comments")
    @Expose
    private String comments;

    @SerializedName("calculation_weight")
    @Expose
    private String calculation_weight;

    @SerializedName("delivery_time")
    @Expose
    private String delivery_time;

    @SerializedName("cartage")
    @Expose
    private String cartage;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("count")
    @Expose
    private int count;

    @SerializedName("customer_name")
    @Expose
    private String customer_name;

    @SerializedName("cartage_discount")
    @Expose
    private String cartage_discount;

    @SerializedName("retail_discount")
    @Expose
    private String retail_discount;

    @SerializedName("tags_new")
    private String tags_new;

    @SerializedName("category_id")
    private String category_id;

    @SerializedName("kothi_flate")
    private String kothi_flate;

    @SerializedName("gali_no")
    private String gali_no;

    @SerializedName("extra")
    private String extra;

    @SerializedName("lift")
    private String lift;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTags_new() {
        return tags_new;
    }

    public void setTags_new(String tags_new) {
        this.tags_new = tags_new;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getmobile() {
        return mobile;
    }

    public void setmobile(String mobile) {
        this.mobile = mobile;
    }

    public String getmobile2() {
        return mobile2;
    }

    public void setmobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public String getmobile3() {
        return mobile3;
    }

    public void setmobile3(String mobile3) {
        this.mobile3 = mobile3;
    }

    public String getmobile4() {
        return mobile4;
    }

    public void setmobile4(String mobile4) {
        this.mobile4 = mobile4;
    }

    public String getmobile5() {
        return mobile5;
    }

    public void setmobile5(String mobile5) {
        this.mobile5 = mobile5;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public String getMobile3() {
        return mobile3;
    }

    public void setMobile3(String mobile3) {
        this.mobile3 = mobile3;
    }

    public String getMobile4() {
        return mobile4;
    }

    public void setMobile4(String mobile4) {
        this.mobile4 = mobile4;
    }

    public String getMobile5() {
        return mobile5;
    }

    public void setMobile5(String mobile5) {
        this.mobile5 = mobile5;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getHouseno() {
        return houseno;
    }

    public void setHouseno(String houseno) {
        this.houseno = houseno;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }


    public String getAddress_one() {
        return address_one;
    }

    public void setAddress_one(String address_one) {
        this.address_one = address_one;
    }

    public String getAddress_three() {
        return address_three;
    }

    public void setAddress_three(String address_three) {
        this.address_three = address_three;
    }

    public String getMobile6() {
        return mobile6;
    }

    public void setMobile6(String mobile6) {
        this.mobile6 = mobile6;
    }

    public String getMobile7() {
        return mobile7;
    }

    public void setMobile7(String mobile7) {
        this.mobile7 = mobile7;
    }

    public String getMobile8() {
        return mobile8;
    }

    public void setMobile8(String mobile8) {
        this.mobile8 = mobile8;
    }

    public String getMobile9() {
        return mobile9;
    }

    public void setMobile9(String mobile9) {
        this.mobile9 = mobile9;
    }

    public String getMobile10() {
        return mobile10;
    }

    public void setMobile10(String mobile10) {
        this.mobile10 = mobile10;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCartage() {
        return cartage;
    }

    public void setCartage(String cartage) {
        this.cartage = cartage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCartage_discount() {
        return cartage_discount;
    }

    public void setCartage_discount(String cartage_discount) {
        this.cartage_discount = cartage_discount;
    }

    public String getRetail_discount() {
        return retail_discount;
    }

    public void setRetail_discount(String retail_discount) {
        this.retail_discount = retail_discount;
    }

    public String getCalculation_weight() {
        return calculation_weight;
    }

    public void setCalculation_weight(String calculation_weight) {
        this.calculation_weight = calculation_weight;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getKothi_flate() {
        return kothi_flate;
    }

    public void setKothi_flate(String kothi_flate) {
        this.kothi_flate = kothi_flate;
    }

    public String getGali_no() {
        return gali_no;
    }

    public void setGali_no(String gali_no) {
        this.gali_no = gali_no;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getLift() {
        return lift;
    }

    public void setLift(String lift) {
        this.lift = lift;
    }
}
