package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.BuildConfig;
import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.CartAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Order;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.OrderWithImageModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.model.ResponseSearchProduct;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.model.SearchProductModel;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.HandleError;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowOrder extends AppCompatActivity implements CartAdapter.CartProductsAdapterListener {
    @BindView(R.id.tv_discount)
    TextView tv_discount;
    @BindView(R.id.tv_retail_dis)
    TextView tv_retail_dis;
    @BindView(R.id.retail_amount)
    TextView retail_amount;

    // left textview
    @BindView(R.id.amount)
    TextView tvAmount;
    @BindView(R.id.tv_tobepaid)
    TextView tv_tobepaid;
    @BindView(R.id.cartage_amount)
    TextView cartage_amount;
    @BindView(R.id.discount_amount)
    TextView discount_amount;
    @BindView(R.id.tv_firm_name1)
    TextView tv_firm_name1;
    @BindView(R.id.tv_address1)
    TextView tv_address1;
    @BindView(R.id.tv_mobile1)
    TextView tv_mobile1;
    @BindView(R.id.tv_invoice)
    TextView tv_invoice;
    @BindView(R.id.et_second_phone)
    TextView et_second_phone;
    @BindView(R.id.et_customer_name)
    TextView et_customer_name;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_customername)
    TextView tv_customername;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.piece)
    TextView piece;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.p_bar)
    ProgressBar p_bar;
    ArrayList<SearchProductModel> list = new ArrayList<>();
    private OrderWithImageModel model;
    private String getFirm, getAmount, getDate, getInvalid, getOrderid, getReceipt_no;
    private Context context;
    private DBAdapter dbAdapter;
    private int cartage_ext_amt = 0, total_cartAmt = 0, cartage_discount = 0;
    private String cartage, customerType, userAddress = "", userMobile;
    private double amount = 0.0;
    private Order order;
    private CartAdapter cartAdapter;
    private List<Cart> cartList = new ArrayList<>();
    private float amt_cartage = 0, amt_dis_cartage = 0, amt_dis_retail = 0, amt_total;
    private float serverCartage = 0, server_dis_cartage = 0, server_dis_retail = 0, calculatoin_weight = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_show_order);

        context = this;
        ButterKnife.bind(this);

        tv_name.setText(R.string.order_id);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getIntentData();


        cartAdapter = new CartAdapter(context, cartList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerview.setAdapter(cartAdapter);
        hitApi();

    }


    @Override
    protected void onResume() {
        super.onResume();
        dbAdapter = DBAdapter.getInstance(context);
        dbAdapter.open();
        order = new Order();
        order = dbAdapter.getOrderListByOrderId(getOrderid);

        customerType = dbAdapter.getCustomerType(userAddress);

        if (dbAdapter.getCartage(order.getAddress()) != null && !dbAdapter.getCartage(order.getAddress()).equalsIgnoreCase(""))
            cartage = dbAdapter.getCartage(order.getAddress());
        else
            cartage = "0";

        setCartage_amount();

        setNameMobOrder();

        String tempfirm = getOrderid.replaceAll("_", "");
        String firm = tempfirm.replaceAll("[0-9]", "");

        setFirmData(firm);
//        userAddress = order.getAddress();

    }

    private void setNameMobOrder() {
        String contact_name = dbAdapter.getcontact_name(order.getAddress());

        tv_customername.setText("Address : " + userAddress + "\n\nPhone :   " + userMobile);
        tv_invoice.setText(getOrderid);

        if (order.getMobile2() != null && !order.getMobile2().isEmpty() && !order.getMobile2().equalsIgnoreCase("null"))
            et_second_phone.setText(order.getMobile2());

        if (contact_name != null && !contact_name.isEmpty() && !contact_name.equalsIgnoreCase("null"))
            et_customer_name.setText(contact_name);
    }

    private void setAdapterData(String getOrderid, String address) {
//        setCartage_amount(cartList, address);
        setDiscountPrice();

        toalFinalAmount();

    }


    private void toalFinalAmount() {

        double total_kg = 0.0;
        for (int i = 0; i < cartList.size(); i++) {
            total_kg = total_kg + Double.parseDouble(cartList.get(i).getQuantity().replaceAll("-", "."));
        }

        if (("" + total_kg) != null && total_kg > 23) {
            double kg = total_kg - 23;
            double finalValue = Math.round(kg * 100) / 100;

            if (kg > finalValue)
                finalValue = finalValue + 1;

            double avgAmtforOneKg = 0.0;

            if (AppUtils.readStringFromPref(context, "cartage") != null && !AppUtils.readStringFromPref(context, "cartage").isEmpty() && Integer.parseInt(AppUtils.readStringFromPref(context, "cartage")) > 0)
                avgAmtforOneKg = Integer.parseInt(AppUtils.readStringFromPref(context, "cartage")) / 20;

            double temp = 0;
            temp = (((int) finalValue) + 3) * avgAmtforOneKg;
//            cartage_ext_amt = (int) temp;
        }

        int t_amount = 0;
        String temp_amount = null;
        for (int i = 0; i < cartList.size(); i++) {
            try {
                temp_amount = cartList.get(i).getAmount();
                if (temp_amount != null && !temp_amount.equals("null")) {

                    Double tempQuantity = Double.parseDouble("" + cartList.get(i).getAmount());
                    int aa = removeDecimalPrice2("" + tempQuantity);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(aa));
                    int netKg = bigDecimal.intValue();
                    t_amount = t_amount + netKg;

                } else {
                    Double temp_amt = Double.parseDouble(cartList.get(i).getPrice()) * Double.parseDouble(cartList.get(i).getQuantity());
                    String temp_amt1 = removeDecimalPrice("" + temp_amt);
                    t_amount = t_amount + Integer.parseInt(temp_amt1);
                }
            } catch (Exception e) {
                Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }

        }

        amount = Double.valueOf("" + t_amount);
        amt_total = (float) (amount + amt_cartage - amt_dis_cartage - amt_dis_retail);

        tvAmount.setText("Rs. " + (int) amt_total);

        if (getIntent().getStringExtra("tv_tobepaid") != null && getIntent().getStringExtra("tv_tobepaid").equalsIgnoreCase("2")) {
            tv_tobepaid.setText("Rs. " + "0");
        } else
            tv_tobepaid.setText("Rs. " + (int) amt_total);

    }

    private void setDiscountPrice() {

        String tempAddress = userAddress.replace("F", "f");


        calculatoin_weight = Float.parseFloat(dbAdapter.getCalculationWeight(tempAddress));


        price.setText(R.string.price_);
        ///// cartage discount
        server_dis_cartage = (0 + Float.parseFloat(dbAdapter.getCartage_discount(tempAddress)));

        //// main cartage amount
        serverCartage = (0 + Float.parseFloat(dbAdapter.getCartageAmount(tempAddress)));

        ////// retail discount
        server_dis_retail = (0 + Float.parseFloat(dbAdapter.getRetailDiscount(tempAddress)));

        tv_discount.setText(getResources().getString(R.string.wholesale_discount) + " (" + server_dis_cartage + "%)");
        tv_retail_dis.setText(getResources().getString(R.string.retail_discount) + " (" + server_dis_retail + ")");

        try {
            float totalKg = 0;
            if (cartList.size() > 0) {
                for (Cart kg : cartList) {
                    totalKg = totalKg + Float.parseFloat(kg.getQuantity().replace("-", "."));
                }
            }

            // retail discount
            if (server_dis_retail > 0) {
                amt_dis_retail = totalKg * server_dis_retail;
                retail_amount.setText("Rs. " + Math.round(amt_dis_retail));

            } else {
                amt_dis_retail = 0;
                retail_amount.setText("Rs. " + Math.round(amt_dis_retail));
            }

            // new formula of cartage and calculation_weight

            if (calculatoin_weight > 0) {
                calculatoin_weight += 3;
            } else
                calculatoin_weight = 23;

            //////// main cartage amount of more than 23 kg and below as well
            if (totalKg > calculatoin_weight) {

                float cartAmtFor1Kg = serverCartage / (calculatoin_weight - 3);

                amt_cartage = cartAmtFor1Kg * totalKg;
                cartage_amount.setText("Rs. " + Math.round(amt_cartage));

            } else {
                amt_cartage = serverCartage;
                cartage_amount.setText("Rs. " + Math.round(amt_cartage));
            }

            // cartage discount value
            if (amt_cartage > 0 && server_dis_cartage > 0) {

                amt_dis_cartage = amt_cartage * (server_dis_cartage / 100);
                discount_amount.setText("Rs. " + Math.round(amt_dis_cartage));

            } else {
                amt_dis_cartage = 0;
                discount_amount.setText("Rs. " + Math.round(amt_dis_cartage));
            }

        } catch (Exception e) {
            amt_dis_retail = 0;
            retail_amount.setText("Rs. " + Math.round(amt_dis_retail));

            amt_cartage = serverCartage;
            cartage_amount.setText("Rs. " + Math.round(amt_cartage));

            amt_dis_cartage = 0;
            discount_amount.setText("Rs. " + Math.round(amt_dis_cartage));
        }
    }


    private void setCartage_amount() {
        if (customerType != null) {

            if (customerType.equalsIgnoreCase("Retails")) {
                piece.setVisibility(View.GONE);
                tv_discount.setText(R.string.retail_discount);
                price.setText(R.string.price_);

            } else {
                piece.setVisibility(View.VISIBLE);
                tv_discount.setText(R.string.wholesale_discount);
                price.setText(R.string.price_);
            }
        }
    }

    private void setFirmData(String firmCode) {
        List<Firm> firms = new ArrayList<>();
        firms = dbAdapter.getFirmList();
        Firm firmData = new Firm();

        if (firms.size() > 0) {

            for (Firm firm : firms) {
                if (firm.getFirmcode().equalsIgnoreCase(firmCode)) {
                    firmData.setId(firm.getFirmcode());
                    firmData.setAddress(firm.getAddress());
                    firmData.setMobile(firm.getMobile());
                    firmData.setName(firm.getName());
                    firmData.setFirmcode(firm.getFirmcode());

                    tv_firm_name1.setText("" + firmData.getName());
                    tv_address1.setText("" + firmData.getAddress());
                    tv_mobile1.setText("" + firmData.getMobile());
                }
            }
        }
    }

    @Override
    public void onCartItemRemoved() {
    }

    @Override
    public void hidePiece() {

    }

    private void getIntentData() {
        if (getIntent().getStringExtra("getFirm") != null) {
            getFirm = getIntent().getStringExtra("getFirm");
        }
        if (getIntent().getStringExtra("getAmount") != null) {
            getAmount = getIntent().getStringExtra("getAmount");
        }
        if (getIntent().getStringExtra("getDate") != null) {
            getDate = getIntent().getStringExtra("getDate");
        }
        if (getIntent().getStringExtra("getInvalid") != null) {
            getInvalid = getIntent().getStringExtra("getInvalid");
        }
        if (getIntent().getStringExtra("getOrderid") != null) {
            getOrderid = getIntent().getStringExtra("getOrderid");
        }
        if (getIntent().getStringExtra("getAddress") != null) {
            userAddress = getIntent().getStringExtra("getAddress");
        }
        if (getIntent().getStringExtra("getMobile") != null) {
            userMobile = getIntent().getStringExtra("getMobile");
        }


        String tempOrderId = getOrderid;

        if (tempOrderId.contains("_")) {

            String[] temp = getOrderid.split("_");


            if (temp[1] != null && !temp[1].isEmpty()) {
                tempOrderId = temp[0] + "_" + temp[1];
            }
        }

        getOrderid = tempOrderId;

    }

    public int removeDecimalPrice2(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        return Integer.parseInt("" + bdprice);
    }

    public String removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        return "" + bdprice;
    }


    private void hitApi() {
        p_bar.setVisibility(View.VISIBLE);

        Call<ResponseSearchProduct> call = RetrofitHandler.getInstance().getApi().getShowOrderData("" + BuildConfig.VERSION_CODE, "" + getOrderid);

        call.enqueue(new Callback<ResponseSearchProduct>() {
            @Override
            public void onResponse(Call<ResponseSearchProduct> call, final Response<ResponseSearchProduct> response) {
                ResponseSearchProduct responseDto = response.body();
                if (response.isSuccessful() && responseDto.isError() == false) {

                    if (response.body().getData() != null && response.body().getData().size() > 0) {

                        for (int i = 0; i < response.body().getData().size(); i++) {
                            Cart model = new Cart();
                            model.setProductid(response.body().getData().get(i).getProduct_id());
                            model.setProductname(response.body().getData().get(i).getProduct_name());
                            model.setPrice(response.body().getData().get(i).getPrice());
                            model.setQuantity(response.body().getData().get(i).getQty());
                            cartList.add(model);
                        }
                        cartAdapter.notifyDataSetChanged();


                        setAdapterData(getOrderid, userAddress);

                    } else {
                        Toast.makeText(context, responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                }
                p_bar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseSearchProduct> call, final Throwable t) {
                p_bar.setVisibility(View.GONE);
                HandleError.handleTimeoutError(context, t, "858");
            }
        });
    }

}
