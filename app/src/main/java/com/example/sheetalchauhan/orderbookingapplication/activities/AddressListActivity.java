package com.example.sheetalchauhan.orderbookingapplication.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.AddAreaActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.order.CustomerOrdersActivity;
import com.example.sheetalchauhan.orderbookingapplication.adapters.AddressAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Customer;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppClass;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressListActivity extends AppCompatActivity implements View.OnClickListener, AddressAdapter.HideView {

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_comment)
    TextView tv_comment;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.bt_shopping)
    Button bt_shopping;
    @BindView(R.id.ll_address)
    LinearLayout ll_address;
    @BindView(R.id.ll_no_address_found)
    LinearLayout ll_no_address_found;
    @BindView(R.id.tv_choose)
    TextView tv_choose;

    DBAdapter dbAdapter;
    String phone, addressid, flag;
    String address2, aid;
    String[] add = null;
    String[] add2 = null;
    private String userType = null, contact_name = null, cartage = null, comments = null;
    private Context context = null;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        context = this;
        back.setOnClickListener(this);
        ll_address.setOnClickListener(this);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AddressListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(linearLayoutManager);
        flag = AppUtils.readStringFromPref(AddressListActivity.this, "flag");

        if (flag.equals("0")) {
            ll_address.setVisibility(View.GONE);
            bt_shopping.setVisibility(View.GONE);
            tv_choose.setVisibility(View.GONE);
            tv_name.setText("" + AppUtils.readStringFromPref(AddressListActivity.this, "phone"));
        } else {
            ll_address.setVisibility(View.VISIBLE);
            tv_name.setText(R.string.address_list);

        }

        dbAdapter = DBAdapter.getInstance(AddressListActivity.this);
        dbAdapter.open();

        try {
            phone = AppUtils.readStringFromPref(AddressListActivity.this, "phone");
            List<Customer> customerList = dbAdapter.getAllCustomer(phone);

            if (customerList != null && customerList.size() > 0) {
                contact_name = customerList.get(0).getName();
                userType = customerList.get(0).getType();
                cartage = customerList.get(0).getCartage();
                comments = customerList.get(0).getComments();

                if (comments != null && !comments.isEmpty())
                    tv_comment.setText(getResources().getString(R.string.customer_comment) + " : " + comments);
            }

            if (customerList.size() > 0) {
                Log.i("Customersize", String.valueOf(customerList.size()));

                AppUtils.writeStringToPref(AddressListActivity.this, "AddressAdding", "address");
                ll_no_address_found.setVisibility(View.GONE);
                AddressAdapter addressAdapter = new AddressAdapter(AddressListActivity.this, customerList, flag);
                recyclerview.setAdapter(addressAdapter);

            } else {
                AppUtils.writeStringToPref(AddressListActivity.this, "AddressAdding", "new");
                if (flag.equals("0")) {
                    bt_shopping.setVisibility(View.GONE);
                    ll_no_address_found.setVisibility(View.VISIBLE);
                } else {
                    bt_shopping.setVisibility(View.GONE);
                    ll_no_address_found.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, "Error to repeat order ...", Toast.LENGTH_SHORT).show();
        }

        bt_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addressid = AppUtils.readStringFromPref(AddressListActivity.this, "addressid");
                AppUtils.writeStringToPref(context, "AddressAdding", "normal");

                AppUtils.writeStringToPref(context, "address", address2);
                AppUtils.writeStringToPref(context, "addressid", aid);


                add = address2.split(",");
                add2 = add[3].split("/");
                AppUtils.writeStringToPref(context, "area", add[0]);
                AppUtils.writeStringToPref(context, "sector", add[1]);
                AppUtils.writeStringToPref(context, "apartment", add[2]);
                AppUtils.writeStringToPref(context, "block", add2[0]);
                AppUtils.writeStringToPref(context, "houseno", add2[1]);
                AppUtils.writeStringToPref(context, "floor", add[4]);

                if (userType != null)
                    AppUtils.writeStringToPref(context, "userType", userType);
                if (cartage != null && !cartage.isEmpty() && !cartage.equalsIgnoreCase("null"))
                    AppUtils.writeStringToPref(context, "cartage", cartage);
                if (contact_name != null)
                    AppUtils.writeStringToPref(context, "userName", contact_name);

                AppUtils.writeStringToPref(AddressListActivity.this, "area", add[0]);
                AppUtils.writeStringToPref(AddressListActivity.this, "sector", add[1]);
                AppUtils.writeStringToPref(AddressListActivity.this, "apartment", add[2]);
                AppUtils.writeStringToPref(AddressListActivity.this, "block", add2[0]);
                AppUtils.writeStringToPref(AddressListActivity.this, "houseno", add2[1]);
                AppUtils.writeStringToPref(AddressListActivity.this, "floor", add[4]);

                Intent intent = new Intent();
                if (AppClass.isOrderFirst) {
                    AppClass.isOrderFirst = false;
                    finish();

                } else {
                    intent = new Intent(AddressListActivity.this, CustomerOrdersActivity.class);
                    intent.putExtra("address", address2);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.ll_address:
                dbAdapter.deleteCart();
                Intent intent = new Intent(AddressListActivity.this, AddAreaActivity.class);
                intent.putExtra("hide", "show");
                AppUtils.writeStringToPref(AddressListActivity.this, "newaddress", "noPhone");
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void hideView(String address, String id) {
        address2 = address;
        aid = id;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUtils.writeStringToPref(this, "userType", "");
    }
}
