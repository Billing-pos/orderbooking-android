package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.math.BigDecimal;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    public List<Cart> itemList;
    public String userid;
    Context context;
    View view1;
    int mCheckedPosition;
    DBAdapter dbAdapter;
    CartAdapter.ViewHolder viewHolder1;
    double temp_amount = 0.0;
    private CartProductsAdapterListener listener;


    public CartAdapter(Context context, List<Cart> itemList, CartProductsAdapterListener listener) {
        this.listener = listener;
        this.context = context;
        this.itemList = itemList;
        dbAdapter = DBAdapter.getInstance(context);

    }

    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false);
        viewHolder1 = new CartAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final CartAdapter.ViewHolder holder, final int position) {
        String quantity = itemList.get(position).getQuantity().replace(".", "-");

        holder.name.setText("" + itemList.get(position).getProductname() + "");
        holder.mrp.setText("" + Float.parseFloat(itemList.get(position).getPrice()));
        holder.quantity.setText("" + quantity);

        try {

            if (AppUtils.readStringFromPref(context, "userType").equalsIgnoreCase("Whole sale")) {
                if (itemList.get(position).getPiece() != null && !itemList.get(position).getPiece().isEmpty() && !itemList.get(position).getPiece().equalsIgnoreCase("null")) {
                    holder.piece.setVisibility(View.VISIBLE);
                    holder.piece.setText(itemList.get(position).getPiece());
                } else {
                    holder.piece.setVisibility(View.GONE);
                    listener.hidePiece();
                }
            }


            String temp_amount = "";
            temp_amount = itemList.get(position).getAmount();

            if (itemList.get(position).getAmount() != null) {
                if (!temp_amount.equalsIgnoreCase("null")) {
                    String userType = AppUtils.readStringFromPref(context, "userType");
                    if (userType.equalsIgnoreCase("Retails")) {
                        holder.price.setText("" + itemList.get(position).getAmount());

                    } else
                        holder.price.setText("" + itemList.get(position).getAmount());

                } else {
                    Double tempMoney = Double.parseDouble(itemList.get(position).getPrice()) * Double.parseDouble(itemList.get(position).getQuantity());
                    BigDecimal amount = removeDecimalPrice("" + tempMoney);
                    holder.price.setText("" + amount);
                }
            } else {
                Double tempMoney = Double.parseDouble(itemList.get(position).getPrice()) * Double.parseDouble(itemList.get(position).getQuantity());
                BigDecimal amount = removeDecimalPrice("" + tempMoney);
                holder.price.setText("" + amount);
            }

        } catch (Exception e) {
//            Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show();
        }


//        try {
//            String[] b = quantity.split("\\-");
//            String dis2 = "";
//            int dis = 0;
//            if (b[1].length() == 2) {
//                holder.quantity.setText("" + quantity + "0");
//            } else
//                holder.quantity.setText("" + quantity);
//
//            if (!quantity.contains("-")) {
//                holder.quantity.setText("" + quantity + "-000");
//            }
//
//        } catch (Exception e) {
//            try {
//                holder.quantity.setText("" + quantity + "-000");
//            } catch (Exception e1) {
//                holder.quantity.setText("" + quantity);
//            }
//        }

        try {
            String[] b = quantity.split("\\-");
            String dis2 = "";
            int dis = 0;
            if (b[1].length() == 2) {
                holder.quantity.setText("" + quantity + "0");

            } else if (b[1].length() == 1) {
                holder.quantity.setText("" + quantity + "00");
            } else
                holder.quantity.setText("" + quantity);

        } catch (Exception e) {
            holder.quantity.setText("" + quantity);
        }



















    }

    public BigDecimal removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);

        return bdprice;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void removeItem(int position) {
        dbAdapter.removeCartItem(itemList.get(position).getId());
        itemList.remove(position);
        notifyItemRemoved(position);
        listener.onCartItemRemoved();
    }

    public void restoreItem(Cart item, int position) {
        itemList.add(position, item);
        notifyItemInserted(position);
    }

    public interface CartProductsAdapterListener {
        void onCartItemRemoved();

        void hidePiece();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, price, quantity, mrp, piece;
        public RelativeLayout viewBackground, viewForeground;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            piece = (TextView) v.findViewById(R.id.piece);
            price = (TextView) v.findViewById(R.id.price);
            mrp = (TextView) v.findViewById(R.id.mrp);
            quantity = (TextView) v.findViewById(R.id.quantity);
            viewBackground = v.findViewById(R.id.view_background);
            viewForeground = v.findViewById(R.id.view_foreground);
        }
    }


}