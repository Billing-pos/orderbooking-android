package com.example.sheetalchauhan.orderbookingapplication.activities.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.BuildConfig;
import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.product.AddProductActivity;
import com.example.sheetalchauhan.orderbookingapplication.adapters.OrderImageAdpter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.PreviousOrderAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.ordersync.CustomerOrderData;
import com.example.sheetalchauhan.orderbookingapplication.model.userordermodel.OrderApiData;
import com.example.sheetalchauhan.orderbookingapplication.model.userordermodel.OrderApiResponseDto;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.RequestOrdreByAddress;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.HandleError;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerOrdersActivity extends AppCompatActivity implements View.OnClickListener, PreviousOrderAdapter.cancelListener {
    @BindView(R.id.bt_neworder)
    Button bt_neworder;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_noData)
    TextView tv_noData;
    @BindView(R.id.rv_recyclerview)
    RecyclerView recyclerView;
    String address, phone;
    DBAdapter dbAdapter;
    @BindView(R.id.p_bar)
    ProgressBar p_bar;
    List<CustomerOrderData> customerOrderDataList = new ArrayList<>();
    private Context context;
    private PreviousOrderAdapter previousOrderAdapter;
    private OrderImageAdpter adapter;
    private List<OrderApiData> listDdata = new ArrayList<>();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_orders);
        ButterKnife.bind(this);
        context = this;
        AppUtils.writeStringToPref(context, "isCheckedBox", "false");
        tv_name.setText(R.string.previous_order);
        customerOrderDataList = new ArrayList<CustomerOrderData>();
        back.setOnClickListener(this);
        bt_neworder.setOnClickListener(this);
        // Log.i("IDDD",AppUtils.readStringFromPref(this,"category_id"));
//        Toast.makeText(this, "-----"+AppUtils.readStringFromPref(this,"category_id"), Toast.LENGTH_SHORT).show();

        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        phone = AppUtils.readStringFromPref(CustomerOrdersActivity.this, "phone");
        address = AppUtils.readStringFromPref(CustomerOrdersActivity.this, "address");

        String tempAddress = null;
        tempAddress = address.replaceAll("/", "/");
//        tempAddress= "KAROL BAGH,JOSHI ROAD,-,14/468,0F";

        adapter = new OrderImageAdpter(CustomerOrdersActivity.this, listDdata);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        hitApi(tempAddress);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_neworder:

                // for edit cart
                AppUtils.writeIntToPref(context, "commentId", 0);
                AppUtils.writeStringToPref(context, "customComment", "");
                AppUtils.writeStringToPref(context, "orderId", "");
                AppUtils.writeStringToPref(context, "secondPhone", "");
                AppUtils.writeStringToPref(context, "editCart", "false");
                dbAdapter.deleteCart();
                Intent intent = new Intent(CustomerOrdersActivity.this, AddProductActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("invalid", "0");
                startActivity(intent);
                break;
        }

    }

    @Override
    public void cancelOrder(String orderId) {
        dbAdapter.cancelOrder(orderId);
        finish();
    }


    private void hitApi(String address) {
        p_bar.setVisibility(View.VISIBLE);

        RequestOrdreByAddress requestModel = new RequestOrdreByAddress();
        requestModel.setAppVersion("" + BuildConfig.VERSION_CODE);
        requestModel.setAddress(address);
        Call<OrderApiResponseDto> call = RetrofitHandler.getInstance().getApi().getOrderByAddress(requestModel);

        call.enqueue(new Callback<OrderApiResponseDto>() {
            @Override
            public void onResponse(Call<OrderApiResponseDto> call, final Response<OrderApiResponseDto> response) {
                OrderApiResponseDto responseDto = response.body();

                if (response.isSuccessful()) {
                    if (responseDto.isError() == false) {

                        if (responseDto.getOrderApiDataList() != null && responseDto.getOrderApiDataList().size() > 0) {
                            List<OrderApiData> listDdata = response.body().getOrderApiDataList();
                            adapter.setData(listDdata);
                            tv_noData.setVisibility(View.GONE);

                        } else {
                            tv_noData.setVisibility(View.VISIBLE);
                            Toast.makeText(CustomerOrdersActivity.this, R.string.no_data_found, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "" + responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                    p_bar.setVisibility(View.GONE);

                } else {
                    tv_noData.setVisibility(View.VISIBLE);
                    p_bar.setVisibility(View.GONE);
                    Toast.makeText(context, "" + response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<OrderApiResponseDto> call, final Throwable t) {
                p_bar.setVisibility(View.GONE);
                HandleError.handleTimeoutError(context, t, "858");
            }
        });

    }

}
