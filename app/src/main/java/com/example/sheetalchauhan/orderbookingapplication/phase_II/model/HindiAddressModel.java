package com.example.sheetalchauhan.orderbookingapplication.phase_II.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class HindiAddressModel {

    @SerializedName("column_name")
    private String column_name;

    @SerializedName("english_name")
    private String english_name;

    @SerializedName("hindi_name")
    private String hindi_name;

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getEnglish_name() {
        return english_name;
    }

    public void setEnglish_name(String english_name) {
        this.english_name = english_name;
    }

    public String getHindi_name() {
        return hindi_name;
    }

    public void setHindi_name(String hindi_name) {
        this.hindi_name = hindi_name;
    }

    @NonNull
    @Override
    public String toString() {
        return "Columnname : " + column_name + ", English : " + english_name + ", Hindi : " + hindi_name;
    }
}
