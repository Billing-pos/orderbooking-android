package com.example.sheetalchauhan.orderbookingapplication.phase_iii;

public class SuperUserResponse {
    private Boolean error;
    private String message;

    public SuperUserResponse(Boolean error, String message) {
        this.error = error;
        this.message = message;
    }

    public Boolean getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
