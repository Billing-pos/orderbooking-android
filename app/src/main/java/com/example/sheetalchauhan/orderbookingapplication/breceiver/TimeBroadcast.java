package com.example.sheetalchauhan.orderbookingapplication.breceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.passcode.PasscodeRequestModel;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ResponsePojo;
import com.example.sheetalchauhan.orderbookingapplication.netwowk.NetworkStatus;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimeBroadcast extends BroadcastReceiver {
    String imeiNumber = "";


    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.i("broadcast", "trigger");
        AppUtils.readStringFromPref(context, "firmselected");
        runContinue(context);
    }

    public void runContinue(final Context context) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    firmDeselect(context);
                } catch (Exception e) {
                } finally {

                    handler.postDelayed(this, 1000);
                }
            }
        };
        handler.post(runnable);
    }

    public void firmDeselect(final Context context) {
        String firmselected = AppUtils.readStringFromPref(context, "firmname");
        if (firmselected != null && firmselected != "") {
            SimpleDateFormat format = new SimpleDateFormat("HH");
            String date = format.format(new Date());

            SimpleDateFormat format3 = new SimpleDateFormat("yyyyMMdd");
            String dailyDate = format3.format(new Date());

            if (Integer.parseInt(date) >= 10) {

                String lastDayDate = AppUtils.readStringFromPref(context, "lastDayDate");
                if (lastDayDate != null && (Integer.parseInt(lastDayDate) == Integer.parseInt(dailyDate))) {
                    Log.i("firm_logout_date_match", "current " + dailyDate + ", lastDate = " + lastDayDate);
                } else
                    checkFirmToDeselect(context);
            }
        } else {
            Log.i("firm_logout", ": firm is already logout");
        }
    }


    private void checkFirmToDeselect(Context context) {
        if (NetworkStatus.getConnectivity2(context)) {
            PasscodeRequestModel model = new PasscodeRequestModel();
            model.setDeviceId(imeiNumber);
            DBAdapter dbAdapter = DBAdapter.getInstance(context);
            dbAdapter.open();
            model.setFirmCode(String.valueOf(dbAdapter.getFirmList().get(Integer.parseInt(AppUtils.readStringFromPref(context, "firmselected"))).getId()));

            Call<ResponsePojo> call = RetrofitHandler.getInstance().getApi().sendDeSelectedFirm(model);
            call.enqueue(new Callback<ResponsePojo>() {
                @Override
                public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                    clearFirmData(context);
                    Log.i("firm_logout", "have internet firmlogut");
                }

                @Override
                public void onFailure(Call<ResponsePojo> call, Throwable t) {
                    Log.i("firm_logout", ": firm lougout onfailure");
                    clearFirmData(context);
                }
            });

        } else {
            Log.i("firm_logout", ": no internet firm lougout condition");
            clearFirmData(context);
        }
    }

    private void clearFirmData(Context context) {
        AppUtils.writeStringToPref(context, "firmname", "");
        AppUtils.writeStringToPref(context, "firmaddress", "");
        AppUtils.writeStringToPref(context, "firmmobile", "");
        AppUtils.writeStringToPref(context, "firmcode", "");
        AppUtils.writeStringToPref(context, "firmselected", "");
        AppUtils.writeStringToPref(context, "firmselected", "");
    }
}


