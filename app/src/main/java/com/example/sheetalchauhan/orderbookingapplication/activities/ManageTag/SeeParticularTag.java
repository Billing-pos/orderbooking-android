package com.example.sheetalchauhan.orderbookingapplication.activities.ManageTag;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.TagAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SeeParticularTag extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.rview_tab)
    RecyclerView rview_tab;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_list)
    TextView tv_list;
    @BindView(R.id.ll_toolbar)
    LinearLayout ll_toolbar;
    @BindView(R.id.back)
    ImageView back;
    String color;
    DBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_particular_tag);
        ButterKnife.bind(this);

        color = getIntent().getStringExtra("color");
        tv_name.setText(color + " Tag");
        checkColor();

        LinearLayoutManager llm = new LinearLayoutManager(SeeParticularTag.this, LinearLayoutManager.VERTICAL, false);
        rview_tab.setLayoutManager(llm);
        back.setOnClickListener(this);

        dbAdapter = DBAdapter.getInstance(SeeParticularTag.this);
        dbAdapter.open();
        List<CustomergetAddress> customerList = dbAdapter.getAllCustomerWithTag(color);

//        Toast.makeText(this, "size  "+customerList.size() , Toast.LENGTH_SHORT).show();

        if (customerList.size() > 0) {
            TagAdapter tagAdapter = new TagAdapter(SeeParticularTag.this, customerList, color);
            rview_tab.setAdapter(tagAdapter);
            tv_list.setVisibility(View.GONE);
            rview_tab.setVisibility(View.VISIBLE);


        } else {
            tv_list.setVisibility(View.VISIBLE);
            rview_tab.setVisibility(View.GONE);
        }
    }

    private void checkColor() {
        if (color.equalsIgnoreCase("Red")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        } else if (color.equalsIgnoreCase("Yellow")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.coloryellow));

        } else if (color.equalsIgnoreCase("Green")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.colorgreen));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }
}
