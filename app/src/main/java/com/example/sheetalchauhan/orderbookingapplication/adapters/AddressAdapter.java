package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.AddressListActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.AddAreaActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Customer;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<Customer> addressListList;
    int mCheckedPosition;
    Customer addressList;
    AddressAdapter.ViewHolder viewHolder1;
    String flag;
    Boolean ischeck = false;
    private HideView hideView;
    private DBAdapter dbAdapter;


    public AddressAdapter(Context context1, List<Customer> addressListList, String flag) {
        context = context1;
        this.addressListList = addressListList;
        this.flag = flag;
        hideView = (HideView) context;
        dbAdapter = DBAdapter.getInstance(context1);
        dbAdapter.open();
    }

    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.address_item, parent, false);
        viewHolder1 = new AddressAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final AddressAdapter.ViewHolder holder, final int position) {
        addressList = addressListList.get(position);

        if (flag.equals("0")) {
            holder.ratio.setVisibility(View.GONE);
            holder.textView.setVisibility(View.VISIBLE);
            holder.tv_tag.setVisibility(View.VISIBLE);
        }

        Log.i("data_cal_weight", "" + position + " = " + addressList.getCalculation_weight());


        holder.addressid.setText(String.valueOf(addressListList.get(position).getId()));
        holder.name.setText(addressList.getAddress());
        holder.addressHinidi.setText(addressList.getHindiAddress());

        if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
            holder.addressHinidi.setVisibility(View.GONE);
            holder.name.setVisibility(View.VISIBLE);
        } else {
            holder.addressHinidi.setVisibility(View.VISIBLE);
            holder.name.setVisibility(View.GONE);
        }

        String tagValue = addressListList.get(position).getTags_() != null ? addressListList.get(position).getTags_() : "";
        Log.i("log_adapter", ":" + tagValue);
        if (!tagValue.isEmpty() && !tagValue.equalsIgnoreCase("null"))
            holder.tags_.setText(tagValue);


        if (addressList.getTags().equalsIgnoreCase("green")) {
            holder.lla.setBackgroundColor(context.getResources().getColor(R.color.colorgreen));

        } else if (addressList.getTags().equalsIgnoreCase("red")) {
            holder.lla.setBackgroundColor(context.getResources().getColor(R.color.colorred));

        } else if (addressList.getTags().equalsIgnoreCase("yellow")) {
            holder.lla.setBackgroundColor(context.getResources().getColor(R.color.coloryellow));

        } else {
            holder.lla.setVisibility(View.GONE);
        }

        holder.ratio.setOnCheckedChangeListener(null);
        if (!ischeck) {
            if (mCheckedPosition == 0) {
                ischeck = true;
                hideView.hideView(holder.name.getText().toString(), holder.addressid.getText().toString());
                AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());
                AppUtils.writeStringToPref(context, "address", holder.name.getText().toString());
                AppUtils.writeStringToPref(context, "address2", holder.addressHinidi.getText().toString());
                AppUtils.writeStringToPref(context, "category_id", addressList.getType());

                setCalculationWeight(addressList.getCalculation_weight());
            }
        }

        holder.ratio.setChecked(position == mCheckedPosition);
        holder.ratio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCheckedPosition = position;
                notifyDataSetChanged();

                setCalculationWeight(addressList.getCalculation_weight());

                AppUtils.writeStringToPref(((AddressListActivity) context), "address", addressList.getAddress());
                AppUtils.writeStringToPref(((AddressListActivity) context), "address2", holder.addressHinidi.getText().toString());
                AppUtils.writeStringToPref(((AddressListActivity) context), "addressid", holder.addressid.getText().toString());
                AppUtils.writeStringToPref(context, "category_id", addressList.getType());
                hideView.hideView(holder.name.getText().toString(), holder.addressid.getText().toString());

            }
        });

        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheckedPosition = position;
                notifyDataSetChanged();

                setCalculationWeight(addressList.getCalculation_weight());

                AppUtils.writeStringToPref(context, "address", addressList.getAddress());
                AppUtils.writeStringToPref(context, "address2", addressList.getHindiAddress());
                AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());
                AppUtils.writeStringToPref(context, "category_id", addressList.getType());
                hideView.hideView(holder.name.getText().toString(), holder.addressid.getText().toString());


            }
        });
        holder.ll_edit_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.writeStringToPref(context, "address", holder.name.getText().toString());
                AppUtils.writeStringToPref(context, "address2", holder.addressHinidi.getText().toString());
                AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());
                AppUtils.writeStringToPref(context, "hide", "show");
                AppUtils.writeStringToPref(context, "area", "");
                AppUtils.writeStringToPref(context, "newaddress", "addPhone");
                AppUtils.writeStringToPref(context, "AddressAdding", "update");
                Intent intent = new Intent(context, AddAreaActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    private void setCalculationWeight(String value) {
        try {
            if (value != null && !value.isEmpty() && !value.equalsIgnoreCase("null"))
                AppUtils.writeStringToPref(context, "calculation_weight", value);

        } catch (Exception e) {
            AppUtils.writeStringToPref(context, "calculation_weight", "20");
            e.printStackTrace();
        }
    }


    public interface HideView {
        void hideView(String address, String id);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, addressid, textView, tv_tag, tags_, addressHinidi;
        public ImageView deleteadress, edit;
        public RadioButton ratio;
        public LinearLayout ll_item, lla;
        public RelativeLayout ll_edit_delete;


        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.address);
            addressHinidi = (TextView) v.findViewById(R.id.addressHinidi);
//            deleteadress = (ImageView) v.findViewById(R.id.deleteadress);
            addressid = (TextView) v.findViewById(R.id.addressid);
            ratio = (RadioButton) v.findViewById(R.id.ratio);
            edit = (ImageView) v.findViewById(R.id.edit);
            textView = (TextView) v.findViewById(R.id.textview);
            tv_tag = (TextView) v.findViewById(R.id.tv_tag);
            tags_ = (TextView) v.findViewById(R.id.tags_);
            ll_item = (LinearLayout) v.findViewById(R.id.ll_item);
            lla = (LinearLayout) v.findViewById(R.id.lla);
            ll_edit_delete = (RelativeLayout) v.findViewById(R.id.ll_edit_delete);
        }
    }


}