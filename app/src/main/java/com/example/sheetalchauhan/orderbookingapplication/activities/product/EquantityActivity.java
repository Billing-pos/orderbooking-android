package com.example.sheetalchauhan.orderbookingapplication.activities.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.order.CartListActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EquantityActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_qty)
    TextView tv_qty;
    @BindView(R.id.tv2_price)
    TextView tv2_price;
    @BindView(R.id.et_equantity)
    EditText et_equantity;
    @BindView(R.id.tv_pay)
    TextView tv_pay;
    @BindView(R.id.tv_price)
    EditText tv_price;

    @BindView(R.id.tv_half)
    TextView tv_half;
    @BindView(R.id.tv_onefourth)
    TextView tv_onefourth;
    @BindView(R.id.tv_two)
    TextView tv_two;
    @BindView(R.id.tv_twelve)
    TextView tv_twelve;
    @BindView(R.id.tv_fifteen)
    TextView tv_fifteen;
    @BindView(R.id.tv_twentyfive)
    TextView tv_twentyfive;

    @BindView(R.id.tv_one)
    TextView tv_one;
    @BindView(R.id.tv_five)
    TextView tv_five;
    @BindView(R.id.tv_ten)
    TextView tv_ten;
    @BindView(R.id.tv_twenty)
    TextView tv_twenty;

    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_fifty)
    TextView tv_fifty;
    @BindView(R.id.tv_hundred)
    TextView tv_hundred;
    @BindView(R.id.tv_onefifty)
    TextView tv_onefifty;
    @BindView(R.id.tv_twohundred)
    TextView tv_twohundred;
    @BindView(R.id.tv_twofifty)
    TextView tv_twofifty;
    @BindView(R.id.tv_fivehundred)
    TextView tv_fivehundred;
    @BindView(R.id.tv_onethousand)
    TextView tv_onethousand;
    @BindView(R.id.tv_fifteenhundred)
    TextView tv_fifteenhundred;
    @BindView(R.id.tv_per_price)
    TextView tv_per_price;
    @BindView(R.id.ll_piece)
    LinearLayout ll_piece;

    String productid, productname, price, quantity = "0", formattedDate, phone, address, sub_id, sub_name, price1;
    Double calQuantity = 0.0;
    DBAdapter dbAdapter;
    String firmcode = "";
    boolean checkTextWatcher = false;
    String tempAddress = "", tempAddress2 = "";


    // phase II

    // upper textview for kg
    @BindView(R.id.tv_halfkg)
    TextView tv_halfkg;
    @BindView(R.id.tv_onekg)
    TextView tv_onekg;
    @BindView(R.id.tv_fivekg)
    TextView tv_fivekg;
    @BindView(R.id.tv_tenkg)
    TextView tv_tenkg;
    @BindView(R.id.tv_twentykg)
    TextView tv_twentykg;

    // lower textview for piece
    @BindView(R.id.tv_onep)
    TextView tv_onep;
    @BindView(R.id.tv_twop)
    TextView tv_twop;
    @BindView(R.id.tv_fourp)
    TextView tv_fourp;
    @BindView(R.id.tv_fivep)
    TextView tv_fivep;
    @BindView(R.id.tv_sixp)
    TextView tv_sixp;
    @BindView(R.id.tv_eightp)
    TextView tv_eightp;
    @BindView(R.id.tv_tenp)
    TextView tv_tenp;
    @BindView(R.id.tv_twelvep)
    TextView tv_twelvep;
    @BindView(R.id.tv_fifteenp)
    TextView tv_fifteenp;
    @BindView(R.id.tv_twentyp)
    TextView tv_twentyp;

    // edittext
    @BindView(R.id.et_kg)
    EditText et_kg;
    @BindView(R.id.et_piece)
    EditText et_piece;

    //Linear layout of wholesale and retail
    @BindView(R.id.ll_wholesaler)
    LinearLayout ll_wholesaler;
    @BindView(R.id.ll_retail)
    LinearLayout ll_retail;
    int length = 0;
    private Context context;
    private String userType;
    private TextWatcher e_quantity = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String working = s.toString();
            boolean isValid = true;

            String arr[] = working.split("\\.");
            if (arr.length > 1) {
                if (arr[1].length() >= 3) {
                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(et_equantity.getText().length());
                    et_equantity.setFilters(filterArray);
                }
            } else {
                InputFilter[] filterArray = new InputFilter[1];
                filterArray[0] = new InputFilter.LengthFilter(9);
                et_equantity.setFilters(filterArray);
            }

            if (isStringInt(et_equantity.getText().toString().trim())) {
                Double tempMoney = Double.parseDouble(et_equantity.getText().toString().trim()) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);

                tv_qty.setVisibility(View.GONE);
                tv2_price.setVisibility(View.GONE);


            } else {
                tv_qty.setVisibility(View.VISIBLE);
                et_equantity.requestFocus();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (et_equantity.getText().toString().isEmpty()) {
                tv_price.setText("0");
            }
        }
    };
    private TextWatcher e_quantity2 = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String working = s.toString();
            boolean isValid = true;

            String arr[] = working.split("\\.");
            if (arr.length > 1) {
                if (arr[1].length() >= 3) {
                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(et_kg.getText().length());
                    et_kg.setFilters(filterArray);
                }
            } else {
                InputFilter[] filterArray = new InputFilter[1];
                filterArray[0] = new InputFilter.LengthFilter(9);
                et_kg.setFilters(filterArray);
            }

            if (isStringInt(et_kg.getText().toString().trim())) {
                Double tempMoney = Double.parseDouble(et_kg.getText().toString().trim()) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);

                tv_qty.setVisibility(View.GONE);
                tv2_price.setVisibility(View.GONE);


            } else {
                tv_qty.setVisibility(View.VISIBLE);
                et_kg.requestFocus();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (et_kg.getText().toString().isEmpty()) {
                ll_piece.setVisibility(View.GONE);
            } else {
                ll_piece.setVisibility(View.VISIBLE);
            }
        }
    };
    private TextWatcher e_price = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (isStringInt(tv_price.getText().toString().trim())) {
                Double tempQuantity = Double.parseDouble(tv_price.getText().toString().trim()) / Double.parseDouble(price);
                BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                int netKg = bigDecimal.intValue();
                removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));

                tv_qty.setVisibility(View.GONE);
                tv2_price.setVisibility(View.GONE);

            } else {

                tv2_price.setVisibility(View.VISIBLE);
                tv_price.requestFocus();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (tv_price.getText().toString().isEmpty()) {
                et_equantity.setText("0");
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equantity);
        ButterKnife.bind(this);
        context = this;
        back.setOnClickListener(this);
        userIsRetail();

        dbAdapter = DBAdapter.getInstance(this);
        productname = AppUtils.readStringFromPref(EquantityActivity.this, "productname");
        price = AppUtils.readStringFromPref(EquantityActivity.this, "productprice");
        productid = AppUtils.readStringFromPref(EquantityActivity.this, "product_id");
        phone = AppUtils.readStringFromPref(EquantityActivity.this, "phone");
        address = AppUtils.readStringFromPref(EquantityActivity.this, "address");
        sub_id = AppUtils.readStringFromPref(EquantityActivity.this, "subcategoryid");
        sub_name = AppUtils.readStringFromPref(EquantityActivity.this, "subcategoryname");
        tempAddress = AppUtils.readStringFromPref(EquantityActivity.this, "address");
        tempAddress2 = AppUtils.readStringFromPref(EquantityActivity.this, "address2");


        if (AppUtils.readStringFromPref(EquantityActivity.this, "changeLanguage").equalsIgnoreCase("english")) {
            tv_address.setText(address + "\n" + phone);

        } else {
            tv_address.setText(tempAddress2 + "\n" + phone);
        }

        et_equantity.setText("" + 1);
        tv_price.setText("" + price);
        tv_per_price.setText(getResources().getString(R.string.price) + price);
        tv_onefourth.setOnClickListener(this);
        tv_half.setOnClickListener(this);
        tv_one.setOnClickListener(this);
        tv_two.setOnClickListener(this);
        tv_five.setOnClickListener(this);
        tv_ten.setOnClickListener(this);
        tv_twelve.setOnClickListener(this);
        tv_fifteen.setOnClickListener(this);
        tv_twenty.setOnClickListener(this);
        tv_twentyfive.setOnClickListener(this);
        tv_pay.setOnClickListener(this);
        //fixed prices
        tv_fifty.setOnClickListener(this);
        tv_hundred.setOnClickListener(this);
        tv_onefifty.setOnClickListener(this);
        tv_twohundred.setOnClickListener(this);
        tv_twofifty.setOnClickListener(this);
        tv_fivehundred.setOnClickListener(this);
        tv_onethousand.setOnClickListener(this);
        tv_fifteenhundred.setOnClickListener(this);
        // for wholesaler
        tv_onep.setOnClickListener(this);
        tv_twop.setOnClickListener(this);
        tv_fourp.setOnClickListener(this);
        tv_fivep.setOnClickListener(this);
        tv_sixp.setOnClickListener(this);
        tv_eightp.setOnClickListener(this);
        tv_tenp.setOnClickListener(this);
        tv_twelvep.setOnClickListener(this);
        tv_fifteenp.setOnClickListener(this);
        tv_twentyp.setOnClickListener(this);

        tv_halfkg.setOnClickListener(this);
        tv_onekg.setOnClickListener(this);
        tv_fivekg.setOnClickListener(this);
        tv_tenkg.setOnClickListener(this);
        tv_twentykg.setOnClickListener(this);

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = df.format(c);
        firmcode = AppUtils.readStringFromPref(EquantityActivity.this, "firmcode");

        String category_id = AppUtils.readStringFromPref(EquantityActivity.this, "category_id");
        Log.i("category_id", category_id + "jkjk");


        et_equantity.setOnFocusChangeListener((View.OnFocusChangeListener) this);
        et_kg.setOnFocusChangeListener((View.OnFocusChangeListener) this);
        tv_price.setOnFocusChangeListener((View.OnFocusChangeListener) this);


    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.equals(findViewById(R.id.et_equantity))) {
            if (hasFocus) {
                et_equantity.addTextChangedListener(e_quantity);
            } else {
                et_equantity.removeTextChangedListener(e_quantity);
            }
        } else if (v.equals(findViewById(R.id.tv_price))) {
            if (hasFocus) {
                tv_price.addTextChangedListener(e_price);
            } else {
                tv_price.removeTextChangedListener(e_price);
            }
        } else if (v.equals(findViewById(R.id.et_kg))) {
            if (hasFocus) {
                et_kg.addTextChangedListener(e_quantity2);
            } else {
                et_kg.removeTextChangedListener(e_quantity2);
            }
        }
    }

    public void removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        tv_price.setText("" + bdprice);
    }

    public int removeDecimalPrice2(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        return Integer.parseInt("" + bdprice);
    }

    public void removeDecimalQuantity(String quantity, String tempp) {
        String[] b = quantity.split("\\.");
        String dis2 = "";
        int dis = 0;

        if (Integer.parseInt(b[1]) >= 100) {
            int num = Integer.parseInt(b[1]) / 100;
            int rem = Integer.parseInt(b[1]) % 100;
            int totalRem = 0;
            int totalGram = 0;

            if (rem >= 10) {
                int tempMod = rem / 10;
                int tempRem = rem % 10;

                if (tempRem >= 5 && tempRem <= 10) {
                    totalRem = 10;
                    totalGram = (num * 100) + (tempMod * 10) + totalRem;

                } else {
                    totalRem = 0;
                    totalGram = (num * 100) + (tempMod * 10) + totalRem;
                }

                et_equantity.setText(tempp + "." + totalGram);

            } else {
                if (rem >= 5 && rem <= 10) {
                    totalRem = 10;
                    totalGram = (num * 100) + totalRem;

                } else {
                    totalRem = 0;
                    totalGram = (num * 100) + totalRem;
                }
                et_equantity.setText(tempp + "." + totalGram);
            }

        } else if (Integer.parseInt(b[1]) >= 10 && Integer.parseInt(b[1]) < 100) {
            int num = Integer.parseInt(b[1]) / 10;
            int rem = Integer.parseInt(b[1]) % 10;
            int totalRem = 0;
            int totalGram = 0;

            if (rem >= 5 && rem <= 10) {
                totalRem = 10;
                totalGram = (num * 10) + totalRem;

            } else {
                totalRem = 0;
                totalGram = (num * 10) + totalRem;
            }
            et_equantity.setText(tempp + ".0" + totalGram);
        } else {
            int totalGram = 0;

            if (Integer.parseInt(b[1]) >= 5 && Integer.parseInt(b[1]) <= 10) {
                totalGram = 010;
                et_equantity.setText(tempp + "." + totalGram);

            } else {
                et_equantity.setText(tempp);
            }
        }
    }

    @Override
    public void onClick(View view) {
        Double tempMoney = 0.0;
        Double tempQuantity = 0.0;
        switch (view.getId()) {

            case R.id.back:
                onBackPressed();
                break;

            case R.id.tv_onefourth:
                tempMoney = Double.parseDouble("" + 0.25) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("0.25");
                break;

            case R.id.tv_half:
                tempMoney = Double.parseDouble("" + 0.5) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("0.5");
                break;

            case R.id.tv_one:
                tempMoney = Double.parseDouble("" + 1) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("1");
                break;

            case R.id.tv_two:
                tempMoney = Double.parseDouble("" + 2) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("2");
                break;


            case R.id.tv_five:
                tempMoney = Double.parseDouble("" + 5) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("5");
                break;

            case R.id.tv_ten:
                tempMoney = Double.parseDouble("" + 10) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("10");
                break;
            case R.id.tv_twelve:
                tempMoney = Double.parseDouble("" + 12) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("12");
                break;
            case R.id.tv_fifteen:
                tempMoney = Double.parseDouble("" + 15) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("15");
                break;

            case R.id.tv_twenty:
                tempMoney = Double.parseDouble("" + 20) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("20");
                break;
            case R.id.tv_twentyfive:
                tempMoney = Double.parseDouble("" + 25) * Double.parseDouble(price);
                removeDecimalPrice("" + tempMoney);
                et_equantity.setText("25");
                break;


            case R.id.tv_fifty:
                if (Float.parseFloat(price) == 0) {
                    et_equantity.setText("0");

                } else {
                    tempQuantity = Double.parseDouble(tv_fifty.getText().toString()) / Double.parseDouble(price);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                    int netKg = bigDecimal.intValue();
                    removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));
                }
                tv_price.setText("50");
                break;

            case R.id.tv_hundred:
                if (Float.parseFloat(price) == 0) {
                    et_equantity.setText("0");

                } else {
                    tempQuantity = Double.parseDouble(tv_hundred.getText().toString()) / Double.parseDouble(price);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                    int netKg = bigDecimal.intValue();
                    removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));
                }
                tv_price.setText("100");
                break;

            case R.id.tv_onefifty:
                if (Float.parseFloat(price) == 0) {
                    et_equantity.setText("0");

                } else {
                    tempQuantity = Double.parseDouble(tv_onefifty.getText().toString()) / Double.parseDouble(price);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                    int netKg = bigDecimal.intValue();
                    removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));
                }
                tv_price.setText("150");
                break;

            case R.id.tv_twohundred:
                if (Float.parseFloat(price) == 0) {
                    et_equantity.setText("0");

                } else {
                    tempQuantity = Double.parseDouble(tv_twohundred.getText().toString()) / Double.parseDouble(price);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                    int netKg = bigDecimal.intValue();
                    removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));
                }
                tv_price.setText("200");
                break;

            case R.id.tv_twofifty:
                if (Float.parseFloat(price) == 0) {
                    et_equantity.setText("0");

                } else {
                    tempQuantity = Double.parseDouble(tv_twofifty.getText().toString()) / Double.parseDouble(price);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                    int netKg = bigDecimal.intValue();
                    removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));
                }
                tv_price.setText("250");
                break;

            case R.id.tv_fivehundred:
                if (Float.parseFloat(price) == 0) {
                    et_equantity.setText("0");

                } else {
                    tempQuantity = Double.parseDouble(tv_fivehundred.getText().toString()) / Double.parseDouble(price);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                    int netKg = bigDecimal.intValue();
                    removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));
                }
                tv_price.setText("500");
                break;

            case R.id.tv_onethousand:
                if (Float.parseFloat(price) == 0) {
                    et_equantity.setText("0");

                } else {
                    tempQuantity = Double.parseDouble(tv_onethousand.getText().toString()) / Double.parseDouble(price);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                    int netKg = bigDecimal.intValue();
                    removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));
                }
                tv_price.setText("1000");
                break;

            case R.id.tv_fifteenhundred:
                if (Float.parseFloat(price) == 0) {
                    et_equantity.setText("0");

                } else {
                    tempQuantity = Double.parseDouble(tv_fifteenhundred.getText().toString()) / Double.parseDouble(price);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(tempQuantity));
                    int netKg = bigDecimal.intValue();
                    removeDecimalQuantity(String.format("%.3f", tempQuantity), String.valueOf(netKg));
                }
                tv_price.setText("1500");
                break;

            case R.id.tv_pay:

                if (et_equantity.getText().toString().isEmpty()) {
                    tv_qty.setVisibility(View.VISIBLE);
                    tv2_price.setVisibility(View.GONE);
                    et_equantity.requestFocus();

                } else if (!isStringInt(et_equantity.getText().toString().trim())) {
                    tv_qty.setVisibility(View.VISIBLE);
                    tv2_price.setVisibility(View.GONE);
                    et_equantity.requestFocus();

                } else if (tv_price.getText().toString().isEmpty()) {
                    tv_qty.setVisibility(View.GONE);
                    tv2_price.setVisibility(View.VISIBLE);
                    tv_price.requestFocus();

                } else if (!isStringInt(tv_price.getText().toString().trim())) {
                    tv_qty.setVisibility(View.GONE);
                    tv2_price.setVisibility(View.VISIBLE);
                    tv_price.requestFocus();

//                } else if (!tempAddress.equalsIgnoreCase("")) {

                } else {

                    try {

                        if (userType.equalsIgnoreCase("retails")) {

                            int final_amount = removeDecimalPrice2(tv_price.getText().toString().trim());
                            dbAdapter.open();
                            Cart cart = new Cart();
                            if (sub_id != null && !sub_id.isEmpty())
                                cart.setProductname(productname + " (" + sub_id + ")");
                            else
                                cart.setProductname(productname);

                            cart.setProductid(productid);

                            if (Float.parseFloat(price) >= 1) {
                                String arr[] = et_equantity.getText().toString().trim().split("\\.");
                                if (arr.length > 1) {
                                    cart.setQuantity(et_equantity.getText().toString().replace(".", "-"));
                                    cart.setAmount("" + final_amount);

                                } else {
                                    cart.setQuantity(et_equantity.getText().toString().trim());
                                    cart.setAmount("" + final_amount);

                                }

                            } else {
                                cart.setQuantity("0");
                                cart.setAmount("0");
                            }
                            tv_price.setText(final_amount + "");
                            cart.setPrice(price);
                            cart.setPhone(phone);
                            cart.setAddress(address);

                            long rowid = dbAdapter.insertCart(cart);
                            if (rowid == -1)
                                Toast.makeText(this, "order is not save", Toast.LENGTH_SHORT).show();
                            else {

                                AppUtils.writeStringToPref(EquantityActivity.this, "subcategoryid", null);
                                AppUtils.writeStringToPref(EquantityActivity.this, "subcategoryname", null);
                                Intent intent = new Intent(EquantityActivity.this, CartListActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                        } else if (userType.equalsIgnoreCase("")) {


                            int final_amount = removeDecimalPrice2(tv_price.getText().toString().trim());
                            dbAdapter.open();
                            Cart cart = new Cart();
                            if (sub_id != null && !sub_id.isEmpty())
                                cart.setProductname(productname + " (" + sub_id + ")");
                            else
                                cart.setProductname(productname);

                            cart.setProductid(productid);

                            if (Float.parseFloat(price) >= 1) {
                                String arr[] = et_equantity.getText().toString().trim().split("\\.");
                                if (arr.length > 1) {
                                    cart.setQuantity(et_equantity.getText().toString().replace(".", "-"));
                                    cart.setAmount("" + final_amount);

                                } else {
                                    cart.setQuantity(et_equantity.getText().toString().trim());
                                    cart.setAmount("" + final_amount);

                                }

                            } else {
                                cart.setQuantity("0");
                                cart.setAmount("0");
                            }
                            tv_price.setText(final_amount + "");
                            cart.setPrice(price);
                            cart.setPhone(phone);
                            cart.setAddress(address);

                            long rowid = dbAdapter.insertCart(cart);
                            if (rowid == -1)
                                Toast.makeText(this, "order is not save", Toast.LENGTH_SHORT).show();
                            else {

                                AppUtils.writeStringToPref(EquantityActivity.this, "subcategoryid", null);
                                AppUtils.writeStringToPref(EquantityActivity.this, "subcategoryname", null);
                                Intent intent = new Intent(EquantityActivity.this, CartListActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }

                        } else {

                            if (TextUtils.isEmpty(et_kg.getText().toString().trim())) {
                                Toast.makeText(context, R.string.please_fill_qty, Toast.LENGTH_SHORT).show();
                            } else if (TextUtils.isEmpty(et_piece.getText().toString().trim())) {
                                Toast.makeText(context, R.string.please_fill_piece, Toast.LENGTH_SHORT).show();
                            } else {
                                try {

                                    final float piece = Float.parseFloat(et_piece.getText().toString());
                                    final float kg = Float.parseFloat(et_kg.getText().toString());
                                    final float totalQuantity = kg * piece;
                                    final float totalAmount = totalQuantity * (Float.parseFloat(price));

                                    int final_amount = removeDecimalPrice2("" + totalAmount);
                                    dbAdapter.open();
                                    Cart cart = new Cart();
                                    if (sub_id != null && !sub_id.isEmpty())
                                        cart.setProductname(productname + " (" + sub_id + ")");
                                    else
                                        cart.setProductname(productname);

                                    cart.setProductid(productid);

                                    if (Float.parseFloat(price) >= 1) {
                                        String totalQty = "" + totalQuantity;
                                        String arr[] = totalQty.split("\\.");
                                        if (arr.length > 1) {

                                            cart.setPiece("" + piece);
                                            cart.setQuantity(totalQty.replace(".", "-"));
                                            cart.setAmount("" + final_amount);

                                        } else {
                                            cart.setPiece("" + piece);
                                            cart.setQuantity(totalQty);
                                            cart.setAmount("" + final_amount);

                                        }

                                    } else {
                                        cart.setQuantity("0");
                                        cart.setAmount("0");
                                    }
                                    tv_price.setText(final_amount + "");
                                    cart.setPrice(price);
                                    cart.setPhone(phone);
                                    cart.setAddress(address);
                                    cart.setPiece(et_kg.getText().toString() + " " + getResources().getString(R.string.ke) + " " + et_piece.getText().toString());

                                    long rowid = dbAdapter.insertCart(cart);
                                    if (rowid == -1)
                                        Toast.makeText(this, "order is not save", Toast.LENGTH_SHORT).show();
                                    else {

                                        AppUtils.writeStringToPref(EquantityActivity.this, "subcategoryid", null);
                                        AppUtils.writeStringToPref(EquantityActivity.this, "subcategoryname", null);
                                        Intent intent = new Intent(EquantityActivity.this, CartListActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                        startActivity(intent);
                                    }


                                } catch (Exception e) {
                                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                                }


                            }
                        }

                    } catch (Exception e) {
                        Toast.makeText(this, "something went wrong ...", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            // phase II for piece
            case R.id.tv_onep:
                et_piece.setText("1");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_twop:
                et_piece.setText("2");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_fourp:
                et_piece.setText("4");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_fivep:
                et_piece.setText("5");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_sixp:
                et_piece.setText("6");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_eightp:
                et_piece.setText("8");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_tenp:
                et_piece.setText("10");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_twelvep:
                et_piece.setText("12");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_fifteenp:
                et_piece.setText("15");
                setEdittextFocus(et_piece);
                break;

            case R.id.tv_twentyp:
                et_piece.setText("20");
                setEdittextFocus(et_piece);
                break;
            // for quantity

            case R.id.tv_halfkg:
                et_kg.setText("0.5");
                setEdittextFocus(et_kg);
                break;

            case R.id.tv_onekg:
                et_kg.setText("1");
                setEdittextFocus(et_kg);
                break;

            case R.id.tv_fivekg:
                et_kg.setText("5");
                setEdittextFocus(et_kg);
                break;

            case R.id.tv_tenkg:
                et_kg.setText("10");
                setEdittextFocus(et_kg);
                break;

            case R.id.tv_twentykg:
                et_kg.setText("20");
                setEdittextFocus(et_kg);
                break;

        }
    }

    private void setEdittextFocus(EditText editText) {
        int pos = editText.getText().length();
        editText.setSelection(pos);
        editText.requestFocus();
        ll_piece.setVisibility(View.VISIBLE);
    }

    public boolean isStringInt(String s) {
        try {
            Double.parseDouble(s);
            return true;

        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void userIsRetail() {
        userType = AppUtils.readStringFromPref(context, "userType");
        if (userType.equalsIgnoreCase("Whole sale")) {
            ll_retail.setVisibility(View.GONE);
            ll_wholesaler.setVisibility(View.VISIBLE);

        } else {
            ll_wholesaler.setVisibility(View.GONE);
            ll_retail.setVisibility(View.VISIBLE);
        }
        tv_name.setText(getResources().getString(R.string.add_quantity) + "( " + userType + " )");
    }

}
