package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.HouseNoAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.Constants;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.utitlity.SetTabLayout;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.google.android.material.tabs.TabLayout;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HouseNoActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.actv_area)
    Spinner actv_area;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.tv_address_error)
    TextView tv_address_error;
    @BindView(R.id.bt_address_search)
    Button bt_address_search;

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_area)
    EditText et_area;
    @BindView(R.id.bt_add_area)
    Button bt_add_area;
    @BindView(R.id.bt_skip)
    Button bt_skip;
    @BindView(R.id.bt_next)
    Button bt_next;
    @BindView(R.id.tv_id)
    TextView tv_id;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    DBAdapter dbAdapter;
    String blockString, aprt, area, sectorString;

    @BindView(R.id.ll_layout)
    LinearLayout ll_layout;
    @BindView(R.id.ll_add_area)
    LinearLayout ll_add_area;
    String hide_layout;
    List<String> areaList;

    @BindView(R.id.et_area2)
    EditText et_area2;
    @BindView(R.id.bt_next2)
    Button bt_next2;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_no);
        ButterKnife.bind(this);
        tv_name.setText(R.string.add_house_no);

        back.setOnClickListener(this);
        bt_add_area.setOnClickListener(this);
        bt_next.setOnClickListener(this);
        bt_next2.setOnClickListener(this);
        bt_skip.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();


        if (AppUtils.readStringFromPref(this, "changeLanguage").equalsIgnoreCase("hindi")) {
            blockString = AppUtils.readStringFromPref(HouseNoActivity.this, "block2");
            aprt = AppUtils.readStringFromPref(HouseNoActivity.this, "apartment2");
            area = AppUtils.readStringFromPref(HouseNoActivity.this, "area2");
            sectorString = AppUtils.readStringFromPref(HouseNoActivity.this, "sector2");

        } else {
            blockString = AppUtils.readStringFromPref(HouseNoActivity.this, "block");
            aprt = AppUtils.readStringFromPref(HouseNoActivity.this, "apartment");
            area = AppUtils.readStringFromPref(HouseNoActivity.this, "area");
            sectorString = AppUtils.readStringFromPref(HouseNoActivity.this, "sector");
        }

        tv_id.setText(area + "," + sectorString + "," + aprt + "," + blockString);

        blockString = AppUtils.readStringFromPref(HouseNoActivity.this, "block");
        aprt = AppUtils.readStringFromPref(HouseNoActivity.this, "apartment");
        area = AppUtils.readStringFromPref(HouseNoActivity.this, "area");
        sectorString = AppUtils.readStringFromPref(HouseNoActivity.this, "sector");

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(HouseNoActivity.this, 2);
        recyclerview.setLayoutManager(layoutManager);
        areaList = dbAdapter.getHousenoList(blockString);

        //sorting
        Collections.sort(areaList, (s1, s2) -> s1.compareToIgnoreCase(s2));

        if (areaList.size() > 0) {
            HouseNoAdapter sectorAdapter = new HouseNoAdapter(HouseNoActivity.this, areaList);
            recyclerview.setAdapter(sectorAdapter);
        } else {
            bt_address_search.setVisibility(View.GONE);
        }

        // show and hide edittext and button
        hide_layout = AppUtils.readStringFromPref(HouseNoActivity.this, "hide");

        if (hide_layout.contains("hide")) {
            tv_name.setText(R.string.select_house_no);
            et_area.setVisibility(View.GONE);
            bt_next.setVisibility(View.GONE);
//            bt_add_area.setVisibility(View.INVISIBLE);
            //by mj
            bt_add_area.setVisibility(View.VISIBLE);
            //
            rl.setVisibility(View.VISIBLE);
            actv();

        } else {
            ll_layout.setVisibility(View.VISIBLE);
            ll_add_area.setVisibility(View.VISIBLE);
        }

        TabLayout tabs = findViewById(R.id.tabs);
        SetTabLayout tabLayout = SetTabLayout.getInstance();
        tabLayout.setTabView(this, areaList, getSupportFragmentManager(), tabs, "house", "");

    }

    private void actv() {
        bt_address_search.setOnClickListener(this);
        img_close.setOnClickListener(this);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, areaList);
        actv_area.setAdapter(adapter);

        actv_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (areaList != null && !areaList.isEmpty()) {
                    try {
                        ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_next2:
                String area2 = et_area2.getText().toString();
                if (area2.isEmpty()) {
                    et_area2.setError("Enter House");
                    et_area2.requestFocus();
                    return;

                } else {
                    AppUtils.writeStringToPref(HouseNoActivity.this, "houseno", area2);
                    Intent intent = new Intent(HouseNoActivity.this, FloorActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;
            case R.id.bt_next:
                String area = et_area.getText().toString();
                if (area.isEmpty()) {
                    et_area.setError("Please enter House");
                    et_area.requestFocus();
                    return;

                } else {
                    AppUtils.writeStringToPref(HouseNoActivity.this, "houseno", area);
                    Intent intent = new Intent(HouseNoActivity.this, FloorActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_skip:
                AppUtils.writeStringToPref(HouseNoActivity.this, "houseno", "");
                Intent intent = new Intent(HouseNoActivity.this, FloorActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case R.id.bt_add_area:
                final Dialog dialog = new Dialog(HouseNoActivity.this);
                dialog.setContentView(R.layout.password_dialog);
                dialog.show();
                final EditText et_pass = (EditText) dialog.findViewById(R.id.et_password);
                Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);
                bt_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String pass = et_pass.getText().toString();
                        if (pass.equals(Constants.CUSTOMERPASSWORD)) {

                            dialog.dismiss();
                            et_area.setEnabled(true);
                            if (hide_layout.equalsIgnoreCase("hide")) {
                                et_area2.setEnabled(true);
                            }
                        } else {
                            Toast.makeText(HouseNoActivity.this, R.string.password_is_wrong, Toast.LENGTH_SHORT).show();
                            et_area.setEnabled(false);
                        }
                    }
                });
                break;

            case R.id.img_close:
                actv_area.requestFocus();
                img_close.setVisibility(View.GONE);
                actv_area.setEnabled(true);
                break;


            case R.id.bt_address_search:
                String address = "";

                if (areaList.isEmpty() || areaList.size() <= 0) {
                    tv_address_error.setVisibility(View.VISIBLE);
                    tv_address_error.setText("* Please enter houseno !");
                    actv_area.requestFocus();
                    return;

                } else {
                    address = actv_area.getSelectedItem().toString();
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(HouseNoActivity.this, "houseno", address);
                    Intent intent2 = new Intent(HouseNoActivity.this, FloorActivity.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent2);
                }
                break;
        }
    }


}
