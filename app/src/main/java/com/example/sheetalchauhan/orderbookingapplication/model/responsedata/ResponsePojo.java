package com.example.sheetalchauhan.orderbookingapplication.model.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePojo {
    @SerializedName("false")
    @Expose
    private boolean error;

    @SerializedName("next_invoice_no")
    @Expose
    private GetInvoice next_invoice_no;

    @SerializedName("msg")
    @Expose
    private List<String> msg;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<String> getMsg() {
        return msg;
    }

    public void setMsg(List<String> msg) {
        this.msg = msg;
    }

    public GetInvoice getNext_invoice_no() {
        return next_invoice_no;
    }

    public void setNext_invoice_no(GetInvoice next_invoice_no) {
        this.next_invoice_no = next_invoice_no;
    }

    public class GetInvoice{

        public String next_invoice_no;

        public String getNext_invoice_no() {
            return next_invoice_no;
        }

        public void setNext_invoice_no(String next_invoice_no) {
            this.next_invoice_no = next_invoice_no;
        }
    }

}
