package com.example.sheetalchauhan.orderbookingapplication.model.databasemodel;

public class Project {
    private String clientId;
    private String projectId;
    private String projecgtName;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjecgtName() {
        return projecgtName;
    }

    public void setProjecgtName(String projecgtName) {
        this.projecgtName = projecgtName;
    }
}
