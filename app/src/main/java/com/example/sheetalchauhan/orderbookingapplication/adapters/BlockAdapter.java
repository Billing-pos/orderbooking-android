package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.HouseNoActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

public class BlockAdapter extends RecyclerView.Adapter<BlockAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<String> addressListList;
    int mCheckedPosition;
    BlockAdapter.ViewHolder viewHolder1;
    int flag;
    private DBAdapter dbAdapter;

    public BlockAdapter(Context context, List<String> addressListList) {
        this.context = context;
        this.addressListList = addressListList;
        dbAdapter = DBAdapter.getInstance(context);
    }

    @Override
    public BlockAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.area_layout, parent, false);
        viewHolder1 = new BlockAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final BlockAdapter.ViewHolder holder, final int position) {
        if (addressListList == null)
            return;

        if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
            holder.name.setText(addressListList.get(position));
        } else {
            dbAdapter.open();
            String hindiValue = "";
            hindiValue = dbAdapter.getHindiAddress(addressListList.get(position), "pocket_block");
            if (hindiValue != null && !hindiValue.isEmpty()) {
                holder.name.setText(hindiValue);

            } else {
                holder.name.setText(addressListList.get(position));
            }
        }


        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "block", addressListList.get(position));
                AppUtils.writeStringToPref(context, "block2", holder.name.getText().toString());
                Intent intent = new Intent(context, HouseNoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;


        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.tv_area);

        }
    }


}

