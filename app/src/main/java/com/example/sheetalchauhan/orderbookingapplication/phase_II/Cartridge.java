package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.sheetalchauhan.orderbookingapplication.R;

public class Cartridge extends AppCompatActivity implements AreaFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartridge);
        setupFragment(new AreaFragment());

    }

    public void setupFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
