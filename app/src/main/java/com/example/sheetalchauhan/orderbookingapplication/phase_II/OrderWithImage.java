package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.BuildConfig;
import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.OrderImageAdpter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.userordermodel.OrderApiData;
import com.example.sheetalchauhan.orderbookingapplication.model.userordermodel.OrderApiResponseDto;
import com.example.sheetalchauhan.orderbookingapplication.netwowk.NetworkStatus;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.OrderImageListData;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.OrderWithImageModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.LastItemRview;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.RequestOrdreByOrderId;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.HandleError;
import com.otaliastudios.zoom.ZoomLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderWithImage extends AppCompatActivity implements OrderImageAdpter.itemClickListner, RecyclerView.OnScrollChangeListener {

    List<OrderImageListData> list;
    List<OrderWithImageModel> modelList = new ArrayList<>();
    Context context;
    RecyclerView recyclerView;
    DBAdapter dbAdapter;
    ZoomLayout zoomlayout;
    RelativeLayout orderimage_rev;
    ImageView Screenshot_image;
    @BindView(R.id.et_orderid)
    EditText et_orderid;
    @BindView(R.id.tv_next_invoice)
    TextView tv_next_invoice;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_allOrder)
    TextView tv_allOrder;
    @BindView(R.id.total_amount)
    TextView total_amount;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.p_bar)
    ProgressBar p_bar;
    private OrderImageAdpter adapter;
    private List<OrderApiData> listDdata = new ArrayList<>();
    private boolean a = true;
    private int orderpage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_with_image);
        recyclerView = findViewById(R.id.orderimage_recycler);
        Screenshot_image = findViewById(R.id.Screenshot_image);
        orderimage_rev = findViewById(R.id.orderimage_rev);
        zoomlayout = findViewById(R.id.zoomlayout);
        context = this;
        ButterKnife.bind(this);
        tv_name.setText(R.string.order_information);


        adapter = new OrderImageAdpter(OrderWithImage.this, listDdata);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);


//        if (NetworkStatus.getConnectivity(OrderWithImage.this)) {
////            hitApi();
//        }

        recyclerView.setOnScrollChangeListener(this);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.bt_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_orderid.getText().toString().isEmpty()) {
                    et_orderid.setError("enter order id");

                } else if (et_orderid.getText().toString().length() < 3) {
                    et_orderid.setError("enter valid order id");

                } else {
                    et_orderid.setError(null);
                    if (NetworkStatus.getConnectivity(OrderWithImage.this)) {
                        getOrderById(et_orderid.getText().toString().trim());
                    }
                }
            }
        });


        tv_allOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_orderid.setText("");
                tv_allOrder.setVisibility(View.INVISIBLE);
                adapter.clearData();
//                hitApi();
            }
        });

        setOrderId();
    }

    private void setOrderId() {
        String firmcode = AppUtils.readStringFromPref(context, "firmcode");
        String orderids = "";

        List list = new ArrayList<>();
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        try {

            list = dbAdapter.LastOrder(firmcode);

            if (list != null && list.size() > 0) {

                int localdOrderId = 0;

                try {
                    String[] temp = list.get(list.size() - 1).toString().split("_");
                    int orderIdToInt = Integer.parseInt(temp[0]);
                    localdOrderId = orderIdToInt + 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                orderids = firmcode + "" + localdOrderId;

                if (!dbAdapter.isOrderExist(orderids)) {
                    // order id is exist in database
                    localdOrderId++;
                    orderids = firmcode + "" + localdOrderId;

                    for (int i = 0; i < list.size(); i++) {

                        if (dbAdapter.isOrderExist(orderids)) {
//                        setInvoiceNo(getOrderIdWithTime(orderids));
                            return;
                        } else {
                            localdOrderId++;
                            orderids = firmcode + "" + localdOrderId;
                        }
                    }
                }

            } else {
                String firm_invoice = AppUtils.readStringFromPref(context, "firm_next_invoice_id");
                orderids = "" + firm_invoice;
            }
            tv_next_invoice.setText(getResources().getString(R.string.next_invoice_number) + "   " + orderids);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if (!a) {
            setTrue();
        } else
            super.onBackPressed();
    }

    private void setTrue() {
        a = true;
        orderimage_rev.setVisibility(View.GONE);
    }

    private void hitApi() {
        p_bar.setVisibility(View.VISIBLE);

        Call<OrderApiResponseDto> call = RetrofitHandler.getInstance().getApi().getOrderList("" + BuildConfig.VERSION_CODE, "hi", "" + orderpage);
        call.enqueue(new Callback<OrderApiResponseDto>() {
            @Override
            public void onResponse(Call<OrderApiResponseDto> call, final Response<OrderApiResponseDto> response) {
                OrderApiResponseDto responseDto = response.body();

                if (response.isSuccessful()) {
                    if (responseDto.isError() == false) {

                        if (responseDto.getOrderApiDataList().size() > 0) {
                            List<OrderApiData> listDdata = response.body().getOrderApiDataList();
                            adapter.setData(listDdata);

                        } else {
                            Toast.makeText(OrderWithImage.this, R.string.no_data_found, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        orderpage--;
                        Toast.makeText(context, "" + responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                    p_bar.setVisibility(View.GONE);

                } else {
                    orderpage--;
                    p_bar.setVisibility(View.GONE);
                    Toast.makeText(context, "" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OrderApiResponseDto> call, final Throwable t) {
                p_bar.setVisibility(View.GONE);
                HandleError.handleTimeoutError(context, t, "858");
            }
        });

    }

    @Override
    public void ClickonImage(OrderApiData orderImageListData) {


    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (LastItemRview.isLastItemDisplaying(recyclerView)) {
            if (NetworkStatus.getConnectivity(OrderWithImage.this)) {
                orderpage++;
//                hitApi();
            }
        }
    }

    private void getOrderById(String orderId) {
        p_bar.setVisibility(View.VISIBLE);

        RequestOrdreByOrderId requestModel = new RequestOrdreByOrderId();
        requestModel.setAppVersion("" + BuildConfig.VERSION_CODE);
        requestModel.setOrder_id(orderId);

        Call<OrderApiResponseDto> call = RetrofitHandler.getInstance().getApi().getPreviousOrderById(requestModel);
        call.enqueue(new Callback<OrderApiResponseDto>() {
            @Override
            public void onResponse(Call<OrderApiResponseDto> call, final Response<OrderApiResponseDto> response) {
                OrderApiResponseDto responseDto = response.body();

                if (response.isSuccessful()) {
                    if (responseDto.isError() == false) {

                        if (responseDto.getOrderApiDataList() != null && responseDto.getOrderApiDataList().size() > 0) {
                            adapter.clearData();
                            List<OrderApiData> listDdata = response.body().getOrderApiDataList();
                            adapter.setData(listDdata);

                        } else {
                            adapter.clearData();
                            adapter.notifyDataSetChanged();
                            Toast.makeText(OrderWithImage.this, R.string.no_data_found, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        adapter.clearData();
                        adapter.notifyDataSetChanged();
                        Toast.makeText(context, "" + responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                    p_bar.setVisibility(View.GONE);

                } else {
                    p_bar.setVisibility(View.GONE);
                    Toast.makeText(context, "" + response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<OrderApiResponseDto> call, final Throwable t) {
                p_bar.setVisibility(View.GONE);
                HandleError.handleTimeoutError(context, t, "858");
            }
        });


        tv_allOrder.setVisibility(View.GONE);

    }

}
