package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.product.EquantityActivity;
import com.example.sheetalchauhan.orderbookingapplication.model.product.SubCagetoryData;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<SubCagetoryData> addressListList;
    int mCheckedPosition;
    SubCategoryAdapter.ViewHolder viewHolder1;
    int flag;


    public SubCategoryAdapter(Context context, List<SubCagetoryData> addressListList) {
        this.context = context;
        this.addressListList = addressListList;


    }

    @Override
    public SubCategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.area_layout, parent, false);
        viewHolder1 = new SubCategoryAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final SubCategoryAdapter.ViewHolder holder, final int position) {

        holder.tv_name.setText(addressListList.get(position).getSubCategoryName());

        holder.tv_id.setText(addressListList.get(position).getSubCategoryId());
        holder.tv_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppUtils.writeStringToPref(context, "subcategoryid", holder.tv_id.getText().toString());
                AppUtils.writeStringToPref(context, "subcategoryname", holder.tv_name.getText().toString());
                Intent intent = new Intent(context, EquantityActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                context.startActivity(intent);


            }
        });

    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_id, tv_name, tv_price, tv_subcategory;


        public ViewHolder(View v) {
            super(v);
            tv_id = (TextView) v.findViewById(R.id.tv_area);
            tv_name = (TextView) v.findViewById(R.id.tv_name);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            tv_subcategory = (TextView) v.findViewById(R.id.tv_subcategory);

        }
    }


}
