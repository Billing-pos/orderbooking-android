package com.example.sheetalchauhan.orderbookingapplication.model.databasemodel;

public class ProjectImageData {
    private String projectId;
    private String name;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
