package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.Locale;

public class BaseActivity extends AppCompatActivity {
    private Locale mCurrentLocale;
    private Context context;

    public static Locale getLocale(Context context) {
        String lang = AppUtils.readStringFromPref(context, "changeLanguage");
        switch (lang) {
            case "english":
                lang = "en";
                break;
            case "hindi":
                lang = "hi";
                break;
            default:
                lang = "en";
                break;
        }
        return new Locale(lang);
    }

    @Override
    protected void onStart() {
        super.onStart();
        context = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        context = this;
        setLocale(context);
    }

    public void setLocale(Context context) {
        final Resources resources = context.getResources();
        final Configuration configuration = resources.getConfiguration();
        final Locale locale = getLocale(context);
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }
}