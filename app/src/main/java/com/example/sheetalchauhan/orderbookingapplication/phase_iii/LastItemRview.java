package com.example.sheetalchauhan.orderbookingapplication.phase_iii;

import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;

public class LastItemRview {

    public static boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();

            Log.i("lastvisib", "=" + lastVisibleItemPosition);

            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1) {
                return recyclerView.getAdapter().getItemCount() > 49;
            }
        } else {
            Log.i("lastvisib", "=0000 else");

        }
        return false;
    }


    public static int removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        return Integer.parseInt("" + bdprice);
    }

}
