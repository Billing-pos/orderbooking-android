package com.example.sheetalchauhan.orderbookingapplication.model.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductResponseDto extends ResponsePojo {
    @SerializedName("data")
    @Expose
    private List<ProductData> productDataList;

    public List<ProductData> getProductDataList() {
        return productDataList;
    }

    public void setProductDataList(List<ProductData> productDataList) {
        this.productDataList = productDataList;
    }
}
