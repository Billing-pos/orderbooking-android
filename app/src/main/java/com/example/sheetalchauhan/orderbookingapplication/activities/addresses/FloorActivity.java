package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.AddressListActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.PhoneListActivity;
import com.example.sheetalchauhan.orderbookingapplication.adapters.FloorAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.Constants;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.utitlity.SetTabLayout;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.google.android.material.tabs.TabLayout;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FloorActivity extends AppCompatActivity implements View.OnClickListener {

    static final String TAG = "FloorActivity";
    @BindView(R.id.actv_area)
    Spinner actv_area;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.tv_address_error)
    TextView tv_address_error;
    @BindView(R.id.bt_address_search)
    Button bt_address_search;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_area)
    EditText et_area;
    @BindView(R.id.bt_add_area)
    Button bt_add_area;
    @BindView(R.id.bt_next)
    Button bt_next;
    @BindView(R.id.bt_skip)
    Button bt_skip;
    @BindView(R.id.tv_id)
    TextView tv_id;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    DBAdapter dbAdapter;
    String houseno, blockString, aprt, area, sectorString;
    @BindView(R.id.ll_layout)
    LinearLayout ll_layout;
    @BindView(R.id.ll_add_area)
    LinearLayout ll_add_area;
    String hide_layout;
    List<String> areaList;
    int table_rowid = 0;
    String[] add = null;
    String[] add2 = null;
    String lastAddress = null, newAddress = null;

    @BindView(R.id.et_area2)
    EditText et_area2;
    @BindView(R.id.bt_next2)
    Button bt_next2;
    List<CustomergetAddress> customerList;
    String area3, storePhone = "";
    private Context context;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor);
        ButterKnife.bind(this);
        context = this;
        tv_name.setText(R.string.add_floor);
        back.setOnClickListener(this);
        bt_add_area.setOnClickListener(this);
        bt_next.setOnClickListener(this);
        bt_next2.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(this);

        bt_skip.setOnClickListener(this);

        dbAdapter.open();

        if (AppUtils.readStringFromPref(this, "changeLanguage").equalsIgnoreCase("hindi")) {
            houseno = AppUtils.readStringFromPref(FloorActivity.this, "houseno2");
            blockString = AppUtils.readStringFromPref(FloorActivity.this, "block2");
            aprt = AppUtils.readStringFromPref(FloorActivity.this, "apartment2");
            area = AppUtils.readStringFromPref(FloorActivity.this, "area2");
            sectorString = AppUtils.readStringFromPref(FloorActivity.this, "sector2");

        } else {
            houseno = AppUtils.readStringFromPref(FloorActivity.this, "houseno2");
            blockString = AppUtils.readStringFromPref(FloorActivity.this, "block2");
            aprt = AppUtils.readStringFromPref(FloorActivity.this, "apartment2");
            area = AppUtils.readStringFromPref(FloorActivity.this, "area2");
            sectorString = AppUtils.readStringFromPref(FloorActivity.this, "sector2");
        }

        tv_id.setText(area + "," + sectorString + "," + aprt + "," + blockString + "," + houseno);
        houseno = AppUtils.readStringFromPref(FloorActivity.this, "houseno");
        blockString = AppUtils.readStringFromPref(FloorActivity.this, "block");
        aprt = AppUtils.readStringFromPref(FloorActivity.this, "apartment");
        area = AppUtils.readStringFromPref(FloorActivity.this, "area");
        sectorString = AppUtils.readStringFromPref(FloorActivity.this, "sector");


        storePhone = AppUtils.readStringFromPref(FloorActivity.this, "phone");

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(FloorActivity.this, 2);
        recyclerview.setLayoutManager(layoutManager);

        areaList = dbAdapter.getFloorList(houseno);
        Collections.sort(areaList, (s1, s2) -> s1.compareToIgnoreCase(s2));

        if (areaList.size() > 0) {
            FloorAdapter sectorAdapter = new FloorAdapter(FloorActivity.this, areaList);
            recyclerview.setAdapter(sectorAdapter);
        } else {
            bt_address_search.setVisibility(View.GONE);
        }

        // show and hide edittext and button
        hide_layout = AppUtils.readStringFromPref(FloorActivity.this, "hide");

        if (hide_layout.contains("hide")) {
            tv_name.setText(R.string.select_floor);
            et_area.setVisibility(View.GONE);
            bt_next.setVisibility(View.GONE);
//            ll_add_area.setVisibility(View.GONE);
            //by mj
            ll_add_area.setVisibility(View.VISIBLE);
            //
            rl.setVisibility(View.VISIBLE);
            actv();

        } else {
            ll_layout.setVisibility(View.VISIBLE);
            ll_add_area.setVisibility(View.VISIBLE);
        }

        TabLayout tabs = findViewById(R.id.tabs);
        SetTabLayout tabLayout = SetTabLayout.getInstance();
        tabLayout.setTabView(this, areaList, getSupportFragmentManager(), tabs, "floor", "");
    }

    private void actv() {
        bt_address_search.setOnClickListener(this);
        img_close.setOnClickListener(this);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, areaList);
        actv_area.setAdapter(adapter);

        actv_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (areaList != null && !areaList.isEmpty()) {
                    try {
                        ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View view) {

        AppUtils.writeStringToPref(FloorActivity.this, "phone", storePhone);
        switch (view.getId()) {
            case R.id.bt_next2:
                area3 = et_area2.getText().toString();
                if (area3.isEmpty()) {
                    et_area2.setError("Enter Floor");
                    et_area2.requestFocus();
                    return;

                } else {

                    AppUtils.writeStringToPref(FloorActivity.this, "phone", "");
                    AppUtils.writeStringToPref(FloorActivity.this, "floor", area3);
                    checkAddingAddressExist();
                }
                break;
            case R.id.bt_next:
                String area = et_area.getText().toString();
                if (area.isEmpty()) {
                    et_area.setError("Please enter Floor");
                    et_area.requestFocus();
                    return;

                } else {

                    Boolean isFloorExists = false;

                    for (int i = 0; i < areaList.size(); i++) {
                        if (area.equalsIgnoreCase(areaList.get(i).replaceAll("f", ""))) {
                            Toast.makeText(this, "Floor exists", Toast.LENGTH_SHORT).show();
                            isFloorExists = true;
                            return;
                        }
                    }

                    if (!isFloorExists) {

                        String temp = AppUtils.readStringFromPref(FloorActivity.this, "AddressAdding");

                        if (temp.contains("update")) {
                            AppUtils.writeStringToPref(FloorActivity.this, "floor", area);
                            mobileWithLastAddress();

                        } else {
                            AppUtils.writeStringToPref(FloorActivity.this, "floor", area);
                            Intent intent = new Intent(FloorActivity.this, FullAddressActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        }
                    }
                }
                break;
            case R.id.back:
                onBackPressed();
                break;

            case R.id.bt_skip:
                AppUtils.writeStringToPref(FloorActivity.this, "floor", "0");
                checkAddingAddressExist();
                break;

            case R.id.bt_add_area:
                final Dialog dialog = new Dialog(FloorActivity.this);
                dialog.setContentView(R.layout.password_dialog);
                dialog.show();
                final EditText et_pass = (EditText) dialog.findViewById(R.id.et_password);
                Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);
                bt_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String pass = et_pass.getText().toString();
                        if (pass.equals(Constants.CUSTOMERPASSWORD)) {

                            dialog.dismiss();
                            et_area.setEnabled(true);
                            if (hide_layout.equalsIgnoreCase("hide")) {
                                et_area2.setEnabled(true);
                            }
                        } else {
                            Toast.makeText(FloorActivity.this, R.string.password_is_wrong, Toast.LENGTH_SHORT).show();
                            et_area.setEnabled(false);
                        }
                    }
                });
                break;

            case R.id.img_close:
                actv_area.requestFocus();
                img_close.setVisibility(View.GONE);
                actv_area.setEnabled(true);
                break;


            case R.id.bt_address_search:
                String address = "";

                if (areaList.isEmpty() || areaList.size() <= 0) {
                    tv_address_error.setVisibility(View.VISIBLE);
                    tv_address_error.setText("* Please enter floor !");
                    actv_area.requestFocus();
                    return;

                } else {
                    address = actv_area.getSelectedItem().toString();
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(FloorActivity.this, "floor", address);


                    String area2 = AppUtils.readStringFromPref(FloorActivity.this, "area");
                    String phone2 = AppUtils.readStringFromPref(FloorActivity.this, "phone");
                    String sector2 = AppUtils.readStringFromPref(FloorActivity.this, "sector");
                    String apartment2 = AppUtils.readStringFromPref(FloorActivity.this, "apartment");
                    String block2 = AppUtils.readStringFromPref(FloorActivity.this, "block");
                    String house2 = AppUtils.readStringFromPref(FloorActivity.this, "houseno");
                    String floor2 = AppUtils.readStringFromPref(FloorActivity.this, "floor");

                    String strAddress = area2 + "," + sector2 + "," + apartment2 + "," + block2 + "/" + house2 + "," + floor2;
                    AppUtils.writeStringToPref(FloorActivity.this, "address", strAddress);
                    AppUtils.writeStringToPref(FloorActivity.this, "address2", strAddress);
                    Intent intent2 = new Intent(FloorActivity.this, PhoneListActivity.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent2);
                }
                break;
        }
    }

    // ...................  checking new address is exist .................... //
    private Boolean checkNewAddressExist() {
        Boolean isTrue = false;
        String area = AppUtils.readStringFromPref(FloorActivity.this, "area");
        String phone = AppUtils.readStringFromPref(FloorActivity.this, "phone");
        String sector = AppUtils.readStringFromPref(FloorActivity.this, "sector");
        String apartment = AppUtils.readStringFromPref(FloorActivity.this, "apartment");
        String block = AppUtils.readStringFromPref(FloorActivity.this, "block");
        String house = AppUtils.readStringFromPref(FloorActivity.this, "houseno");
        String floor = AppUtils.readStringFromPref(FloorActivity.this, "floor");

        String newAddress = area + "," + sector + "," + apartment + "," + block + "/" + house + "," + floor + "f";

        AppUtils.writeStringToPref(FloorActivity.this, "address", newAddress);
        AppUtils.writeStringToPref(FloorActivity.this, "address2", newAddress);
        AppUtils.writeStringToPref(FloorActivity.this, "companyname", "Firm");

        List<CustomergetAddress> list_lastaddress = null;
        list_lastaddress = dbAdapter.getExistingNumber(phone, newAddress);

        if (list_lastaddress.size() > 0) {

            for (int i = 0; i < list_lastaddress.size(); i++) {

                if (list_lastaddress.get(0).getmobile().isEmpty()) {
                    updateMobile(list_lastaddress.get(0).getId(), "mobile", phone);
                    isTrue = true;

                } else if (list_lastaddress.get(0).getmobile2().isEmpty()) {
                    updateMobile(list_lastaddress.get(0).getId(), "mobile2", phone);
                    isTrue = true;

                } else if (list_lastaddress.get(0).getmobile3().isEmpty()) {
                    updateMobile(list_lastaddress.get(0).getId(), "mobile3", phone);
                    isTrue = true;

                } else if (list_lastaddress.get(0).getmobile4().isEmpty()) {
                    updateMobile(list_lastaddress.get(0).getId(), "mobile4", phone);
                    isTrue = true;

                } else if (list_lastaddress.get(0).getmobile5().isEmpty()) {
                    updateMobile(list_lastaddress.get(0).getId(), "mobile5", phone);
                    isTrue = true;

                } else {

                    if (lastAddress.equalsIgnoreCase(newAddress)) {
                        Toast.makeText(this, R.string.same_address, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(this, "5 Mobile already registered with this address", Toast.LENGTH_SHORT).show();
                    }
                    isTrue = false;
                }
            }
        } else {
            isTrue = true;
            insertAddress(phone, area, sector, apartment, block, house, floor);

        }

        return isTrue;
    }

    //............ check mobile number include with last address ...................... //
    private void mobileWithLastAddress() {
        lastAddress = AppUtils.readStringFromPref(FloorActivity.this, "address");
        String area = AppUtils.readStringFromPref(FloorActivity.this, "area");
        String phone = AppUtils.readStringFromPref(FloorActivity.this, "phone");
        String sector = AppUtils.readStringFromPref(FloorActivity.this, "sector");
        String apartment = AppUtils.readStringFromPref(FloorActivity.this, "apartment");
        String block = AppUtils.readStringFromPref(FloorActivity.this, "block");
        String house = AppUtils.readStringFromPref(FloorActivity.this, "houseno");
        String floor = AppUtils.readStringFromPref(FloorActivity.this, "floor");

        String newAddress = area + "," + sector + "," + apartment + "," + block + "/" + house + "," + floor + "f";

        AppUtils.writeStringToPref(FloorActivity.this, "address", newAddress);
        AppUtils.writeStringToPref(FloorActivity.this, "address2", newAddress);
        AppUtils.writeStringToPref(FloorActivity.this, "companyname", "Firm");

        List<CustomergetAddress> list_lastaddress = null;
        list_lastaddress = dbAdapter.getExistingNumber(phone, lastAddress);

        if (list_lastaddress.get(0).getmobile().contains(phone)) {

            if (list_lastaddress.get(0).getmobile2().isEmpty() && list_lastaddress.get(0).getmobile3().isEmpty() &&
                    list_lastaddress.get(0).getmobile4().isEmpty() && list_lastaddress.get(0).getmobile5().isEmpty()) {

                UpdateAddress(newAddress, Integer.parseInt(AppUtils.readStringFromPref(FloorActivity.this, "addressid")));

            } else {
                removePhone("mobile", AppUtils.readStringFromPref(FloorActivity.this, "addressid"));

            }

        } else if (list_lastaddress.get(0).getmobile2().contains(phone)) {

            if (list_lastaddress.get(0).getmobile().isEmpty() && list_lastaddress.get(0).getmobile3().isEmpty() &&
                    list_lastaddress.get(0).getmobile4().isEmpty() && list_lastaddress.get(0).getmobile5().isEmpty()) {


                UpdateAddress(newAddress, Integer.parseInt(AppUtils.readStringFromPref(FloorActivity.this, "addressid")));

            } else {
                removePhone("mobile2", AppUtils.readStringFromPref(FloorActivity.this, "addressid"));
            }

        } else if (list_lastaddress.get(0).getmobile3().contains(phone)) {

            if (list_lastaddress.get(0).getmobile().isEmpty() && list_lastaddress.get(0).getmobile2().isEmpty() &&
                    list_lastaddress.get(0).getmobile4().isEmpty() && list_lastaddress.get(0).getmobile5().isEmpty()) {

                UpdateAddress(newAddress, Integer.parseInt(AppUtils.readStringFromPref(FloorActivity.this, "addressid")));

            } else {
                removePhone("mobile3", AppUtils.readStringFromPref(FloorActivity.this, "addressid"));
            }

        } else if (list_lastaddress.get(0).getmobile4().contains(phone)) {

            if (list_lastaddress.get(0).getmobile().isEmpty() && list_lastaddress.get(0).getmobile2().isEmpty() &&
                    list_lastaddress.get(0).getmobile3().isEmpty() && list_lastaddress.get(0).getmobile5().isEmpty()) {

                UpdateAddress(newAddress, Integer.parseInt(AppUtils.readStringFromPref(FloorActivity.this, "addressid")));

            } else {
                removePhone("mobile4", AppUtils.readStringFromPref(FloorActivity.this, "addressid"));
            }

        } else if (list_lastaddress.get(0).getmobile5().contains(phone)) {

            if (list_lastaddress.get(0).getmobile().isEmpty() && list_lastaddress.get(0).getmobile2().isEmpty() &&
                    list_lastaddress.get(0).getmobile3().isEmpty() && list_lastaddress.get(0).getmobile4().isEmpty()) {

                UpdateAddress(newAddress, Integer.parseInt(AppUtils.readStringFromPref(FloorActivity.this, "addressid")));

            } else {
                removePhone("mobile5", AppUtils.readStringFromPref(FloorActivity.this, "addressid"));
            }

        } else {

        }
    }

    private void UpdateAddress(String address, int tableId) {
        int id = 0;
        id = tableId;

        if (id > 0) {
            Long rowId = dbAdapter.singleMobileUpdateAddress1(tableId, address);

            if (rowId == -1) {
                Toast.makeText(FloorActivity.this, "Address is not updated", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(FloorActivity.this, "Address is updated", Toast.LENGTH_LONG).show();
                startActivity(new Intent(FloorActivity.this, AddressListActivity.class));
                finish();
            }
        }
    }

    private void updateMobile(int tableId, String columnName, String phone) {
        int id = 0;
        id = tableId;

        if (id > 0) {
            Long rowId = dbAdapter.updateExistingAddress(tableId + "", columnName, phone);

            if (rowId == -1) {
                Toast.makeText(FloorActivity.this, "Address is not Updated !", Toast.LENGTH_LONG).show();

            } else {
                startActivity(new Intent(FloorActivity.this, AddressListActivity.class));
                finish();
            }
        } else {

        }
    }

    private void removePhone(String columnName, String tableId) {
        Boolean isNewAddressInsertMobile = false;

        isNewAddressInsertMobile = checkNewAddressExist();

        if (isNewAddressInsertMobile) {

            if (Integer.parseInt(tableId) > 0) {

                Long rowId = dbAdapter.removeUserPhone(columnName, Integer.parseInt(tableId));

                if (rowId == -1) {
                    Toast.makeText(FloorActivity.this, "Phone is not removed", Toast.LENGTH_LONG).show();
                } else {
                    startActivity(new Intent(FloorActivity.this, AddressListActivity.class));
                    finish();
                }
            }
        } else {

        }
    }

    private void insertAddress(String phone, String area, String sector, String apartment, String block, String house, String floor) {
        String strAddress = area + "," + sector + "," + apartment + "," + block + "/" + house + "," + floor + "f";
        AppUtils.writeStringToPref(FloorActivity.this, "address", strAddress);
        AppUtils.writeStringToPref(FloorActivity.this, "address2", strAddress);
        AppUtils.writeStringToPref(FloorActivity.this, "companyname", "Firm");
        dbAdapter.open();

        CustomergetAddress customerAddress = new CustomergetAddress();
        customerAddress.setAddress(strAddress);
        customerAddress.setArea(area);
        customerAddress.setSector(sector);
        customerAddress.setApartment(apartment);
        customerAddress.setBlock(block);
        customerAddress.setHouseno(house);
        customerAddress.setFloor(floor);
        customerAddress.setMobile(phone);
        customerAddress.setMobile2("");
        customerAddress.setMobile3("");
        customerAddress.setMobile4("");
        customerAddress.setMobile5("");
        customerAddress.setFlag("1");
        customerAddress.setTags("green");
        customerAddress.setFirm("" + AppUtils.readStringFromPref(FloorActivity.this, "firmname"));
        customerAddress.setOrderid("00");
        customerAddress.setType(AppUtils.readStringFromPref(context, "userType"));


        long rowid = dbAdapter.insertCustomerDatatest(customerAddress);
        if (rowid == -1) {
            Toast.makeText(FloorActivity.this, "Address is not Updated !", Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(FloorActivity.this, "Address is Updated !", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(FloorActivity.this, AddressListActivity.class));
            finish();
        }
    }

    public void checkAddingAddressExist() {
        String area2 = AppUtils.readStringFromPref(FloorActivity.this, "area");
        String phone2 = AppUtils.readStringFromPref(FloorActivity.this, "phone");
        String sector2 = AppUtils.readStringFromPref(FloorActivity.this, "sector");
        String apartment2 = AppUtils.readStringFromPref(FloorActivity.this, "apartment");
        String block2 = AppUtils.readStringFromPref(FloorActivity.this, "block");
        String house2 = AppUtils.readStringFromPref(FloorActivity.this, "houseno");
        String floor2 = AppUtils.readStringFromPref(FloorActivity.this, "floor");

        String strAddress = area2 + "," + sector2 + "," + apartment2 + "," + block2 + "/" + house2 + "," + floor2 + "f";

        dbAdapter.open();
        customerList = dbAdapter.getAllPhoneList(strAddress);

        if (customerList.size() > 0) {
            Toast.makeText(FloorActivity.this, "Address already exist", Toast.LENGTH_SHORT).show();
            AppUtils.writeStringToPref(context, "address", strAddress);
            AppUtils.writeStringToPref(context, "address2", strAddress);
            Intent intent = new Intent(context, PhoneListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            context.startActivity(intent);

        } else {
            AppUtils.writeStringToPref(FloorActivity.this, "AddressAdding", "new");
            AppUtils.writeStringToPref(FloorActivity.this, "floor", floor2);
            Intent intent = new Intent(FloorActivity.this, FullAddressActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        String tempMobile = "";
        tempMobile = AppUtils.readStringFromPref(FloorActivity.this, "newaddress");


        AppUtils.writeStringToPref(FloorActivity.this, "phone", storePhone);


    }
}
