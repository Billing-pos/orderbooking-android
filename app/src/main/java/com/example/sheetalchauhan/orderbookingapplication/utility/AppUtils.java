package com.example.sheetalchauhan.orderbookingapplication.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import java.io.File;
import java.util.Set;


public class AppUtils implements Constants {
    /****************
     * get prefrences
     * ***************/
    public static String readStringFromPref(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        if (!sharedPref.contains(key)) {
            return null;
        }
        return sharedPref.getString(key, "");
    }

    public static int readIntFromPref(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        if (!sharedPref.contains(key)) {
            return 0;
        }
        return sharedPref.getInt(key, 0);
    }

    public static boolean readBoolFromPref(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        if (!sharedPref.contains(key)) {
            return false;
        }
        return sharedPref.getBoolean(key, false);
    }

    public static Set<String> readSetFromPref(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        if (!sharedPref.contains(key)) {
            return null;
        }
        return sharedPref.getStringSet(key, null);
    }

    /****************
     * save prefrences
     * ***************/
    public static boolean writeStringToPref(Context context, String key, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        return sharedPref.edit().putString(key, value).commit();
    }

    public static void writeIntToPref(Context context, String key, int value) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        sharedPref.edit().putInt(key, value).apply();
    }

    public static void writeBoolToPref(Context context, String key, boolean value) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        sharedPref.edit().putBoolean(key, value).apply();
    }

    public static void writeSetToPref(Context context, String key, Set<String> value) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        sharedPref.edit().putStringSet(key, value).apply();
    }

    /*clear data*/
    public static boolean clearSharedPrefData(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        return editor.commit();
    }


    /**
     * Checks if the device is rooted.
     *
     * @return <code>true</code> if the device is rooted, <code>false</code> otherwise.
     */
    public static boolean isRooted() {

        // get from build info
        String buildTags = Build.TAGS;
        if (buildTags != null && buildTags.contains("test-keys")) {
            return true;
        }

        // check if /system/app/Superuser.apk is present
        try {
            File file = new File("/system/app/Superuser.apk");
            if (file.exists()) {
                return true;
            }
        } catch (Exception e1) {
            // ignore
        }

        // try executing commands
        return canExecuteCommand("/system/xbin/which su")
                || canExecuteCommand("/system/bin/which su") || canExecuteCommand("which su");
    }

    // executes a command on the system
    private static boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }

    public static boolean isEmulator1() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    public static boolean isEmulator2() {
        boolean isEmulator = Build.HARDWARE.contains("golfdish");
        return isEmulator;
    }


//    public static boolean openLink(Context context, String url, Params coupon, FullUserDetail userDetail)
//    {
//
//
//
//        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(trackingUrl));
//        PackageManager manager = context.getPackageManager();
//        List<ResolveInfo> infos = manager.queryIntentActivities(browserIntent, 0);
//        if (infos.size() > 0) {
//            context.startActivity(browserIntent);
//            return true;
//        } else {
//            return false;
//        }*/
//        return false;
//    }

//    public static String getUrl(Context context, String url, Params coupon, FullUserDetail userDetail) {
//
//        String trackingUrl = url;/*+PARAMETER_SEP+coupon.getParams().getTranctionId()+PARAMETER_EQUALS+"349"*/
//               /* + PARAMETER_SEP + coupon.getAndroidId() + PARAMETER_EQUALS + "com.pesa.happy"
//                + PARAMETER_SEP + coupon.getMobileNo() + PARAMETER_EQUALS + userDetail.getTelephone()
//                + PARAMETER_SEP + coupon.getReferalId() + PARAMETER_EQUALS + userDetail.getReferId()
//                + PARAMETER_SEP + coupon.getUserId() + PARAMETER_EQUALS + userDetail.getCustomerId();
//*/
//        return trackingUrl;
//    }

 /*   public static void goTo(Activity activity, String orderId, String amount, String redirect, String cancleUrl) {
        Intent intent = new Intent(activity, WebViewPaymentActivity.class);
        intent.putExtra(STR_ACCESS_CODE, ACCESS_CODE);
        intent.putExtra(STR_MERCHANT_ID, MERCHANT_ID);
        intent.putExtra(STR_ORDER_ID, orderId);
        intent.putExtra(STR_CURRENCY, CURRENCY);
        intent.putExtra(STR_AMOUNT, amount);

        intent.putExtra(STR_REDIRECT_URL, redirect.trim());
        intent.putExtra(STR_CANCEL_URL, cancleUrl.trim());
        intent.putExtra(STR_RSA_KEY_URL, RSA_URL.trim());

        activity.startActivity(intent);
    }
*/
/*
    public static void goTo1(Activity activity, String orderId, String amount, String redirect, String cancleUrl) {
        Intent intent = new Intent(activity, WebViewPurchasePaymentActivity.class);
        intent.putExtra(STR_ACCESS_CODE, ACCESS_CODE);
        intent.putExtra(STR_MERCHANT_ID, MERCHANT_ID);
        intent.putExtra(STR_ORDER_ID, orderId);
        intent.putExtra(STR_CURRENCY, CURRENCY);
        intent.putExtra(STR_AMOUNT, amount);

        intent.putExtra(STR_REDIRECT_URL, redirect.trim());
        intent.putExtra(STR_CANCEL_URL, cancleUrl.trim());
        intent.putExtra(STR_RSA_KEY_URL, RSA_URL.trim());

        activity.startActivity(intent);
    }
*/

    /************************************
     * Resize
     * *******************************/
/*
    public static Bitmap downSizeImage(String image, int size) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image, bmOptions);
        if (bitmap == null) {
            return null;
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, size, size, true);
        return bitmap;
    }
*/

    /*public static Bitmap downSizeImage(Bitmap bitmap, int size) {
        if (bitmap == null) {
            return null;
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, size, size, true);
        return bitmap;
    }
*/
  /*  public static String monthName(String number) {

        String month = number;
        switch (number) {
            case "01":
                month = "Jan";
                break;
            case "02":
                month = "Feb";
                break;
            case "03":
                month = "March";
                break;
            case "04":
                month = "April";
                break;
            case "05":
                month = "May";
                break;
            case "06":
                month = "June";
                break;
            case "07":
                month = "July";
                break;
            case "08":
                month = "August";
                break;
            case "09":
                month = "Sept";
                break;
            case "10":
                month = "Oct";
                break;
            case "11":
                month = "Nov";
                break;
            case "12":
                month = "Dec";
                break;
        }
        return month;
    }
*/
    public static File getInternalStorage(Context context) {
        File file = new File(context.getFilesDir() + "/HappyPesa" + "/");
        return file;
    }

    /********************************
     * Load file from Assest
     * **************************/
/*
    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
*/

    /*****************************
     * Notification
     * ****************************/
/*
    public static void showNotification2(Context context, NotificationDto dto, int count) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(count, buildNotification(context, dto, count).build());
    }
*/


    /*************
     * custom
     * ***************/
/*
    protected static NotificationCompat.Builder buildNotification(Context context, NotificationDto dto, int count) {

        // Open NotificationView.java Activity
        Intent intent = new Intent(context, SplashScreen.class);
        intent.putExtra(STR_INTENT_BODY, dto);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent pIntent = PendingIntent.getActivity(
                context,
                count,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_logo_sv);
        Uri customSound = Uri.parse(context.getContentResolver().SCHEME_ANDROID_RESOURCE
                + "://" + context.getPackageName() + "/raw/coins");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.ic_logo_sv)
                .setContentTitle("HappyPesa")
                .setContentText(dto.getMsg())
                // Set Ticker Message
                .setTicker("HappyPesa")
                // Dismiss Notification
                .setAutoCancel(true)
                .setVibrate(new long[]{500, 100, 600})
                .setSound(customSound)
                .setLargeIcon(bmp)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            // build a complex notification, with buttons and such
            //
            builder = builder.setCustomBigContentView(getComplexNotificationView(context, dto.getMsg()));
        } else {
            // Build a simpler notification, without buttons
            builder = builder.setContentTitle("HappyPesa")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(dto.getMsg()))
//                    .setContentText(msg)
                    .setSmallIcon(android.R.drawable.ic_menu_gallery);
        }
        return builder;
    }
*/

    /***********************
     * Remote view
     * **************************/
/*
    private static RemoteViews getComplexNotificationView(Context context, String msg) {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews notificationView = new RemoteViews(
                context.getPackageName(),
                R.layout.notification_layout
        );

        // Locate and set the Image into customnotificationtext.xml ImageViews
        notificationView.setImageViewResource(
                R.id.imagenotileft,
                R.drawable.ic_logo_sv);

        // Locate and set the Text into customnotificationtext.xml TextViews
        notificationView.setTextViewText(R.id.title, "HappyPesa");
        notificationView.setTextViewText(R.id.text, msg);

        return notificationView;
    }
*/

    /********************
     *String to date conversion
     *************************/
/*
    public static Date stringToDate(String date) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date startDate = null;
        try {
            startDate = df.parse(date);
            String newDateString = df.format(startDate);
            System.out.println(newDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }
*/

  /*  public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("tag", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("tag", ex.toString());
        }
        return null;
    }*/

   /* public static void setImageUrl(final ImageView imageView, final String url) {
        if (imageView.getHeight() == 0) {
            // wait for layout, same as Glide's SizeDeterminer does
            imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    // re-query viewTreeObserver, because the one used to add the listener may be dead: http://stackoverflow.com/a/29172475/253468
                    imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                    // call the same method, but now we can be sure getHeight() has a value
                    setImageUrl(imageView, url);
                    return true; // == allow drawing
                }
            });
        } else {

            if(url==null || url.trim().equals("")){
                return;
            }
            Picasso.with(HappyApplication.getmAppContext())
                    .load(url)
//                    .fit()
                    .placeholder(R.drawable.ic_no_image)
                    .into(imageView);

        }
    }
*/
    /*************************
     * get Bitmap image form cdn
     * ************************/
    /*  public static Bitmap getImage(Context context, String img, ImageReciever mReceiver,boolean isInDrawble) {

     *//*get density folder*//*
        String folder;
        if(isInDrawble){
            folder="drawable";
        }else {
            folder=getDensityName(context);
        }
        *//*check permission*//*
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {

            return BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_no_image);
        }

        *//*get file*//*
        File f = new File(AppUtils.getInternalStorage(context), img);
        Bitmap bitmap= null;

        if (f.exists()) {
//            ivProfilePic.setImageBitmap(AppUtils.downSizeImage(f.getAbsolutePath(), 124));
//            path = f.getAbsolutePath();
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
        } else {
            if(!folder.trim().equals("")){
                img=CDN_PATH+folder+"/"+img;
            }else {
                img=CDN_PATH+img;
            }

            bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_no_image);
        }
        return bitmap;
    }*/

    /********************************
     * Set image to imageview form cdn
     * ******************************/
  /*  public static void setImageFromCDN(ImageView image, String img, ImageReciever mReceiver,boolean isInDrawble) {

        Context context=image.getContext();

        *//*get density folder*//*
        String folder;
        if(isInDrawble){
            folder="drawable";
        }else {
            folder=getDensityName(context);
        }

        if(image.getTag()!=null){
            boolean b= (boolean) image.getTag();
            if(!b) {
                return;
            }
        }

        *//*check permission*//*
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {

            image.setImageResource(R.drawable.ic_no_image);
            image.setTag(true);
            return;
//            return BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_no_image);
        }

        *//*get file*//*
        File f = new File(AppUtils.getInternalStorage(context), img);
        Bitmap bitmap= null;

        if (f.exists()) {
//            ivProfilePic.setImageBitmap(AppUtils.downSizeImage(f.getAbsolutePath(), 124));
//            path = f.getAbsolutePath();
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
            image.setImageBitmap(bitmap);
            image.setTag(false);
            return;
        } else {
            if(!folder.trim().equals("")){
                img=CDN_PATH+folder+"/"+img;
            }else {
                img=CDN_PATH+img;
            }

            image.setImageResource(R.drawable.ic_no_image);
            image.setTag(true);
//            bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_no_image);
        }
//        return bitmap;
    }

*/
    /********************************
     * Download image and notify
     * ***************************/
  /*  public static boolean downloadImageFromServer(Context context, String img, ImageReciever mReceiver) {

        boolean isPresent;
        *//*check permission*//*
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {

            isPresent=false;
            return isPresent;
        }

        *//*get file*//*
        File f = new File(AppUtils.getInternalStorage(context), img);
        if (f.exists()) {
            isPresent=true;
        } else {

            isPresent=true;
        }
        return isPresent;
    }


    private static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
//            return "xxxhdpi";
            return "xxhdpi";

        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
//        return "ldpi";
        return "mdpi";
    }*/
}
