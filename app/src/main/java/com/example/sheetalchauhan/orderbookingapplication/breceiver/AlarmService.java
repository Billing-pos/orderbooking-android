package com.example.sheetalchauhan.orderbookingapplication.breceiver;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;

import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.GetAlarmManager;

public class AlarmService extends Service {
    MediaPlayer mediaPlayer;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {

            if (intent.getExtras().getString("ON_OFF") != null) {
                String on_Off = "" + intent.getExtras().getString("ON_OFF");
                switch (on_Off) {
                    case Constants.ADD_INTENT:

                        AudioManager amanager1 = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        amanager1.setStreamMute(AudioManager.STREAM_ALARM, true);
                        int maxVolume = amanager1.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                        amanager1.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);

                        Uri uri = GetAlarmManager.getRingtone(this);

                        if (AppUtils.readStringFromPref(this, "silentMode") != null) {
                            if (AppUtils.readStringFromPref(this, "silentMode").equalsIgnoreCase("true")) {
                                mediaPlayer = MediaPlayer.create(this, null);
                                amanager1.setStreamMute(AudioManager.STREAM_ALARM, false);
                            } else {
                                mediaPlayer = MediaPlayer.create(this, uri);
                            }
                        } else {
                            mediaPlayer = MediaPlayer.create(this, uri);
                        }
                        mediaPlayer.start();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                                    mediaPlayer.stop();
                                    mediaPlayer.reset();
                                }

                            }
                        }, 60000);


                        break;

                    case Constants.OFF_INTENT:
                        int alarmId = intent.getExtras().getInt("AlarmId");
                        if (mediaPlayer != null && mediaPlayer.isPlaying() && alarmId == AlarmReceiver.pendingId) {
                            mediaPlayer.stop();
                            mediaPlayer.reset();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
    }


    public IBinder onBind(Intent intent) {
        return null;
    }
}