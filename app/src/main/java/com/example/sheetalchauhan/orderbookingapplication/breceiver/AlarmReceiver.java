package com.example.sheetalchauhan.orderbookingapplication.breceiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.Alarm;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.Constant;

public class AlarmReceiver extends BroadcastReceiver {
    public static int pendingId;
    private NotificationManager notificationManager;


    @Override
    public void onReceive(Context context, Intent intent) {
        intent.getIntExtra("id", 0);
        if (intent != null) {
            Intent intentToService = new Intent(context, AlarmService.class);
            try {

                String intentType = intent.getExtras().getString("intentType");
                String todo = intent.getExtras().getString("TODO");
                String orderids = intent.getExtras().getString("orderids");
                String alarmId = intent.getExtras().getString("alarmId");
                int alarmId1 = intent.getExtras().getInt("PendingId");
                long alarmTime = intent.getExtras().getLong("alarmTime");

                Intent i = new Intent(context, Alarm.class);
                i.putExtra("closeAlarm", "closeAlarm");
                i.putExtra(Constant.ORDER_ID, intent.getExtras().getString("orderids"));
                i.putExtra(Constant.ALARM_ID, alarmId1);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, (int) alarmTime, i, PendingIntent.FLAG_CANCEL_CURRENT);

                switch (intentType) {
                    case Constants.ADD_INTENT:
                        Notification.Builder builder = new Notification.Builder(context);


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            int notifyID = 1000;
                            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", "name", NotificationManager.IMPORTANCE_HIGH);
                            builder.setContentTitle("Reminder for Order - " + orderids);
                            builder.setContentText(todo);
                            builder.setSmallIcon(R.drawable.ic_access_alarm_black_24dp);
                            builder.setContentIntent(pendingIntent);
                            builder.setChannelId("CHANNEL_ID");
                            if (AppUtils.readStringFromPref(context, "vibrateMode") != null && AppUtils.readStringFromPref(context, "vibrateMode").equalsIgnoreCase("true")) {
                                Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(VibrationEffect.createWaveform(new long[]{1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500}, -1));
                            }
                            builder.setAutoCancel(true);
                            Notification notification = builder.build();

                            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                            manager.createNotificationChannel(channel);
                            manager.notify(1, notification);
                            pendingId = intent.getExtras().getInt("PendingId");
                            intentToService.putExtra("ON_OFF", Constants.ADD_INTENT);
                            intentToService.putExtra("AlarmId", alarmId);
                            intentToService.putExtra("orderids", orderids);
                            manager.createNotificationChannel(channel);
                            context.startService(intentToService);

                        } else {

                            builder.setContentTitle("Reminder for Order - " + orderids);
                            builder.setContentText(todo);
                            builder.setSmallIcon(R.drawable.ic_access_alarm_black_24dp);
                            builder.setContentIntent(pendingIntent);
                            if (AppUtils.readStringFromPref(context, "vibrateMode") != null && AppUtils.readStringFromPref(context, "vibrateMode").equalsIgnoreCase("true"))
                                builder.setVibrate(new long[]{1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500});
                            builder.setAutoCancel(true);
                            Notification notification = builder.build();

                            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                            manager.notify((int) 1, notification);
                            pendingId = intent.getExtras().getInt("PendingId");
                            intentToService.putExtra("ON_OFF", Constants.ADD_INTENT);
                            intentToService.putExtra("AlarmId", alarmId);
                            intentToService.putExtra("orderids", orderids);
                            context.startService(intentToService);
                        }

                        DBAdapter dbAdapter = DBAdapter.getInstance(context);
                        dbAdapter.open();
                        dbAdapter.updateAlarm(orderids, "1");
                        dbAdapter.close();
                        break;

                    case Constants.OFF_INTENT:
                        closeAlarm(intent, intentToService, context);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void closeAlarm(Intent intent, Intent intentToService, Context context) {
        String alarmId = intent.getExtras().getString("AlarmId");
        intentToService.putExtra("ON_OFF", Constants.OFF_INTENT);
        intentToService.putExtra("AlarmId", alarmId);
        context.stopService(intentToService);
    }

}