package com.example.sheetalchauhan.orderbookingapplication.interfaces;


import com.example.sheetalchauhan.orderbookingapplication.model.ReceiptTypeResponse;
import com.example.sheetalchauhan.orderbookingapplication.model.ResponsePojoisSelect;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomerResponse;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.FirmData;
import com.example.sheetalchauhan.orderbookingapplication.model.isFirmSelected;
import com.example.sheetalchauhan.orderbookingapplication.model.ordersync.OrderSyncData;
import com.example.sheetalchauhan.orderbookingapplication.model.passcode.PasscodeRequestModel;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.CustomerResponseDto;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductResponseDto;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ResponsePojo;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ResponsePojoFirm;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.SubCategoryResponse;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.WholesaleResponse;
import com.example.sheetalchauhan.orderbookingapplication.model.userordermodel.OrderApiResponseDto;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CommentResponseModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CustomerUpsyncAddress;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.OrderImageResponse;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.RequestOrdreByAddress;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.RequestOrdreByOrderId;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.RequestUserPermissionModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.SuperUserResponse;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.UserActivityModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.model.ResponseSearchProduct;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    @POST("all-comments")
    Call<CommentResponseModel> getAllComment(@Header("appVersion") String appVersion, @Header("language") String pageno);

    // api 7 days customer for phase 2
//    @GET("customer-list")
//    Call<CustomerResponseDto> getCustomerList(@Header("appVersion") String appVersion);

    // api 7 days customer for phase 3
    @GET("customer-list-7days")
    Call<CustomerResponseDto> getCustomerList(@Header("appVersion") String appVersion);

    @GET("receipts-type")
    Call<ReceiptTypeResponse> getReceiptType(@Header("appVersion") String appVersion);

    // api for phase 2
//    @GET("customer-list-all")
//    Call<CustomerResponse> getCustomerListAll(@Header("pageNo") String pageno, @Header("appVersion") String appVersion);

    // api for phase 3
    @GET("customer-list-all-data")
    Call<CustomerResponse> getCustomerListAll(@Header("pageNo") String pageno, @Header("appVersion") String appVersion);

    @GET("order-list")
    Call<OrderApiResponseDto> getOrderList();

    @POST("previous-orders")
    Call<OrderApiResponseDto> getOrderByAddress(@Body RequestOrdreByAddress model);

    @POST("previous-orders-byOrderId")
    Call<OrderApiResponseDto> getPreviousOrderById(@Body RequestOrdreByOrderId model);

    @GET("allOrderList")
    Call<OrderApiResponseDto> getOrderList(@Header("appVersion") String appVersion, @Header("language") String lang, @Header("pageNo") String pageno);

    @GET("totalOrders")
    Call<OrderApiResponseDto> getTotalOrder(@Header("appVersion") String appVersion, @Header("pageNo") String pageno);

    @POST("customer-list-all-data")
    Call<ResponsePojo> getAllCustomerData(@Header("appVersion") String appVersion, @Body List<CustomerUpsyncAddress> customerlist);

    //  old phase 2
    //  @POST("sync-customer")
    //  Call<ResponsePojo> sendCustomerData(@Header("appVersion") String appVersion, @Body List<CustomerUpsyncAddress> customerlist);

    // new phase 3
    @POST("upsync-customers")
    Call<ResponsePojo> sendCustomerData(@Header("appVersion") String appVersion, @Body List<CustomerUpsyncAddress> customerlist);

    @POST("insert-order-new")
    Call<ResponsePojo> sendOrderDataForNew(@Header("appVersion") String appVersion, @Body List<OrderSyncData> orderlist);

    // upsync new order
    @POST("store-order")
    Call<ResponsePojo> upsyncOrderDataForP3(@Header("appVersion") String appVersion, @Body List<OrderSyncData> orderlist);

    // get sub product detail of new order
    @FormUrlEncoded
    @POST("serach-order-products")
    Call<ResponseSearchProduct> getShowOrderData(@Field("appVersion") String appVersion, @Field("order_id") String model);

    @GET("product-list")
    Call<ProductResponseDto> getProdcutList(@Header("appVersion") String appVersion, @Header("language") String lang);

    @POST("firm-list")
    Call<FirmData> getFirmList(@Header("appVersion") String appVersion, @Header("language") String pageno);

    @POST("hindi-address/{pageNo}/")
    Call<OrderImageResponse> getAllHindiAddress(@Path("pageNo") String pageno);

    @GET("sub-cat-list")
    Call<SubCategoryResponse> getSubCategory(@Header("appVersion") String appVersion, @Header("language") String pageno);

    @GET("wholesale-price")
    Call<WholesaleResponse> getWholesalePrice();

    // for passcode
    @POST("select-firm")
    Call<ResponsePojo> sendSelectedFirm(@Body PasscodeRequestModel model);

    // phase 3
    @POST("select-firm-new")
    Call<ResponsePojoFirm> selectedNewFirm(@Body PasscodeRequestModel model);

    @POST("deselect-firm")
    Call<ResponsePojo> sendDeSelectedFirm(@Body PasscodeRequestModel model);

    @POST("isselected-firm")
    Call<ResponsePojoisSelect> sendisSelectedFirm(@Body isFirmSelected model);

    @POST("user-login")
    Call<SuperUserResponse> getUserPermission(@Body RequestUserPermissionModel model);

    @POST("user-activity")
    Call<SuperUserResponse> getUserActivity(@Body UserActivityModel model);

}
