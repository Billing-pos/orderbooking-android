package com.example.sheetalchauhan.orderbookingapplication.databse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.model.OrderIdDate;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Customer;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Order;
import com.example.sheetalchauhan.orderbookingapplication.model.ordersync.OrderSyncData;
import com.example.sheetalchauhan.orderbookingapplication.model.product.SubCagetoryData;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductData;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductSubCategoryData;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.WholesaleDataResponse;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.AlarmModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CommentDataResponseModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CustomerUpsyncAddress;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.HindiAddressModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.OrderImageListData;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.OrderWithImageModel;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class DBAdapter extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    public static String DB_NAME = "Order_Booking1";
    private static int LENGTH_LONG = 400;
    private static DBAdapter mInstance;
    private final Context context;
    public List<String> afterDuplicates = new ArrayList<>();
    List<String> list_phone = new ArrayList<String>();
    private SQLiteDatabase sqlite;


    public DBAdapter(Context context) {
        // TODO Auto-generated constructor stub
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public static DBAdapter getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DBAdapter(context);
        }
        return mInstance;
    }

    public static Cursor selectAlarm(SQLiteDatabase db, String selection) {
        return db.query("AlarmTable", null, selection, null, null, null, null, null);
    }

    public static List<String> removeDuplicates(List<String> list) {
        List<String> newList = new ArrayList<String>();

        for (String element : list) {
            if (!newList.contains(element)) {
                newList.add(element);
            } else {

            }
        }
        return newList;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {

            db.execSQL("CREATE TABLE 'Project'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'clientid' VARCHAR,'projectid' VARCHAR,'projectname' VARCHAR)");
            db.execSQL("CREATE TABLE 'Client'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'clientid' VARCHAR,'name' VARCHAR)");
            db.execSQL("CREATE TABLE 'Items'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'itemname' VARCHAR,'mrpprice' VARCHAR,'sellprice' VARCHAR,'weight' VARCHAR,'qty' VARCHAR,'currentQty' VARCHAR,'date' VARCHAR)");
            db.execSQL("CREATE TABLE 'Customer'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'phone' VARCHAR ,'address' VARCHAR,'area' VARCHAR,'sector' VARCHAR,'apartment' VARCHAR,'block' VARCHAR,'houseno' VARCHAR,'floor' VARCHAR)");

            db.execSQL("CREATE TABLE 'OrderTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'productname' VARCHAR," +
                    "'price' VARCHAR," +
                    "'quantity' VARCHAR," +
                    "'customername' VARCHAR," +
                    "'date' VARCHAR," +
                    "'payAmount' VARCHAR)");

            db.execSQL("CREATE TABLE 'Cart'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'productname'VARCHAR ,'prodcutid' VARCHAR,'price' VARCHAR,'quantity' VARCHAR,'phone' VARCHAR,'address' VARCHAR,'amount' VARCHAR, 'piece' VARCHAR)");
            db.execSQL("CREATE TABLE 'ImagesType'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'projectid' VARCHAR,'name' VARCHAR)");
            db.execSQL("CREATE TABLE 'OrderData'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'projectid' VARCHAR,'latitude' VARCHAR,'longitude' VARCHAR,'width' VARCHAR,'height' VARCHAR,'file' VARCHAR)");
            db.execSQL("CREATE TABLE 'Stock'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'itemname' VARCHAR,'qty' VARCHAR,'date' VARCHAR)");
            db.execSQL("CREATE TABLE 'AfterOrderTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'productname' VARCHAR," +
                    "'productid' VARCHAR," +
                    "'price' VARCHAR," +
                    "'piece' VARCHAR," +
                    "'quantity' VARCHAR," +
                    "'orderid' VARCHAR," +
                    "'flag' VARCHAR," +
                    "'address' VARCHAR," +
                    "'amount' VARCHAR," +
                    "'phone' VARCHAR)");

            db.execSQL("CREATE TABLE 'AfterOrderTableNew'('id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'productname' VARCHAR," +
                    "'productid' VARCHAR," +
                    "'price' VARCHAR," +
                    "'piece' VARCHAR," +
                    "'quantity' VARCHAR," +
                    "'orderid' VARCHAR," +
                    "'flag' VARCHAR," +
                    "'address' VARCHAR," +
                    "'amount' VARCHAR," +
                    "'phone' VARCHAR)");

            db.execSQL("CREATE TABLE 'AddressTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'area' VARCHAR,'sector' VARCHAR,'apartment' VARCHAR,'block' VARCHAR,'houseno' VARCHAR,'floor' VARCHAR,'address' VARCHAR)");

            db.execSQL("CREATE TABLE 'CustomerTable'(" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'orderid' VARCHAR," +
                    "'firm' VARCHAR," +
                    "'mobile' VARCHAR ," +
                    "'mobile2' VARCHAR," +
                    "'mobile3' VARCHAR," +
                    "'mobile4' VARCHAR," +
                    "'mobile5' VARCHAR, " +
                    "'mobile6' VARCHAR, " +
                    "'mobile7' VARCHAR, " +
                    "'mobile8' VARCHAR, " +
                    "'mobile9' VARCHAR, " +
                    "'mobile10' VARCHAR, " +
                    "'contact_name' VARCHAR, " +
                    "'extra' VARCHAR," +
                    "'lift' VARCHAR," +
                    "'kothi_flate' VARCHAR," +
                    "'gali_no' VARCHAR," +
                    "'houseno' VARCHAR," +
                    "'floor' VARCHAR," +
                    "'block' VARCHAR," +
                    "'apartment' VARCHAR," +
                    "'sector' VARCHAR," +
                    "'area' VARCHAR," +
                    "'address' VARCHAR NOT NULL UNIQUE," +
                    "'flag' Varchar," +
                    "'date' Varchar," +
                    "'comments' Varchar," +
                    "'cartage_discount' Varchar," +
                    "'retail_discount' Varchar," +
                    "'tags_new' Varchar," +
                    "'cartage' Varchar," +
                    "'calculation_weight' Varchar," +
                    "'delivery_time' Varchar," +
                    "'type' Varchar," +
                    "'full_new_address' Varchar," +
                    "'tags' Varchar)");

            db.execSQL("CREATE TABLE 'CustomerTableNew'(" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'orderid' VARCHAR," +
                    "'firm' VARCHAR," +
                    "'mobile' VARCHAR ," +
                    "'mobile2' VARCHAR," +
                    "'mobile3' VARCHAR," +
                    "'mobile4' VARCHAR," +
                    "'mobile5' VARCHAR, " +
                    "'mobile6' VARCHAR, " +
                    "'mobile7' VARCHAR, " +
                    "'mobile8' VARCHAR, " +
                    "'mobile9' VARCHAR, " +
                    "'mobile10' VARCHAR, " +
                    "'contact_name' VARCHAR, " +
                    "'extra' VARCHAR," +
                    "'lift' VARCHAR," +
                    "'kothi_flate' VARCHAR," +
                    "'gali_no' VARCHAR," +
                    "'houseno' VARCHAR," +
                    "'floor' VARCHAR," +
                    "'block' VARCHAR," +
                    "'apartment' VARCHAR," +
                    "'sector' VARCHAR," +
                    "'area' VARCHAR," +
                    "'address' VARCHAR NOT NULL UNIQUE," +
                    "'flag' Varchar," +
                    "'date' Varchar," +
                    "'comments' Varchar," +
                    "'cartage_discount' Varchar," +
                    "'retail_discount' Varchar," +
                    "'tags_new' Varchar," +
                    "'cartage' Varchar," +
                    "'calculation_weight' Varchar," +
                    "'delivery_time' Varchar," +
                    "'type' Varchar," +
                    "'full_new_address' Varchar," +
                    "'tags' Varchar)");

            db.execSQL("CREATE TABLE 'ProductTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'productid' VARCHAR,'productname' VARCHAR,'price' ,'wholesale_price' VARCHAR,'subcategory' VARCHAR)");

            db.execSQL("CREATE TABLE 'UserOrder'('id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'receipt_type' VARCHAR," +
                    "'total_amount' VARCHAR," +
                    "'firm_code' VARCHAR," +
                    "'date' VARCHAR," +
                    "'phone' VARCHAR," +
                    "'address' VARCHAR," +
                    "'lock_status' VARCHAR," +
                    "'orderid' VARCHAR," +
                    "'firm' VARCHAR," +
                    "'comment_id' VARCHAR," +
                    "'custom_comment' VARCHAR," +
                    "'mobile2' VARCHAR," +
                    "'invalid' VARCHAR," +
                    "'status' VARCHAR," +
                    "'flag' VARCHAR)");


            db.execSQL("CREATE TABLE 'UserOrderNew'('id' INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "'receipt_type' VARCHAR," +
                    "'total_amount' VARCHAR," +
                    "'firm_code' VARCHAR," +
                    "'date' VARCHAR," +
                    "'phone' VARCHAR," +
                    "'address' VARCHAR," +
                    "'lock_status' VARCHAR," +
                    "'orderid' VARCHAR," +
                    "'firm' VARCHAR," +
                    "'comment_id' VARCHAR," +
                    "'custom_comment' VARCHAR," +
                    "'mobile2' VARCHAR," +
                    "'invalid' VARCHAR," +
                    "'status' VARCHAR," +
                    "'flag' VARCHAR)");

            db.execSQL("CREATE TABLE IF NOT EXISTS 'AlarmTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'orderid' VARCHAR,'date' VARCHAR,'time' VARCHAR,'text' VARCHAR,'flag' VARCHAR, 'alarm_status' VARCHAR)");

            db.execSQL("CREATE TABLE 'FirmTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'firmname' VARCHAR,'firmaddress' VARCHAR,'firmmobile' VARCHAR,'firmcode' VARCHAR, 'firmid', VARCHAR)");

            db.execSQL("CREATE TABLE 'SubcategoryTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'subid' VARCHAR,'subname' VARCHAR)");

            db.execSQL("CREATE TABLE 'ReceiptTypeTable'('s_no' INTEGER PRIMARY KEY AUTOINCREMENT,'id' VARCHAR,'type' VARCHAR)");

            db.execSQL("CREATE TABLE IF NOT EXISTS 'CommentTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'cid' VARCHAR,'title' VARCHAR,'date' VARCHAR)");

            db.execSQL("CREATE TABLE IF NOT EXISTS 'OrderImage'('id'INTEGER PRIMARY KEY AUTOINCREMENT," + "'receipt_no' VARCHAR," + "'image' VARCHAR," + "'firm_id' VARCHAR," + "'amount' VARCHAR," + "'orderDate' VARCHAR," + "'flag' VARCHAR)");

            db.execSQL("CREATE TABLE 'WholesaleProductprice'('id'INTEGER PRIMARY KEY AUTOINCREMENT," + "'customer_address' VARCHAR," + "'product_id' VARCHAR," + "'price' VARCHAR)");

            db.execSQL("CREATE TABLE 'HindiAddressTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'column_name' VARCHAR,'english_name' VARCHAR,'hindi_name' VARCHAR,'flag' VARCHAR)");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("database_version", "old-" + oldVersion + ", -" + newVersion);
        if (newVersion > oldVersion) {
            db.execSQL("CREATE TABLE IF NOT EXISTS 'OrderImage'('id'INTEGER PRIMARY KEY AUTOINCREMENT," + "'receipt_no' VARCHAR," + "'image' VARCHAR," + "'firm_id' VARCHAR," + "'amount' VARCHAR," + "'orderDate' VARCHAR," + "'flag' VARCHAR)");
            db.execSQL("CREATE TABLE IF NOT EXISTS 'CommentTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'cid' VARCHAR,'title' VARCHAR,'date' VARCHAR)");
            db.execSQL("CREATE TABLE IF NOT EXISTS 'AlarmTable'('id' INTEGER PRIMARY KEY AUTOINCREMENT,'orderid' VARCHAR,'date' VARCHAR,'time' VARCHAR,'text' VARCHAR,'flag' VARCHAR)");
//            db.execSQL("ALTER TABLE 'CustomerTable' ADD COLUMN 'date' VARCHAR  AFTER `flag`, comments VARCHAR, ADD COLUMN  cartage VARCHAR, ADD COLUMN  type VARCHAR");
        }

    }

    public List<ProductData> getProductListWholeSale(String address) {
        List<ProductData> list = new ArrayList<ProductData>();
        try {
            String selectQuery = "SELECT  * FROM ProductTable";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    ProductData item = new ProductData();
                    item.setProduct_id(cursor.getString(cursor.getColumnIndex("productid")));
                    item.setProduct_name(cursor.getString(cursor.getColumnIndex("productname")));

                    String price = (!getwholesalePrice(cursor.getString(cursor.getColumnIndex("productid")), address).equals("")) ?
                            getwholesalePrice(cursor.getString(cursor.getColumnIndex("productid")), address) : cursor.getString(cursor.getColumnIndex("price"));

                    item.setPrice(price);
                    item.setWholesale_price(cursor.getString(cursor.getColumnIndex("wholesale_price")));
                    item.setSubcategory(cursor.getString(cursor.getColumnIndex("subcategory")));
                    list.add(item);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getwholesalePrice(String id, String address) {
        String price = "";
        try {
            String selectQuery = "SELECT  * FROM WholesaleProductprice where product_id='" + id + "' and customer_address = '" + address + "'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {

                    price = cursor.getString(cursor.getColumnIndex("price"));

                } while (cursor.moveToNext());

            } else {
                price = "";
            }
            cursor.close();
        } catch (Exception e) {
            price = "";
        }
        return price;
    }

    public synchronized void open() {
        sqlite = getWritableDatabase();
    }

    public synchronized void close() {
        sqlite = getWritableDatabase();
    }

    public String updateMobile(String columnName, String mobile, int id) {
        ContentValues cv = new ContentValues();
        cv.put(columnName, mobile);
        cv.put("flag", "1");

        try {
            String selectQuery = "SELECT  * FROM CustomerTable where id='" + id + "'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    String m1 = cursor.getString(cursor.getColumnIndex("mobile"));
                    String m2 = cursor.getString(cursor.getColumnIndex("mobile2"));
                    String m3 = cursor.getString(cursor.getColumnIndex("mobile3"));
                    String m4 = cursor.getString(cursor.getColumnIndex("mobile4"));
                    String m5 = cursor.getString(cursor.getColumnIndex("mobile5"));
                    String m6 = cursor.getString(cursor.getColumnIndex("mobile6"));
                    String m7 = cursor.getString(cursor.getColumnIndex("mobile7"));
                    String m8 = cursor.getString(cursor.getColumnIndex("mobile8"));
                    String m9 = cursor.getString(cursor.getColumnIndex("mobile9"));
                    String m10 = cursor.getString(cursor.getColumnIndex("mobile10"));

                    String mobile1 = (m1 != null) ? m1 : "";
                    String mobile2 = (m2 != null) ? m2 : "";
                    String mobile3 = (m3 != null) ? m3 : "";
                    String mobile4 = (m4 != null) ? m4 : "";
                    String mobile5 = (m5 != null) ? m5 : "";
                    String mobile6 = (m6 != null) ? m6 : "";
                    String mobile7 = (m7 != null) ? m7 : "";
                    String mobile8 = (m8 != null) ? m8 : "";
                    String mobile9 = (m9 != null) ? m9 : "";
                    String mobile10 = (m10 != null) ? m10 : "";
                    final String msg1 = "mobile already exist";
                    if (mobile1.equals(mobile)
                            || mobile2.equals(mobile)
                            || mobile3.equals(mobile)
                            || mobile4.equals(mobile)
                            || mobile5.equals(mobile)
                            || mobile6.equals(mobile)
                            || mobile7.equals(mobile)
                            || mobile8.equals(mobile)
                            || mobile9.equals(mobile)
                            || mobile10.equals(mobile)) {
                        return msg1;
                    }
                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            return "Something error";
        }

        long rowId = sqlite.update("CustomerTable", cv, "id= '" + id + "'  ", null);
        if (rowId == -1) {
            return "not inserted";
        } else {
            return "success";
        }
    }

    public long updateOrderEditCart(Order order) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("total_amount", "" + order.getTotal_amount());
        contentValues.put("comment_id", "" + order.getComment_id());
        contentValues.put("custom_comment", "" + order.getCustom_comment());
        contentValues.put("mobile2", "" + order.getMobile2());

        long rowId1 = sqlite.update("UserOrder", contentValues, "orderid = '" + order.getOrderId() + "' ", null);
        if (rowId1 == -1) {
            Toast.makeText(context, R.string.edit_order_not_update, Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(context, R.string.edit_order_update, Toast.LENGTH_LONG).show();
        }
        return rowId1;
    }

    public long cancelOrder(String orderId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("invalid", "2");

        long rowId1 = sqlite.update("UserOrder", contentValues, "orderid = '" + orderId + "' ", null);
        if (rowId1 == -1) {
            Toast.makeText(context, R.string.order_has_not_cancelled, Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(context, R.string.order_has_cancelled, Toast.LENGTH_LONG).show();
        }
        return rowId1;
    }

    public long insertOrderNew(Order order) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1 = new Date();
        String date = (!order.getDate().equals("")) ? order.getDate() : dateFormat.format(date1);
        long rowId = -0;
        String sql = "Select * from UserOrderNew where orderid = '" + order.getOrderId() + "' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("date", date != null ? date : "");
                contentValues.put("phone", order.getPhone() != null ? order.getPhone() : "");
                contentValues.put("address", order.getAddress() != null ? order.getAddress() : "");
                contentValues.put("lock_status", order.getLock_status() != null ? order.getLock_status() : "");
                contentValues.put("orderid", order.getOrderId() != null ? order.getOrderId() : "");
                contentValues.put("flag", order.getFlag() != null ? order.getFlag() : "");
                contentValues.put("firm", order.getFirm() != null ? order.getFirm() : "");
                contentValues.put("comment_id", order.getComment_id() != null ? order.getComment_id() : "");
                contentValues.put("custom_comment", order.getCustom_comment() != null ? order.getCustom_comment() : "");
                contentValues.put("mobile2", order.getMobile2() != null ? order.getMobile2() : "");
                contentValues.put("total_amount", order.getTotal_amount() != null ? order.getTotal_amount() : "");
                contentValues.put("invalid", order.getInvalid() != null ? order.getInvalid() : "");
                contentValues.put("receipt_type", order.getReceipt_type() != null ? order.getReceipt_type() : "");

                rowId = sqlite.insert("UserOrderNew", null, contentValues);
                if (rowId == -1) {
                    Toast.makeText(context, "Data is not Successfully Saved", Toast.LENGTH_LONG).show();
                    Log.e("order_inserted", "no");
                } else {
                    Log.e("order_inserted", "yesssssss");
                }
                Log.e("order_inserted", "" + rowId + "yesssssss_ifblockkkkkkk = " + order.getOrderId() + ", " + order.getLock_status() + ",-" + order.getTotal_amount());

            } else {
                Log.e("order_inserted", "exists" + "orderid = '" + order.getOrderId() + "lock = '" + order.getLock_status());

                ContentValues contentValues = new ContentValues();
                contentValues.put("lock_status", "" + order.getLock_status());
                contentValues.put("invalid", "" + order.getInvalid());

                rowId = sqlite.update("UserOrderNew", contentValues, "orderid = '" + order.getOrderId() + "' ", null);

                if (rowId == -1) {
                    Toast.makeText(context, "OrderId : " + order.getOrderId() + " is not updated ", Toast.LENGTH_LONG).show();
                } else {
                    rowId = -4;
                    Log.e("order_inserted", "no_3434no_noono45");
                }
            }
        } finally {
            Log.e("order_inserted", "catch_catvhhhh");

            if (null != cursor)
                cursor.close();
        }
        return rowId;
    }

    public long insertOrder(Order order) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1 = new Date();
        String date = (!order.getDate().equals("")) ? order.getDate() : dateFormat.format(date1);
        long rowId = -0;
        String sql = "Select * from UserOrder where orderid = '" + order.getOrderId() + "' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("date", "" + date);
                contentValues.put("phone", "" + order.getPhone());
                contentValues.put("address", "" + order.getAddress());
                contentValues.put("lock_status", "" + order.getLock_status());
                contentValues.put("orderid", "" + order.getOrderId());
                contentValues.put("flag", "" + order.getFlag());
                contentValues.put("firm", "" + order.getFirm());
                contentValues.put("comment_id", "" + order.getComment_id());
                contentValues.put("custom_comment", "" + order.getCustom_comment());
                contentValues.put("mobile2", "" + order.getMobile2());
                contentValues.put("total_amount", "" + order.getTotal_amount());
                contentValues.put("invalid", "" + order.getInvalid());
                contentValues.put("receipt_type", "" + order.getReceipt_type());
                contentValues.put("status", "" + order.getStatus());

                rowId = sqlite.insert("UserOrder", null, contentValues);
                if (rowId == -1) {
                    Toast.makeText(context, "Data is not Successfully Saved", Toast.LENGTH_LONG).show();
                    Log.e("order_inserted", "no");
                } else {
                    Log.e("order_inserted", "yes");
                }

            } else {
                Log.e("order_inserted", "exists" + "orderid = '" + order.getOrderId() + "lock = '" + order.getLock_status());

                ContentValues contentValues = new ContentValues();
                contentValues.put("lock_status", "" + order.getLock_status());
                contentValues.put("invalid", "" + order.getInvalid());

                sqlite.update("UserOrder", contentValues, "orderid = '" + order.getOrderId() + "' ", null);

                if (rowId == -1) {
                    Toast.makeText(context, "OrderId : " + order.getOrderId() + " lock staus is not updated ", Toast.LENGTH_LONG).show();
                }

            }
        } finally {
            if (null != cursor)
                cursor.close();
        }
        return rowId;
    }

    public boolean isOrderExist(String OrderId) {
        long rowId = -0;
        String sql = "Select * from UserOrder where orderid = '" + OrderId + "' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {
                //not exist.
                return true;
            } else {
                // orderid exist
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    public long insertSubCategory(ProductSubCategoryData subCagetoryData) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("subid", subCagetoryData.getSubcatgeory_id());
        contentValues.put("subname", subCagetoryData.getSubcategory_name());
        long rowId = sqlite.insert("SubcategoryTable", null, contentValues);
        return rowId;
    }

    public long insertComment(CommentDataResponseModel model) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("cid", "" + model.getId());
        contentValues.put("title", "" + model.getTitle());
        contentValues.put("date", "" + model.getCreated());
        long rowId = sqlite.insert("CommentTable", null, contentValues);
        return rowId;
    }

    public long insertReceiptType(String recieptId, String receiptType) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", "" + recieptId);
        contentValues.put("type", "" + receiptType);
        long rowId = sqlite.insert("ReceiptTypeTable", null, contentValues);
        return rowId;
    }

    public List<String> getComment() {
        List<String> list = new ArrayList<String>();
        try {
            String selectQuery = "SELECT  * FROM CommentTable ORDER BY id ASC ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(cursor.getColumnIndex("title")));

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<String> getCommentId() {
        List<String> list = new ArrayList<String>();
        try {
            String selectQuery = "SELECT  * FROM CommentTable ORDER BY id ASC ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
//                    CommentDataResponseModel model = new CommentDataResponseModel();
//                    model.setId(cursor.getString(cursor.getColumnIndex("cid")));
//                    model.setCreated(cursor.getString(cursor.getColumnIndex("date")));
//                    model.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                    list.add(cursor.getString(cursor.getColumnIndex("cid")));

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public long insertAlarmDatabase(String orderId, String date, String time, String text) {
        long rowId = -1;
        String sql = "Select * from AlarmTable where orderid = '" + orderId + "' ";
        Cursor cursor = null;
        try {

            Log.i("insertAlarm", "try block ");
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {

                Log.i("insertAlarm", "if called");
                ContentValues cv = new ContentValues();
                cv.put("orderid", "" + orderId);
                cv.put("date", "" + date);
                cv.put("time", "" + time);
                cv.put("text", "" + text);
                cv.put("flag", "1");
                cv.put("alarm_status", "0");

                rowId = sqlite.insert("AlarmTable", null, cv);
                Log.i("insertAlarm", "if returing value long=" + rowId);
            } else {
                String alarmId = null;
                if (cursor.moveToFirst()) {
                    do {
                        alarmId = cursor.getString(cursor.getColumnIndex("id"));
                    } while (cursor.moveToNext());
                }
                cursor.close();

                Log.i("insertAlarm", "else block called");
                Log.i("insertAlarm", "existing id=" + alarmId);
                ContentValues cv = new ContentValues();
                cv.put("date", "" + date);
                cv.put("time", "" + time);
                cv.put("text", "" + text);
                cv.put("flag", "1");
                cv.put("alarm_status", "0");

                rowId = sqlite.update("AlarmTable", cv, "orderid= '" + orderId + "'  ", null);

                if (alarmId != null) {
                    rowId = Long.parseLong(alarmId);
                }
                Log.i("insertAlarm", "returing value long=" + rowId);

            }
        } catch (Exception e) {
            Log.i("catch", "called");
        }
        return rowId;
    }

    public Long deleteAlarmData(String id) {
        long rowId = sqlite.delete("AlarmTable", "id= '" + id + "'  ", null);
        if (rowId == -1)
            Toast.makeText(context, "Alarm is not deleted", Toast.LENGTH_LONG).show();

        return rowId;
    }

    public void insertAfterOrderCreate(Cart cart) {
        Cursor cursor = null;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("productname", cart.getProductname());
            contentValues.put("productid", cart.getProductid());
            contentValues.put("price", cart.getPrice());
            contentValues.put("piece", cart.getPiece());
            contentValues.put("quantity", cart.getQuantity().replace("-", "."));
            contentValues.put("orderid", cart.getOrderid());
            contentValues.put("address", cart.getAddress());
            contentValues.put("phone", cart.getPhone());
            contentValues.put("amount", cart.getAmount());
            contentValues.put("flag", cart.getFlag());

            long rowId = sqlite.insert("AfterOrderTable", null, contentValues);
            if (rowId == -1) {
                Toast.makeText(context, "Data is not Successfully Saved", Toast.LENGTH_LONG).show();
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    public void insertAfterOrderCreateNew(Cart cart) {
        Cursor cursor = null;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("productname", cart.getProductname());
            contentValues.put("productid", cart.getProductid());
            contentValues.put("price", cart.getPrice());
            contentValues.put("piece", cart.getPiece());
            contentValues.put("quantity", cart.getQuantity().replace("-", "."));
            contentValues.put("orderid", cart.getOrderid());
            contentValues.put("address", cart.getAddress());
            contentValues.put("phone", cart.getPhone());
            contentValues.put("amount", cart.getAmount());
            contentValues.put("flag", cart.getFlag());

            long rowId = sqlite.insert("AfterOrderTableNew", null, contentValues);
            if (rowId == -1) {
                Toast.makeText(context, "Data is not Successfully Saved", Toast.LENGTH_LONG).show();
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    public Long deleteAfterOrder(String orderId) {
        long rowId = sqlite.delete("AfterOrderTable", "orderid= '" + orderId + "'  ", null);
        if (rowId == -1)
            Toast.makeText(context, "Item is not deleted", Toast.LENGTH_LONG).show();
        return rowId;
    }

    public void insertWholeSalePrice(WholesaleDataResponse wholesaleDataResponse) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("product_id", wholesaleDataResponse.getProduct_id());
        contentValues.put("customer_address", wholesaleDataResponse.getCustomer_address().toLowerCase());
        contentValues.put("price", wholesaleDataResponse.getPrice());

        long rowid = sqlite.insert("WholesaleProductprice", null, contentValues);
        if (rowid == -1) {
            Toast.makeText(context, "WholeSaleProduct not Successfully Saved", Toast.LENGTH_LONG).show();

        }

    }

    public Long deleteUserOrder() {
        long rowId = -1;
        Cursor cursor = null;
        try {
            String count = "SELECT count(*) FROM UserOrder";
            Cursor mcursor = sqlite.rawQuery(count, null);
            mcursor.moveToFirst();
            int icount = mcursor.getInt(0);
            if (icount > 0) {
            } else {
                rowId = sqlite.delete("UserOrder", "flag = 0 ", null);
                if (rowId == -1)
                    Toast.makeText(context, "Item is not deleted", Toast.LENGTH_LONG).show();
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }

    public long insertProduct(ProductData productData) {
        long rowId = -1;
        Cursor cursor = null;
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("productid", productData.getProduct_id());
            contentValues.put("price", productData.getPrice());
            contentValues.put("productname", productData.getProduct_name());
            contentValues.put("subcategory", productData.getSubcategory());
            contentValues.put("wholesale_price", productData.getWholesale_price());

            rowId = sqlite.insert("ProductTable", null, contentValues);
            if (rowId == -1)
                Toast.makeText(context, "Data is not Successfully Saved", Toast.LENGTH_LONG).show();

        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }

    public long updateAlarm(String id, String value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("alarm_status", value);

        long rowId = sqlite.update("AlarmTable", contentValues, "orderid= '" + id + "'  ", null);

        if (rowId == -1) {
//            Toast.makeText(context, "UserOrder is not update Successfully", Toast.LENGTH_LONG).show();

        } else {
            if (value.equalsIgnoreCase("2"))
                Toast.makeText(context, context.getResources().getString(R.string.alarm_is_stopped), Toast.LENGTH_LONG).show();
        }

        return rowId;
    }

    public void updateOrderImage() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("flag", "0");
        long rowId = sqlite.update("OrderImage", contentValues, "", null);

    }

    public void updateCartBatch(List<Order> orderList) {
        for (Order order : orderList) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("flag", "0");
            long rowId1 = sqlite.update("UserOrder", contentValues, "orderid = '" + order.getOrderId() + "' ", null);

            if (rowId1 == -1) {
                Toast.makeText(context, "OrderId : " + order.getOrderId() + " is not updated ", Toast.LENGTH_LONG).show();

            }
        }
    }

    public void deleteOrder(List<Order> orderList) {
        for (Order order : orderList) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("flag", "0");

            long rowId = sqlite.delete("UserOrder", "orderid= '" + order.getOrderId() + "'  ", null);
            if (rowId == -1)
                Toast.makeText(context, "Order is not deleted", Toast.LENGTH_LONG).show();
        }
    }

    public void deleteSubOrder(List<OrderSyncData> orderList) {
        for (int i = 0; i < orderList.size(); i++) {
            for (int j = 0; j < orderList.get(i).getOrderProductSyncList().size(); j++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("flag", "0");
                long rowId = sqlite.delete("AfterOrderTable", "orderid= '" + orderList.get(i).getOrderid() + "'  ", null);
            }
        }
    }

    public void updateSub(List<OrderSyncData> orderList) {
        for (int i = 0; i < orderList.size(); i++) {
            for (int j = 0; j < orderList.get(i).getOrderProductSyncList().size(); j++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("flag", "0");
                long rowId1 = sqlite.update("AfterOrderTable", contentValues, "orderid = '" + orderList.get(i).getOrderid() + "' ", null);
            }
        }
    }

    public void updateCustomerBatch(List<CustomerUpsyncAddress> customerAddressesList) {
        for (CustomerUpsyncAddress address : customerAddressesList) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("flag", "0");
            long rowId1 = sqlite.update("CustomerTable", contentValues, "address = '" + address.getAddress() + "' ", null);

            if (rowId1 == -1) {
                Toast.makeText(context, "Address : " + address.getAddress() + " is not updated ", Toast.LENGTH_LONG).show();
            }
        }
    }

    public Long removeCartItem(int tableId) {
        ContentValues contentValues = new ContentValues();

        long rowId = sqlite.delete("Cart", "id= '" + tableId + "'  ", null);
        if (rowId == -1)
            Toast.makeText(context, "Item is not deleted", Toast.LENGTH_LONG).show();

        return rowId;
    }

    public Long insertCart(Cart cart) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("productname", cart.getProductname());
        contentValues.put("prodcutid", cart.getProductid());
        contentValues.put("price", cart.getPrice());
        contentValues.put("quantity", cart.getQuantity());
        contentValues.put("phone", cart.getPhone());
        contentValues.put("address", cart.getAddress());
        contentValues.put("amount", cart.getAmount());
        contentValues.put("piece", "" + cart.getPiece());

        long rowId = sqlite.insert("Cart", null, contentValues);
        if (rowId == -1)
            Toast.makeText(context, "Data is not Successfully Saved", Toast.LENGTH_LONG).show();
        return rowId;
    }

    public Long insertFirmData(Firm firm) {
        long rowId = -1;
        String sql = "Select * from FirmTable where firmcode = '" + firm.getFirmcode() + "' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("firmname", firm.getName());
                contentValues.put("firmaddress", firm.getAddress());
                contentValues.put("firmmobile", firm.getMobile());
                contentValues.put("firmcode", firm.getFirmcode());
                contentValues.put("firmid", "" + firm.getId());
                rowId = sqlite.insert("FirmTable", null, contentValues);

            } else {
                ContentValues contentValues = new ContentValues();
                contentValues.put("firmname", firm.getName());
                contentValues.put("firmaddress", firm.getAddress());
                rowId = sqlite.update("FirmTable", contentValues, "firmcode = '" + firm.getFirmcode() + "' ", null);

            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }

    //..................From Cartlistr
    public Long insertCustomerDatatest(CustomergetAddress customer) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("orderid", customer.getOrderid());
        contentValues.put("firm", customer.getFirm());
        contentValues.put("mobile", customer.getmobile());
        contentValues.put("mobile2", customer.getmobile2());
        contentValues.put("mobile3", customer.getmobile3());
        contentValues.put("mobile4", customer.getmobile4());
        contentValues.put("mobile5", customer.getmobile5());
        contentValues.put("contact_name", customer.getContact_name());
        contentValues.put("houseno", customer.getHouseno());
        contentValues.put("floor", customer.getFloor());
        contentValues.put("block", customer.getBlock());
        contentValues.put("apartment", customer.getApartment());
        contentValues.put("sector", customer.getSector());
        contentValues.put("area", customer.getArea());
        contentValues.put("address", customer.getAddress());

        contentValues.put("kothi_flate", customer.getKothi_flate());
        contentValues.put("gali_no", customer.getGali_no());
        contentValues.put("extra", customer.getExtra());
        contentValues.put("lift", customer.getLift());

        contentValues.put("cartage", "0");
        contentValues.put("flag", "1");
        contentValues.put("tags", customer.getTags());
        contentValues.put("type", customer.getType());
        contentValues.put("calculation_weight", customer.getCalculation_weight());

        contentValues.put("full_new_address", customer.getAddress() + "," + customer.getKothi_flate() + "," + customer.getGali_no() + "," + customer.getLift() + "," + customer.getExtra());


        long rowId = sqlite.insert("CustomerTable", null, contentValues);
        return rowId;
    }

    //..................DELETE User Phone there is multiple phone on this ADDRESS .....................................//
    public Long removeUserPhone(String columnName, int tableId) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(columnName, "");
        contentValues.put("flag", "1");

        long rowId = sqlite.update("CustomerTable", contentValues, "id= '" + tableId + "'  ", null);
        return rowId;
    }

    //.................. updating existing address of user with phone .............................//
    public Long updateExistingAddress(String id, String column, String phone) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(column, phone);
        contentValues.put("flag", "1");
        long rowId = sqlite.update("CustomerTable", contentValues, "id= '" + id + "'  ", null);

        if (rowId == -1) {
            Toast.makeText(context, "Address is not Updated !", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(context, "Address is Updated !", Toast.LENGTH_LONG).show();
        }

        return rowId;

    }

    public Long singleMobileUpdateAddress1(int id, String address) {
        String area2 = null, sector = null, apartment = null, block = null, houseno = null, floor = null, phone = null,
                firmname = null, strAddress = null, add[] = null, add2[] = null;

        add = address.split(",");
        add2 = add[3].split("/");

        area2 = add[0];
        sector = add[1];
        apartment = add[2];
        block = add2[0];
        houseno = add2[1];
        floor = add[4];

        strAddress = area2 + sector + apartment + block + houseno + floor;

        ContentValues contentValues = new ContentValues();
        contentValues.put("houseno", houseno);
        contentValues.put("floor", floor);
        contentValues.put("block", block);
        contentValues.put("apartment", apartment);
        contentValues.put("sector", sector);
        contentValues.put("area", area2);
        contentValues.put("address", address);
        contentValues.put("flag", "1");

        long rowId = sqlite.update("CustomerTable", contentValues, "id= '" + id + "'  ", null);

        return rowId;
    }

    public void updateCustomerTable() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("flag", "0");
        long rowId = sqlite.update("CustomerTable", contentValues, "", null);

    }

    // get all tag
    public List<CustomergetAddress> getAllCustomerWithTag(String phone) {
        List<CustomergetAddress> list = new ArrayList<CustomergetAddress>();
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where tags='" + phone + "' ";
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    CustomergetAddress customer = new CustomergetAddress();
                    customer.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    customer.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
                    customer.setTags(cursor.getString(cursor.getColumnIndex("tags")));

                    list.add(customer);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return list;
    }

    // dummy get all customer id
    public List<String> getallCustomerId() {
        List<String> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable ";
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add("" + cursor.getInt(cursor.getColumnIndex("id")));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return list;
    }

    // checking existing numbere and adress
    public List<CustomergetAddress> getExistingNumber(String mobile, String address) {
        List<CustomergetAddress> list = new ArrayList<CustomergetAddress>();
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where address='" + address + "' ";
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    CustomergetAddress customer = new CustomergetAddress();
                    customer.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));

                    customer.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
                    customer.setMobile2(cursor.getString(cursor.getColumnIndex("mobile2")));
                    customer.setMobile3(cursor.getString(cursor.getColumnIndex("mobile3")));
                    customer.setMobile4(cursor.getString(cursor.getColumnIndex("mobile4")));
                    customer.setMobile5(cursor.getString(cursor.getColumnIndex("mobile5")));

                    list.add(customer);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return list;
    }

    //for new customer table
    public Long insertCustomerData2(String id, String column, String phone, String firm, String
            orderid) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(column, phone);
        contentValues.put("firm", firm);
        contentValues.put("orderid", orderid);
        contentValues.put("flag", "1");
        long rowId = sqlite.update("CustomerTable", contentValues, "id= '" + id + "'  ", null);

        if (rowId == -1) {
            Toast.makeText(context, R.string.customer_table_not_update, Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(context, R.string.customer_table_update, Toast.LENGTH_LONG).show();
        }

        return rowId;

    }

    //for customer mobile update
    public Long updateCustomerMobile(String id, String column, String phone, String
            firm, String orderid) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(column, phone);
        contentValues.put("firm", firm);
        contentValues.put("orderid", orderid);
        contentValues.put("flag", "1");
        long rowId = sqlite.update("CustomerTable", contentValues, "id= '" + id + "'  ", null);

        if (rowId == -1) {
//            Toast.makeText(context, "CustomerTable is not update Successfully", Toast.LENGTH_LONG).show();

        } else {
//            Toast.makeText(context, "CustomerTable is updated", Toast.LENGTH_LONG).show();
        }
        return rowId;
    }

    public void insertCustomerData1(CustomergetAddress customer) {
        long rowId = -1;
        String sql = "Select * from CustomerTableNew where address = '" + customer.getAddress() + "' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("id", customer.getId());
                contentValues.put("orderid", customer.getOrderid());
                contentValues.put("firm", customer.getFirm());
                contentValues.put("mobile", customer.getmobile());
                contentValues.put("mobile2", customer.getmobile2());
                contentValues.put("mobile3", customer.getmobile3());
                contentValues.put("mobile4", customer.getmobile4());
                contentValues.put("mobile5", customer.getmobile5());
                contentValues.put("mobile6", customer.getMobile6());
                contentValues.put("mobile7", customer.getMobile7());
                contentValues.put("mobile8", customer.getMobile8());
                contentValues.put("mobile9", customer.getMobile9());
                contentValues.put("mobile10", customer.getMobile10());
                contentValues.put("contact_name", "" + customer.getContact_name());
                contentValues.put("houseno", customer.getHouseno());
                contentValues.put("floor", customer.getFloor());
                contentValues.put("block", customer.getBlock());
                contentValues.put("apartment", customer.getApartment());
                contentValues.put("sector", customer.getSector());
                contentValues.put("area", customer.getArea());
                contentValues.put("address", customer.getAddress());
                contentValues.put("flag", customer.getFlag());
                contentValues.put("tags", customer.getTags());
                contentValues.put("comments", customer.getComments());
                contentValues.put("cartage", customer.getCartage());
                contentValues.put("calculation_weight", customer.getCalculation_weight());
                contentValues.put("delivery_time", customer.getDelivery_time());
                contentValues.put("retail_discount", customer.getRetail_discount());
                contentValues.put("cartage_discount", customer.getCartage_discount());
                contentValues.put("type", customer.getType());
                contentValues.put("tags_new", customer.getTags_new());
                contentValues.put("full_new_address", customer.getAddress() + "," + customer.getKothi_flate() + "," + customer.getGali_no() + "," + customer.getLift() + "," + customer.getExtra());

                rowId = sqlite.insert("CustomerTableNew", null, contentValues);
                if (rowId == -1) {
                    Log.i("user_update_insertnot", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

                } else {
                    Log.i("user_update_insert", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

                }
            } else {
                ContentValues contentValues = new ContentValues();
                contentValues.put("id", customer.getId());
                contentValues.put("orderid", customer.getOrderid());
                contentValues.put("firm", customer.getFirm());
                contentValues.put("mobile", customer.getmobile());
                contentValues.put("mobile2", customer.getmobile2());
                contentValues.put("mobile3", customer.getmobile3());
                contentValues.put("mobile4", customer.getmobile4());
                contentValues.put("mobile5", customer.getmobile5());
                contentValues.put("contact_name", "" + customer.getContact_name());
                contentValues.put("houseno", customer.getHouseno());
                contentValues.put("floor", customer.getFloor());
                contentValues.put("block", customer.getBlock());
                contentValues.put("apartment", customer.getApartment());
                contentValues.put("sector", customer.getSector());
                contentValues.put("area", customer.getArea());
                contentValues.put("address", customer.getAddress());
                contentValues.put("flag", customer.getFlag());
                contentValues.put("tags", customer.getTags());
                contentValues.put("comments", customer.getComments());
                contentValues.put("cartage", customer.getCartage());
                contentValues.put("retail_discount", customer.getRetail_discount());
                contentValues.put("cartage_discount", customer.getCartage_discount());
                contentValues.put("type", customer.getType());
                contentValues.put("tags_new", customer.getTags_new());
                contentValues.put("full_new_address", customer.getAddress() + "," + customer.getKothi_flate() + "," + customer.getGali_no() + "," + customer.getLift() + "," + customer.getExtra());

                rowId = sqlite.update("CustomerTableNew", contentValues, "address= '" + customer.getAddress() + "'", null);
                if (rowId == -1) {
                    Log.i("user_updated__exist_not", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

                } else {
                    Log.i("user_updated__exists", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

                }
            }
        } catch (Exception e) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", customer.getId());
            contentValues.put("orderid", customer.getOrderid());
            contentValues.put("firm", customer.getFirm());
            contentValues.put("mobile", customer.getmobile());
            contentValues.put("mobile2", customer.getmobile2());
            contentValues.put("mobile3", customer.getmobile3());
            contentValues.put("mobile4", customer.getmobile4());
            contentValues.put("mobile5", customer.getmobile5());
            contentValues.put("contact_name", "" + customer.getContact_name());
            contentValues.put("houseno", customer.getHouseno());
            contentValues.put("floor", customer.getFloor());
            contentValues.put("block", customer.getBlock());
            contentValues.put("apartment", customer.getApartment());
            contentValues.put("sector", customer.getSector());
            contentValues.put("area", customer.getArea());
            contentValues.put("address", customer.getAddress());
            contentValues.put("flag", customer.getFlag());
            contentValues.put("tags", customer.getTags());
            contentValues.put("comments", customer.getComments());
            contentValues.put("cartage", customer.getCartage());
            contentValues.put("retail_discount", customer.getRetail_discount());
            contentValues.put("cartage_discount", customer.getCartage_discount());
            contentValues.put("type", customer.getType());
            rowId = sqlite.insert("CustomerTableNew", null, contentValues);
            if (rowId == -1) {
                Log.i("user_update_catchnot", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

            } else {
                Log.i("user_update_catch", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

            }
        }
        if (cursor != null)
            cursor.close();
    }

    public void insertCustomerIntoOldTable(CustomergetAddress customer) {
        long rowId = -1;
        String sql = "Select * from CustomerTable where address = '" + customer.getAddress() + "' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("id", customer.getId());
                contentValues.put("orderid", customer.getOrderid());
                contentValues.put("firm", customer.getFirm());
                contentValues.put("mobile", customer.getmobile());
                contentValues.put("mobile2", customer.getmobile2());
                contentValues.put("mobile3", customer.getmobile3());
                contentValues.put("mobile4", customer.getmobile4());
                contentValues.put("mobile5", customer.getmobile5());
                contentValues.put("mobile6", customer.getMobile6());
                contentValues.put("mobile7", customer.getMobile7());
                contentValues.put("mobile8", customer.getMobile8());
                contentValues.put("mobile9", customer.getMobile9());
                contentValues.put("mobile10", customer.getMobile10());
                contentValues.put("contact_name", "" + customer.getContact_name());
                contentValues.put("houseno", customer.getHouseno());
                contentValues.put("floor", customer.getFloor());
                contentValues.put("block", customer.getBlock());
                contentValues.put("apartment", customer.getApartment());
                contentValues.put("sector", customer.getSector());
                contentValues.put("area", customer.getArea());
                contentValues.put("address", customer.getAddress());
                contentValues.put("flag", customer.getFlag());
                contentValues.put("tags", customer.getTags());
                contentValues.put("comments", customer.getComments());
                contentValues.put("cartage", customer.getCartage());
                contentValues.put("retail_discount", customer.getRetail_discount());
                contentValues.put("cartage_discount", customer.getCartage_discount());
                contentValues.put("calculation_weight", customer.getCalculation_weight());
                contentValues.put("type", customer.getType());
                contentValues.put("tags_new", customer.getTags_new());
                contentValues.put("delivery_time", customer.getDelivery_time());

                contentValues.put("kothi_flate", customer.getKothi_flate());
                contentValues.put("gali_no", customer.getGali_no());
                contentValues.put("extra", customer.getExtra());
                contentValues.put("lift", customer.getLift());
                contentValues.put("full_new_address", customer.getAddress() + "," + customer.getKothi_flate() + "," + customer.getGali_no() + "," + customer.getLift() + "," + customer.getExtra());

                rowId = sqlite.insert("CustomerTable", null, contentValues);
                if (rowId == -1) {
                    Log.i("user_notcustomer", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

                } else {
                    Log.i("user_customer", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

                }
            } else {
                ContentValues contentValues = new ContentValues();
                contentValues.put("id", customer.getId());
                contentValues.put("orderid", customer.getOrderid());
                contentValues.put("firm", customer.getFirm());
                contentValues.put("mobile", customer.getmobile());
                contentValues.put("mobile2", customer.getmobile2());
                contentValues.put("mobile3", customer.getmobile3());
                contentValues.put("mobile4", customer.getmobile4());
                contentValues.put("mobile5", customer.getmobile5());
                contentValues.put("contact_name", "" + customer.getContact_name());
                contentValues.put("houseno", customer.getHouseno());
                contentValues.put("floor", customer.getFloor());
                contentValues.put("block", customer.getBlock());
                contentValues.put("apartment", customer.getApartment());
                contentValues.put("sector", customer.getSector());
                contentValues.put("area", customer.getArea());
                contentValues.put("address", customer.getAddress());
                contentValues.put("flag", customer.getFlag());
                contentValues.put("tags", customer.getTags());
                contentValues.put("comments", customer.getComments());
                contentValues.put("cartage", customer.getCartage());
                contentValues.put("retail_discount", customer.getRetail_discount());
                contentValues.put("cartage_discount", customer.getCartage_discount());
                contentValues.put("type", customer.getType());
                contentValues.put("tags_new", customer.getTags_new());
                rowId = sqlite.update("CustomerTable", contentValues, "address= '" + customer.getAddress() + "'", null);
                if (rowId == -1) {
                    Log.i("user_notupdated__exists", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

                } else {
                    Log.i("user_updated__exists", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

                }
            }
        } catch (Exception e) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", customer.getId());
            contentValues.put("orderid", customer.getOrderid());
            contentValues.put("firm", customer.getFirm());
            contentValues.put("mobile", customer.getmobile());
            contentValues.put("mobile2", customer.getmobile2());
            contentValues.put("mobile3", customer.getmobile3());
            contentValues.put("mobile4", customer.getmobile4());
            contentValues.put("mobile5", customer.getmobile5());
            contentValues.put("contact_name", "" + customer.getContact_name());
            contentValues.put("houseno", customer.getHouseno());
            contentValues.put("floor", customer.getFloor());
            contentValues.put("block", customer.getBlock());
            contentValues.put("apartment", customer.getApartment());
            contentValues.put("sector", customer.getSector());
            contentValues.put("area", customer.getArea());
            contentValues.put("address", customer.getAddress());
            contentValues.put("flag", customer.getFlag());
            contentValues.put("tags", customer.getTags());
            contentValues.put("comments", customer.getComments());
            contentValues.put("cartage", customer.getCartage());
            contentValues.put("retail_discount", customer.getRetail_discount());
            contentValues.put("cartage_discount", customer.getCartage_discount());
            contentValues.put("type", customer.getType());
            rowId = sqlite.insert("CustomerTable", null, contentValues);
            if (rowId == -1) {
                Log.i("user_notcustomer_catch", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

            } else {
                Log.i("user_customer_catch", "**" + customer.getId() + ", o- " + customer.getOrderid() + ", f- " + customer.getFirm() + ", m1- " + customer.getmobile() + ",m2- " + customer.getmobile2() + ", name- " + customer.getContact_name() + ",tag- " + customer.getTags() + " - " + customer.getAddress());

            }
        }
        if (cursor != null)
            cursor.close();
    }

    public List<Cart> getAllCartItem(String address) {
        List<Cart> list = new ArrayList<Cart>();
        // Select All Query
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT * FROM Cart where address='" + address + "'";
            cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Cart cart = new Cart();
                    cart.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    cart.setProductname(cursor.getString(cursor.getColumnIndex("productname")));
                    cart.setProductid(cursor.getString(cursor.getColumnIndex("prodcutid")));
                    cart.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    cart.setQuantity(cursor.getString(cursor.getColumnIndex("quantity")));
                    cart.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                    cart.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    cart.setAmount(cursor.getString(cursor.getColumnIndex("amount")));
                    cart.setPiece("" + cursor.getString(cursor.getColumnIndex("piece")));

                    list.add(cart);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<Cart> getAllUserOrder(String address) {
        List<Cart> list = new ArrayList<Cart>();
        // Select All Query
        try {
            String selectQuery = "SELECT * FROM UserOrder where address='" + address + "'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Cart cart = new Cart();
                    cart.setOrderid(cursor.getString(cursor.getColumnIndex("orderid")));

                    list.add(cart);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public long updateCustomerLastOrder(String address, String contact_name, String orderid, String firmcode, String tags_new, String total_amount) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("orderid", orderid);
        contentValues.put("firm", firmcode);
        contentValues.put("contact_name", contact_name);
        contentValues.put("flag", "1");
        contentValues.put("tags_new", tags_new);
//        contentValues.put("total_amount", total_amount);
        //long rowId = sqlite.update("CustomerTable", contentValues, "address= '" + address + "' and mobile='" + phone + "'", null);
        long rowId = sqlite.update("CustomerTable", contentValues, "address= '" + address + "'", null);

        if (rowId == -1) {
            Toast.makeText(context, R.string.customer_table_not_update, Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(context, R.string.customer_table_update, Toast.LENGTH_LONG).show();

        }
        return rowId;
    }

    public long updateCustomerLastOrderForEdit(String address, String contact_name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("contact_name", contact_name);
        contentValues.put("flag", "1");
        //long rowId = sqlite.update("CustomerTable", contentValues, "address= '" + address + "' and mobile='" + phone + "'", null);
        long rowId = sqlite.update("CustomerTable", contentValues, "address= '" + address + "'", null);

        if (rowId == -1) {
            Toast.makeText(context, "CustomerTable is not update Successfully", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(context, R.string.customer_table_update, Toast.LENGTH_LONG).show();

        }
        return rowId;
    }

    public List<Cart> getOrderProduct(String orderid) {
        List<Cart> list = new ArrayList<Cart>();
        try {
            String selectQuery = "SELECT  * FROM AfterOrderTable where orderid='" + orderid + "'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Cart cart = new Cart();
                    cart.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    cart.setProductname(cursor.getString(cursor.getColumnIndex("productname")));
                    cart.setProductid(cursor.getString(cursor.getColumnIndex("productid")));
                    cart.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    cart.setPiece(cursor.getString(cursor.getColumnIndex("piece")));
                    cart.setPhone("" + cursor.getString(cursor.getColumnIndex("phone")));
                    cart.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    cart.setQuantity(cursor.getString(cursor.getColumnIndex("quantity")));
                    cart.setAmount(cursor.getString(cursor.getColumnIndex("amount")));
                    list.add(cart);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    ////// dummy delete
    public List<Cart> getAfterORderTableDummy() {
        List<Cart> list = new ArrayList<Cart>();
        try {
            String selectQuery = "SELECT  * FROM AfterOrderTable";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Cart cart = new Cart();
                    cart.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    cart.setProductname(cursor.getString(cursor.getColumnIndex("productname")));
                    cart.setProductid(cursor.getString(cursor.getColumnIndex("productid")));
                    cart.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    cart.setPhone("" + cursor.getString(cursor.getColumnIndex("phone")));
                    cart.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    cart.setQuantity(cursor.getString(cursor.getColumnIndex("quantity")));
                    cart.setAmount(cursor.getString(cursor.getColumnIndex("amount")));
                    list.add(cart);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> getOrderList(String id) {
        List<Order> list = new ArrayList<Order>();
        try {
            String selectQuery = "SELECT  * FROM UserOrder where orderid='" + id + "' and lock_status='0'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Order order = new Order();
                    order.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                    order.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    order.setInvalid(cursor.getString(cursor.getColumnIndex("invalid")));
                    list.add(order);
                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Order getOrderListByOrderId(String id) {
        Order order = new Order();
        try {
            String selectQuery = "SELECT  * FROM UserOrder where orderid='" + id + "' and lock_status='0'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                order.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                order.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                order.setDate(cursor.getString(cursor.getColumnIndex("date")));
                order.setInvalid(cursor.getString(cursor.getColumnIndex("invalid")));
                order.setFirm(cursor.getString(cursor.getColumnIndex("firm")));
                order.setMobile2(cursor.getString(cursor.getColumnIndex("mobile2")));
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return order;
    }

    public List<Order> getOrderListThroughAddress(String address) {
        List<Order> list = new ArrayList<Order>();
        try {
            String selectQuery = "SELECT  * FROM UserOrder where address='" + address + "' and lock_status='0' ORDER BY id DESC LIMIT 1 ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Order order = new Order();
                    order.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                    order.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    order.setInvalid(cursor.getString(cursor.getColumnIndex("invalid")));
                    list.add(order);
                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getOrderList1(String address) {
        String addressStr = "";
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM UserOrder where address='" + address + "' and lock_status='0' ORDER BY date DESC LIMIT 1";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    addressStr = cursor.getString(cursor.getColumnIndex("orderid"));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return addressStr;
    }

    public List<String> getOrderListForSZ(String address) {
        List<String> orderIdlist = new ArrayList<>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM UserOrder where address='" + address + "' and lock_status='0'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    orderIdlist.add(cursor.getString(cursor.getColumnIndex("orderid")));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return orderIdlist;
    }

    /// get dummy order id
    public List<String> getOrderdummy() {
        List<String> list = new ArrayList<>();
        try {
            String selectQuery = "SELECT  * FROM UserOrder where flag='0'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add("" + cursor.getString(cursor.getColumnIndex("orderid")));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /// get dummy order id
    public List<String> testingAllOrder() {
        List<String> list = new ArrayList<>();
        try {
            String selectQuery = "SELECT  * FROM UserOrder";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add("" + cursor.getString(cursor.getColumnIndex("orderid")));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    //get dummy order 2
    public List<String> getOrderdummy2() {
        List<String> list = new ArrayList<>();
        try {
//                String selectQuery = "SELECT  * FROM UserOrder where flag='0'";
            String selectQuery = "SELECT  * FROM UserOrder ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add("" + cursor.getString(cursor.getColumnIndex("orderid")));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<OrderIdDate> getOrderIdDate(String address) {
        String addressStr = "";
        List<OrderIdDate> list = new ArrayList<>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM UserOrder where address='" + address + "' and lock_status='0' ORDER BY date DESC LIMIT 1";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    OrderIdDate orderIdDate = new OrderIdDate();
                    orderIdDate.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    orderIdDate.setOrderDate(cursor.getString(cursor.getColumnIndex("date")));

                    list.add(orderIdDate);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<OrderIdDate> getOrderIdDate2(String phone) {
        String addressStr = "";
        List<OrderIdDate> list = new ArrayList<>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM UserOrder where phone='" + phone + "' and lock_status='0' ORDER BY date DESC LIMIT 1";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    OrderIdDate orderIdDate = new OrderIdDate();
                    orderIdDate.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    orderIdDate.setOrderDate(cursor.getString(cursor.getColumnIndex("date")));

                    list.add(orderIdDate);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<Order> getOrderIdList(String address, String phone) {

        List<Order> list = new ArrayList<Order>();

        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM UserOrder where  phone='" + phone + "' and address='" + address + "' and lock_status='0'  ";


            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            // looping through all rows and adding to list

            if (cursor.moveToFirst()) {

                do {


                    Order order = new Order();

                    order.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    order.setInvalid(cursor.getString(cursor.getColumnIndex("invalid")));
//                    customer.setArea(cursor.getString(cursor.getColumnIndex("area")));
//                    customer.setSector(cursor.getString(cursor.getColumnIndex("sector")));
//                    customer.setApartment(cursor.getString(cursor.getColumnIndex("apartment")));
//                    customer.setBlock(cursor.getString(cursor.getColumnIndex("block")));
//                    customer.setHouseno(cursor.getString(cursor.getColumnIndex("houseno")));
//                    customer.setFloor(cursor.getString(cursor.getColumnIndex("floor")));


                    list.add(order);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;


    }

    public List<Order> getOrderSyncData() {
        List<Order> list = new ArrayList<Order>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM UserOrder where flag='1' and lock_status='0' ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Order order = new Order();
                    order.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                    order.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    order.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    order.setFirm(cursor.getString(cursor.getColumnIndex("firm")));
                    order.setComment_id("" + cursor.getString(cursor.getColumnIndex("comment_id")));
                    order.setCustom_comment("" + cursor.getString(cursor.getColumnIndex("custom_comment")));
                    order.setMobile2("" + cursor.getString(cursor.getColumnIndex("mobile2")));
                    order.setInvalid("" + cursor.getString(cursor.getColumnIndex("invalid")));

                    list.add(order);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database


        // return contact list
        return list;


    }
//.............Written by himani...........................
    //..........All order info...................

    public List<Order> getOrderSyncDataUnlock() {
        List<Order> list = new ArrayList<Order>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM UserOrder where flag='0' and lock_status='0' ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Order order = new Order();
                    order.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                    order.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    order.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    order.setFirm(cursor.getString(cursor.getColumnIndex("firm")));
                    order.setComment_id("" + cursor.getString(cursor.getColumnIndex("comment_id")));
                    order.setCustom_comment("" + cursor.getString(cursor.getColumnIndex("custom_comment")));
                    order.setMobile2("" + cursor.getString(cursor.getColumnIndex("mobile2")));
                    order.setInvalid("" + cursor.getString(cursor.getColumnIndex("invalid")));

                    list.add(order);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;


    }

    public List<Order> getOrderFullInfo() {
        List<Order> list = new ArrayList<>();

        try {
            String query = "SELECT phone, lock_status, orderid, total_amount FROM UserOrderNew";
            Cursor cursor = sqlite.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Order order = new Order();
                    order.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                    order.setLock_status(cursor.getString(cursor.getColumnIndex("lock_status")));
                    order.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    order.setTotal_amount(cursor.getString(cursor.getColumnIndex("total_amount")));
                    list.add(order);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Order> getOrderFullInfoOldTable() {
        List<Order> list = new ArrayList<>();

        try {
            String query = "SELECT phone, lock_status, orderid, total_amount FROM UserOrder";
            Cursor cursor = sqlite.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Order order = new Order();
                    order.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                    order.setLock_status(cursor.getString(cursor.getColumnIndex("lock_status")));
                    order.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    order.setTotal_amount(cursor.getString(cursor.getColumnIndex("total_amount")));
                    list.add(order);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    //...................End..................................
    public List<Order> getOrderSyncDataBatch40() {
        List<Order> list = new ArrayList<Order>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM UserOrder where flag='1'  limit 40 ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Order order = new Order();
                    order.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                    order.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    order.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    order.setFirm(cursor.getString(cursor.getColumnIndex("firm")));
                    order.setComment_id("" + cursor.getString(cursor.getColumnIndex("comment_id")));
                    order.setCustom_comment("" + cursor.getString(cursor.getColumnIndex("custom_comment")));
                    order.setMobile2("" + cursor.getString(cursor.getColumnIndex("mobile2")));
                    order.setInvalid("" + cursor.getString(cursor.getColumnIndex("invalid")));
                    order.setTotal_amount("" + cursor.getString(cursor.getColumnIndex("total_amount")));
//                    order.setFirm_code("" + cursor.getString(cursor.getColumnIndex("firm_code")));
                    order.setStatus(cursor.getInt(cursor.getColumnIndex("status")));
                    order.setFirm_code("FirmCode");
                    order.setReceipt_type("" + cursor.getString(cursor.getColumnIndex("receipt_type")));
                    list.add(order);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;

    }

    public List<SubCagetoryData> getSubCategory() {

        List<SubCagetoryData> list = new ArrayList<SubCagetoryData>();

        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM SubcategoryTable";


            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            // looping through all rows and adding to list

            if (cursor.moveToFirst()) {

                do {

                    SubCagetoryData subCagetoryData = new SubCagetoryData();

                    subCagetoryData.setSubCategoryId(cursor.getString(cursor.getColumnIndex("subid")));
                    subCagetoryData.setSubCategoryName(cursor.getString(cursor.getColumnIndex("subname")));

                    list.add(subCagetoryData);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database


        // return contact list
        return list;


    }

    public List<ProductData> getProductList() {
        List<ProductData> list = new ArrayList<ProductData>();
        try {
            String selectQuery = "SELECT  * FROM ProductTable";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    ProductData item = new ProductData();
                    item.setProduct_id(cursor.getString(cursor.getColumnIndex("productid")));
                    item.setProduct_name(cursor.getString(cursor.getColumnIndex("productname")));
                    item.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    item.setSubcategory(cursor.getString(cursor.getColumnIndex("subcategory")));
                    item.setWholesale_price(cursor.getString(cursor.getColumnIndex("wholesale_price")));
                    list.add(item);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<AlarmModel> getAlarmList() {
        List<AlarmModel> list = new ArrayList<AlarmModel>();
        try {
            String selectQuery = "SELECT  * FROM AlarmTable order by id desc ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    AlarmModel model = new AlarmModel();
                    model.setAlarmId(cursor.getString(cursor.getColumnIndex("id")));
                    model.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    model.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    model.setTime(cursor.getString(cursor.getColumnIndex("time")));
                    model.setText(cursor.getString(cursor.getColumnIndex("text")));
                    model.setAlarm_status(cursor.getString(cursor.getColumnIndex("alarm_status")));
                    list.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Firm> getFirmList() {
        List<Firm> list = new ArrayList<Firm>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM FirmTable";

            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {
                    Firm firm = new Firm();
                    firm.setName(cursor.getString(cursor.getColumnIndex("firmname")));
                    firm.setAddress(cursor.getString(cursor.getColumnIndex("firmaddress")));
                    firm.setMobile(cursor.getString(cursor.getColumnIndex("firmmobile")));
                    firm.setFirmcode(cursor.getString(cursor.getColumnIndex("firmcode")));
                    firm.setId(String.valueOf(cursor.getInt(cursor.getColumnIndex("firmid"))));
                    list.add(firm);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    // Select All Query
    public List<Firm> getFirmName(String firmcode) {
        List<Firm> list = new ArrayList<Firm>();
        try {
            String selectQuery = " SELECT * FROM FirmTable where firmcode='" + firmcode + "' ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {

                do {
                    Firm firm = new Firm();
                    firm.setName(cursor.getString(cursor.getColumnIndex("firmname")));
                    firm.setAddress(cursor.getString(cursor.getColumnIndex("firmaddress")));
                    firm.setMobile(cursor.getString(cursor.getColumnIndex("firmmobile")));
                    firm.setFirmcode(cursor.getString(cursor.getColumnIndex("firmcode")));
                    firm.setId(String.valueOf(cursor.getInt(cursor.getColumnIndex("firmid"))));
                    list.add(firm);

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<String> getAreLista2() {
        List<String> list = new ArrayList<String>();
        try {
            String selectQuery = "SELECT distinct area FROM CustomerTable where area IS NOT NULL and area <>'' ORDER BY area ASC ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(cursor.getColumnIndex("area")));

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<String> getAreLista() {
        List<String> list = new ArrayList<String>();
        String data = null;
        try {
            String selectQuery = "SELECT distinct area FROM CustomerTable where area IS NOT NULL and area <>'' ORDER BY area ASC ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    if (!cursor.getString(cursor.getColumnIndex("area")).trim().equals("")) {
                        try {
                            if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                                list.add(cursor.getString(cursor.getColumnIndex("area")).trim());
                            } else {
                                data = getHindiAddress(cursor.getString(cursor.getColumnIndex("area")).trim(), "area");
                                if (data != null && !data.isEmpty())
                                    list.add(data);
                                else
                                    list.add(cursor.getString(cursor.getColumnIndex("area")).trim());
                            }
                        } catch (Exception e) {
                            list.add(cursor.getString(cursor.getColumnIndex("area")).trim());
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getHindiAddress(String englishName, String column_name) {
        String hindiAdd = "";
        try {
            String selectQuery = null;
            if ("area".equalsIgnoreCase(column_name))
                selectQuery = "SELECT distinct hindi_name FROM HindiAddressTable where column_name='" + column_name + "' and english_name like '%" + englishName + "%' and hindi_name IS NOT NULL ";

            else if ("sector_district".equalsIgnoreCase(column_name))
                selectQuery = "SELECT distinct hindi_name FROM HindiAddressTable where column_name='" + column_name + "' and english_name like '%" + englishName + "%' and hindi_name IS NOT NULL ";

            else if ("apartment".equalsIgnoreCase(column_name))
                selectQuery = "SELECT distinct hindi_name FROM HindiAddressTable where column_name='" + column_name + "' and english_name like '%" + englishName + "%' and hindi_name IS NOT NULL ";

            else if ("pocket_block".equalsIgnoreCase(column_name))
                selectQuery = "SELECT distinct hindi_name FROM HindiAddressTable where column_name='" + column_name + "' and english_name like '%" + englishName + "%' and hindi_name IS NOT NULL ";

            else
                selectQuery = "SELECT distinct hindi_name FROM HindiAddressTable where english_name like '%" + englishName + "%' and hindi_name IS NOT NULL ";

            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst())
                hindiAdd = cursor.getString(cursor.getColumnIndex("hindi_name"));

            else {
                hindiAdd = "";
            }
            cursor.close();
        } catch (Exception e) {
            hindiAdd = "";
            e.printStackTrace();
        }
        return hindiAdd;
    }

    public List<String> getSectorList(String areaString) {
        List<String> list = new ArrayList<String>();
        String data = null;
        try {
            String selectQuery = "SELECT distinct sector FROM CustomerTable WHERE area like '%" + areaString + "%' and sector IS NOT NULL ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Boolean isExist = false;
                    if (list.size() == 0) {
                        if (!cursor.getString(cursor.getColumnIndex("sector")).trim().equals("")) {
                            try {
                                if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                                    list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                                } else {
                                    data = getHindiAddress(cursor.getString(cursor.getColumnIndex("sector")).trim(), "sector_district");
                                    if (data != null && !data.isEmpty())
                                        list.add(data);
                                    else
                                        list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                                }
                            } catch (Exception e) {
                                list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                            }
                        }

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("sector")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            //by mj
                            if (!cursor.getString(cursor.getColumnIndex("sector")).trim().equals(""))
                                list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                            //end
//                            list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<String> getSectorList2(String areaString) {
        List<String> list = new ArrayList<String>();
        try {
            String selectQuery = "SELECT distinct sector FROM CustomerTable WHERE area like '%" + areaString + "%' and sector IS NOT NULL ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Boolean isExist = false;
                    if (list.size() == 0) {
                        //by mj
                        if (!cursor.getString(cursor.getColumnIndex("sector")).trim().equals(""))
                            list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                        //end
//                        list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("sector")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            //by mj
                            if (!cursor.getString(cursor.getColumnIndex("sector")).trim().equals(""))
                                list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                            //end
//                            list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<String> getApartmnetList(String sectorString) {
        List<String> list = new ArrayList<String>();

        String area = AppUtils.readStringFromPref(context, "area"), data = null;


        try {
            String selectQuery = "SELECT distinct apartment FROM CustomerTable where area like '%" + area + "%' and sector like '%" + sectorString + "%' and apartment IS NOT NULL";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    Boolean isExist = false;
                    if (list.size() == 0) {

                        if (!cursor.getString(cursor.getColumnIndex("apartment")).trim().equals("")) {
                            try {
                                if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                                    list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                                } else {
                                    data = getHindiAddress(cursor.getString(cursor.getColumnIndex("apartment")).trim(), "apartment");
                                    if (data != null && !data.isEmpty())
                                        list.add(data);
                                    else
                                        list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                                }
                            } catch (Exception e) {
                                list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                            }
                        }

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("apartment")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            if (!cursor.getString(cursor.getColumnIndex("apartment")).trim().equals("")) {

                                try {
                                    if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                                        list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                                    } else {
                                        data = getHindiAddress(cursor.getString(cursor.getColumnIndex("apartment")).trim(), "apartment");
                                        if (data != null && !data.isEmpty())
                                            list.add(data);
                                        else
                                            list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                                    }
                                } catch (Exception e) {
                                    list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                                }
                            }
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<String> getApartmnetList2(String sectorString) {
        List<String> list = new ArrayList<String>();

        String area = AppUtils.readStringFromPref(context, "area");


        try {
            String selectQuery = "SELECT distinct apartment FROM CustomerTable where area like '%" + area + "%' and sector like '%" + sectorString + "%' and apartment IS NOT NULL";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    Boolean isExist = false;
                    if (list.size() == 0) {
                        //by mj
                        if (!cursor.getString(cursor.getColumnIndex("apartment")).trim().equals("")) {
                            list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                        }
                        // end
//                        list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("apartment")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            //by mj
                            if (!cursor.getString(cursor.getColumnIndex("apartment")).trim().equals("")) {
                                list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                            }
                            // end
//                            list.add(cursor.getString(cursor.getColumnIndex("apartment")));
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<String> getBlockList(String blockString) {

        List<String> list = new ArrayList<String>();
        String area = AppUtils.readStringFromPref(context, "area");
        String sector = AppUtils.readStringFromPref(context, "sector"), data = null;

        try {
            String selectQuery = "SELECT distinct block FROM CustomerTable where area like '%" + area + "%' and sector like '%" + sector + "%' and apartment like '%" + blockString + "%' and block IS NOT NULL";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    Boolean isExist = false;
                    if (list.size() == 0) {

                        if (!cursor.getString(cursor.getColumnIndex("block")).trim().equals("")) {
                            try {
                                if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                                    list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                                } else {
                                    data = getHindiAddress(cursor.getString(cursor.getColumnIndex("block")).trim(), "pocket_block");
                                    if (data != null && !data.isEmpty())
                                        list.add(data);
                                    else
                                        list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                                }
                            } catch (Exception e) {
                                list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                            }
                        }

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("block")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            //by mj
                            if (!cursor.getString(cursor.getColumnIndex("block")).trim().equals(""))
                                list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                            //end
//                            list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<String> getBlockList2(String blockString) {

        List<String> list = new ArrayList<String>();
        String area = AppUtils.readStringFromPref(context, "area");
        String sector = AppUtils.readStringFromPref(context, "sector");

        try {
            String selectQuery = "SELECT distinct block FROM CustomerTable where area like '%" + area + "%' and sector like '%" + sector + "%' and apartment like '%" + blockString + "%' and block IS NOT NULL";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    Boolean isExist = false;
                    if (list.size() == 0) {
                        //by mj
                        if (!cursor.getString(cursor.getColumnIndex("block")).trim().equals(""))
                            list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                        //end
//                        list.add(cursor.getString(cursor.getColumnIndex("block")).trim());

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("block")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            //by mj
                            if (!cursor.getString(cursor.getColumnIndex("block")).trim().equals(""))
                                list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                            //end
//                            list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<String> getHousenoList(String blockString) {

        List<String> list = new ArrayList<String>();

        String area = AppUtils.readStringFromPref(context, "area");
        String sector = AppUtils.readStringFromPref(context, "sector");
        String apartment = AppUtils.readStringFromPref(context, "apartment");

        // Select All Query
        try {
            String selectQuery = "SELECT distinct houseno FROM CustomerTable where area like '%" + area + "%' and sector like '%" + sector + "%' and apartment like '%" + apartment + "%' and block like '%" + blockString + "%' and houseno IS NOT NULL";

            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            // looping through all rows and adding to list

            if (cursor.moveToFirst()) {

                do {

                    Boolean isExist = false;
                    if (list.size() == 0) {
                        if (!cursor.getString(cursor.getColumnIndex("houseno")).trim().equals("")) {
                            list.add(cursor.getString(cursor.getColumnIndex("houseno")).trim());
                        }
//                        list.add(cursor.getString(cursor.getColumnIndex("houseno")).trim());

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("houseno")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            //by mj
                            if (!cursor.getString(cursor.getColumnIndex("houseno")).trim().equals("")) {
                                list.add(cursor.getString(cursor.getColumnIndex("houseno")).trim());
                            }
                            //end
//                            list.add(cursor.getString(cursor.getColumnIndex("houseno")).trim());
                        }
                    }

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database


        // return contact list
        return list;


    }

    public List<String> getFloorList(String houseno) {
        List<String> list = new ArrayList<String>();

        String area = AppUtils.readStringFromPref(context, "area");
        String sector = AppUtils.readStringFromPref(context, "sector");
        String apartment = AppUtils.readStringFromPref(context, "apartment");
        String block = AppUtils.readStringFromPref(context, "block");

        // Select All Query
        try {
            String selectQuery = "SELECT distinct floor FROM CustomerTable where area like '%" + area + "%' and sector like '%" + sector + "%' and apartment like '%" + apartment + "%' and block like '%" + block + "%' and houseno like '%" + houseno + "%' and floor IS NOT NULL";


            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            // looping through all rows and adding to list

            if (cursor.moveToFirst()) {

                do {

                    Boolean isExist = false;
                    if (list.size() == 0) {
                        if (!cursor.getString(cursor.getColumnIndex("floor")).trim().equals(""))
                            list.add(cursor.getString(cursor.getColumnIndex("floor")).trim());
//                        list.add(cursor.getString(cursor.getColumnIndex("floor")).trim());

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("floor")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            //by mj
                            if (!cursor.getString(cursor.getColumnIndex("floor")).trim().equals(""))
                                list.add(cursor.getString(cursor.getColumnIndex("floor")).trim());
                            //end
//                            list.add(cursor.getString(cursor.getColumnIndex("floor")).trim());
                        }
                    }

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database


        // return contact list
        return list;


    }

    public List<Customer> getAllCustomer(String phone) {
        List<Customer> list = new ArrayList<Customer>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where mobile like '" + phone + "' or mobile2 like '" + phone + "' or mobile3 like '" + phone + "' or mobile4 like '" + phone + "' or mobile5 like '" + phone + "' ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {
                    Customer customer = new Customer();
                    customer.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    customer.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
                    customer.setTags(cursor.getString(cursor.getColumnIndex("tags")));
                    customer.setTags_(cursor.getString(cursor.getColumnIndex("tags_new")));
                    customer.setType(cursor.getString(cursor.getColumnIndex("type")));
                    customer.setName(cursor.getString(cursor.getColumnIndex("contact_name")));
                    customer.setCartage(cursor.getString(cursor.getColumnIndex("cartage")));
                    customer.setComments(cursor.getString(cursor.getColumnIndex("comments")));
                    customer.setCalculation_weight(cursor.getString(cursor.getColumnIndex("calculation_weight")));
                    customer.setDelivery_time(cursor.getString(cursor.getColumnIndex("delivery_time")));

                    customer.setHouse(cursor.getString(cursor.getColumnIndex("houseno")));
                    customer.setFloor(cursor.getString(cursor.getColumnIndex("floor")));
                    customer.setBlock(cursor.getString(cursor.getColumnIndex("block")));
                    customer.setApartment(cursor.getString(cursor.getColumnIndex("apartment")));
                    customer.setSetor(cursor.getString(cursor.getColumnIndex("sector")));
                    customer.setArea(cursor.getString(cursor.getColumnIndex("area")));

                    try {

                        if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                            customer.setHindiAddress(cursor.getString(cursor.getColumnIndex("address")));

                        } else {
                            String tempAddresss = "";

                            if (!cursor.getString(cursor.getColumnIndex("sector")).equalsIgnoreCase("-")) {
                                String area = getHindiAddress(cursor.getString(cursor.getColumnIndex("area")), "area");
                                if (area != null && !area.isEmpty() && !area.equalsIgnoreCase(""))
                                    tempAddresss = area;
                                else {
                                    tempAddresss = cursor.getString(cursor.getColumnIndex("area"));
                                }
                            } else {
                                tempAddresss = cursor.getString(cursor.getColumnIndex("area"));
                            }

                            if (!cursor.getString(cursor.getColumnIndex("sector")).equalsIgnoreCase("-")) {
                                String sector = getHindiAddress(cursor.getString(cursor.getColumnIndex("sector")), "sector_district");
                                if (sector != null && !sector.isEmpty() && !sector.equalsIgnoreCase(""))
                                    tempAddresss = tempAddresss + "," + sector;
                                else {
                                    tempAddresss = tempAddresss + "," + cursor.getString(cursor.getColumnIndex("sector"));
                                }
                            } else {
                                tempAddresss = tempAddresss + "," + cursor.getString(cursor.getColumnIndex("sector"));
                            }

                            if (!cursor.getString(cursor.getColumnIndex("apartment")).equalsIgnoreCase("-")) {
                                String apartment = getHindiAddress(cursor.getString(cursor.getColumnIndex("apartment")), "apartment");
                                if (apartment != null && !apartment.isEmpty() && !apartment.equalsIgnoreCase(""))
                                    tempAddresss = tempAddresss + "," + apartment;
                                else {
                                    tempAddresss = tempAddresss + "," + cursor.getString(cursor.getColumnIndex("apartment"));
                                }
                            } else {
                                tempAddresss = tempAddresss + "," + cursor.getString(cursor.getColumnIndex("apartment"));
                            }

                            tempAddresss = tempAddresss + "," + customer.getBlock();
                            tempAddresss = tempAddresss + "/" + customer.getHouse();
                            tempAddresss = tempAddresss + "," + customer.getFloor();
                            customer.setHindiAddress(tempAddresss);
                        }

                    } catch (Exception e) {
                        customer.setHindiAddress(cursor.getString(cursor.getColumnIndex("address")));
                    }

                    list.add(customer);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public String getCartage_discount(String address) {
        String discount = "0";
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where address='" + address + "' ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    String temp = cursor.getString(cursor.getColumnIndex("cartage_discount"));
                    if (temp == null || temp.isEmpty() || temp.equalsIgnoreCase(""))
                        discount = "0";
                    else
                        discount = temp;
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return discount;
    }

    public String getCartageAmount(String address) {
        String discount = "0";
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where address='" + address + "' ";
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                String temp = cursor.getString(cursor.getColumnIndex("cartage"));
                if (temp == null || temp.isEmpty() || temp.equalsIgnoreCase(""))
                    discount = "0";
                else
                    discount = temp;
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return discount;
    }

    public String getRetailDiscount(String address) {
        String discount = "0";
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where address='" + address + "' ";
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                String temp = cursor.getString(cursor.getColumnIndex("retail_discount"));
                if (temp == null || temp.isEmpty() || temp.equalsIgnoreCase(""))
                    discount = "0";
                else
                    discount = temp;
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return discount;
    }

    public String getCalculationWeight(String address) {
        String discount = "0";
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where address='" + address + "' ";
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                String temp = cursor.getString(cursor.getColumnIndex("calculation_weight"));
                if (temp == null || temp.isEmpty() || temp.equalsIgnoreCase(""))
                    discount = "0";
                else
                    discount = temp;
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return discount;
    }

    public List<CustomerUpsyncAddress> getNewCustomer() {
        List<CustomerUpsyncAddress> list = new ArrayList<CustomerUpsyncAddress>();
        Cursor cursor = null;
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where flag='1'";
            cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {

                    CustomerUpsyncAddress customer = new CustomerUpsyncAddress();

                    customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    customer.setOrderid(cursor.getString(cursor.getColumnIndex("orderid")));
                    customer.setFirm(cursor.getString(cursor.getColumnIndex("firm")));
                    customer.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
                    customer.setMobile2(cursor.getString(cursor.getColumnIndex("mobile2")));
                    customer.setMobile3(cursor.getString(cursor.getColumnIndex("mobile3")));
                    customer.setMobile4(cursor.getString(cursor.getColumnIndex("mobile4")));
                    customer.setMobile5(cursor.getString(cursor.getColumnIndex("mobile5")));
                    customer.setCartage(0);
                    customer.setComments("");
                    customer.setName(cursor.getString(cursor.getColumnIndex("contact_name")));
                    list.add(customer);
                } while (cursor.moveToNext());

            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        // close inserting data from database
        // return contact list
        return list;

    }

    public String getCustomerType(String address) {
        String usertype = null;
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where address='" + address + "'";
            cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                usertype = cursor.getString(cursor.getColumnIndex("type"));
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return usertype;
    }

    public String getcontact_name(String address) {
        String contact_name = null;
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where address='" + address + "'";
            cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                contact_name = cursor.getString(cursor.getColumnIndex("contact_name"));
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return contact_name;
    }

    public String getCartage(String address) {
        String cartage = null;
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where address='" + address + "'";
            cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                cartage = cursor.getString(cursor.getColumnIndex("cartage"));
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return cartage;
    }

    public List<CustomerUpsyncAddress> getNewCustomerBatch50() {
        List<CustomerUpsyncAddress> list = new ArrayList<CustomerUpsyncAddress>();
        // Select All Query
        Cursor cursor = null;
        try {
            String selectQuery = "SELECT  * FROM CustomerTable where flag='1' limit 50 ";
            cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {
                    CustomerUpsyncAddress customer = new CustomerUpsyncAddress();
                    customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    customer.setOrderid(cursor.getString(cursor.getColumnIndex("orderid")));
                    customer.setFirm(cursor.getString(cursor.getColumnIndex("firm")));
                    customer.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
                    customer.setMobile2(cursor.getString(cursor.getColumnIndex("mobile2")));
                    customer.setMobile3(cursor.getString(cursor.getColumnIndex("mobile3")));
                    customer.setMobile4(cursor.getString(cursor.getColumnIndex("mobile4")));
                    customer.setMobile5(cursor.getString(cursor.getColumnIndex("mobile5")));
                    customer.setMobile6(cursor.getString(cursor.getColumnIndex("mobile6")));
                    customer.setMobile7(cursor.getString(cursor.getColumnIndex("mobile7")));
                    customer.setMobile8(cursor.getString(cursor.getColumnIndex("mobile8")));
                    customer.setMobile9(cursor.getString(cursor.getColumnIndex("mobile9")));
                    customer.setMobile10(cursor.getString(cursor.getColumnIndex("mobile10")));
                    customer.setComments(cursor.getString(cursor.getColumnIndex("comments")));
                    try {
                        customer.setCartage(Integer.parseInt(cursor.getString(cursor.getColumnIndex("cartage"))));
                    } catch (Exception e) {
                        customer.setCartage(0);
                    }
                    customer.setName(cursor.getString(cursor.getColumnIndex("contact_name")));
                    customer.setTags_new(cursor.getString(cursor.getColumnIndex("tags_new")));

                    customer.setKothi_flate(cursor.getString(cursor.getColumnIndex("kothi_flate")) != null ? cursor.getString(cursor.getColumnIndex("kothi_flate")) : "-");
                    customer.setGali_no(cursor.getString(cursor.getColumnIndex("gali_no")) != null ? cursor.getString(cursor.getColumnIndex("gali_no")) : "-");
                    customer.setExtra(cursor.getString(cursor.getColumnIndex("extra")) != null ? cursor.getString(cursor.getColumnIndex("extra")) : "-");
                    customer.setLift(cursor.getString(cursor.getColumnIndex("lift")) != null ? cursor.getString(cursor.getColumnIndex("lift")) : "-");
                    list.add(customer);

                } while (cursor.moveToNext());
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        // close inserting data from database
        // return contact list
        return list;

    }

    public List<CustomergetAddress> getAllPhoneList(String address) {
        List<CustomergetAddress> list = new ArrayList<CustomergetAddress>();
        // Select All Query
        Cursor cursor = null;
        try {
            String address2 = address.replaceAll(",", ", ");
            String selectQuery = "SELECT  * FROM CustomerTable where address = '" + address + "' or address = '" + address2 + "'";
            cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    CustomergetAddress customer = new CustomergetAddress();
                    customer.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    customer.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
                    customer.setmobile2(cursor.getString(cursor.getColumnIndex("mobile2")));
                    customer.setMobile3(cursor.getString(cursor.getColumnIndex("mobile3")));
                    customer.setMobile4(cursor.getString(cursor.getColumnIndex("mobile4")));
                    customer.setMobile5(cursor.getString(cursor.getColumnIndex("mobile5")));
                    customer.setMobile6(cursor.getString(cursor.getColumnIndex("mobile6")));
                    customer.setMobile7(cursor.getString(cursor.getColumnIndex("mobile7")));
                    customer.setMobile8(cursor.getString(cursor.getColumnIndex("mobile8")));
                    customer.setMobile9(cursor.getString(cursor.getColumnIndex("mobile9")));
                    customer.setMobile10(cursor.getString(cursor.getColumnIndex("mobile10")));
                    customer.setTags(cursor.getString(cursor.getColumnIndex("tags")));
                    customer.setType(cursor.getString(cursor.getColumnIndex("type")));
                    customer.setContact_name(cursor.getString(cursor.getColumnIndex("contact_name")));
                    customer.setCartage(cursor.getString(cursor.getColumnIndex("cartage")));
                    customer.setComments(cursor.getString(cursor.getColumnIndex("comments")));
                    customer.setCalculation_weight(cursor.getString(cursor.getColumnIndex("calculation_weight")));
                    customer.setDelivery_time(cursor.getString(cursor.getColumnIndex("delivery_time")));


                    list.add(customer);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        // return contact list
        return list;
    }

    public String getTag(String address) {
        String tag = "";
        try {
            String address2 = address.replaceAll(",", ", ");
            String selectQuery = "SELECT  * FROM CustomerTable where address = '" + address + "' or address = '" + address2 + "'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    tag = cursor.getString(cursor.getColumnIndex("tags"));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tag;
    }

    public List<String> getOrderIdlist() {
        List<String> list = new ArrayList<String>();
        try {
            String selectQuery = "SELECT  * FROM UserOrder where lock_status='0'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Customer customer = new Customer();
                    customer.setOrderId(cursor.getString(cursor.getColumnIndex("orderid")));
                    list.add(cursor.getString(cursor.getColumnIndex("orderid")));

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<String> getAllPhoneList1New() {
        List<String> list = new ArrayList<String>();
        // Select All Query
        try {
            String selectQuery = "SELECT  * FROM CustomerTable";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Customer customer = new Customer();
                    customer.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    customer.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
                    list.add(cursor.getString(cursor.getColumnIndex("full_new_address")).replaceAll(",", ", ").replaceAll("/", "/ "));

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Customer> getAllPhoneList1NewPhase3() {
        List<Customer> list = new ArrayList<Customer>();
        // Select All Query
        Log.i("customer_filter", "better2");
        try {
            String selectQuery = "SELECT  * FROM CustomerTable";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Customer customer = new Customer();
                    customer.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                    customer.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));

                    customer.setFloor(cursor.getString(cursor.getColumnIndex("floor")));
                    customer.setHouse(cursor.getString(cursor.getColumnIndex("houseno")));
                    customer.setBlock(cursor.getString(cursor.getColumnIndex("block")));
                    customer.setApartment(cursor.getString(cursor.getColumnIndex("apartment")));
                    customer.setSetor(cursor.getString(cursor.getColumnIndex("sector")));
                    customer.setArea(cursor.getString(cursor.getColumnIndex("area")));

//                    String address = cursor.getString(cursor.getColumnIndex("full_new_address")).replaceAll(",", ", ");
//                    String kothi = cursor.getString(cursor.getColumnIndex("full_new_address")).replaceAll(",", ", ");
//                    String gali = cursor.getString(cursor.getColumnIndex("full_new_address")).replaceAll(",", ", ");
//                    String lift = cursor.getString(cursor.getColumnIndex("lift")).replaceAll(",", ", ");
//                    String extra = cursor.getString(cursor.getColumnIndex("extra")).replaceAll(",", ", ");


                    customer.setFull_new_address(cursor.getString(cursor.getColumnIndex("full_new_address")).replaceAll(",", ", ").replaceAll("/", "/ "));


                    Log.i("customer_filter", "better ");

                    list.add(customer);
//                    list.add(cursor.getString(cursor.getColumnIndex("address")).replaceAll(",", ", ").replaceAll("/", "/ "));

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public int getCustomerAddressList(String address) {
        int count = 0;
        try {
            String selectQuery = "SELECT  * FROM AddressTable where address='" + address + "'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            count = cursor.getCount();
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }


    public void deleteCart() {
        sqlite.execSQL("DELETE FROM Cart ");
        // Toast.makeText(context, "Cart Data delete successfully", Toast.LENGTH_LONG).show();
//sqlite.execSQL("DELETE FROM OrderData");

    }

    public void deleteCustomerTable() {
        String sql = "Select * from CustomerTable where flag='1' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() >= 1) {
                if (cursor.moveToFirst()) {
                    do {
                        CustomergetAddress customer = new CustomergetAddress();
                        customer.setId(cursor.getInt(cursor.getColumnIndex("id")));
                        customer.setFirm(cursor.getString(cursor.getColumnIndex("firm")));
                        customer.setmobile(cursor.getString(cursor.getColumnIndex("mobile")));
                        customer.setmobile2(cursor.getString(cursor.getColumnIndex("mobile2")));
                        customer.setmobile3(cursor.getString(cursor.getColumnIndex("mobile3")));
                        customer.setmobile4(cursor.getString(cursor.getColumnIndex("mobile4")));
                        customer.setmobile5(cursor.getString(cursor.getColumnIndex("mobile5")));
                        customer.setMobile6(cursor.getString(cursor.getColumnIndex("mobile6")));
                        customer.setMobile7(cursor.getString(cursor.getColumnIndex("mobile7")));
                        customer.setMobile8(cursor.getString(cursor.getColumnIndex("mobile8")));
                        customer.setMobile9(cursor.getString(cursor.getColumnIndex("mobile9")));
                        customer.setMobile10(cursor.getString(cursor.getColumnIndex("mobile10")));
                        customer.setContact_name(cursor.getString(cursor.getColumnIndex("contact_name")));
                        customer.setHouseno(cursor.getString(cursor.getColumnIndex("houseno")));
                        customer.setFloor(cursor.getString(cursor.getColumnIndex("floor")));
                        customer.setBlock(cursor.getString(cursor.getColumnIndex("block")));
                        customer.setApartment(cursor.getString(cursor.getColumnIndex("apartment")));
                        customer.setSector(cursor.getString(cursor.getColumnIndex("sector")));
                        customer.setArea(cursor.getString(cursor.getColumnIndex("area")));
                        customer.setCartage_discount(cursor.getString(cursor.getColumnIndex("cartage_discount")));
                        customer.setRetail_discount(cursor.getString(cursor.getColumnIndex("retail_discount")));
                        customer.setTags_new(cursor.getString(cursor.getColumnIndex("tags_new")));
                        customer.setOrderid(cursor.getString(cursor.getColumnIndex("orderid")));
                        customer.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                        customer.setFlag(cursor.getString(cursor.getColumnIndex("flag")));
                        customer.setTags(cursor.getString(cursor.getColumnIndex("tags")));
                        customer.setComments(cursor.getString(cursor.getColumnIndex("comments")));
                        customer.setCartage(cursor.getString(cursor.getColumnIndex("cartage")));
                        customer.setType(cursor.getString(cursor.getColumnIndex("type")));
                        insertCustomerData1(customer);
                    } while (cursor.moveToNext());
                }
            } else {
                Log.i("data_adapter", "having no customer empty");
            }
            cursor.close();
            moveCustomerDataToNewTable();
        } catch (Exception e) {
            Log.i("data_adapter", "catch_2_customer " + e.toString());
            e.printStackTrace();
        }
    }


    public void moveCustomerDataToNewTable() {
        String sql = "Select * from CustomerTable";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() > 1) {
                Log.i("data_adapter", "in_customer1 = " + cursor.getCount());
                sqlite.execSQL("DELETE FROM CustomerTable ");
            } else {
                Log.i("data_adapter", "in_customer1 is empty");
            }
            cursor.close();
        } catch (Exception e) {
            Log.i("data_adapter", "in_customer_catch " + e.toString());
            e.printStackTrace();
        }
        sqlite.execSQL("INSERT INTO CustomerTable Select * from CustomerTableNew ");
    }

    public void moveUserOrderDataToNewTable() {
        String sql = "Select * from UserOrder";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() > 1) {
                Log.i("data_adapter", "in_UserOrder1 = " + cursor.getCount());
                sqlite.execSQL("DELETE FROM UserOrder ");
            } else {
                Log.i("data_adapter", "in_UserOrder1 is empty");
            }
            cursor.close();
        } catch (Exception e) {
            Log.i("data_adapter", "in_UserOrder_catch " + e.toString());
            e.printStackTrace();
        }
        sqlite.execSQL("INSERT INTO UserOrder Select * from UserOrderNew ");
    }

    public void moveAfterOrderTableDataToNewTable() {
        String sql = "Select * from AfterOrderTable";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() > 1) {
                Log.i("data_adapter", "in_AfterOrderTable1 = " + cursor.getCount());
                sqlite.execSQL("DELETE FROM AfterOrderTable ");
            } else {
                Log.i("data_adapter", "in_AfterOrderTable is empty");
            }
            cursor.close();
        } catch (Exception e) {
            Log.i("data_adapter", "in_AfterOrderTable_catch " + e.toString());
            e.printStackTrace();
        }
        sqlite.execSQL("INSERT INTO AfterOrderTable Select * from AfterOrderTableNew ");
    }


    public void deleteFirm() {
        sqlite.execSQL("DELETE FROM FirmTable ");
        // Toast.makeText(context, "Cart Data delete successfully", Toast.LENGTH_LONG).show();
//sqlite.execSQL("DELETE FROM OrderData");

    }

    public void deleteAfterOrderTableNew() {
        sqlite.execSQL("DELETE FROM AfterOrderTableNew ");

    }

    public void deleteUserOrderNew() {
        sqlite.execSQL("DELETE FROM UserOrderNew ");

    }

    public void deleteCustomerTableNew() {
        sqlite.execSQL("DELETE FROM CustomerTableNew ");

    }

    public void deleteCommentTable() {
        sqlite.execSQL("DELETE FROM CommentTable ");

    }

    public void deletReceiptType() {
        sqlite.execSQL("DELETE FROM ReceiptTypeTable ");

    }

    public void deleteSubCategory() {
        sqlite.execSQL("DELETE FROM SubcategoryTable ");
        // Toast.makeText(context, "SubcategoryTable delete successfully", Toast.LENGTH_LONG).show();
//sqlite.execSQL("DELETE FROM OrderData");

    }

    public void deleteOrderImage() {
        sqlite.execSQL("DELETE FROM OrderImage ");

    }

    public void deleteCustomerData() {
        sqlite.execSQL("DELETE FROM CustomerTable ");

    }

    public void deleteAlarm() {
        sqlite.execSQL("DELETE FROM AlarmTable ");
        // Toast.makeText(context, "SubcategoryTable delete successfully", Toast.LENGTH_LONG).show();
//sqlite.execSQL("DELETE FROM OrderData");

    }

    public void deleteProduct() {
        sqlite.execSQL("DELETE FROM ProductTable ");
        // Toast.makeText(context, "ProductTable delete successfully", Toast.LENGTH_LONG).show();
//sqlite.execSQL("DELETE FROM OrderData");

    }

    public List LastOrder(String firmcode) {
        List<String> list = new ArrayList<>();
//        String firm = firmcode.replaceAll(" ", "") + "%";
        try {
//            String selectQuery = "SELECT * FROM CustomerTable where orderid  like '" + firmcode + "%' or orderid like '" + firm + "'";
            String selectQuery = "SELECT * FROM UserOrder";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    String value2[];
                    String value = cursor.getString(cursor.getColumnIndex("orderid"));
                    if (value.contains(firmcode)) {
                        Log.i("firm1", "-" + value);
                        value2 = value.split(firmcode);
                        Log.i("firm2", "-" + value2);
                        if (value2.length > 0) {
                            if (value2[0].equals("")) {
                                list.add(value2[1]);
                                Log.i("firm3", "-" + value2[1]);
                            }
                        }
                    }

//                    list.add(cursor.getString(cursor.getColumnIndex("orderid")));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    //.......................................................................................................................................//

    public List<String> getCustomerListSize() {
        String size = "";
        List<String> list = new ArrayList<>();
        try {
            String selectQuery = "SELECT  * FROM CustomerTable";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(cursor.getColumnIndex("mobile")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile2")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile3")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile4")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile5")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile6")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile7")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile8")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile9")));
                    list.add(cursor.getString(cursor.getColumnIndex("mobile10")));

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return RemoveNull(list);
    }

    public List RemoveNull(List<String> list) {
        String temp = null;
        List<String> newList = new ArrayList<>();

        for (String element : list) {
            temp = element;
            if (temp != null) {
                if (!newList.contains(element)) {
                    newList.add(element);
                }
            }
        }
        return newList;
    }


    public long insertOrderImage(OrderImageListData orderImage) {
        long rowId = -1;
        String sql = "Select * from OrderImage where receipt_no = '" + orderImage.getReceipt_no() + "' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("receipt_no", orderImage.getReceipt_no());
                contentValues.put("image", orderImage.getImage());
                contentValues.put("firm_id", orderImage.getFirm());
                contentValues.put("amount", orderImage.getAmount());
                contentValues.put("orderDate", orderImage.getDate());
                contentValues.put("flag", orderImage.getFlag());
                rowId = sqlite.insert("OrderImage", null, contentValues);
                if (rowId == -1) {
//                    Toast.makeText(context, "OrderId : " + orderImage.getReceipt_no() + " OrderImage is not updated ", Toast.LENGTH_LONG).show();
                }

            } else {
                ContentValues contentValues = new ContentValues();
                contentValues.put("receipt_no", orderImage.getReceipt_no());
                contentValues.put("image", orderImage.getImage());
                contentValues.put("firm_id", orderImage.getFirm());
                contentValues.put("amount", orderImage.getAmount());
                contentValues.put("orderDate", orderImage.getDate());
                contentValues.put("flag", orderImage.getFlag());

                long rowId1 = sqlite.update("OrderImage", contentValues, "receipt_no = '" + orderImage.getReceipt_no() + "' ", null);

                if (rowId1 == -1) {
//                    Toast.makeText(context, "OrderId : " + orderImage.getReceipt_no() + " OrderImage is not updated ", Toast.LENGTH_LONG).show();
                }
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }


    public long insertHindiAddress(HindiAddressModel model) {
        long rowId = -1;
        String sql = "Select * from HindiAddressTable where column_name = '" + model.getColumn_name() + "' and english_name = '" + model.getEnglish_name() + "' ";
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(sql, null);
            if (cursor.getCount() < 1) {

                Log.i("address_if_is_running", "-" + model.getHindi_name());

                ContentValues contentValues = new ContentValues();
                contentValues.put("column_name", model.getColumn_name());
                contentValues.put("english_name", model.getEnglish_name());
                contentValues.put("hindi_name", model.getHindi_name());
//                contentValues.put("hindi_name", "अलार्म");
                rowId = sqlite.insert("HindiAddressTable", null, contentValues);
                if (rowId == -1) {
                    Toast.makeText(context, "Address : " + model.getEnglish_name() + " is not updated ", Toast.LENGTH_LONG).show();
                }

            } else {
                Log.i("address_else_is_running", "-" + model.getHindi_name());

                ContentValues contentValues = new ContentValues();
                contentValues.put("column_name", model.getColumn_name());
                contentValues.put("english_name", model.getEnglish_name());
                contentValues.put("hindi_name", model.getHindi_name());

                rowId = sqlite.update("HindiAddressTable", contentValues, "column_name = '" + model.getColumn_name() + "' and english_name = '" + model.getEnglish_name() + "' ", null);

                if (rowId == -1) {
                    Toast.makeText(context, "Address else : " + model.getEnglish_name() + " is not updated ", Toast.LENGTH_LONG).show();
                }
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return rowId;
    }


    public long updateOrderImage(OrderImageListData orderImage) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("image", orderImage.getImage());
        contentValues.put("amount", orderImage.getAmount());
        contentValues.put("orderDate", orderImage.getDate());
        long rowId = sqlite.update("OrderImage", contentValues, "receipt_no= '" + orderImage.getReceipt_no() + "'", null);
        return rowId;
    }


    public List<OrderImageListData> sendReceiptData() {
        List<OrderImageListData> list = new ArrayList<OrderImageListData>();
        try {
            String selectQuery = "select  * FROM OrderImage";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OrderImageListData order = new OrderImageListData();
                    order.setAmount(cursor.getString(cursor.getColumnIndex("amount")));
                    order.setImage(cursor.getString(cursor.getColumnIndex("image")));
                    order.setFirm(cursor.getString(cursor.getColumnIndex("firm_id")));
                    order.setReceipt_no(cursor.getString(cursor.getColumnIndex("receipt_no")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("orderDate")));
                    list.add(order);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public List<OrderImageListData> getReceiptDataByFlag() {
        List<OrderImageListData> list = new ArrayList<OrderImageListData>();
        try {
            String selectQuery = "select  * FROM OrderImage where flag='1'";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OrderImageListData order = new OrderImageListData();
                    order.setAmount(cursor.getString(cursor.getColumnIndex("amount")));
                    order.setImage(cursor.getString(cursor.getColumnIndex("image")));
                    order.setFirm(cursor.getString(cursor.getColumnIndex("firm_id")));
                    order.setReceipt_no(cursor.getString(cursor.getColumnIndex("receipt_no")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("orderDate")));
                    list.add(order);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public List<OrderImageListData> getReceiptDataWithInvalid() {
        List<OrderImageListData> list = new ArrayList<OrderImageListData>();
        try {
            String selectQuery = "select  * FROM OrderImage Left join UserOrder where UserOrder.orderid = OrderImage.receipt_no ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OrderImageListData order = new OrderImageListData();
                    order.setAmount(cursor.getString(cursor.getColumnIndex("amount")));
                    order.setImage(cursor.getString(cursor.getColumnIndex("image")));
                    order.setFirm(cursor.getString(cursor.getColumnIndex("firm_id")));
                    order.setReceipt_no(cursor.getString(cursor.getColumnIndex("receipt_no")));
                    order.setInvalid(cursor.getString(cursor.getColumnIndex("invalid")));
                    order.setDate(cursor.getString(cursor.getColumnIndex("orderDate")));

                    list.add(order);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public List<OrderWithImageModel> getReceiptDataWithInvalid2() {
        List<OrderWithImageModel> list = new ArrayList<OrderWithImageModel>();
        try {
            String selectQuery = "select * FROM UserOrder ORDER BY date DESC ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OrderWithImageModel order = new OrderWithImageModel();
                    order.setOrderid("" + cursor.getString(cursor.getColumnIndex("orderid")));
                    order.setFirm("" + cursor.getString(cursor.getColumnIndex("firm")));
                    order.setAmount("" + cursor.getString(cursor.getColumnIndex("total_amount")));
                    order.setDate("" + cursor.getString(cursor.getColumnIndex("date")));
                    order.setInvalid("" + cursor.getString(cursor.getColumnIndex("invalid")));
                    list.add(order);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public List<OrderWithImageModel> getRecepitbyFirm(String firmCode) {
        List<OrderWithImageModel> list = new ArrayList<OrderWithImageModel>();
        try {
            String selectQuery = "select UserOrder.orderid, UserOrder.firm, UserOrder.total_amount, UserOrder.invalid,  UserOrder.date, OrderImage.receipt_no, OrderImage.image  FROM UserOrder Left join OrderImage on UserOrder.orderid = OrderImage.receipt_no ORDER BY UserOrder.date DESC ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    int length = firmCode.length();
                    String removeInt = cursor.getString(cursor.getColumnIndex("orderid")).substring(0, length);
                    if (removeInt.equalsIgnoreCase(firmCode)) {
                        OrderWithImageModel order = new OrderWithImageModel();
                        order.setOrderid("" + cursor.getString(cursor.getColumnIndex("orderid")));
                        order.setFirm("" + cursor.getString(cursor.getColumnIndex("firm")));
                        order.setAmount("" + cursor.getString(cursor.getColumnIndex("total_amount")));
                        order.setDate("" + cursor.getString(cursor.getColumnIndex("date")));
                        order.setReceipt_no("" + cursor.getString(cursor.getColumnIndex("receipt_no")));
                        order.setImage("" + cursor.getString(cursor.getColumnIndex("image")));
                        order.setInvalid("" + cursor.getString(cursor.getColumnIndex("invalid")));
                        list.add(order);
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<OrderWithImageModel> getRecepitbyDate(String from, String to) {
        List<OrderWithImageModel> list = new ArrayList<OrderWithImageModel>();
        try {
            String selectQuery = "select UserOrder.orderid, UserOrder.firm, UserOrder.total_amount, UserOrder.invalid,  UserOrder.date, OrderImage.receipt_no, OrderImage.image  FROM UserOrder Left join OrderImage on UserOrder.orderid = OrderImage.receipt_no  where UserOrder.date >= '" + from + "' AND  UserOrder.date <='" + to + "' ORDER BY UserOrder.date DESC ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    OrderWithImageModel order = new OrderWithImageModel();
                    order.setOrderid("" + cursor.getString(cursor.getColumnIndex("orderid")));
                    order.setFirm("" + cursor.getString(cursor.getColumnIndex("firm")));
                    order.setAmount("" + cursor.getString(cursor.getColumnIndex("total_amount")));
                    order.setDate("" + cursor.getString(cursor.getColumnIndex("date")));
                    order.setReceipt_no("" + cursor.getString(cursor.getColumnIndex("receipt_no")));
                    order.setImage("" + cursor.getString(cursor.getColumnIndex("image")));
                    order.setInvalid("" + cursor.getString(cursor.getColumnIndex("invalid")));
                    list.add(order);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<OrderWithImageModel> getRecepitbyReceiptNumber(String from, String to) {
        List<OrderWithImageModel> list = new ArrayList<OrderWithImageModel>();

        String firmFrom = from.replaceAll("[0-9]", "");
        String firmTo = to.replaceAll("[0-9]", "");

        int numFrom = (Integer.parseInt(from.replaceAll(firmFrom, "")));
        int numTo = (Integer.parseInt(to.replaceAll(firmTo, "")));
        int length = firmFrom.length();


        try {
            String selectQuery = "select UserOrder.orderid, UserOrder.firm, UserOrder.total_amount, UserOrder.invalid,  UserOrder.date, OrderImage.receipt_no, OrderImage.image  FROM UserOrder Left join OrderImage on UserOrder.orderid = OrderImage.receipt_no ORDER BY UserOrder.date DESC ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    String removeInt = cursor.getString(cursor.getColumnIndex("orderid")).substring(0, length);
                    int orderidLength = cursor.getString(cursor.getColumnIndex("orderid")).length();
                    String orderNumber = cursor.getString(cursor.getColumnIndex("orderid")).substring(length, orderidLength).replaceAll("[a-zA-Z]", "");

                    if (removeInt.equalsIgnoreCase(firmFrom)) {

                        if (Integer.parseInt(orderNumber) >= numFrom && Integer.parseInt(orderNumber) <= numTo) {
                            OrderWithImageModel order = new OrderWithImageModel();
                            order.setOrderid("" + cursor.getString(cursor.getColumnIndex("orderid")));
                            order.setFirm("" + cursor.getString(cursor.getColumnIndex("firm")));
                            order.setAmount("" + cursor.getString(cursor.getColumnIndex("total_amount")));
                            order.setDate("" + cursor.getString(cursor.getColumnIndex("date")));
                            order.setReceipt_no("" + cursor.getString(cursor.getColumnIndex("receipt_no")));
                            order.setImage("" + cursor.getString(cursor.getColumnIndex("image")));
                            order.setInvalid("" + cursor.getString(cursor.getColumnIndex("invalid")));
                            list.add(order);
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    //........................................................................................................................................//


////// phase 2 /////


    public List<String> getRecentAreaList() {
        List<String> list = new ArrayList<String>();
        try {
            String selectQuery = "SELECT distinct area FROM CustomerTable where flag = '1' and area IS NOT NULL and area <>''";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(cursor.getColumnIndex("area")));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public List<String> getRecentSectorList(String areaString) {
        List<String> list = new ArrayList<String>();
        try {
            String selectQuery = "SELECT distinct sector FROM CustomerTable WHERE flag = '1' and area like '%" + areaString + "%' and sector IS NOT NULL ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Boolean isExist = false;
                    if (list.size() == 0) {
                        if (!cursor.getString(cursor.getColumnIndex("sector")).trim().equals(""))
                            list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("sector")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            if (!cursor.getString(cursor.getColumnIndex("sector")).trim().equals(""))
                                list.add(cursor.getString(cursor.getColumnIndex("sector")).trim());
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<String> getRecentApartmentList(String sectorString) {
        List<String> list = new ArrayList<String>();
        String area = AppUtils.readStringFromPref(context, "area");

        try {
            String selectQuery = "SELECT distinct apartment FROM CustomerTable WHERE flag = '1' and area like '%" + area + "%' and sector like '%" + sectorString + "%' and apartment IS NOT NULL";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Boolean isExist = false;
                    if (list.size() == 0) {
                        if (!cursor.getString(cursor.getColumnIndex("apartment")).trim().equals("")) {
                            list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                        }
                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("apartment")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }
                        if (!isExist) {
                            if (!cursor.getString(cursor.getColumnIndex("apartment")).trim().equals("")) {
                                list.add(cursor.getString(cursor.getColumnIndex("apartment")).trim());
                            }
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<String> getRecentBlockList(String blockString) {
        List<String> list = new ArrayList<String>();
        String area = AppUtils.readStringFromPref(context, "area");
        String sector = AppUtils.readStringFromPref(context, "sector");

        try {
            String selectQuery = "SELECT distinct block FROM CustomerTable where flag = '1' and area like '%" + area + "%' and sector like '%" + sector + "%' and apartment like '%" + blockString + "%' and block IS NOT NULL";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Boolean isExist = false;
                    if (list.size() == 0) {
                        if (!cursor.getString(cursor.getColumnIndex("block")).trim().equals(""))
                            list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("block")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            if (!cursor.getString(cursor.getColumnIndex("block")).trim().equals(""))
                                list.add(cursor.getString(cursor.getColumnIndex("block")).trim());
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

    public List<String> getRecentHousenoList(String blockString) {

        List<String> list = new ArrayList<String>();

        String area = AppUtils.readStringFromPref(context, "area");
        String sector = AppUtils.readStringFromPref(context, "sector");
        String apartment = AppUtils.readStringFromPref(context, "apartment");

        try {
            String selectQuery = "SELECT distinct houseno FROM CustomerTable where flag = '1' and area like '%" + area + "%' and sector like '%" + sector + "%' and apartment like '%" + apartment + "%' and block like '%" + blockString + "%' and houseno IS NOT NULL";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {

                do {

                    Boolean isExist = false;
                    if (list.size() == 0) {
                        if (!cursor.getString(cursor.getColumnIndex("houseno")).trim().equals("")) {
                            list.add(cursor.getString(cursor.getColumnIndex("houseno")).trim());
                        }

                    } else {

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("houseno")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }

                        if (!isExist) {
                            if (!cursor.getString(cursor.getColumnIndex("houseno")).trim().equals("")) {
                                list.add(cursor.getString(cursor.getColumnIndex("houseno")).trim());
                            }
                        }
                    }

                } while (cursor.moveToNext());

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<String> getRecentFloorList(String houseno) {
        List<String> list = new ArrayList<String>();
        String area = AppUtils.readStringFromPref(context, "area");
        String sector = AppUtils.readStringFromPref(context, "sector");
        String apartment = AppUtils.readStringFromPref(context, "apartment");
        String block = AppUtils.readStringFromPref(context, "block");

        try {
            String selectQuery = "SELECT distinct floor FROM CustomerTable where flag = '1' and area like '%" + area + "%' and sector like '%" + sector + "%' and apartment like '%" + apartment + "%' and block like '%" + block + "%' and houseno like '%" + houseno + "%' and floor IS NOT NULL";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Boolean isExist = false;
                    if (list.size() == 0) {
                        if (!cursor.getString(cursor.getColumnIndex("floor")).trim().equals(""))
                            list.add(cursor.getString(cursor.getColumnIndex("floor")).trim());
                    } else {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("floor")).trim())) {
                                isExist = true;
                            } else {
                                isExist = false;
                            }
                        }
                        if (!isExist) {
                            if (!cursor.getString(cursor.getColumnIndex("floor")).trim().equals(""))
                                list.add(cursor.getString(cursor.getColumnIndex("floor")).trim());
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public List<String> getReceiptType() {
        List<String> list = new ArrayList<String>();
        // Select All Query
        try {
            String selectQuery = "SELECT * FROM ReceiptTypeTable ";
            Cursor cursor = sqlite.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(cursor.getColumnIndex("type")).trim());
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // close inserting data from database
        // return contact list
        return list;
    }

}

