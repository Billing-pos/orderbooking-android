package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.product.AddProductActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FullAddressActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_area_error)
    TextView tv_area_error;
    @BindView(R.id.tv_phone_error)
    TextView tv_phone_error;
    @BindView(R.id.tv_house_error)
    TextView tv_house_error;
    @BindView(R.id.tv_floor_error)
    TextView tv_floor_error;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.et_area)
    EditText et_area;
    @BindView(R.id.et_sector)
    EditText et_sector;
    @BindView(R.id.et_apartment)
    EditText et_apartment;
    @BindView(R.id.et_block)
    EditText et_block;
    @BindView(R.id.et_house)
    EditText et_house;

    @BindView(R.id.et_floor)
    EditText et_floor;

    @BindView(R.id.et_gali)
    EditText et_gali;

    @BindView(R.id.et_kothi)
    EditText et_kothi;

    @BindView(R.id.et_extra)
    EditText et_extra;

    @BindView(R.id.rb_yes)
    RadioButton rb_yes;

    @BindView(R.id.rb_no)
    RadioButton rb_no;

    @BindView(R.id.rg_lift)
    RadioGroup rg_lift;

    String isLift = "Yes";

    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.ll_phone)
    LinearLayout ll_phone;
    @BindView(R.id.bt_submit)
    Button bt_submit;
    List<Firm> frimList;
    String phone, area, sector, apartment, block, house, floor, firm, gali, kothi, extra;
    DBAdapter dbAdapter;

    // to validate phone number & telephone number
    public static boolean isValidMobile(String s) {
        Pattern p = Pattern.compile("(0/91)?[5-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    public static boolean isValidTelephone(String s) {
        Pattern p = Pattern.compile("[2-9][0-9]{7}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_address);
        ButterKnife.bind(this);
        tv_name.setText(R.string.address);
        bt_submit.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(FullAddressActivity.this);


        area = AppUtils.readStringFromPref(FullAddressActivity.this, "area");
        phone = AppUtils.readStringFromPref(FullAddressActivity.this, "phone");
        sector = AppUtils.readStringFromPref(FullAddressActivity.this, "sector");
        apartment = AppUtils.readStringFromPref(FullAddressActivity.this, "apartment");
        block = AppUtils.readStringFromPref(FullAddressActivity.this, "block");
        house = AppUtils.readStringFromPref(FullAddressActivity.this, "houseno");
        floor = AppUtils.readStringFromPref(FullAddressActivity.this, "floor");

        if (AppUtils.readStringFromPref(FullAddressActivity.this, "phone") == null || AppUtils.readStringFromPref(FullAddressActivity.this, "phone").isEmpty()) {
            et_phone.setText("");
            et_phone.setFocusable(true);
        } else {
            et_phone.setText("" + phone);
            et_phone.setFocusable(false);
        }

        et_area.setText("" + area);
        et_sector.setText("" + sector);
        et_apartment.setText("" + apartment);
        et_block.setText("" + block);
        et_house.setText("" + house);
        et_floor.setText("" + floor);
        dbAdapter.open();
        frimList = dbAdapter.getFirmList();

        et_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tv_phone_error.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        rg_lift.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (rb_yes.isChecked()) {
                    isLift = "Yes";
                } else {
                    isLift = "No";
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;

            case R.id.bt_submit:
                String phonestr = et_phone.getText().toString();

                if (et_area.getText().toString().isEmpty()) {
                    tv_area_error.setVisibility(View.VISIBLE);
                    tv_house_error.setVisibility(View.GONE);
                    tv_floor_error.setVisibility(View.GONE);

                } else if (et_house.getText().toString().isEmpty()) {
                    tv_area_error.setVisibility(View.GONE);
                    tv_house_error.setVisibility(View.VISIBLE);
                    tv_floor_error.setVisibility(View.GONE);

                } else if (et_floor.getText().toString().isEmpty()) {
                    tv_area_error.setVisibility(View.GONE);
                    tv_house_error.setVisibility(View.GONE);
                    tv_floor_error.setVisibility(View.VISIBLE);


//               shahil said user can add address without phone ........... //
//                else if (phonestr.isEmpty()) {
//                    tv_phone_error.setVisibility(View.VISIBLE);
//                    tv_phone_error.setText("* Please enter phone number !");
//                    et_phone.requestFocus();
//                    return;
//
                } else if (!isValidMobile(phonestr) && !isValidTelephone(phonestr) && phonestr.length() > 0) {
                    tv_phone_error.setVisibility(View.VISIBLE);
                    tv_phone_error.setText(getResources().getString(R.string.invalid_phone));
                    et_phone.requestFocus();
                    return;

                } else {

                    phonestr = et_phone.getText().toString();
                    tv_phone_error.setVisibility(View.GONE);
                    area = et_area.getText().toString().trim().replace("  ", " ");
                    sector = et_sector.getText().toString().trim().replace("  ", " ");
                    apartment = et_apartment.getText().toString().trim().replace("  ", " ");
                    block = et_block.getText().toString().trim().replace("  ", " ");
                    house = et_house.getText().toString().trim().replace("  ", " ");
                    floor = et_floor.getText().toString().trim().replace("  ", " ");

                    gali = et_gali.getText().toString().trim().replace("  ", " ");
                    kothi = et_kothi.getText().toString().trim().replace("  ", " ");
                    extra = et_extra.getText().toString().trim().replace("  ", " ");

                    AppUtils.writeStringToPref(FullAddressActivity.this, "gali", gali);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "kothi", kothi);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "extra", extra);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "lift", isLift);

                    AppUtils.writeStringToPref(FullAddressActivity.this, "phone", phonestr);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "area", area);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "sector", sector);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "apartment", apartment);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "block", block);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "houseno", house);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "floor", floor);
                    AppUtils.writeStringToPref(FullAddressActivity.this, "userType", "Retails");

                    String strAddress = area + "," + sector + "," + apartment + "," + block + "/" + house + "," + floor + "f";

                    AppUtils.writeStringToPref(FullAddressActivity.this, "address", strAddress);

                    AppUtils.writeStringToPref(FullAddressActivity.this, "companyname", "Firm");

                    if (AppUtils.readStringFromPref(FullAddressActivity.this, "newaddress").contains("addPhone")) {
                        Intent intent1 = new Intent(FullAddressActivity.this, AddProductActivity.class);
                        AppUtils.writeStringToPref(FullAddressActivity.this, "address", strAddress);
                        AppUtils.writeStringToPref(FullAddressActivity.this, "address2", strAddress);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent1);
                        finish();

                    } else {

                        AppUtils.writeStringToPref(FullAddressActivity.this, "AddressAdding", "address");
                        AppUtils.writeStringToPref(FullAddressActivity.this, "address", strAddress);
                        AppUtils.writeStringToPref(FullAddressActivity.this, "address2", strAddress);
                        Intent intent1 = new Intent(FullAddressActivity.this, AddProductActivity.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent1);
                        finish();

                    }
                }
                break;
        }
    }
}

