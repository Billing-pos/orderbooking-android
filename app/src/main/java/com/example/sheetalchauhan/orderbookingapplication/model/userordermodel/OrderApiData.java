package com.example.sheetalchauhan.orderbookingapplication.model.userordermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderApiData {
    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("lock_status")
    @Expose
    private String lock_status;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("invalid")
    @Expose
    private String invalid;

    @SerializedName("receipt_type")
    @Expose
    private String receipt_type;

    @SerializedName("total_amount")
    @Expose
    private String total_amount;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("order_product")
    @Expose
    private List<OrderApiProductData> orderApiProductDataList;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<OrderApiProductData> getOrderApiProductDataList() {
        return orderApiProductDataList;
    }

    public void setOrderApiProductDataList(List<OrderApiProductData> orderApiProductDataList) {
        this.orderApiProductDataList = orderApiProductDataList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInvalid() {
        return invalid;
    }

    public void setInvalid(String invalid) {
        this.invalid = invalid;
    }

    public String getLock_status() {
        return lock_status;
    }

    public void setLock_status(String lock_status) {
        this.lock_status = lock_status;
    }

    public String getReceipt_type() {
        return receipt_type;
    }

    public void setReceipt_type(String receipt_type) {
        this.receipt_type = receipt_type;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
