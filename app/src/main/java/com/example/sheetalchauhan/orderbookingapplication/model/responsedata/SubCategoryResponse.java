package com.example.sheetalchauhan.orderbookingapplication.model.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoryResponse extends ResponsePojo {
    @SerializedName("data")
    @Expose
    private List<ProductSubCategoryData> productSubCategoryData;

    public List<ProductSubCategoryData> getProductSubCategoryData() {
        return productSubCategoryData;
    }

    public void setProductSubCategoryData(List<ProductSubCategoryData> productSubCategoryData) {
        this.productSubCategoryData = productSubCategoryData;
    }
}
