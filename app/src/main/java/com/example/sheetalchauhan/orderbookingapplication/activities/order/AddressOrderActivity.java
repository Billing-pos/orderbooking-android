package com.example.sheetalchauhan.orderbookingapplication.activities.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Order;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressOrderActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_phone)
    TextView tv_phone;
    @BindView(R.id.tv_tags)
    TextView tv_tags;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.bt_repeat_order)
    Button bt_repeat_order;
    @BindView(R.id.tv_order)
    TextView tv_order;
    @BindView(R.id.ll_layout)
    LinearLayout ll_layout;
    String invoiceno, orderitems = "", orderAddress, orderPhone, order_address, tags;
    DBAdapter dbAdapter;
    List<Cart> cartList;
    List<Order> orderList;
    List<CustomergetAddress> customerList;
    long rowid;
    Context context;
    @BindView(R.id.bt_cancel_order)
    Button bt_cancel_order;
    @BindView(R.id.tv_cancel_order)
    TextView tv_cancel_order;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_info_details);
        ButterKnife.bind(this);
        context = this;
        tv_name.setText(R.string.order_details);
        back.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(this);
        orderAddress = AppUtils.readStringFromPref(AddressOrderActivity.this, "orderAddress").replaceAll("/ ", "/");

        dbAdapter.open();

        try {

            String addressOrderId = dbAdapter.getOrderList1(orderAddress);
            cartList = dbAdapter.getOrderProduct(addressOrderId);

            tags = dbAdapter.getTag(orderAddress);
            tv_address.setText("" + orderAddress);

            if (tags.contains("green")) {
                tv_tags.setBackgroundColor(context.getResources().getColor(R.color.colorgreen));

            } else if (tags.contains("red")) {
                tv_tags.setBackgroundColor(context.getResources().getColor(R.color.colorred));

            } else if (tags.contains("yellow")) {
                tv_tags.setBackgroundColor(context.getResources().getColor(R.color.coloryellow));

            } else {
                tv_tags.setBackgroundColor(context.getResources().getColor(R.color.colorwhite));
            }

            if (cartList.size() > 0) {
                order_address = cartList.get(0).getAddress();
                orderPhone = cartList.get(0).getPhone();
                tv_phone.setText("" + cartList.get(0).getPhone());

                for (int i = 0; i < cartList.size(); i++) {
                    if (orderitems.isEmpty()) {
                        orderitems = "- " + cartList.get(i).getProductname();
                    } else {
                        orderitems = orderitems + "\n" + "- " + cartList.get(i).getProductname();
                    }
                }
                tv_order.setText("" + orderitems);

            } else {
                ll_layout.setVisibility(View.GONE);
                Toast.makeText(AddressOrderActivity.this, R.string.no_orderfound, Toast.LENGTH_SHORT).show();
            }


            if (orderList.get(0).getInvalid().equalsIgnoreCase("2")) {
                tv_cancel_order.setVisibility(View.VISIBLE);
                bt_cancel_order.setVisibility(View.GONE);
            } else {
                tv_cancel_order.setVisibility(View.GONE);
                bt_cancel_order.setVisibility(View.VISIBLE);
            }

            bt_cancel_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long rowId = dbAdapter.cancelOrder(invoiceno);
                    if (rowId == -1) {
                    } else {
                        finish();
                    }
                }
            });


            dbAdapter.close();
            bt_repeat_order.setOnClickListener(this);

        } catch (Exception e) {
//            Toast.makeText(context, "Error while create order ...", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_repeat_order:
                try {
                    if (cartList.size() > 0) {

                        try {
                            String userTye = dbAdapter.getCustomerType(order_address);
                            if (userTye != null && !userTye.isEmpty()) {
                                AppUtils.writeStringToPref(this, "userType", userTye);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        dbAdapter.deleteCart();
                        AppUtils.writeStringToPref(AddressOrderActivity.this, "AddressAdding", "normal");
                        for (int i = 0; i < cartList.size(); i++) {
                            Cart cart = new Cart();
                            cart.setProductname(cartList.get(i).getProductname());
                            cart.setProductid(cartList.get(i).getProductid());
                            cart.setPrice(cartList.get(i).getPrice());
                            cart.setPiece(cartList.get(i).getPiece() != null ? cartList.get(i).getPiece() : "null");
                            cart.setQuantity(cartList.get(i).getQuantity());
                            cart.setPhone(orderPhone);
                            cart.setAddress(order_address);
                            rowid = dbAdapter.insertCart(cart);
                        }
                        if (rowid > 0) {
                            AppUtils.writeStringToPref(AddressOrderActivity.this, "address", orderAddress);
                            Intent intent = new Intent(AddressOrderActivity.this, CartListActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        }
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Error to repeat order ...", Toast.LENGTH_SHORT).show();
                }


                break;
        }

    }
}
