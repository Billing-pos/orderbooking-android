package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Customer;

import java.util.ArrayList;
import java.util.List;

public class BetterSearchAdapter extends RecyclerView.Adapter<BetterSearchAdapter.ViewHolder> implements Filterable {
    public String userid;
    Context context;
    View view1;
    List<Customer> addressListList;
    BetterSearchAdapter.ViewHolder viewHolder1;
    private List<Customer> tempList = new ArrayList<>();
    private List<Customer> contactListFiltered;
    private SearchAdapterListener listener;
    private DBAdapter dbAdapter;

    public BetterSearchAdapter(Context context, List<Customer> addressListList, List<Customer> tempList, SearchAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.addressListList = addressListList;
        this.contactListFiltered = addressListList;

        this.tempList = tempList;
        dbAdapter = DBAdapter.getInstance(context);
    }


    @Override
    public BetterSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.better_serach_layout, parent, false);
        viewHolder1 = new BetterSearchAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final BetterSearchAdapter.ViewHolder holder, final int position) {
        if (contactListFiltered == null)
            return;

        holder.name.setText(contactListFiltered.get(position).getFull_new_address());

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onAddressSelected(contactListFiltered.get(position).getAddress());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = addressListList;
                } else {
                    List<Customer> filteredList = new ArrayList<>();
                    for (Customer row : addressListList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getFull_new_address().toLowerCase().contains(charString.toLowerCase()) || row.getFull_new_address().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Customer>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface SearchAdapterListener {
        void onAddressSelected(String address);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.tv_area);
        }
    }

}

