package com.example.sheetalchauhan.orderbookingapplication.model;

public class OrderDetails {

    String Product;
    String Date;

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
