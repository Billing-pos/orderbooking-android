package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.SectorAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.Constants;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.utitlity.SetTabLayout;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.google.android.material.tabs.TabLayout;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddSectorActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.actv_area)
    Spinner actv_area;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.tv_address_error)
    TextView tv_address_error;
    @BindView(R.id.bt_address_search)
    Button bt_address_search;

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_sector)
    EditText et_sector;
    @BindView(R.id.bt_sector)
    Button bt_sector;
    @BindView(R.id.bt_next)
    Button bt_next;
    @BindView(R.id.bt_skip)
    Button bt_skip;
    @BindView(R.id.tv_id)
    TextView tv_id;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    DBAdapter dbAdapter;
    String areaString;
    String hide_layout;

    @BindView(R.id.ll_add_area)
    LinearLayout ll_add_area;
    @BindView(R.id.ll_layout)
    LinearLayout ll_layout;
    List<String> areaList;
    List<String> areaList2;

    @BindView(R.id.et_area2)
    EditText et_area2;
    @BindView(R.id.bt_next2)
    Button bt_next2;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sector);
        ButterKnife.bind(this);
        tv_name.setText(R.string.add_sector_district);
        back.setOnClickListener(this);
        bt_sector.setOnClickListener(this);
        bt_next.setOnClickListener(this);
        bt_next2.setOnClickListener(this);
        bt_skip.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();

        if (AppUtils.readStringFromPref(this, "changeLanguage").equalsIgnoreCase("hindi"))
            areaString = AppUtils.readStringFromPref(AddSectorActivity.this, "area2");
        else
            areaString = AppUtils.readStringFromPref(AddSectorActivity.this, "area");
        tv_id.setText(areaString);
        areaString = AppUtils.readStringFromPref(AddSectorActivity.this, "area");
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(AddSectorActivity.this, 2);
        recyclerview.setLayoutManager(layoutManager);
        areaList2 = dbAdapter.getSectorList(areaString);
        areaList = dbAdapter.getSectorList2(areaString);


        Collections.sort(areaList, (s1, s2) -> s1.compareToIgnoreCase(s2));
        Collections.sort(areaList2, (s1, s2) -> s1.compareToIgnoreCase(s2));

        if (areaList.size() > 0) {
            SectorAdapter sectorAdapter = new SectorAdapter(AddSectorActivity.this, areaList);
            recyclerview.setAdapter(sectorAdapter);
        } else {
            bt_address_search.setVisibility(View.GONE);
        }

        hide_layout = AppUtils.readStringFromPref(AddSectorActivity.this, "hide");

        if (hide_layout.contains("hide")) {
            tv_name.setText(R.string.select_sector);
            et_sector.setVisibility(View.GONE);
            bt_next.setVisibility(View.GONE);
//            bt_sector.setVisibility(View.INVISIBLE);
            //by mj
            bt_sector.setVisibility(View.VISIBLE);
            //
            rl.setVisibility(View.VISIBLE);
            actv();

        } else {
            ll_layout.setVisibility(View.VISIBLE);
            ll_add_area.setVisibility(View.VISIBLE);
        }


        TabLayout tabs = findViewById(R.id.tabs);
        SetTabLayout tabLayout = SetTabLayout.getInstance();
        tabLayout.setTabView(this, areaList, getSupportFragmentManager(), tabs, "sector", "");

    }

    private void actv() {
        bt_address_search.setOnClickListener(this);
        img_close.setOnClickListener(this);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, areaList2);
        actv_area.setAdapter(adapter);

        actv_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!areaList2.isEmpty()) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_skip:
                AppUtils.writeStringToPref(AddSectorActivity.this, "sector", "");
                AppUtils.writeStringToPref(AddSectorActivity.this, "sector2", "");
                Intent intent = new Intent(AddSectorActivity.this, AddApartmentActivity.class);
                intent.putExtra(hide_layout, hide_layout);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_next:

                String sector = et_sector.getText().toString();
                if (sector.isEmpty()) {
                    et_sector.setError("Please enter Sector");
                    et_sector.requestFocus();
                    return;

                } else {
                    AppUtils.writeStringToPref(AddSectorActivity.this, "sector", sector);
                    AppUtils.writeStringToPref(AddSectorActivity.this, "sector2", sector);
                    Intent intent1 = new Intent(AddSectorActivity.this, AddApartmentActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent1);
                }
                break;
            case R.id.bt_next2:
                String sector2 = et_area2.getText().toString();
                if (sector2.isEmpty()) {
                    et_area2.setError("Enter Sector");
                    et_area2.requestFocus();
                    return;

                } else {
                    AppUtils.writeStringToPref(AddSectorActivity.this, "sector", sector2);
                    AppUtils.writeStringToPref(AddSectorActivity.this, "sector2", sector2);
                    Intent intent1 = new Intent(AddSectorActivity.this, AddApartmentActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent1);
                }
                break;

            case R.id.bt_sector:
                final Dialog dialog = new Dialog(AddSectorActivity.this);
                dialog.setContentView(R.layout.password_dialog);
                dialog.show();
                final EditText et_pass = (EditText) dialog.findViewById(R.id.et_password);
                Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);
                bt_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String pass = et_pass.getText().toString();
                        if (pass.equals(Constants.getPaasword())) {

                            dialog.dismiss();
                            et_sector.setEnabled(true);
                            //by mj
                            if (hide_layout.equals("hide")) {
                                et_area2.setEnabled(true);
                            }
                            //
                        } else {
                            Toast.makeText(AddSectorActivity.this, R.string.password_is_wrong, Toast.LENGTH_SHORT).show();
                            et_sector.setEnabled(false);
                        }
                    }
                });
                break;

            case R.id.img_close:
                actv_area.requestFocus();
                img_close.setVisibility(View.GONE);
                actv_area.setEnabled(true);
                break;


            case R.id.bt_address_search:
                String address = "";

                if (areaList.isEmpty() || areaList.size() <= 0) {
                    tv_address_error.setVisibility(View.VISIBLE);
                    tv_address_error.setText("* Please enter sector !");
                    actv_area.requestFocus();
                    return;

                } else {
                    address = actv_area.getSelectedItem().toString();
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(AddSectorActivity.this, "sector", address);
                    AppUtils.writeStringToPref(AddSectorActivity.this, "sector2", address);
                    Intent intent2 = new Intent(AddSectorActivity.this, AddApartmentActivity.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent2);
                }
                break;

        }

    }

    public boolean isValidAddress(String s) {
        boolean isValidAddress = false;

        for (int i = 0; i < areaList.size(); i++) {
            if (areaList.contains(s)) {
                isValidAddress = true;
                break;
            } else {
                isValidAddress = false;
            }
        }
        return isValidAddress;
    }

}
