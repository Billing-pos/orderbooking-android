package com.example.sheetalchauhan.orderbookingapplication.model.ordersync;

import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;

import java.util.List;

public class CustomerOrderData {
    private String invoiceNo;
    private String phone;
    private String address;
    private String invalid;
    private List<Cart> cartList;

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public List<Cart> getCartList() {
        return cartList;
    }

    public void setCartList(List<Cart> cartList) {
        this.cartList = cartList;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInvalid() {
        return invalid;
    }

    public void setInvalid(String invalid) {
        this.invalid = invalid;
    }
}
