package com.example.sheetalchauhan.orderbookingapplication.activities.ManageTag;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.PhoneListTagAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.OrderIdDate;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhoneListTag extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tv_pro_price)
    TextView tv_pro_price;
    @BindView(R.id.tv_pro_qty)
    TextView tv_pro_qty;
    @BindView(R.id.tv_pro_address)
    TextView tv_pro_address;
    @BindView(R.id.tv_pro_name)
    TextView tv_pro_name;
    @BindView(R.id.tv_pro_date)
    TextView tv_pro_date;
    @BindView(R.id.rview_tab)
    RecyclerView rview_tab;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_list)
    TextView tv_list;
    @BindView(R.id.ll_toolbar)
    LinearLayout ll_toolbar;
    @BindView(R.id.back)
    ImageView back;
    String color, id, tableId, OrderId;
    DBAdapter dbAdapter;
    String address;
    List<CustomergetAddress> customerList;
    List<String> listPhone;
    String addressOrderId, addressOrderdate;

    String invoiceno, orderitems = "", orderAddress, orderPhone, order_address;
    List<Cart> cartList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_list_tag);
        ButterKnife.bind(this);
        dbAdapter = DBAdapter.getInstance(PhoneListTag.this);
        dbAdapter.open();

        color = getIntent().getStringExtra("color");
        tv_name.setText(color + " Tag Phone List");
        checkColor();

        String temp = getIntent().getStringExtra("address");
        id = temp;
        checkColor();

        address = AppUtils.readStringFromPref(PhoneListTag.this, "orderAddress");
        List<OrderIdDate> list = new ArrayList<>();


        list = dbAdapter.getOrderIdDate(address);
        for (int i = 0; i < list.size(); i++) {
            addressOrderId = list.get(i).getOrderId();

        }

        cartList = dbAdapter.getOrderProduct(addressOrderId);
        if (cartList.size() > 0) {

            tv_pro_address.setText("Address : " + address);
            tv_pro_name.setText("Product Name : " + cartList.get(0).getProductname());
            tv_pro_qty.setText("Quantity : " + cartList.get(0).getQuantity());
            tv_pro_price.setText("Price : " + cartList.get(0).getPrice());
            tv_pro_date.setText("* " + addressOrderdate);

            for (int i = 0; i < cartList.size(); i++) {
                if (orderitems.isEmpty()) {
                    orderitems = cartList.get(i).getProductname();
                } else {
                    orderitems = orderitems + cartList.get(i).getProductname();
                }
            }
            tv_pro_name.setText("Product Name : " + orderitems);

        } else {
//            ll_layout.setVisibility(View.GONE);
            Toast.makeText(PhoneListTag.this, R.string.no_orderfound, Toast.LENGTH_SHORT).show();
        }


        LinearLayoutManager llm = new LinearLayoutManager(PhoneListTag.this, LinearLayoutManager.VERTICAL, false);
        rview_tab.setLayoutManager(llm);
        back.setOnClickListener(this);

        customerList = dbAdapter.getAllPhoneList(id);


        listPhone = new ArrayList<>();
        tableId = "" + customerList.get(0).getId();
        OrderId = "" + customerList.get(0).getOrderid();
        listPhone.add(customerList.get(0).getmobile());

        if (!customerList.get(0).getmobile2().contains("") || !customerList.get(0).getmobile2().isEmpty()) {
            listPhone.add(customerList.get(0).getmobile2());
        }

        if (!customerList.get(0).getmobile3().contains("") || !customerList.get(0).getmobile3().isEmpty()) {
            listPhone.add(customerList.get(0).getmobile3());
        }
        if (!customerList.get(0).getmobile4().contains("") || !customerList.get(0).getmobile4().isEmpty()) {
            listPhone.add(customerList.get(0).getmobile4());
        }

        if (!customerList.get(0).getmobile5().contains("") || !customerList.get(0).getmobile5().isEmpty()) {
            listPhone.add(customerList.get(0).getmobile5());
        }

        if (customerList.size() > 0) {

            PhoneListTagAdapter addressAdapter = new PhoneListTagAdapter(PhoneListTag.this, listPhone, color);
            rview_tab.setAdapter(addressAdapter);
            tv_list.setVisibility(View.GONE);
            rview_tab.setVisibility(View.VISIBLE);

        } else {
            tv_list.setVisibility(View.VISIBLE);
            rview_tab.setVisibility(View.GONE);
        }
    }

    private void checkColor() {
        if (color.contains("red")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        } else if (color.contains("yellow")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.coloryellow));

        } else if (color.contains("green")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.colorgreen));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }
}
