package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.AreaAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.Constants;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.utitlity.SetTabLayout;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.google.android.material.tabs.TabLayout;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddAreaActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.actv_area)
    Spinner actv_area;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.tv_address_error)
    TextView tv_address_error;
    @BindView(R.id.bt_address_search)
    Button bt_address_search;

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_area)
    EditText et_area;
    @BindView(R.id.bt_add_area)
    Button bt_add_area;
    @BindView(R.id.bt_next)
    Button bt_next;
    @BindView(R.id.rv_recyclerview)
    RecyclerView rv_recyclerview;
    DBAdapter dbAdapter;

    @BindView(R.id.ll_layout)
    LinearLayout ll_layout;
    @BindView(R.id.ll_add_area)
    LinearLayout ll_add_area;
    String hide_layout;
    List<String> areaList;
    List<String> areaList2;

    @BindView(R.id.et_area2)
    EditText et_area2;
    @BindView(R.id.bt_next2)
    Button bt_next2;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_area);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ButterKnife.bind(this);
        tv_name.setText(R.string.add_area);
        dbAdapter = DBAdapter.getInstance(this);
        back.setOnClickListener(this);
        bt_add_area.setOnClickListener(this);
        bt_next.setOnClickListener(this);
        bt_next2.setOnClickListener(this);


        dbAdapter.open();
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(AddAreaActivity.this, 2);
        rv_recyclerview.setLayoutManager(layoutManager);
        areaList2 = dbAdapter.getAreLista();
        areaList = dbAdapter.getAreLista2();

        ////// sorting
        Collections.sort(areaList, (s1, s2) -> s1.compareToIgnoreCase(s2));
        Collections.sort(areaList2, (s1, s2) -> s1.compareToIgnoreCase(s2));

        for (int i = 0; i < areaList.size(); i++) {
            Log.i("data_area", "-" + areaList.get(i) + "-");
        }


        if (areaList.size() > 0) {
            AreaAdapter areaAdapter = new AreaAdapter(AddAreaActivity.this, areaList);
            rv_recyclerview.setAdapter(areaAdapter);
        } else {
//            areaList.add("No Data Found");
            bt_address_search.setVisibility(View.GONE);
        }

        // show and hide edittext and button
        hide_layout = AppUtils.readStringFromPref(AddAreaActivity.this, "hide");

        if (hide_layout.contains("hide")) {
            tv_name.setText(R.string.select_area);
            et_area.setVisibility(View.GONE);
            bt_next.setVisibility(View.GONE);
//            ll_add_area.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);

            actv();


            ll_layout.setVisibility(View.VISIBLE);
        } else {
            ll_layout.setVisibility(View.VISIBLE);
            ll_add_area.setVisibility(View.VISIBLE);
        }
        if (!AppUtils.readStringFromPref(AddAreaActivity.this, "newaddress").contains("noPhone")) {
            et_area.setText("" + AppUtils.readStringFromPref(AddAreaActivity.this, "area"));

        } else {
            et_area.setText("");
        }


        TabLayout tabs = findViewById(R.id.tabs);
        SetTabLayout tabLayout = SetTabLayout.getInstance();
        tabLayout.setTabView(this, areaList, getSupportFragmentManager(), tabs, "area", "");


    }

    private void actv() {
        bt_address_search.setOnClickListener(this);
        img_close.setOnClickListener(this);


        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, areaList2);
        actv_area.setAdapter(adapter);


        actv_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!areaList2.isEmpty()) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_next2:
                String area2 = et_area2.getText().toString().trim().replace("  ", " ");
                if (et_area2.getText().toString().isEmpty()) {
                    et_area2.setError("Enter Area");
                    et_area2.requestFocus();
                    return;

                } else {
                    AppUtils.writeStringToPref(AddAreaActivity.this, "area", area2);
                    AppUtils.writeStringToPref(AddAreaActivity.this, "area2", area2);
                    Intent intent = new Intent(AddAreaActivity.this, AddSectorActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;
            case R.id.bt_next:
                String area = et_area.getText().toString().trim().replace("  ", " ");
                if (area.isEmpty()) {
                    et_area.setError("Please enter Area");
                    et_area.requestFocus();
                    return;

                } else {
                    AppUtils.writeStringToPref(AddAreaActivity.this, "area", area);
                    AppUtils.writeStringToPref(AddAreaActivity.this, "area2", area);
                    Intent intent = new Intent(AddAreaActivity.this, AddSectorActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;
            case R.id.back:
                onBackPressed();
                break;

            case R.id.bt_add_area:
                setDialog();
                break;

            case R.id.img_close:
//                actv_area.setText("");
                actv_area.requestFocus();
                img_close.setVisibility(View.GONE);
                actv_area.setEnabled(true);
                break;


            case R.id.bt_address_search:
                String address = "";

                if (areaList.isEmpty() || areaList.size() <= 0) {
                    tv_address_error.setVisibility(View.VISIBLE);
                    tv_address_error.setText("* Please enter area !");
                    actv_area.requestFocus();
                    return;

                } else {
                    address = actv_area.getSelectedItem().toString();
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(AddAreaActivity.this, "area", address);
                    AppUtils.writeStringToPref(AddAreaActivity.this, "area2", address);
                    Intent intent = new Intent(AddAreaActivity.this, AddSectorActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;
        }

    }

    public boolean isValidAddress(String s) {
        boolean isValidAddress = false;

        for (int i = 0; i < areaList.size(); i++) {
            if (areaList.contains(s)) {
                isValidAddress = true;
                break;
            } else {
                isValidAddress = false;
            }
        }
        return isValidAddress;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void setDialog() {
        final Dialog dialog = new Dialog(AddAreaActivity.this);
        dialog.setContentView(R.layout.password_dialog);
        dialog.show();

        final EditText et_pass = (EditText) dialog.findViewById(R.id.et_password);
        Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pass = et_pass.getText().toString();

                if (pass.equals(Constants.CUSTOMERPASSWORD)) {

                    dialog.dismiss();
                    if (hide_layout.equalsIgnoreCase("hide")) {
                        et_area2.setEnabled(true);
                    }
                    et_area.setEnabled(true);
                } else {
                    Toast.makeText(AddAreaActivity.this, R.string.password_is_wrong, Toast.LENGTH_SHORT).show();
                    //et_area.setEnabled(true);
                }
            }
        });
    }
}
