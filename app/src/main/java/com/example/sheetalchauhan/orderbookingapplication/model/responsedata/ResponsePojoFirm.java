package com.example.sheetalchauhan.orderbookingapplication.model.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePojoFirm {

    @SerializedName("false")
    @Expose
    private boolean error;

    @SerializedName("msg")
    @Expose
    private List<String> msg;

    @SerializedName("firm_code")
    @Expose
    private String firm_code;

    @SerializedName("next_invoice_no")
    @Expose
    private String next_invoice_no;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<String> getMsg() {
        return msg;
    }

    public void setMsg(List<String> msg) {
        this.msg = msg;
    }

    public String getFirm_code() {
        return firm_code;
    }

    public void setFirm_code(String firm_code) {
        this.firm_code = firm_code;
    }

    public String getNext_invoice_no() {
        return next_invoice_no;
    }

    public void setNext_invoice_no(String next_invoice_no) {
        this.next_invoice_no = next_invoice_no;
    }
}
