package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.product.AddProductActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddPhoneActivity2 extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.img_close1)
    ImageView img_close1;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.bt_proceed)
    Button bt_proceed;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_phone_error)
    TextView tv_phone_error;
    String phonestr, address;
    DBAdapter dbAdapter;
    String phoneCount, tableId;

    // to validate phone number & telephone number
    public static boolean isValidMobile(String s) {
        Pattern p = Pattern.compile("(0/91)?[5-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    public static boolean isValidTelephone(String s) {
        Pattern p = Pattern.compile("[2-9][0-9]{7}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_phone);
        ButterKnife.bind(this);
        tv_name.setText(R.string.add_phone_no);
        back.setOnClickListener(this);
        phoneCount = getIntent().getStringExtra("phoneCount");
        tableId = getIntent().getStringExtra("tableId");
        dbAdapter = DBAdapter.getInstance(this);
        bt_proceed.setOnClickListener(this);
        img_close1.setOnClickListener(this);
        address = AppUtils.readStringFromPref(AddPhoneActivity2.this, "address");

        et_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (et_phone.getText().toString().isEmpty()) {
                    img_close1.setVisibility(View.GONE);
                    tv_phone_error.setVisibility(View.GONE);
                } else {
                    img_close1.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_proceed:
                phonestr = et_phone.getText().toString();
                if (phonestr.isEmpty()) {
                    img_close1.setVisibility(View.VISIBLE);
                    tv_phone_error.setVisibility(View.VISIBLE);
                    tv_phone_error.setText(getResources().getString(R.string.enter_phone_no));
                    et_phone.requestFocus();
                    return;

                } else if (!isValidMobile(phonestr) && !isValidTelephone(phonestr)) {
                    img_close1.setVisibility(View.VISIBLE);
                    tv_phone_error.setVisibility(View.VISIBLE);
                    tv_phone_error.setText(getResources().getString(R.string.invalid_phone));
                    et_phone.requestFocus();
                    return;

                } else {
                    AppUtils.writeStringToPref(AddPhoneActivity2.this, "phone", phonestr);
                    Intent intent1 = new Intent(AddPhoneActivity2.this, AddProductActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent1);
                    finish();
                }

                break;

            case R.id.img_close1:
                et_phone.setText("");
                et_phone.requestFocus();
                img_close1.setVisibility(View.GONE);
                break;
        }

    }
}
