package com.example.sheetalchauhan.orderbookingapplication.utility;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Vibrator;
import android.widget.Toast;

import com.example.sheetalchauhan.orderbookingapplication.MainActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.AEMPrinter;
import com.example.sheetalchauhan.orderbookingapplication.activities.AEMScrybeDevice;
import com.example.sheetalchauhan.orderbookingapplication.activities.CardReader;
import com.example.sheetalchauhan.orderbookingapplication.activities.IAemCardScanner;
import com.example.sheetalchauhan.orderbookingapplication.activities.IAemScrybe;

import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.sheetalchauhan.orderbookingapplication.phase_II.BaseActivity.getLocale;

public class AppClass extends Application implements IAemCardScanner, IAemScrybe {
    public static boolean isOrderFirst = false;
    public static AEMScrybeDevice m_AemScrybeDevice;
    private static AppClass instance;
    private static Vibrator vibrator;
    public CardReader m_cardReader = null;
    public AEMPrinter m_AemPrinter = null;
    private Context context;

    public static AppClass getInstance() {
        if (instance == null)
            instance = new AppClass();
        return instance;
    }

    public static Vibrator getVibrator(Context context) {
        if (vibrator == null) {
            Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        }
        return vibrator;
    }

    public static String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }


    // printer connection

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        if (AppUtils.readStringFromPref(context, "changeLanguage") == null || AppUtils.readStringFromPref(context, "changeLanguage").isEmpty()) {
            AppUtils.writeStringToPref(context, "changeLanguage", "english");
            setLocale(context);
        } else {
            setLocale(context);
        }

        printerInitate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setLocale(context);
    }

    public void setLocale(Context context) {
        final Resources resources = context.getResources();
        final Configuration configuration = resources.getConfiguration();
        final Locale locale = getLocale(context);
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    @Override
    public void onScanMSR(String buffer, CardReader.CARD_TRACK cardtrack) {

    }

    @Override
    public void onScanDLCard(String buffer) {

    }

    @Override
    public void onScanRCCard(String buffer) {

    }

    @Override
    public void onScanRFD(String buffer) {

    }

    @Override
    public void onScanPacket(String buffer) {

    }

    @Override
    public void onDiscoveryComplete(ArrayList<String> aemPrinterList) {

    }


    private void printerInitate() {
        m_AemScrybeDevice = new AEMScrybeDevice(this);

        String print = AppUtils.readStringFromPref(this, "printername");
        String printerName = "BTprinter6079";
        try {
            if (print != "") {
//                        Log.i("Printername", print);

//                m_AemScrybeDevice.connectToPrinter(print);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            }
//                Toast.makeText(PrintBluetoothActivity.this, "Printed successfully", Toast.LENGTH_SHORT).show();
//            startService(new Intent(PrintBluetoothActivity.this, PrinterService.class));
            // PrintBluetoothActivity.this.RasterBT(_bitmap);
            //  m_cardReader.readMSR();
        } catch (Exception e) {
            if (e.getMessage().contains("Service discovery failed")) {
                Toast.makeText(this, "Not Connected\n" + printerName + " is unreachable or off otherwise it is connected with other device", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            } else if (e.getMessage().contains("Device or resource busy")) {
                Toast.makeText(this, "the device is already connected", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            } else {
                Toast.makeText(this, "Unable to connect", Toast.LENGTH_SHORT).show();
                AppUtils.writeStringToPref(this, "printername", "");
                // clearPref();
//                Intent intent = new Intent(PrintBluetoothActivity.this, MainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                startActivity(intent);
//                finish();
            }
        }
    }


    public Boolean getPrinterStatus(Context context) {

        if (m_AemScrybeDevice == null) {
            m_AemScrybeDevice = new AEMScrybeDevice(this);
        }

        return m_AemScrybeDevice.BtConnStatus();
    }


}
