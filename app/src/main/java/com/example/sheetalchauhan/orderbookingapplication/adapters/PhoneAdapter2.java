package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.EditPhone;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneAdapter2 extends RecyclerView.Adapter<PhoneAdapter2.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<CustomergetAddress> addressListList;
    int mCheckedPosition = 0;
    PhoneAdapter2.ViewHolder viewHolder1;
    String flag, address1, tableId;
    DBAdapter dbAdapter;
    List<String> listPhone;
    Boolean ischeck = false;
    HideView2 hideView;

    public PhoneAdapter2(Context context, List<CustomergetAddress> addressListList, String flag, String address, List<String> listPhone, String tableId) {
        this.context = context;
        this.addressListList = addressListList;
        this.flag = flag;
        this.address1 = address;
        this.listPhone = listPhone;
        this.tableId = tableId;
        hideView = (HideView2) context;
        dbAdapter = DBAdapter.getInstance(context);
    }

    // to validate phone number and telephone
    public static boolean isValidMobile(String s) {
        Pattern p = Pattern.compile("(0/91)?[5-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    public static boolean isValidTelephone(String s) {
        Pattern p = Pattern.compile("[2-9][0-9]{7}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    public void updateData(List<CustomergetAddress> viewModels) {
        addressListList.clear();
        addressListList.addAll(viewModels);
        notifyDataSetChanged();
    }

    @Override
    public PhoneAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.address_item, parent, false);
        viewHolder1 = new PhoneAdapter2.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final PhoneAdapter2.ViewHolder holder, final int position) {
        Log.i("MOBILE_TEST", listPhone.get(position) + "MJJJ");
        if (AppUtils.readStringFromPref(context, "flag").contains("0")) {
            holder.ratio.setVisibility(View.GONE);
            holder.ll_edit_delete.setVisibility(View.VISIBLE);
            holder.name.setText(listPhone.get(position));
            holder.et_address.setText(listPhone.get(position));


        } else {
            holder.ratio.setVisibility(View.VISIBLE);
            holder.ll_edit_delete.setVisibility(View.VISIBLE);
            holder.name.setText(listPhone.get(position));
            holder.et_address.setText(listPhone.get(position));


            holder.ll_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCheckedPosition = position;
                    notifyDataSetChanged();
                    hideView.hideView(holder.name.getText().toString(), holder.addressid.getText().toString());
                    AppUtils.writeStringToPref(context, "phone", holder.name.getText().toString());
                    AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());
                }
            });

        }


        holder.addressid.setText(tableId);

        holder.ratio.setOnCheckedChangeListener(null);
        if (!ischeck) {
            if (mCheckedPosition == 0) {
                ischeck = true;
                hideView.hideView(holder.name.getText().toString(), holder.addressid.getText().toString());
                AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());
                AppUtils.writeStringToPref(context, "phone", holder.name.getText().toString());
            }
        }
        holder.ratio.setChecked(position == mCheckedPosition);

        holder.ratio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCheckedPosition = position;
                notifyDataSetChanged();
                AppUtils.writeStringToPref(context, "phone", holder.name.getText().toString());
                AppUtils.writeStringToPref(context, "addressid", holder.addressid.getText().toString());
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                holder.et_address.setVisibility(View.VISIBLE);
//                holder.img_address_submit.setVisibility(View.VISIBLE);
//                holder.name.setVisibility(View.GONE);
//                holder.edit.setVisibility(View.GONE);
//                holder.et_address.setVisibility(View.VISIBLE);
//                holder.et_address.requestFocus();
                String column = "";

                if (position == 0) {
                    column = "mobile";

                } else {
                    column = "mobile" + "" + (position + 1);
                }

                Intent intent = new Intent(context, EditPhone.class);
                intent.putExtra("tableId", tableId);
                intent.putExtra("column", column);
                intent.putExtra("mobile", holder.name.getText().toString());
                context.startActivity(intent);

            }
        });

        holder.img_address_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phonestr = holder.et_address.getText().toString();
                if (phonestr.isEmpty()) {
                    holder.et_address.setError("Please Enter Phone No");
                    holder.et_address.requestFocus();
                    return;

                } else if (!isValidMobile(phonestr) && !isValidTelephone(phonestr)) {
                    holder.et_address.setError("invalid mobile !");
                    holder.et_address.requestFocus();
                    return;

                } else {

                    holder.name.setText(holder.et_address.getText().toString());
                    dbAdapter.open();
                    String column = "";

                    if (position == 0) {
                        column = "mobile";

                    } else {
                        column = "mobile" + (position + 1);
                    }

                    long rowId = dbAdapter.updateCustomerMobile(tableId, column, phonestr, "", "");
//                    long rowId = dbAdapter.updateCustomerMobile(phonestr, id, orderId);
                    if (rowId == -1) {
                        Toast.makeText(context, R.string.update_phone_success_not, Toast.LENGTH_SHORT).show();

                    } else {

                        if (AppUtils.readStringFromPref(context, "flag").contains("0")) {
                            holder.name.setText(holder.et_address.getText().toString());

                        } else {
                            holder.name.setText(holder.et_address.getText().toString());
                        }

                        holder.et_address.setVisibility(View.GONE);
                        holder.img_address_submit.setVisibility(View.GONE);
                        holder.name.setVisibility(View.VISIBLE);
                        holder.edit.setVisibility(View.VISIBLE);
                        hideView.hideView(holder.name.getText().toString(), holder.addressid.getText().toString());
                        Toast.makeText(context, R.string.update_phone_success, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return listPhone.size();
    }

    public interface HideView2 {
        void hideView(String address, String id);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, addressid, textview, tv_tag;
        public ImageView deleteadress, edit, img_address_submit;
        public RadioButton ratio;
        public EditText et_address;
        public LinearLayout ll_item;
        public RelativeLayout ll_edit_delete;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.address);
//            deleteadress = (ImageView) v.findViewById(R.id.deleteadress);
            addressid = (TextView) v.findViewById(R.id.addressid);
            ratio = (RadioButton) v.findViewById(R.id.ratio);
            edit = (ImageView) v.findViewById(R.id.edit);
            img_address_submit = (ImageView) v.findViewById(R.id.img_address_submit);
            et_address = (EditText) v.findViewById(R.id.et_address);
            textview = (TextView) v.findViewById(R.id.textview);
            tv_tag = (TextView) v.findViewById(R.id.tv_tag);
            ll_item = (LinearLayout) v.findViewById(R.id.ll_item);
            ll_edit_delete = (RelativeLayout) v.findViewById(R.id.ll_edit_delete);
        }
    }


}