package com.example.sheetalchauhan.orderbookingapplication.phase_II.model;

import com.google.gson.annotations.SerializedName;

public class CustomerUpsyncAddress {

    @SerializedName("address")
    private String address;

    @SerializedName("order_id")
    private String orderid;

    @SerializedName("firm")
    private String firm;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("mobile2")
    private String mobile2;

    @SerializedName("mobile3")
    private String mobile3;

    @SerializedName("mobile4")
    private String mobile4;

    @SerializedName("mobile5")
    private String mobile5;

    @SerializedName("mobile6")
    private String mobile6;

    @SerializedName("mobile7")
    private String mobile7;

    @SerializedName("mobile8")
    private String mobile8;

    @SerializedName("mobile9")
    private String mobile9;

    @SerializedName("mobile10")
    private String mobile10;

    @SerializedName("cartage")
    private int cartage = 0;

    @SerializedName("comments")
    private String comments;

    @SerializedName("tags_new")
    private String tags_new;

    @SerializedName("name")
    private String name;

    @SerializedName("kothi_flate")
    private String kothi_flate;

    @SerializedName("gali_no")
    private String gali_no;

    @SerializedName("extra")
    private String extra;

    @SerializedName("lift")
    private String lift;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public String getMobile3() {
        return mobile3;
    }

    public void setMobile3(String mobile3) {
        this.mobile3 = mobile3;
    }

    public String getMobile4() {
        return mobile4;
    }

    public void setMobile4(String mobile4) {
        this.mobile4 = mobile4;
    }

    public String getMobile5() {
        return mobile5;
    }

    public void setMobile5(String mobile5) {
        this.mobile5 = mobile5;
    }

    public String getMobile6() {
        return mobile6;
    }

    public void setMobile6(String mobile6) {
        this.mobile6 = mobile6;
    }

    public String getMobile7() {
        return mobile7;
    }

    public void setMobile7(String mobile7) {
        this.mobile7 = mobile7;
    }

    public String getMobile8() {
        return mobile8;
    }

    public void setMobile8(String mobile8) {
        this.mobile8 = mobile8;
    }

    public String getMobile9() {
        return mobile9;
    }

    public void setMobile9(String mobile9) {
        this.mobile9 = mobile9;
    }

    public String getMobile10() {
        return mobile10;
    }

    public void setMobile10(String mobile10) {
        this.mobile10 = mobile10;
    }

    public int getCartage() {
        return cartage;
    }

    public void setCartage(int cartage) {
        this.cartage = cartage;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTags_new() {
        return tags_new;
    }

    public void setTags_new(String tags_new) {
        this.tags_new = tags_new;
    }

    public String getKothi_flate() {
        return kothi_flate;
    }

    public void setKothi_flate(String kothi_flate) {
        this.kothi_flate = kothi_flate;
    }

    public String getGali_no() {
        return gali_no;
    }

    public void setGali_no(String gali_no) {
        this.gali_no = gali_no;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getLift() {
        return lift;
    }

    public void setLift(String lift) {
        this.lift = lift;
    }
}
