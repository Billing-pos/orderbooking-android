package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.ManageTag.PhoneListTag;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.Viewholder> {
    Context context;
    List<CustomergetAddress> addressList = new ArrayList<>();
    String color;
    DBAdapter dbAdapter;

    public TagAdapter(Context context, List<CustomergetAddress> addressList, String color) {
        this.addressList.clear();
        this.context = context;
        this.addressList = addressList;
        this.color = color;
    }

    @Override
    public TagAdapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_address, parent, false);
        return new TagAdapter.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(final TagAdapter.Viewholder holder, final int position) {
        if (color.equalsIgnoreCase(addressList.get(position).getTags())) {
            holder.tv_address.setText(addressList.get(position).getAddress());
            holder.ll_address.setVisibility(View.VISIBLE);

        }

        holder.tv_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbAdapter = DBAdapter.getInstance(context);
                dbAdapter.open();

                AppUtils.writeStringToPref(context, "orderAddress", holder.tv_address.getText().toString());
                Intent intent = new Intent(context, PhoneListTag.class);
                intent.putExtra("color", color);
                intent.putExtra("address", addressList.get(position).getAddress());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView tv_address;
        LinearLayout ll_address;

        public Viewholder(View itemView) {
            super(itemView);
            tv_address = itemView.findViewById(R.id.tv_address);
            ll_address = itemView.findViewById(R.id.ll_address);
        }
    }
}
