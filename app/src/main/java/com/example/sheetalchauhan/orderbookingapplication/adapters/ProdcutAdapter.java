package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.product.EquantityActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.product.SubCategoryActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductData;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

public class ProdcutAdapter extends RecyclerView.Adapter<ProdcutAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<ProductData> addressListList;
    ProdcutAdapter.ViewHolder viewHolder1;
    private DBAdapter dbAdapter;


    public ProdcutAdapter(Context context, List<ProductData> addressListList) {
        this.context = context;
        this.addressListList = addressListList;
        dbAdapter = DBAdapter.getInstance(context);
    }

    @Override
    public ProdcutAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.area_layout, parent, false);
        viewHolder1 = new ProdcutAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ProdcutAdapter.ViewHolder holder, final int position) {

        holder.tv_name.setText(addressListList.get(position).getProduct_name());
        holder.tv_price.setText(addressListList.get(position).getPrice());
        holder.tv_subcategory.setText(addressListList.get(position).getSubcategory());

        String category_id = AppUtils.readStringFromPref(context, "category_id");
        if (category_id != null && category_id.equalsIgnoreCase("Whole sale")) {
            String tempAddress = AppUtils.readStringFromPref(context, "address");
            String price = dbAdapter.getwholesalePrice(addressListList.get(position).getProduct_id(), tempAddress.toLowerCase());

            try {
                if (price != null && !price.isEmpty() && Float.parseFloat(price) > 0) {
                    holder.tv_price.setText(price);
                } else {
                    holder.tv_price.setText(addressListList.get(position).getWholesale_price());
                }
            } catch (Exception e) {
                holder.tv_price.setText(addressListList.get(position).getWholesale_price());
            }

        } else {
            holder.tv_price.setText(addressListList.get(position).getPrice());
        }


//        if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
        holder.tv_id.setText(addressListList.get(position).getProduct_id());
//        } else
//            holder.tv_id.setText(addressListList.get(position).getProduct_name());

        holder.tv_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.tv_subcategory.getText().toString().equals("0")) {
                    AppUtils.writeStringToPref(context, "productname", holder.tv_name.getText().toString());
                    AppUtils.writeStringToPref(context, "productprice", holder.tv_price.getText().toString());
                    AppUtils.writeStringToPref(context, "subcategory", holder.tv_subcategory.getText().toString());
                    AppUtils.writeStringToPref(context, "product_id", holder.tv_id.getText().toString());
                    Intent intent = new Intent(context, EquantityActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    context.startActivity(intent);

                } else {
                    AppUtils.writeStringToPref(context, "productname", holder.tv_name.getText().toString());
                    AppUtils.writeStringToPref(context, "productprice", holder.tv_price.getText().toString());
                    AppUtils.writeStringToPref(context, "subcategory", holder.tv_subcategory.getText().toString());
                    AppUtils.writeStringToPref(context, "product_id", holder.tv_id.getText().toString());
                    Intent intent = new Intent(context, SubCategoryActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    context.startActivity(intent);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_id, tv_name, tv_price, tv_subcategory;


        public ViewHolder(View v) {
            super(v);
            tv_id = (TextView) v.findViewById(R.id.tv_area);
            tv_name = (TextView) v.findViewById(R.id.tv_name);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            tv_subcategory = (TextView) v.findViewById(R.id.tv_subcategory);

        }
    }


}
