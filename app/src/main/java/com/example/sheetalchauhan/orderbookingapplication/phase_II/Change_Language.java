package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.MainActivity;
import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.ResponsePojoisSelect;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.FirmData;
import com.example.sheetalchauhan.orderbookingapplication.model.isFirmSelected;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductData;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductResponseDto;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.SubCategoryResponse;
import com.example.sheetalchauhan.orderbookingapplication.netwowk.NetworkStatus;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CommentDataResponseModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CommentResponseModel;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppClass;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.HandleError;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Change_Language extends AppCompatActivity {
    List<Firm> frimList = new ArrayList<>();
    private TextView tv_current_lang, tv_name;
    private ImageView back;
    private Button btn_c_lang;
    private Context context;
    private DBAdapter dbAdapter;
    private String language = "en";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change__language);
        context = this;
        setId();
        setClick();
        tv_name.setText(R.string.switch_language);
        tv_current_lang.setText(R.string.current_language);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dbAdapter = DBAdapter.getInstance(context);
        dbAdapter.open();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dbAdapter.close();
    }

    private void setClick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToHome();
            }
        });

        btn_c_lang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                    language = "hi";
                    AppUtils.writeStringToPref(context, "changeLanguage", "hindi");
                    AppClass.getInstance().setLocale(context);

                } else {
                    language = "en";
                    AppUtils.writeStringToPref(context, "changeLanguage", "english");
                    AppClass.getInstance().setLocale(context);
                }
                commonApiFor7andAllDays();
            }
        });
    }

    private void setId() {
        tv_current_lang = findViewById(R.id.tv_current_lang);
        tv_name = findViewById(R.id.tv_name);
        back = findViewById(R.id.back);
        btn_c_lang = findViewById(R.id.btn_c_lang);
    }

    @Override
    public void onBackPressed() {
        goToHome();
    }

    public void goToHome() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }

    private void commonApiFor7andAllDays() {
        if (NetworkStatus.getConnectivity(context)) {
            // firm list
            new AsyncTaskForFirmList().execute("firm-list");
            // comment list
            new AsyncTaskForComment().execute("comment-list");
            // product list
//            dialog = new ProgressDialog(this);
//            dialog.setMessage("Language is Changing ...");
//            dialog.setCancelable(false);
//            dialog.show();
            new AsyncTaskForProductList().execute("product-list");
            // product's subcategory list
            new AsyncTaskForSubcategoryList().execute("sub-cat-list");

            new AsyncTaskRunner().execute("isselected-firm");

            goToHome();
        }
    }

    private String deviceId() {
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    private class AsyncTaskForFirmList extends AsyncTask<String, String, String> {
        private String resp = "response";

        @Override
        protected String doInBackground(String... params) {
            Call<FirmData> call = RetrofitHandler.getInstance().getApi().getFirmList("1", language);
            call.enqueue(new Callback<FirmData>() {
                @Override
                public void onResponse(Call<FirmData> call, Response<FirmData> response) {
                    FirmData responseDto = response.body();

                    if (response.isSuccessful()) {
                        if (responseDto.isError() == false) {

                            dbAdapter.deleteFirm();

                            for (int i = 0; i < responseDto.getFirmList().size(); i++) {
                                long row_id = dbAdapter.insertFirmData(responseDto.getFirmList().get(i));
                            }

                        } else {
                            Toast.makeText(context, "" + response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<FirmData> call, Throwable t) {
                    HandleError.handleTimeoutError(context, t, "1480");
                }
            });
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class AsyncTaskForComment extends AsyncTask<String, String, String> {
        private String resp = "comment-list";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(context)) {
                Change_Language.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Call<CommentResponseModel> call = RetrofitHandler.getInstance().getApi().getAllComment("1", language);
                call.enqueue(new Callback<CommentResponseModel>() {
                    @Override
                    public void onResponse(Call<CommentResponseModel> call, Response<CommentResponseModel> response) {

                        CommentResponseModel commentResponseModel = response.body();
                        if (commentResponseModel != null) {

                            try {
                                dbAdapter.deleteCommentTable();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            for (int i = 0; i < commentResponseModel.getData().size(); i++) {
                                CommentDataResponseModel model = commentResponseModel.getData().get(i);
                                long rowid = dbAdapter.insertComment(model);
                                if (rowid > -1) {
                                    Log.i("isDateinserted2", "yes" + rowid);
                                } else {
                                    Log.i("isDateinserted2", "no" + rowid);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommentResponseModel> call, Throwable t) {
                        HandleError.handleTimeoutError(context, t, "1297");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class AsyncTaskForProductList extends AsyncTask<String, String, String> {
        private String resp = "product-list";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(context)) {
                Change_Language.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Call<ProductResponseDto> call = RetrofitHandler.getInstance().getApi().getProdcutList("1", language);
                call.enqueue(new Callback<ProductResponseDto>() {
                    @Override
                    public void onResponse(Call<ProductResponseDto> call, Response<ProductResponseDto> response) {
                        ProductResponseDto responseDto = response.body();

                        if (response.isSuccessful()) {
                            if (responseDto.isError() == false) {

                                dbAdapter.deleteProduct();

                                if (responseDto.getProductDataList().size() > 0) {
                                    for (int i = 0; i < responseDto.getProductDataList().size(); i++) {
                                        ProductData productData = new ProductData();
                                        productData.setProduct_id(responseDto.getProductDataList().get(i).getProduct_id());
                                        productData.setPrice(responseDto.getProductDataList().get(i).getPrice());
//                                        if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
//                                            productData.setProduct_name(responseDto.getProductDataList().get(i).getProduct_id());
//                                        } else
                                        productData.setProduct_name(responseDto.getProductDataList().get(i).getProduct_name());
                                        productData.setSubcategory(responseDto.getProductDataList().get(i).getSubcategory());
                                        productData.setWholesale_price(responseDto.getProductDataList().get(i).getWholesale_price());
                                        Log.i("user_prod", productData.getProduct_name());
                                        long rowid = dbAdapter.insertProduct(productData);
                                    }
//                                    dialog.dismiss();
                                }
                            } else {
                                Toast.makeText(context, responseDto.getMsg().get(0), Toast.LENGTH_LONG).show();
                            }

                        } else {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductResponseDto> call, Throwable t) {
                        HandleError.handleTimeoutError(context, t, "1363");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class AsyncTaskForSubcategoryList extends AsyncTask<String, String, String> {
        private String resp = "sub-cat-list";

        @Override
        protected String doInBackground(String... params) {
            if (!NetworkStatus.getConnectivity(context)) {
                Change_Language.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {

                Call<SubCategoryResponse> call = RetrofitHandler.getInstance().getApi().getSubCategory("1", language);
                call.enqueue(new Callback<SubCategoryResponse>() {
                    @Override
                    public void onResponse(Call<SubCategoryResponse> call, Response<SubCategoryResponse> response) {
                        SubCategoryResponse responseDto = response.body();

                        if (response.isSuccessful()) {
                            if (responseDto.isError() == false) {


                                dbAdapter.deleteSubCategory();
                                for (int i = 0; i < responseDto.getProductSubCategoryData().size(); i++) {
//                                    Log.i("user_subcate", responseDto.getProductSubCategoryData().get(i).getSubcategory_name());
                                    long row_id = dbAdapter.insertSubCategory(responseDto.getProductSubCategoryData().get(i));
                                }
                            } else {
                                Toast.makeText(context, "" + responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "" + response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SubCategoryResponse> call, Throwable t) {
                        HandleError.handleTimeoutError(context, t, "1421");
                    }
                });
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        private String resp = "response";

        @Override
        protected String doInBackground(String... params) {
            isFirmSelected model = new isFirmSelected();
            model.setDeviceId("" + deviceId());
            Call<ResponsePojoisSelect> call = RetrofitHandler.getInstance().getApi().sendisSelectedFirm(model);
            call.enqueue(new Callback<ResponsePojoisSelect>() {
                @Override
                public void onResponse(Call<ResponsePojoisSelect> call, Response<ResponsePojoisSelect> response) {

                    AppUtils.writeBoolToPref(Change_Language.this, "isFirstTimeInstall", true);
                    ResponsePojoisSelect responseDto = response.body();

                    if (response.isSuccessful()) {

                        if (responseDto.isError() == false) {
                            String code = null;
                            code = responseDto.getcode();

                            try {
                                if (code != null) {
                                    AppUtils.writeStringToPref(Change_Language.this, "firmcode", responseDto.getcode());
                                    frimList = dbAdapter.getFirmName(responseDto.getcode());
                                    AppUtils.writeStringToPref(Change_Language.this, "firmname", frimList.get(0).getName());
                                    AppUtils.writeStringToPref(Change_Language.this, "firmaddress", frimList.get(0).getAddress());
                                    AppUtils.writeStringToPref(Change_Language.this, "firmmobile", frimList.get(0).getMobile());
                                }
                            } catch (Exception e) {
//                                Toast.makeText(MainActivity.this, "Please select firm", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponsePojoisSelect> call, Throwable t) {
                    HandleError.handleTimeoutError(context, t, "920");
                }
            });

            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }


        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

}
