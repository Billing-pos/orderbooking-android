package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;

import butterknife.BindView;

public class EditCustomerActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.et_area)
    EditText et_area;
    @BindView(R.id.et_sector)
    EditText et_sector;
    @BindView(R.id.et_apartment)
    EditText et_apartment;
    @BindView(R.id.et_block)
    EditText et_block;
    @BindView(R.id.et_house)
    EditText et_house;
    @BindView(R.id.et_floor)
    EditText et_floor;
    @BindView(R.id.bt_submit)
    Button bt_submit;

    String phone, area, sector, apartment, block, house, floor;
    DBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_customer);
    }

    @Override
    public void onClick(View v) {

    }
}
