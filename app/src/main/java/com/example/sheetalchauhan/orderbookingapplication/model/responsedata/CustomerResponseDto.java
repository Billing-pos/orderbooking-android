package com.example.sheetalchauhan.orderbookingapplication.model.responsedata;

import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerResponseDto extends ResponsePojo {
    @SerializedName("data")
    @Expose
    private List<CustomergetAddress> customerResponseDataList;

    @SerializedName("page")
    @Expose
    private String page;

    public List<CustomergetAddress> getCustomerResponseDataList() {
        return customerResponseDataList;
    }

    public void setCustomerResponseDataList(List<CustomergetAddress> customerResponseDataList) {
        this.customerResponseDataList = customerResponseDataList;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
