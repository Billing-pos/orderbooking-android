package com.example.sheetalchauhan.orderbookingapplication.activities.ManageTag;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.OrderIdDate;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhoneLastOrder extends AppCompatActivity {
    @BindView(R.id.tv_pro_price)
    TextView tv_pro_price;
    @BindView(R.id.tv_pro_qty)
    TextView tv_pro_qty;
    @BindView(R.id.tv_pro_address)
    TextView tv_pro_address;
    @BindView(R.id.tv_pro_name)
    TextView tv_pro_name;
    @BindView(R.id.tv_pro_date)
    TextView tv_pro_date;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.ll_toolbar)
    LinearLayout ll_toolbar;

    String invoiceno, orderitems = "", orderAddress, orderPhone, order_address;
    List<Cart> cartList;
    DBAdapter dbAdapter;
    String orderphonenumber, addressOrderId, addressOrderdate = "date", color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_last_order);
        ButterKnife.bind(this);
        dbAdapter = DBAdapter.getInstance(PhoneLastOrder.this);
        dbAdapter.open();

        color = getIntent().getStringExtra("color");
        tv_name.setText("Phone last order");
        checkColor();

        orderphonenumber = getIntent().getStringExtra("ordernumber");

        List<OrderIdDate> list = new ArrayList<>();

        list = dbAdapter.getOrderIdDate2(orderphonenumber);
        for (int i = 0; i < list.size(); i++) {
            addressOrderId = list.get(i).getOrderId();
        }

        cartList = dbAdapter.getOrderProduct(addressOrderId);
        if (cartList.size() > 0) {

            tv_pro_address.setText("Phone : " + orderphonenumber);
            tv_pro_name.setText("Product Name : " + cartList.get(0).getProductname());
            tv_pro_qty.setText("Quantity : " + cartList.get(0).getQuantity());
            tv_pro_price.setText("Price : " + cartList.get(0).getPrice());
            tv_pro_date.setText("* " + addressOrderdate);

            for (int i = 0; i < cartList.size(); i++) {
                if (orderitems.isEmpty()) {
                    orderitems = cartList.get(i).getProductname();
                } else {
                    orderitems = orderitems + cartList.get(i).getProductname();
                }
            }
            tv_pro_name.setText("Product Name : " + orderitems);

        } else {
//            ll_layout.setVisibility(View.GONE);
            Toast.makeText(PhoneLastOrder.this, R.string.no_orderfound, Toast.LENGTH_SHORT).show();
        }


        tv_pro_address.setText("Phone : " + orderphonenumber);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void checkColor() {
        if (color.contains("red")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.colorred));

        } else if (color.contains("yellow")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.coloryellow));

        } else if (color.contains("green")) {
            ll_toolbar.setBackgroundColor(getResources().getColor(R.color.colorgreen));
        }
    }
}
