package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.order.CartListActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Order;
import com.example.sheetalchauhan.orderbookingapplication.model.ordersync.CustomerOrderData;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PreviousOrderAdapter extends RecyclerView.Adapter<PreviousOrderAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<CustomerOrderData> customerOrderDataList;
    int mCheckedPosition;
    PreviousOrderAdapter.ViewHolder viewHolder1;
    int flag;
    Long rowid = null;
    DBAdapter dbAdapter;
    List<Cart> cartList = new ArrayList<>();
    List<Order> orderList = new ArrayList<>();
    String[] add = null;
    String[] add2 = null;
    private cancelListener listener;

    public PreviousOrderAdapter(Activity activity, Context context, List<CustomerOrderData> customerOrderDataList) {
        this.context = context;
        this.customerOrderDataList = customerOrderDataList;
        dbAdapter = DBAdapter.getInstance(context);
        listener = (cancelListener) activity;
    }

    @Override
    public PreviousOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.repeat_order, parent, false);
        viewHolder1 = new PreviousOrderAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final PreviousOrderAdapter.ViewHolder holder, final int position) {
        holder.tv_orderid.setText(customerOrderDataList.get(position).getInvoiceNo());
        for (int i = 0; i < customerOrderDataList.get(position).getCartList().size(); i++) {
            holder.tv_name.setText("" + customerOrderDataList.get(position).getCartList().get(i).getProductname() + "\n");

        }
        holder.bt_repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbAdapter.open();
                orderList = dbAdapter.getOrderList(customerOrderDataList.get(position).getInvoiceNo());
                cartList = dbAdapter.getOrderProduct(customerOrderDataList.get(position).getInvoiceNo());
                dbAdapter.deleteCart();

                String phone = orderList.get(0).getPhone();
                String address = orderList.get(0).getAddress();

                if (cartList.size() > 0) {

                    AppUtils.writeStringToPref(context, "AddressAdding", "normal");
                    for (int i = 0; i < cartList.size(); i++) {
                        Cart cart = new Cart();
                        cart.setProductname(cartList.get(i).getProductname());
                        cart.setProductid(cartList.get(i).getProductid());
                        cart.setPrice(cartList.get(i).getPrice());
                        cart.setPiece(cartList.get(i).getPiece() != null ? cartList.get(i).getPiece() : "null");
                        cart.setQuantity(cartList.get(i).getQuantity());
                        cart.setAmount("" + cartList.get(i).getAmount());
                        cart.setPhone(phone);
                        cart.setAddress(address);
                        rowid = dbAdapter.insertCart(cart);
                    }


                    if (rowid != null) {
                        if (rowid > 0) {
                            AppUtils.writeStringToPref(context, "phone", phone);
                            AppUtils.writeStringToPref(context, "address", address);

                            add = address.split(",");
                            add2 = add[3].split("/");
                            AppUtils.writeStringToPref(context, "area", add[0]);
                            AppUtils.writeStringToPref(context, "sector", add[1]);
                            AppUtils.writeStringToPref(context, "apartment", add[2]);
                            AppUtils.writeStringToPref(context, "block", add2[0]);
                            AppUtils.writeStringToPref(context, "houseno", add2[1]);
                            AppUtils.writeStringToPref(context, "floor", add[4]);

                            Intent intent = new Intent(context, CartListActivity.class);
                            intent.putExtra("invalid", "1");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            context.startActivity(intent);
                        }
                    } else {
                        Log.i("testing", "data");
                    }
                }

            }
        });

        if (customerOrderDataList.get(position).getInvalid().equalsIgnoreCase("2")) {
            holder.tv_cancel_order.setVisibility(View.VISIBLE);
            holder.bt_cancel_order.setVisibility(View.GONE);
        } else {
            holder.tv_cancel_order.setVisibility(View.GONE);
            holder.bt_cancel_order.setVisibility(View.VISIBLE);
        }

        holder.bt_cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.cancelOrder(customerOrderDataList.get(position).getInvoiceNo());
            }
        });

    }

    @Override
    public int getItemCount() {
        return customerOrderDataList.size();
    }

    public String removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        return "" + bdprice;
    }


    public interface cancelListener {
        void cancelOrder(String orderId);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_orderid, tv_name, tv_cancel_order;
        public Button bt_repeat, bt_cancel_order;

        public ViewHolder(View v) {
            super(v);
            tv_cancel_order = (TextView) v.findViewById(R.id.tv_cancel_order);
            tv_orderid = (TextView) v.findViewById(R.id.tv_orderid);
            tv_name = (TextView) v.findViewById(R.id.tv_product);
            bt_repeat = (Button) v.findViewById(R.id.bt_repeat_order);
            bt_cancel_order = (Button) v.findViewById(R.id.bt_cancel_order);

        }
    }

}
