package com.example.sheetalchauhan.orderbookingapplication.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.example.sheetalchauhan.orderbookingapplication.R;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.xmlpull.v1.XmlPullParserException;

import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class HandleError {

    //    private static long snoozeTime = 180000;
    private static long snoozeTime = 3600000;

    public static void checkErrorType(Context context, Throwable error) {
        try {
            if (error instanceof NoConnectionError) {
                ConnectivityManager cm = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = null;
                if (cm != null) {
                    activeNetwork = cm.getActiveNetworkInfo();
                }
                if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                    Toast.makeText(context, context.getResources().getString(R.string.yourdeviceisnotconnected), Toast.LENGTH_SHORT).show();
                }
            } else if (error instanceof MalformedURLException) {
                Toast.makeText(context, context.getResources().getString(R.string.badrequest), Toast.LENGTH_SHORT).show();

            } else if (error instanceof ParseError || error instanceof IllegalStateException || error instanceof JSONException || error instanceof XmlPullParserException) {
                Toast.makeText(context, context.getResources().getString(R.string.parseerror), Toast.LENGTH_SHORT).show();

            } else if (error instanceof OutOfMemoryError) {
                Toast.makeText(context, context.getResources().getString(R.string.outofmemoryerror), Toast.LENGTH_SHORT).show();

            } else if (error instanceof AuthFailureError) {
                Toast.makeText(context, context.getResources().getString(R.string.servercoudntfind), Toast.LENGTH_SHORT).show();

            } else if (error instanceof ServerError || error instanceof ServerError) {
                Toast.makeText(context, context.getResources().getString(R.string.serverisnotrespond), Toast.LENGTH_SHORT).show();

            } else if (error instanceof TimeoutError || error instanceof SocketTimeoutException || error instanceof ConnectTimeoutException || error instanceof SocketException || (error.getMessage() != null && error.getMessage().equalsIgnoreCase("Connection timed out"))) {
                Toast.makeText(context, context.getResources().getString(R.string.connectiontimeout), Toast.LENGTH_SHORT).show();

            } else {
//                Toast.makeText(context, context.getResources().getString(R.string.anunknownerror), Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Toast.makeText(context, context.getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

        }
    }

    public static void handleTimeoutError(Context context, Throwable t, String line) {
        Log.i("handle_error", "name - " + t.getMessage() + "line - " + line + "  -- " + AppUtils.readStringFromPref(context, "order_page"));

        if (t.getMessage() != null && context != null) {
            if (t.getMessage().equalsIgnoreCase(context.getResources().getString(R.string.error_when_failed))) {
                Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            } else if (t.getMessage().equalsIgnoreCase(context.getResources().getString(R.string.connection_timeout)))
                Toast.makeText(context, R.string.connectiontimeout, Toast.LENGTH_SHORT).show();
            else
                checkErrorType(context, t);
        }
    }

    public static void setSnoozingTime(Context context, int snoozesetTime) {
        snoozeTime = snoozesetTime * 60 * 1000;


    }

    public static long getSnoozingTime(Context context) {
        return snoozeTime;
    }
}
