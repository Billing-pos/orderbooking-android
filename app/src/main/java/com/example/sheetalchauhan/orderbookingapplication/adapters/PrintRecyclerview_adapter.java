package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.ItemShape;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PrintRecyclerview_adapter extends RecyclerView.Adapter<PrintRecyclerview_adapter.ViewHolder> {
    Context context;
    List<Cart> cartList = new ArrayList<>();
    int sizeofList = 0;
    PrintRecyclerview_adapter.ViewHolder viewHolder1;
    int amountFinal;
    private float total_amount = 0;
    private float amt_cartage = 0, amt_dis_cartage = 0, amt_dis_retail = 0, amt_total = 0, to_be_paid = 0;


    public PrintRecyclerview_adapter(Context context, List<Cart> cartList, float amt_cartage, float amt_dis_cartage, float amt_dis_retail, float amt_total, float to_be_paid) {
        this.context = context;
        this.cartList = cartList;
        sizeofList = cartList.size();

        this.amt_cartage = amt_cartage;
        this.amt_dis_cartage = amt_dis_cartage;
        this.amt_dis_retail = amt_dis_retail;
        this.amt_total = amt_total;
        this.to_be_paid = to_be_paid;
    }

    public PrintRecyclerview_adapter(Context applicationContext, List<Cart> list) {
        this.context = applicationContext;
        this.cartList = list;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_print, parent, false);
        viewHolder1 = new PrintRecyclerview_adapter.ViewHolder(view);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(PrintRecyclerview_adapter.ViewHolder holder, final int position) {

        if (position == 0) {
            holder.lnr_print_bill.setVisibility(View.VISIBLE);

        } else {
            holder.lnr_print_bill.setVisibility(View.GONE);
        }


        String name = cartList.get(position).getProductname();
        int leng = name.length();
        int size = leng / 16;
        int mod = leng % 16;
        if (mod > 0) {
            size = size + 1;
        }

        String tem = "";
        int num = 0;
        for (int i = 0; i < size; i++) {
            if (i + 1 == size) {
                tem = tem + name.substring(num);
            } else {
                tem = tem + name.substring(num, (num + 16)) + "\n";
            }
            num = num + 16;
        }

        ItemShape itemShape = new ItemShape();

        String tempQty = cartList.get(position).getQuantity().replace(".", "-");
        String arr[] = cartList.get(position).getProductname().split(" \\(");


        try {
            String[] b = tempQty.split("\\-");

            if (itemShape.shape().get(arr[0].toLowerCase()) != null) {
                Integer drawable_name = itemShape.shape().get(arr[0].toLowerCase());
                holder.tv_qty.setBackground(ContextCompat.getDrawable(context, drawable_name));
            }

            if (b[1].length() == 2) {
                holder.tv_qty.setText("" + tempQty + "0");
            } else
                holder.tv_qty.setText("" + tempQty);

        } catch (Exception e) {
            try {
                holder.tv_qty.setText("" + tempQty + "-000");
            } catch (Exception e1) {
                holder.tv_qty.setText("" + tempQty);
            }
        }

        holder.sno_header.setVisibility(View.GONE);
        holder.sno_rl.setVisibility(View.GONE);
        holder.tv_sNo.setText("" + (position + 1));
        holder.tv_particlulars.setText("" + tem);
        holder.tv_price.setText("" + cartList.get(position).getPrice());

        if (AppUtils.readStringFromPref(context, "userType").equalsIgnoreCase("Whole sale")) {
            if (cartList.get(position).getPiece() != null && !cartList.get(position).getPiece().isEmpty() && !cartList.get(position).getPiece().equalsIgnoreCase("null")) {
                holder.rv_piece.setVisibility(View.VISIBLE);
                holder.rel_piece.setVisibility(View.VISIBLE);
                holder.relative_p.setVisibility(View.VISIBLE);
                holder.rev_pie.setVisibility(View.VISIBLE);
                holder.rev_piece.setVisibility(View.VISIBLE);
                holder.rv_p.setVisibility(View.VISIBLE);
                holder.r_piece.setVisibility(View.VISIBLE);
                holder.tv_piece.setText(cartList.get(position).getPiece());
                holder.rl_qty.setVisibility(View.GONE);
                holder.rl_header_qty.setVisibility(View.GONE);
                holder.rl_belowqty.setVisibility(View.GONE);
                holder.rl_below_qty_ctg.setVisibility(View.GONE);
                holder.rl_below_qty_dis.setVisibility(View.GONE);
                holder.rl_below_qty_rdis.setVisibility(View.GONE);
                holder.rl_below_qty_f_amt.setVisibility(View.GONE);
            } else {
                holder.rv_piece.setVisibility(View.GONE);
                holder.rel_piece.setVisibility(View.GONE);
                holder.relative_p.setVisibility(View.GONE);
                holder.rev_pie.setVisibility(View.GONE);
                holder.rev_piece.setVisibility(View.GONE);
                holder.r_piece.setVisibility(View.GONE);
                // holder.rv_.setVisibility(View.GONE);
            }
        }

        String temp_amount = "";

        temp_amount = cartList.get(position).getAmount();

        if (temp_amount == null || temp_amount.equals("null")) {
            Double tempMoney = Double.parseDouble(cartList.get(position).getPrice()) * Double.parseDouble(cartList.get(position).getQuantity());
            BigDecimal amount = removeDecimalPrice("" + tempMoney);
            holder.tv_amount.setText("" + amount);

        } else {
            holder.tv_amount.setText("" + cartList.get(position).getAmount());
        }

        if (position == (sizeofList - 1)) {

            holder.tv_total_final_adv.setText("" + Math.round(to_be_paid));
            holder.tv_total_final.setText("" + Math.round(amt_total));
            holder.tv_cartridge.setText("" + Math.round(amt_cartage));
            holder.tv_discount.setText("- " + Math.round(amt_dis_cartage));
            holder.tv_rdiscount.setText("- " + Math.round(amt_dis_retail));
            holder.ll_total.setVisibility(View.VISIBLE);
            holder.ll_cartridge.setVisibility(View.VISIBLE);
            holder.ll_total_final.setVisibility(View.VISIBLE);
            holder.ll_after_advance_payment.setVisibility(View.VISIBLE);
            holder.view_bottom.setVisibility(View.VISIBLE);

            if (amt_dis_cartage > 0)
                holder.ll_discount.setVisibility(View.VISIBLE);
            else {
                holder.ll_discount.setVisibility(View.GONE);
            }
            if (amt_dis_retail > 0)
                holder.ll_rdiscount.setVisibility(View.VISIBLE);
            else {
                holder.ll_rdiscount.setVisibility(View.GONE);
            }
        } else {
            holder.tv_cartridge.setText("0");
            holder.tv_rdiscount.setText("0");
            holder.tv_discount.setText("0");
            holder.ll_total.setVisibility(View.GONE);
            holder.ll_cartridge.setVisibility(View.GONE);
            holder.ll_discount.setVisibility(View.GONE);
            holder.ll_total_final.setVisibility(View.GONE);
            holder.ll_after_advance_payment.setVisibility(View.GONE);
            holder.view_bottom.setVisibility(View.GONE);
        }


    }


    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public BigDecimal removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);

        return bdprice;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View view_bottom;
        public LinearLayout ll_total, ll_cartridge, ll_discount, ll_rdiscount, ll_total_final, lnr_print_bill, ll_after_advance_payment;
        public TextView tv_total_final, tv_total_final_adv, tv_discount, tv_rdiscount, tv_cartridge, tv_total;

        public TextView tv_sNo, tv_qty, tv_particlulars, tv_price, tv_amount, tv_piece;
        public RelativeLayout rv_piece, rel_piece, rev_piece, rv_p, rev_pie, relative_p, r_piece,
                rl_qty, rl_header_qty, rl_belowqty, rl_below_qty_ctg, rl_below_qty_dis, rl_below_qty_rdis,
                rl_below_qty_f_amt, sno_header, sno_rl;

        public ViewHolder(View itemView) {
            super(itemView);

            view_bottom = itemView.findViewById(R.id.view_bottom);
            ll_total = itemView.findViewById(R.id.ll_total);
            ll_cartridge = itemView.findViewById(R.id.ll_cartridge);
            ll_discount = itemView.findViewById(R.id.ll_discount);
            ll_rdiscount = itemView.findViewById(R.id.ll_rdiscount);
            ll_total_final = itemView.findViewById(R.id.ll_total_final);
            ll_after_advance_payment = itemView.findViewById(R.id.ll_after_advance_payment);

            tv_total_final_adv = itemView.findViewById(R.id.tv_total_final_adv);
            tv_total_final = itemView.findViewById(R.id.tv_total_final);
            tv_discount = itemView.findViewById(R.id.tv_discount);
            tv_rdiscount = itemView.findViewById(R.id.tv_rdiscount);
            tv_cartridge = itemView.findViewById(R.id.tv_cartridge);
            tv_total = itemView.findViewById(R.id.tv_total);

            tv_sNo = itemView.findViewById(R.id.tv_sNo);
            tv_qty = itemView.findViewById(R.id.tv_qty);
            rl_belowqty = itemView.findViewById(R.id.rl_belowqty);
            rl_below_qty_dis = itemView.findViewById(R.id.rl_below_qty_dis);
            rl_below_qty_rdis = itemView.findViewById(R.id.rl_below_qty_rdis);
            rl_below_qty_f_amt = itemView.findViewById(R.id.rl_below_qty_f_amt);
            rl_below_qty_ctg = itemView.findViewById(R.id.rl_below_qty_ctg);
            rl_header_qty = itemView.findViewById(R.id.rl_header_qty);
            rl_qty = itemView.findViewById(R.id.rl_qty);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_particlulars = itemView.findViewById(R.id.tv_particulars);
            tv_piece = itemView.findViewById(R.id.tv_piece);
            rv_piece = itemView.findViewById(R.id.rv_piece);
            rel_piece = itemView.findViewById(R.id.relv_piece);
            rev_piece = itemView.findViewById(R.id.rev_piece);
            rv_p = itemView.findViewById(R.id.rv_p);
            rev_pie = itemView.findViewById(R.id.rev_pie);
            relative_p = itemView.findViewById(R.id.relative_p);
            r_piece = itemView.findViewById(R.id.r_piece);
            sno_header = itemView.findViewById(R.id.sno_header);
            sno_rl = itemView.findViewById(R.id.sno_rl);


            lnr_print_bill = itemView.findViewById(R.id.lnr_print_bill);

        }
    }
}
