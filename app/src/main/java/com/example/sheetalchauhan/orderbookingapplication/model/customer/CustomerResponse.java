package com.example.sheetalchauhan.orderbookingapplication.model.customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerResponse {

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("msg")
    @Expose
    private List<String> msg;

    @SerializedName("data")
    @Expose
    private List<CustomergetAddress> customerResponseDataList;

    @SerializedName("page")
    @Expose
    private String page;

    @SerializedName("count")
    @Expose
    private String count;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<String> getMsg() {
        return msg;
    }

    public void setMsg(List<String> msg) {
        this.msg = msg;
    }

    public List<CustomergetAddress> getCustomerResponseDataList() {
        return customerResponseDataList;
    }

    public void setCustomerResponseDataList(List<CustomergetAddress> customerResponseDataList) {
        this.customerResponseDataList = customerResponseDataList;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
