package com.example.sheetalchauhan.orderbookingapplication;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.utility.AppClass;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AppUtils.writeStringToPref(SplashActivity.this, "printername", "");

        if (AppUtils.readStringFromPref(this, "changeLanguage").equalsIgnoreCase("hindi")) {
            AppClass.getInstance().setLocale(this);

        } else if (AppUtils.readStringFromPref(this, "changeLanguage").equalsIgnoreCase("english")) {
            AppClass.getInstance().setLocale(this);

        } else {
            AppClass.getInstance().setLocale(this);
            AppUtils.writeStringToPref(this, "changeLanguage", "english");
        }

        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
