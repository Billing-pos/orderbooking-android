package com.example.sheetalchauhan.orderbookingapplication.activities.product;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.ProdcutAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ProductData;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddProductActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_next)
    TextView tv_next;
    @BindView(R.id.rv_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.tv_address)
    TextView tv_address;
    DBAdapter dbAdapter;
    String address, phone;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        ButterKnife.bind(this);
        tv_name.setText(R.string.product_list);
        tv_next.setOnClickListener(this);
        back.setOnClickListener(this);

        address = AppUtils.readStringFromPref(AddProductActivity.this, "address");
        String addressHindi = AppUtils.readStringFromPref(AddProductActivity.this, "address2");
        phone = AppUtils.readStringFromPref(AddProductActivity.this, "phone");

        if (AppUtils.readStringFromPref(AddProductActivity.this, "changeLanguage").equalsIgnoreCase("english")) {
            tv_address.setText(address + "\n" + phone);
        } else {
            tv_address.setText(addressHindi + "\n" + phone);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(AddProductActivity.this, 2);
        recyclerView.setLayoutManager(layoutManager);

        dbAdapter = DBAdapter.getInstance(AddProductActivity.this);
        dbAdapter.open();
        List<ProductData> productDataList;
//        String category_id = AppUtils.readStringFromPref(this,"category_id");

//        if (category_id != null) {
//            if (category_id.equalsIgnoreCase("Whole sale")) {
//                productDataList = dbAdapter.getProductListWholeSale(AppUtils.readStringFromPref(this, "address"));
//            } else {
//                productDataList = dbAdapter.getProductList();
//            }
//        } else
        productDataList = dbAdapter.getProductList();

        dbAdapter.close();
        ProdcutAdapter prodcutAdapter = new ProdcutAdapter(AddProductActivity.this, productDataList);
        recyclerView.setAdapter(prodcutAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.tv_next:
                Intent intent = new Intent(AddProductActivity.this, EquantityActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
        }
    }
}
