package com.example.sheetalchauhan.orderbookingapplication.model.userordermodel;

import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ResponsePojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderApiResponseDto extends ResponsePojo {
    @SerializedName("data")
    @Expose
    private List<OrderApiData> orderApiDataList;

    @SerializedName("page")
    @Expose
    private String page;

    @SerializedName("count")
    @Expose
    private String count;

    @SerializedName("unlock")
    @Expose
    private String unlock;

    @SerializedName("lock")
    @Expose
    private String lock;

    public String getUnlock() {
        return unlock;
    }

    public void setUnlock(String unlock) {
        this.unlock = unlock;
    }

    public String getLock() {
        return lock;
    }

    public void setLock(String lock) {
        this.lock = lock;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }


    public List<OrderApiData> getOrderApiDataList() {
        return orderApiDataList;
    }

    public void setOrderApiDataList(List<OrderApiData> orderApiDataList) {
        this.orderApiDataList = orderApiDataList;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
