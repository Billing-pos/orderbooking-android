package com.example.sheetalchauhan.orderbookingapplication.model.ordersync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderSyncData {
    @SerializedName("firm_code")
    @Expose
    private String firm;

    @SerializedName("total_amount")
    @Expose
    private String total_amount;

    @SerializedName("receipt_type")
    @Expose
    private String receipt_type;

    @SerializedName("orderid")
    @Expose
    private String orderid;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("mobile2")
    @Expose
    private String mobile2;
    @SerializedName("comment_id")
    @Expose
    private String comment_id;
    @SerializedName("custom_comment")
    @Expose
    private String custom_comment;

    @SerializedName("invalid")
    @Expose
    private String invalid;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("product")
    @Expose
    private List<OrderProductSync> orderProductSyncList;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<OrderProductSync> getOrderProductSyncList() {
        return orderProductSyncList;
    }

    public void setOrderProductSyncList(List<OrderProductSync> orderProductSyncList) {
        this.orderProductSyncList = orderProductSyncList;
    }

    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getCustom_comment() {
        return custom_comment;
    }

    public void setCustom_comment(String custom_comment) {
        this.custom_comment = custom_comment;
    }

    public String getInvalid() {
        return invalid;
    }

    public void setInvalid(String invalid) {
        this.invalid = invalid;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getReceipt_type() {
        return receipt_type;
    }

    public void setReceipt_type(String receipt_type) {
        this.receipt_type = receipt_type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
