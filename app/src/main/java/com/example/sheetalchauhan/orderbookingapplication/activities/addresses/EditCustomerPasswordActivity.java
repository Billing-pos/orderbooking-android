package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditCustomerPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_old_pass)
    EditText tv_old_pass;
    @BindView(R.id.tv_new_password)
    EditText tv_new_password;
    @BindView(R.id.bt_save)
    Button bt_save;
    String oldpass, newpass;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_customer_password);
        ButterKnife.bind(this);
        tv_name.setText(R.string.customer_change_password);
        back.setOnClickListener(this);
        bt_save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_save:
                oldpass = tv_old_pass.getText().toString();
                newpass = tv_new_password.getText().toString();
                if (oldpass.isEmpty()) {
                    Toast.makeText(EditCustomerPasswordActivity.this, R.string.enter_old_password, Toast.LENGTH_SHORT).show();
                } else {
                    if (newpass.isEmpty()) {
                        Toast.makeText(EditCustomerPasswordActivity.this, R.string.enter_new_password, Toast.LENGTH_SHORT).show();
                    } else {
                        if (oldpass.equals(Constants.getPaasword())) {
                            Constants.setpassword(newpass);
                            Toast.makeText(EditCustomerPasswordActivity.this, R.string.password_has_changed, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EditCustomerPasswordActivity.this, R.string.invalid_old_password, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }

    }
}
