package com.example.sheetalchauhan.orderbookingapplication.activities.navigationsactivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.order.CartListActivity;
import com.example.sheetalchauhan.orderbookingapplication.adapters.FirmAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecieptFormatActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.rv_recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.iv_cart)
    ImageView iv_cart;
    DBAdapter dbAdapter;
    List<Firm> firmList;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reciept_format);
        ButterKnife.bind(this);
        iv_cart.setVisibility(View.GONE);
        tv_name.setText(R.string.change_firm);

        back.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();
        firmList = dbAdapter.getFirmList();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(RecieptFormatActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layoutManager);
        FirmAdapter firmAdapter = new FirmAdapter(RecieptFormatActivity.this, firmList);
        recyclerview.setAdapter(firmAdapter);

        iv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecieptFormatActivity.this, CartListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
        }
    }


}
