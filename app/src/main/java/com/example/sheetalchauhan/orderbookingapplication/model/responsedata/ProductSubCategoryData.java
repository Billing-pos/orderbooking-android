package com.example.sheetalchauhan.orderbookingapplication.model.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSubCategoryData {
    @SerializedName("sub_id")
    @Expose
    private String subcatgeory_id;
    @SerializedName("name")
    @Expose
    private String subcategory_name;

    public String getSubcatgeory_id() {
        return subcatgeory_id;
    }

    public void setSubcatgeory_id(String subcatgeory_id) {
        this.subcatgeory_id = subcatgeory_id;
    }

    public String getSubcategory_name() {
        return subcategory_name;
    }

    public void setSubcategory_name(String subcategory_name) {
        this.subcategory_name = subcategory_name;
    }
}
