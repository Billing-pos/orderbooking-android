package com.example.sheetalchauhan.orderbookingapplication.phase_iii;

public class RequestOrdreByOrderId {
    private String appVersion;
    private String order_id;

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
