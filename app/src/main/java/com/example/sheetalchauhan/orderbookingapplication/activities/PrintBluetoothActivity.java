package com.example.sheetalchauhan.orderbookingapplication.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.MainActivity;
import com.example.sheetalchauhan.orderbookingapplication.MyClient;
import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.order.CartListActivity;
import com.example.sheetalchauhan.orderbookingapplication.adapters.PrintRecyclerview_adapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.netwowk.NetworkStatus;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.RequestUserPermissionModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.SuperUserResponse;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.UserActivityModel;
import com.example.sheetalchauhan.orderbookingapplication.screenshot.ScreenshotUtil;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppClass;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrintBluetoothActivity extends AppCompatActivity implements IAemCardScanner, IAemScrybe {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    public static Boolean isYesClicked = false;
    public static Boolean sendBack = false;
    private Boolean isAdvancePaid=false;
    private static int nFontSize, nScaleTimesWidth, nScaleTimesHeight, nFontStyle;
    ArrayList<String> printerList;
    String creditData;
    CardReader.CARD_TRACK cardTrackType;
    int glbPrinterWidth;
    MyClient mcl = null;
    EditText editText;
    Spinner spinner;
    String encoding = "US-ASCII";
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.iv_paid)
    ImageView iv_paid;
    @BindView(R.id._imv)
    ImageView _imv;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_printer_name)
    TextView tv_printer_name;
    @BindView(R.id.lnr_printer_connected)
    LinearLayout lnr_printer_connected;
    @BindView(R.id.recyclerview_print)
    RecyclerView recyclerView_print;
    @BindView(R.id.lnr_print_bill)
    LinearLayout lnr_print_bill;
    @BindView(R.id.ll_alarm)
    LinearLayout ll_alarm;
    @BindView(R.id.txt_printer_connected)
    TextView txt_printer_connected;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.ll_edit)
    LinearLayout ll_edit;
    @BindView(R.id.rl_pnot_connect)
    RelativeLayout rl_pnot_connect;
    @BindView(R.id.rl_p_connect)
    RelativeLayout rl_p_connect;
    @BindView(R.id.rl_inchType)
    RelativeLayout rl_inchType;
    @BindView(R.id.et_customer_name)
    EditText et_customer_name;
    @BindView(R.id.et_second_phone)
    EditText et_second_phone;
    @BindView(R.id.et_ordercomment)
    EditText et_ordercomment;
    @BindView(R.id.et_customercomment)
    EditText et_customercomment;
    @BindView(R.id.qrCode)
    ImageView qrCode;
    @BindView(R.id.ll_qrcode)
    RelativeLayout ll_qrcode;
    String phone, address, orderid = null, firmname, firmaddress, firmmobile;
    String navigation_side = "";
    String print;
    DBAdapter dbAdapter;
    List<Cart> cartList = new ArrayList<>();
    Context context;
    Bitmap _bitmap;
    Bitmap bitmap2;
    Bitmap bitmap3;
    ProgressDialog progressDialog;
    Handler.Callback realCallback = null;
    Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {

            if (realCallback != null) {

                realCallback.handleMessage(msg);

            }

        }

        ;

    };
    private PrintWriter printOut;
    private String userName = "", secondPhone = "", customComment = "", customerComment = "";
    private int commentId;
    private float amt_cartage = 0, amt_dis_cartage = 0, amt_dis_retail = 0, amt_total, to_be_paid = 0;
    private AppClass appClass;
    private Dialog printDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_bluetooth);
        ButterKnife.bind(this);
        context = this;
        tv_name.setText(R.string.print_a_bill);
        printerList = new ArrayList<String>();
        creditData = new String();
        dbAdapter = DBAdapter.getInstance(this);
        editText = (EditText) findViewById(R.id.edittext);
        spinner = (Spinner) findViewById(R.id.spinner);

        amt_dis_cartage = getIntent().getFloatExtra("dis_cartage", 0);
        amt_dis_retail = getIntent().getFloatExtra("dis_retail", 0);
        amt_cartage = getIntent().getFloatExtra("cartage_amt", 0);
        amt_total = getIntent().getFloatExtra("total_amt", 0);
        to_be_paid = getIntent().getFloatExtra("to_be_paid", 0);
        isAdvancePaid = AppUtils.readBoolFromPref(context, "isAdvancePaid");

        if (isAdvancePaid){
            iv_paid.setVisibility(View.VISIBLE);
        }

        phone = AppUtils.readStringFromPref(PrintBluetoothActivity.this, "phone");
        address = AppUtils.readStringFromPref(PrintBluetoothActivity.this, "address");
        orderid = AppUtils.readStringFromPref(PrintBluetoothActivity.this, "order_id");
        firmname = AppUtils.readStringFromPref(PrintBluetoothActivity.this, "firmname");
        firmaddress = AppUtils.readStringFromPref(PrintBluetoothActivity.this, "firmaddress");
        firmmobile = AppUtils.readStringFromPref(PrintBluetoothActivity.this, "firmmobile");
        dbAdapter.open();

        if (orderid != null) {

            cartList = dbAdapter.getOrderProduct(orderid);

            int amountFinal = 0;


            String temp_amount = null;
            for (int i = 0; i < cartList.size(); i++) {

                temp_amount = cartList.get(i).getAmount();

                if (temp_amount == null || temp_amount.equals("null")) {
                    Double temp_amt = Double.parseDouble(cartList.get(i).getPrice()) * Double.parseDouble(cartList.get(i).getQuantity());
                    String temp_amt1 = removeDecimalPrice("" + temp_amt);
                    amountFinal = amountFinal + Integer.parseInt(temp_amt1);

                } else {

                    Double tempQuantity = Double.parseDouble("" + cartList.get(i).getAmount());
                    int aa = removeDecimalPrice2("" + tempQuantity);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(aa));
                    int netKg = bigDecimal.intValue();

                    amountFinal = amountFinal + netKg;
                }
            }

            //.....................................................................//
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PrintBluetoothActivity.this, LinearLayoutManager.VERTICAL, false);
            recyclerView_print.setLayoutManager(linearLayoutManager);
            PrintRecyclerview_adapter adapter_rec = new PrintRecyclerview_adapter(getApplicationContext(), cartList, amt_cartage, amt_dis_cartage, amt_dis_retail, amt_total, to_be_paid);
            recyclerView_print.setAdapter(adapter_rec);

        } else {
//            Toast.makeText(context, "something went wrong ...", Toast.LENGTH_SHORT).show();
        }

//        bitmap = ScreenshotUtil.getInstance().takeScreenshotForViewBluetooth(lnr_print_bill);
        navigation_side = AppUtils.readStringFromPref(this, "navigation_side");
        if (navigation_side == "") {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    bitmap2 = ScreenshotUtil.getScreenshotFromRecyclerView(recyclerView_print);

                    printQRCode();

                }
            }, 1000);
        }
        //.....................................................................//
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.printer_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 1) {
                    glbPrinterWidth = 32;
                    onSetPrinterType(view);

                } else {
                    glbPrinterWidth = 48;
                    onSetPrinterType(view);
                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        //btnSetPrnType.setText("Set 3 Inch Printer");
        Button discoverButton = (Button) findViewById(R.id.pairing);

        registerForContextMenu(discoverButton);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent().hasExtra("byteArray")) {
            _bitmap = BitmapFactory.decodeByteArray(getIntent().getByteArrayExtra("byteArray"), 0, getIntent().getByteArrayExtra("byteArray").length);
            _imv.setImageBitmap(_bitmap);
        }

        if (getIntent().getStringExtra("userName") != null)
            userName = getIntent().getStringExtra("userName");
        if (getIntent().getStringExtra("secondPhone") != null)
            secondPhone = getIntent().getStringExtra("secondPhone");
        if (("" + getIntent().getIntExtra("commentId", 0) + "") != null)
            commentId = getIntent().getIntExtra("commentId", 0);
        if (getIntent().getStringExtra("customComment") != null)
            customComment = getIntent().getStringExtra("customComment");
        if (getIntent().getStringExtra("customerComment") != null)
            customerComment = getIntent().getStringExtra("customerComment");

        Log.i("printerusrdat", "" + userName + "," + secondPhone + "," + commentId + "," + customComment + ", " + customerComment);

        ll_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrintBluetoothActivity.this, CartListActivity.class);
                intent.putExtra("editCart", "true");
                intent.putExtra("secondPhone", secondPhone);
                intent.putExtra("userName", "" + userName);
                intent.putExtra("commentId", "" + commentId);
                intent.putExtra("customComment", "" + customComment);
                intent.putExtra("orderId", orderid);
                startActivity(intent);
                finish();
            }
        });


        et_customer_name.setText(userName);
        et_second_phone.setText(secondPhone);

        if (commentId != 0) {
            final List<String> commentList = dbAdapter.getComment();
            et_ordercomment.setText(commentList.get(commentId));
        } else
            et_ordercomment.setText(customComment);
        et_customercomment.setText("" + customerComment);


        print = AppUtils.readStringFromPref(this, "printername");

        if (navigation_side == "") {

        } else {
            AppUtils.writeStringToPref(this, "navigation_side", "");
            recyclerView_print.setVisibility(View.INVISIBLE);
            rl_inchType.setVisibility(View.INVISIBLE);
            ll_edit.setVisibility(View.INVISIBLE);
            linear.setVisibility(View.INVISIBLE);
        }

        checkPrinterConnected();


        String qr_code = "TotalAmount : " + amt_total + "\nInvoiceNo : " + orderid;

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(qr_code, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            qrCode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }


    }

    public int removeDecimalPrice2(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);

        return Integer.parseInt("" + bdprice);
    }

    public void checkPrinterConnected() {
        appClass = AppClass.getInstance();

        if (appClass.getPrinterStatus(context)) {
            rl_p_connect.setVisibility(View.VISIBLE);
            tv_printer_name.setText(AppUtils.readStringFromPref(context, "printername") + " Printer is connectd");
            rl_pnot_connect.setVisibility(View.GONE);

        } else {

            rl_pnot_connect.setVisibility(View.VISIBLE);
            rl_p_connect.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Select Printer to connect");

        for (int i = 0; i < printerList.size(); i++) {
            menu.add(0, v.getId(), 0, printerList.get(i));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        String printerName = item.getTitle().toString();
        try {
            if (printerName != null) {
                AppUtils.writeStringToPref(this, "printername", printerName);
            }
            appClass.m_AemScrybeDevice.connectToPrinter(printerName);
            appClass.m_cardReader = appClass.m_AemScrybeDevice.getCardReader(this);
            appClass.m_AemPrinter = appClass.m_AemScrybeDevice.getAemPrinter();

            Toast.makeText(PrintBluetoothActivity.this, "Connected with " + printerName, Toast.LENGTH_SHORT).show();
            checkPrinterConnected();

//            startService(new Intent(PrintBluetoothActivity.this, PrinterService.class));

            //  appClass.m_cardReader.readMSR();
        } catch (IOException e) {
            if (e.getMessage().contains("Service discovery failed")) {
                Toast.makeText(PrintBluetoothActivity.this, "Not Connected\n" + printerName + " is unreachable or off otherwise it is connected with other device", Toast.LENGTH_SHORT).show();
            } else if (e.getMessage().contains("Device or resource busy")) {
                Toast.makeText(PrintBluetoothActivity.this, "the device is already connected", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(PrintBluetoothActivity.this, "Unable to connect", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    public void onShowPairedPrinters(View v) {

        String p = appClass.m_AemScrybeDevice.pairPrinter("BTprinter0314");
        // showAlert(p);
        printerList = appClass.m_AemScrybeDevice.getPairedPrinters();

        if (printerList.size() > 0)
            openContextMenu(v);
        else
            showAlert("No Paired Printers found");
    }

    public void onDisconnectDevice(View v) {
        if (appClass.m_AemScrybeDevice != null) {
            try {
                appClass.m_AemScrybeDevice.disConnectPrinter();
                Toast.makeText(PrintBluetoothActivity.this, "disconnected", Toast.LENGTH_SHORT).show();
                checkPrinterConnected();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onSetPrinterType(View v) {
        if (glbPrinterWidth == 32) {
            glbPrinterWidth = 32;
            showAlert("32 Characters / Line or 2 Inch (58mm) Printer Selected!");

        } else {
            glbPrinterWidth = 48;
//            showAlert("48 Characters / Line or 3 Inch (80mm) Printer Selected!");
        }
    }

    public void getTextBill() {
        int numChars = glbPrinterWidth;//CheckPrinterWidth();
        onPrintBillBluetooth(numChars);
    }

    public void printRasterImageBT(View v) {

        if (this.appClass.m_AemPrinter == null) {
            if (print == null) {

                this.showAlert("Printer not connected");
                return;
            }
        } else {
            String address__ = "";
            address__ = AppUtils.readStringFromPref(PrintBluetoothActivity.this, "address");
            if (address__ != "") {
                Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.digit);
                this.RasterBT(_bitmap);
                return;
            }

        }

    }

    public void onPrintBillBluetooth(int numChars) {
        if (appClass.m_AemPrinter == null) {
            Toast.makeText(PrintBluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        String address_ = "\n\n Address: " + AppUtils.readStringFromPref(context, "area") + ", " + AppUtils.readStringFromPref(context, "sector") + ", " + AppUtils.readStringFromPref(context, "apartment");
        String d = "\n";
        String houseno = "          " + AppUtils.readStringFromPref(context, "block") + "/" + AppUtils.readStringFromPref(context, "houseno") + ", " + AppUtils.readStringFromPref(context, "floor") + "\n";
        String phone = " Phone: " + AppUtils.readStringFromPref(PrintBluetoothActivity.this, "phone") + "\n\n";

        try {
            appClass.m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
            appClass.m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            if (numChars == 32) {
                appClass.m_AemPrinter.print(address_);
                appClass.m_AemPrinter.print(d);
                appClass.m_AemPrinter.print(houseno);
                appClass.m_AemPrinter.print(d);
                appClass.m_AemPrinter.print(phone);

                clearPref();
                Intent intent = new Intent(PrintBluetoothActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                address_ = "\n\n Address: " + AppUtils.readStringFromPref(context, "area") + ", " + AppUtils.readStringFromPref(context, "sector") + ", " + AppUtils.readStringFromPref(context, "apartment");
                d = "\n";
                houseno = "          " + AppUtils.readStringFromPref(context, "block") + "/" + AppUtils.readStringFromPref(context, "houseno") + ", " + AppUtils.readStringFromPref(context, "floor") + "\n";
                phone = " Phone: " + AppUtils.readStringFromPref(PrintBluetoothActivity.this, "phone") + "\n\n";

                appClass.m_AemPrinter.printThreeInch(address_);
                appClass.m_AemPrinter.printThreeInch(d);
                appClass.m_AemPrinter.printThreeInch(houseno);
                appClass.m_AemPrinter.printThreeInch(d);
                appClass.m_AemPrinter.printThreeInch(phone);

                clearPref();
                Intent intent = new Intent(PrintBluetoothActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }


        } catch (IOException e) {
            if (e.getMessage().contains("socket closed"))
                Toast.makeText(PrintBluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();

        }

    }

    public void onPrint(View v) {
        String data = editText.getText().toString();
        if (appClass.m_AemPrinter == null) {
            Toast.makeText(PrintBluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        } else if (data.isEmpty()) {
            showAlert("Write Text");
        } else if (appClass.m_AemPrinter != null) {
            try {
                if (glbPrinterWidth == 32) {
                    appClass.m_AemPrinter.print(data);
                    // appClass.m_AemPrinter.PrintHindi(data);
                    appClass.m_AemPrinter.setCarriageReturn();
                    appClass.m_AemPrinter.setCarriageReturn();
                    appClass.m_AemPrinter.setCarriageReturn();
                    appClass.m_AemPrinter.setCarriageReturn();
                } else {
                    appClass.m_AemPrinter.POS_S_TextOutThreeInch(data, encoding, 0, nScaleTimesWidth, nScaleTimesHeight, nFontSize, nFontStyle);
                    // appClass.m_AemPrinter.printThreeInch(data);
                    // appClass.m_AemPrinter.PrintHindi(data);
                    appClass.m_AemPrinter.setLineFeed(4);
                }

            } catch (IOException e) {
                if (e.getMessage().contains("socket closed"))
                    Toast.makeText(PrintBluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();

            }
        } else {
            mcl.sendDataOnSocket(data, printOut);
            mcl.sendDataOnSocket("\n\n\n", printOut);
            //mcl.sendDataOnSocket("\n\n", printOut);
            //mcl.sendDataOnSocket("\n\n", printOut);
        }
    }

    public void onPrintMultilingual(View v) {
        if (appClass.m_AemPrinter == null) {
            Toast.makeText(PrintBluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }

        String data = editText.getText().toString();
        if (data.isEmpty()) {
            showAlert("Write Text");

        } else {
            try {
                if (glbPrinterWidth == 32) {
                    appClass.m_AemPrinter.printTextAsImage(data);
                } else {
                    appClass.m_AemPrinter.printTextAsImageThreeInch(data);
                }
                appClass.m_AemPrinter.setCarriageReturn();
                appClass.m_AemPrinter.setCarriageReturn();
                appClass.m_AemPrinter.setCarriageReturn();
                appClass.m_AemPrinter.setCarriageReturn();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onPrintRasterImage(View v) {
        selectImage();
        // selectImageFromSDCard();
        // dispatchTakePictureIntent();
    }

    public void selectImage() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && null != data) {
            Uri selectedImageUri = data.getData();
            uriToBitmap(selectedImageUri);

        } else {
            Toast.makeText(PrintBluetoothActivity.this, "You have not selected and image", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        AppUtils.writeIntToPref(context, "commentId", 0);
        AppUtils.writeStringToPref(context, "customComment", "");
        AppUtils.writeStringToPref(context, "userName", "");
        AppUtils.writeStringToPref(context, "orderId", "");
        AppUtils.writeStringToPref(context, "secondPhone", "");
        AppUtils.writeStringToPref(context, "cartage", "");
        AppUtils.writeStringToPref(context, "editCart", "false");

        Intent intent = new Intent(PrintBluetoothActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }

    private void uriToBitmap(Uri selectedFileUri) {
        try {
            ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            Bitmap resizedBitmap = null;
            if (glbPrinterWidth == 32)
                resizedBitmap = Bitmap.createScaledBitmap(image, 384, 384, false);
            else
                resizedBitmap = Bitmap.createScaledBitmap(image, 577, 700, false);
            // resizedBitmap = Bitmap.createScaledBitmap(image, 384, 384, false);
            if (appClass.m_AemPrinter == null) {
                Toast.makeText(PrintBluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
                return;
            }
            RasterBT(resizedBitmap);
            parcelFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onScanMSR(final String buffer, CardReader.CARD_TRACK cardTrack) {
        cardTrackType = cardTrack;

        creditData = buffer;
        PrintBluetoothActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                editText.setText(buffer.toString());
            }
        });
    }

    public void onScanDLCard(final String buffer) {
        CardReader.DLCardData dlCardData = appClass.m_cardReader.decodeDLData(buffer);
        String name = "NAME:" + dlCardData.NAME + "\n";
        String SWD = "SWD Of: " + dlCardData.SWD_OF + "\n";
        String dob = "DOB: " + dlCardData.DOB + "\n";
        String dlNum = "DLNUM: " + dlCardData.DL_NUM + "\n";
        String issAuth = "ISS AUTH: " + dlCardData.ISS_AUTH + "\n";
        String doi = "DOI: " + dlCardData.DOI + "\n";
        String tp = "VALID TP: " + dlCardData.VALID_TP + "\n";
        String ntp = "VALID NTP: " + dlCardData.VALID_NTP + "\n";

        final String data = name + SWD + dob + dlNum + issAuth + doi + tp + ntp;

        runOnUiThread(new Runnable() {
            public void run() {
                editText.setText(data);
            }
        });
    }

    public void onScanRCCard(final String buffer) {
        CardReader.RCCardData rcCardData = appClass.m_cardReader.decodeRCData(buffer);

        String regNum = "REG NUM: " + rcCardData.REG_NUM + "\n";
        String regName = "REG NAME: " + rcCardData.REG_NAME + "\n";
        String regUpto = "REG UPTO: " + rcCardData.REG_UPTO + "\n";

        final String data = regNum + regName + regUpto;

        runOnUiThread(new Runnable() {
            public void run() {
                editText.setText(data);
            }
        });
    }

    @Override
    public void onScanRFD(final String buffer) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(buffer);
        String temp = "";
        try {
            temp = stringBuffer.deleteCharAt(8).toString();
        } catch (Exception e) {
            // TODO: handle exception
        }
        final String data = temp;

        PrintBluetoothActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                editText.setText("ID " + data);
                try {
                    appClass.m_AemPrinter.print(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void onDiscoveryComplete(ArrayList<String> aemPrinterList) {
        printerList = aemPrinterList;
        for (int i = 0; i < aemPrinterList.size(); i++) {
            String Device_Name = aemPrinterList.get(i);
            String status = appClass.m_AemScrybeDevice.pairPrinter(Device_Name);
        }

    }

    public void showAlert(String alertMsg) {
        AlertDialog.Builder alertBox = new AlertDialog.Builder(
                PrintBluetoothActivity.this);

        alertBox.setMessage(alertMsg).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        return;
                    }
                });

        AlertDialog alert = alertBox.create();
        alert.show();
    }

    public void onError(String response) {
        showAlert("Please try again... " + " " + response);
    }

    @Override
    public void onScanPacket(String buffer) {
        // TODO Auto-generated method stub

    }

    protected void RasterBT(Bitmap image) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Printing...");
        progressDialog.show();
        //...........Printer..................//
        // PrintManager manager=(PrintManager) getSystemService(Context.PRINT_SERVICE);
        //....................................//

        try {
            if (glbPrinterWidth == 32) {
                appClass.m_AemPrinter.setCarriageReturn();
                appClass.m_AemPrinter.setCarriageReturn();
                appClass.m_AemPrinter.printImage(image);
            } else {
                appClass.m_AemPrinter.setCarriageReturn();
                appClass.m_AemPrinter.setCarriageReturn();
                appClass.m_AemPrinter.printImageThreeInch(image);
            }
        } catch (IOException e) {
            showAlert("IO EX:  " + e.toString());
        }

        //getTextBill();

        try {
            if (glbPrinterWidth == 32) {
                appClass.m_AemPrinter.printImage(bitmap2);

            } else {
                appClass.m_AemPrinter.printImageThreeInch(bitmap2);
            }

        } catch (IOException e) {
            showAlert("IO EX:  " + e.toString());
        }

        //get QR Code();

        try {
            if (glbPrinterWidth == 32) {

                appClass.m_AemPrinter.printImage(bitmap3);
                appClass.m_AemPrinter.setLineFeed(4);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.hide();

                        if (!sendBack)
                            rePrint();
                        else
                            sendToMainActivity();
                    }
                }, 5000);

            } else {
                appClass.m_AemPrinter.printImageThreeInch(bitmap3);
                appClass.m_AemPrinter.setLineFeed(4);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.hide();

                        if (!sendBack)
                            rePrint();
                        else
                            sendToMainActivity();
                    }
                }, 5000);

            }

        } catch (IOException e) {
            showAlert("IO EX:  " + e.toString());
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clearPref();

                if (!isYesClicked) {

                    if (printDialog != null) {
                        printDialog.dismiss();
                    }

                    Intent intent = new Intent(PrintBluetoothActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    finish();
                }
            }
        }, 30000);

    }

    private void rePrint() {
        dialogForReprint(this, 2, "User Want to again print invoice - " + orderid, "0");
    }

    private void clearPref() {
        AppUtils.writeStringToPref(PrintBluetoothActivity.this, "area", "");
        AppUtils.writeStringToPref(PrintBluetoothActivity.this, "phone", "");
        AppUtils.writeStringToPref(PrintBluetoothActivity.this, "sector", "");
        AppUtils.writeStringToPref(PrintBluetoothActivity.this, "apartment", "");
        AppUtils.writeStringToPref(PrintBluetoothActivity.this, "block", "");
        AppUtils.writeStringToPref(PrintBluetoothActivity.this, "houseno", "");
        AppUtils.writeStringToPref(PrintBluetoothActivity.this, "floor", "");
        AppUtils.writeStringToPref(PrintBluetoothActivity.this, "colNo", "");

    }

    public Handler getHandler() {

        return handler;

    }

    public String removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);

        return "" + bdprice;
    }

    public void printQRCode() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                bitmap3 = ScreenshotUtil.getInstance().takeScreenshotForView(ll_qrcode);
            }
        }, 1000);
    }

    private void dialogForReprint(final Context context, final int permissionId, final String statement, final String balance) {
        printDialog = new Dialog(context);
        printDialog.setCancelable(false);
        printDialog.setContentView(R.layout.layout_reprint_bill);
        printDialog.show();

        final TextView bt_no = (TextView) printDialog.findViewById(R.id.bt_no);
        final TextView bt_yes = (TextView) printDialog.findViewById(R.id.bt_yes);

        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isYesClicked = true;
                sendBack = true;
                printDialog.dismiss();
                openDialog(context, permissionId, statement, balance);
            }
        });

        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printDialog.dismiss();
                sendToMainActivity();
            }
        });
    }


    private void openDialog(final Context context, final int permissionId, final String statement, final String balance) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.layout_advance_payment);
        dialog.show();

        final EditText et_username = (EditText) dialog.findViewById(R.id.et_username);
        final EditText et_passwrod = (EditText) dialog.findViewById(R.id.et_passwrod);
        Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = et_username.getText().toString().trim();
                String pass = et_passwrod.getText().toString().trim();
                if (user.isEmpty()) {
                    Toast.makeText(context, "please enter username", Toast.LENGTH_SHORT).show();

                } else if (pass.isEmpty()) {
                    Toast.makeText(context, "please enter password", Toast.LENGTH_SHORT).show();

                } else {
                    if (!NetworkStatus.getConnectivity(context)) {
                        Toast.makeText(context, context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

                    } else {
                        RequestUserPermissionModel model = new RequestUserPermissionModel();
                        model.setUserName(user);
                        model.setPassword(pass);
                        model.setPermissionId(permissionId);

                        final ProgressDialog dialog1 = new ProgressDialog(context);
                        dialog1.setTitle("Wait Loading ...");
                        dialog1.show();

                        Call<SuperUserResponse> call = RetrofitHandler.getInstance().getApi().getUserPermission(model);
                        call.enqueue(new Callback<SuperUserResponse>() {
                            @Override
                            public void onResponse(Call<SuperUserResponse> call, Response<SuperUserResponse> response) {
                                SuperUserResponse responseDto = response.body();
                                if (response.isSuccessful()) {
                                    if (!responseDto.getError()) {
                                        sendUserActivity(dialog, context, statement, user, balance);

                                    } else {
                                        Toast.makeText(context, responseDto.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                dialog1.dismiss();
                            }

                            @Override
                            public void onFailure(Call<SuperUserResponse> call, Throwable t) {
                                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                                dialog1.dismiss();
                            }
                        });
                    }

                }
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                sendToMainActivity();
            }
        });

    }


    private void sendUserActivity(final Dialog dialog, final Context context, final String statement, final String userName, final String balance) {
        if (!NetworkStatus.getConnectivity(context)) {
            Toast.makeText(context, context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

        } else {

            UserActivityModel model = new UserActivityModel();
            model.setUesrName(userName);
            model.setBalance(balance);
            model.setActivity(statement);

            final ProgressDialog dialog1 = new ProgressDialog(context);
            dialog1.show();
            dialog1.setTitle("Wait Loading ...");

            Call<SuperUserResponse> call = RetrofitHandler.getInstance().getApi().getUserActivity(model);
            call.enqueue(new Callback<SuperUserResponse>() {
                @Override
                public void onResponse(Call<SuperUserResponse> call, Response<SuperUserResponse> response) {
                    SuperUserResponse responseDto = response.body();
                    if (response.isSuccessful()) {
                        if (!responseDto.getError()) {
                            Toast.makeText(context, responseDto.getMessage(), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                            RasterBT(_bitmap);


//                            sendToMainActivity();

                        } else {
                            Toast.makeText(context, responseDto.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    dialog1.dismiss();
                }

                @Override
                public void onFailure(Call<SuperUserResponse> call, Throwable t) {
                    Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                    dialog1.dismiss();
                }
            });
        }

    }

    private void sendToMainActivity() {
        if (printDialog != null) {
            printDialog.dismiss();
        }

        clearPref();
        Intent intent = new Intent(PrintBluetoothActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

    }

}
