package com.example.sheetalchauhan.orderbookingapplication.phase_II.model;

import com.google.gson.annotations.SerializedName;

public class OrderImageListData {
    @SerializedName("receipt_no")
    private String receipt_no;
    @SerializedName("image")
    private String image;
    @SerializedName("firm")
    private String firm;
    @SerializedName("amount")
    private String amount;
    @SerializedName("receipt_date")
    private String date;

    private String flag;
    private String invalid;


    public String getReceipt_no() {
        return receipt_no;
    }

    public void setReceipt_no(String receipt_no) {
        this.receipt_no = receipt_no;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getInvalid() {
        return invalid;
    }

    public void setInvalid(String invalid) {
        this.invalid = invalid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
