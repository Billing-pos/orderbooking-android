package com.example.sheetalchauhan.orderbookingapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class isFirmSelected implements Serializable {

    @SerializedName("device_id")
    @Expose
    private String deviceId;


    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}

