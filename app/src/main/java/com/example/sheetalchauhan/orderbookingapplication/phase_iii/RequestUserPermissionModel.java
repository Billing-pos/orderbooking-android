package com.example.sheetalchauhan.orderbookingapplication.phase_iii;

public class RequestUserPermissionModel {

    private String userName;
    private String password;
    private int permissionId;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }
}
