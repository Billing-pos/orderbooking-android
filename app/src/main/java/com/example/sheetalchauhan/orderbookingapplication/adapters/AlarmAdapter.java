package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.breceiver.AlarmReceiver;
import com.example.sheetalchauhan.orderbookingapplication.breceiver.Constants;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.AlarmModel;

import java.util.List;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.ViewHolder> {
    Context context;
    View view1;
    List<AlarmModel> list;
    AlarmAdapter.ViewHolder viewHolder1;
    StopAlarm stopAlarm;
    private DBAdapter dbAdapter;

    public AlarmAdapter(Context context1, List<AlarmModel> list) {
        context = context1;
        this.list = list;
        stopAlarm = (StopAlarm) context1;
        dbAdapter = DBAdapter.getInstance(context);
    }

    public void setData(List<AlarmModel> list) {
        if (this.list.size() > 0)
            this.list.clear();
        this.list = list;
        Toast.makeText(context, "clear list", Toast.LENGTH_SHORT).show();
    }

    public void removeItem(int position) {

        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("intentType", Constants.OFF_INTENT);
        intent.putExtra("AlarmId", list.get(position).getAlarmId());
        intent.putExtra("TODO", "text");
        intent.putExtra("orderids", "orderids");
        context.sendBroadcast(intent);
        stopAlarm.finishSwipeAlarm(list.get(position));
    }

    @Override
    public AlarmAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.layout_alarm, parent, false);
        viewHolder1 = new AlarmAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final AlarmAdapter.ViewHolder holder, final int position) {
        AlarmModel model = list.get(position);
        holder.tv_id.setText(model.getAlarmId());
        holder.tv_orderid.setText(context.getResources().getString(R.string.order_id) + " : " + model.getOrderId());
        holder.tv_date.setText(context.getResources().getString(R.string.date) + " : " + model.getDate());
        holder.tv_time.setText(context.getResources().getString(R.string.time) + " : " + model.getTime());
        holder.tv_text.setText(context.getResources().getString(R.string.description) + " : " + model.getText());

        if (model.getAlarm_status().equalsIgnoreCase("1")) {
            holder.tv_alarm_status.setVisibility(View.VISIBLE);
            holder.tv_alarm_status.setText(context.getResources().getString(R.string.alarm_close));
            holder.tv_alarm_status.setBackgroundColor(context.getResources().getColor(R.color.colorgreen));

        } else if (model.getAlarm_status().equalsIgnoreCase("2")) {
            holder.tv_alarm_status.setVisibility(View.VISIBLE);
            holder.tv_alarm_status.setText(context.getResources().getString(R.string.alarm_done));
            holder.tv_alarm_status.setBackgroundColor(context.getResources().getColor(R.color.colorred));

        } else {
            holder.tv_alarm_status.setVisibility(View.VISIBLE);
            holder.tv_alarm_status.setBackgroundColor(context.getResources().getColor(R.color.coloryellow));
            holder.tv_alarm_status.setText(context.getResources().getString(R.string.cancell));
        }

        holder.tv_alarm_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!model.getAlarm_status().equalsIgnoreCase("2")) {
                    stopAlarm.stopCurrentAlarm(model);
                    Intent intent = new Intent(context, AlarmReceiver.class);
                    intent.putExtra("intentType", Constants.OFF_INTENT);
                    intent.putExtra("AlarmId", model.getAlarmId());
                    intent.putExtra("TODO", "text");
                    intent.putExtra("orderids", "orderids");
                    context.sendBroadcast(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface StopAlarm {
        void stopCurrentAlarm(AlarmModel model);

        void finishSwipeAlarm(AlarmModel model);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_id, tv_orderid, tv_date, tv_time, tv_text;
        public TextView tv_alarm_status;

        public ViewHolder(View v) {
            super(v);
            tv_id = (TextView) v.findViewById(R.id.tv_id);
            tv_orderid = (TextView) v.findViewById(R.id.tv_orderid);
            tv_date = (TextView) v.findViewById(R.id.tv_date);
            tv_time = (TextView) v.findViewById(R.id.tv_time);
            tv_text = (TextView) v.findViewById(R.id.tv_text);
            tv_alarm_status = (TextView) v.findViewById(R.id.tv_alarm_status);
        }
    }

}
