package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.ApartmentAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.AreaAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.BlockAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.FloorAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.HouseNoAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.SectorAdapter;

import java.util.ArrayList;
import java.util.List;


public class AreaFragment extends Fragment {
    private static final String TITLE = "title";
    private static final String AREA = "area";
    private static final String AREALIST = "areaList";
    private Context context;
    private String title = "recent", area = null;
    private View root;
    private ArrayList<String> areaList;
    private OnFragmentInteractionListener mListener;

    public AreaFragment() {
    }

    public static AreaFragment newInstance(ArrayList<String> areaList, String title, String area) {
        AreaFragment fragment = new AreaFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AREA, area);
        bundle.putString(TITLE, title);
        bundle.putStringArrayList(AREALIST, areaList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        areaList = new ArrayList<>();

        if (getArguments() != null) {
            area = getArguments().getString(AREA);
            title = getArguments().getString(TITLE);
            areaList = getArguments().getStringArrayList(AREALIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_area, container, false);

        RecyclerView rv_recyclerview = root.findViewById(R.id.rview);
        TextView tv_noData = root.findViewById(R.id.tv_noData);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        rv_recyclerview.setLayoutManager(layoutManager);

        if (areaList != null && areaList.size() > 0) {
            setAdapter(title, areaList, rv_recyclerview);
            tv_noData.setVisibility(View.GONE);
            rv_recyclerview.setVisibility(View.VISIBLE);

        } else {
            tv_noData.setVisibility(View.VISIBLE);
            rv_recyclerview.setVisibility(View.GONE);
        }

        return root;
    }

    private void setRecyclerView(View view) {
        List<String> areaList = new ArrayList<>();
        RecyclerView rview = view.findViewById(R.id.rview);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        rview.setLayoutManager(layoutManager);
        AreaAdapter areaAdapter = new AreaAdapter(context, areaList);
        rview.setAdapter(areaAdapter);
    }


    private void setAdapter(String title, List areaList, RecyclerView rv_recyclerview) {
        if (title == null) {
            return;
        }

        if (title.equalsIgnoreCase("area")) {
            AreaAdapter areaAdapter = new AreaAdapter(getActivity(), areaList);
            rv_recyclerview.setAdapter(areaAdapter);

        } else if (title.equalsIgnoreCase("sector")) {
            SectorAdapter sectorAdapter = new SectorAdapter(getActivity(), areaList);
            rv_recyclerview.setAdapter(sectorAdapter);

        } else if (title.equalsIgnoreCase("apartment")) {
            ApartmentAdapter apartmentAdapter = new ApartmentAdapter(getActivity(), areaList);
            rv_recyclerview.setAdapter(apartmentAdapter);

        } else if (title.equalsIgnoreCase("block")) {
            BlockAdapter blockAdapter = new BlockAdapter(getActivity(), areaList);
            rv_recyclerview.setAdapter(blockAdapter);

        } else if (title.equalsIgnoreCase("house")) {
            HouseNoAdapter houseAdapter = new HouseNoAdapter(getActivity(), areaList);
            rv_recyclerview.setAdapter(houseAdapter);

        } else if (title.equalsIgnoreCase("floor")) {
            FloorAdapter floorAdapter = new FloorAdapter(getActivity(), areaList);
            rv_recyclerview.setAdapter(floorAdapter);
        }
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

}
