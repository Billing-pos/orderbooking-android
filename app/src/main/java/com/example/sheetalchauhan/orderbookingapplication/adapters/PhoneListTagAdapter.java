package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.ManageTag.PhoneLastOrder;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.OrderDetails;

import java.util.List;

public class PhoneListTagAdapter extends RecyclerView.Adapter<PhoneListTagAdapter.Viewholder> {
    Context context;
    List<String> listPhone;
    String color;
    Boolean isShow;
    DBAdapter dbAdapter;
    List<OrderDetails> list;

    public PhoneListTagAdapter(Context context, List<String> listPhone, String color) {
        this.context = context;
        this.listPhone = listPhone;
        this.color = color;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tag_phone, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(final PhoneListTagAdapter.Viewholder holder, final int position) {
        dbAdapter = DBAdapter.getInstance(context);
        isShow = false;
        holder.ll_address.setVisibility(View.VISIBLE);
        holder.tv_address.setText(listPhone.get(position));

        holder.ll_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PhoneLastOrder.class);
                intent.putExtra("ordernumber", listPhone.get(position));
                intent.putExtra("color", color);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return listPhone.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView tv_address;
        LinearLayout ll_address;

        public Viewholder(View itemView) {
            super(itemView);
            tv_address = itemView.findViewById(R.id.tv_address);
            ll_address = itemView.findViewById(R.id.ll_address);
        }
    }
}