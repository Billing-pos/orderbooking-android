package com.example.sheetalchauhan.orderbookingapplication.phase_iii;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.netwowk.NetworkStatus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertDialogSuperUser {


    public static void dialogForReprint(final Context context, final int permissionId, final String statement, final String balance) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.layout_reprint_bill);
        dialog.show();

        final TextView bt_no = (TextView) dialog.findViewById(R.id.bt_no);
        final TextView bt_yes = (TextView) dialog.findViewById(R.id.bt_yes);

        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(context, permissionId, statement, balance);
                dialog.dismiss();
            }
        });

        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }


    public static void openDialog(final Context context, final int permissionId, final String statement, final String balance) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.layout_advance_payment);
        dialog.show();

        final EditText et_username = (EditText) dialog.findViewById(R.id.et_username);
        final EditText et_passwrod = (EditText) dialog.findViewById(R.id.et_passwrod);
        Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = et_username.getText().toString().trim();
                String pass = et_passwrod.getText().toString().trim();
                if (user.isEmpty()) {
                    Toast.makeText(context, "please enter username", Toast.LENGTH_SHORT).show();

                } else if (pass.isEmpty()) {
                    Toast.makeText(context, "please enter password", Toast.LENGTH_SHORT).show();

                } else {
                    if (!NetworkStatus.getConnectivity(context)) {
                        Toast.makeText(context, context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

                    } else {
                        RequestUserPermissionModel model = new RequestUserPermissionModel();
                        model.setUserName(user);
                        model.setPassword(pass);
                        model.setPermissionId(permissionId);

                        final ProgressDialog dialog1 = new ProgressDialog(context);
                        dialog1.setTitle("Wait Loading ...");
                        dialog1.show();

                        Call<SuperUserResponse> call = RetrofitHandler.getInstance().getApi().getUserPermission(model);
                        call.enqueue(new Callback<SuperUserResponse>() {
                            @Override
                            public void onResponse(Call<SuperUserResponse> call, Response<SuperUserResponse> response) {
                                SuperUserResponse responseDto = response.body();
                                if (response.isSuccessful()) {
                                    if (!responseDto.getError()) {
                                        sendUserActivity(dialog, context, statement, user, balance);

                                    } else {
                                        Toast.makeText(context, responseDto.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                dialog1.dismiss();
                            }

                            @Override
                            public void onFailure(Call<SuperUserResponse> call, Throwable t) {
                                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                                dialog1.dismiss();
                            }
                        });
                    }

                }
            }
        });
    }


    public static void sendUserActivity(final Dialog dialog, final Context context, final String statement, final String userName, final String balance) {
        if (!NetworkStatus.getConnectivity(context)) {
            Toast.makeText(context, context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

        } else {

            UserActivityModel model = new UserActivityModel();
            model.setUesrName(userName);
            model.setBalance(balance);
            model.setActivity(statement);

            final ProgressDialog dialog1 = new ProgressDialog(context);
            dialog1.show();
            dialog1.setTitle("Wait Loading ...");

            Call<SuperUserResponse> call = RetrofitHandler.getInstance().getApi().getUserActivity(model);
            call.enqueue(new Callback<SuperUserResponse>() {
                @Override
                public void onResponse(Call<SuperUserResponse> call, Response<SuperUserResponse> response) {
                    SuperUserResponse responseDto = response.body();
                    if (response.isSuccessful()) {
                        if (!responseDto.getError()) {
                            Toast.makeText(context, responseDto.getMessage(), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                        } else {
                            Toast.makeText(context, responseDto.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    dialog1.dismiss();
                }

                @Override
                public void onFailure(Call<SuperUserResponse> call, Throwable t) {
                    Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                    dialog1.dismiss();
                }
            });
        }

    }

}
