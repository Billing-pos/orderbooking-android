package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.order.CartListActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.userordermodel.OrderApiData;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.ShowOrder;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

public class OrderImageAdpter extends RecyclerView.Adapter<OrderImageAdpter.ViewHolder> {
    List<OrderApiData> list;
    Context context;
    itemClickListner listner;
    Long rowid = null;
    DBAdapter dbAdapter;
    String[] add = null;
    String[] add2 = null;


    public OrderImageAdpter(Context context, List<OrderApiData> list) {
        this.context = context;
        this.list = list;
        dbAdapter = DBAdapter.getInstance(context);
    }

    public void clearData() {
        list.clear();
        notifyDataSetChanged();
    }

    public void setData(List<OrderApiData> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.orderwithimage_items, null);
        return new OrderImageAdpter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if (list == null || list.isEmpty())
            return;

        final OrderApiData model = list.get(position);

        holder.tv_date.setText(context.getResources().getString(R.string.date) + "               :  " + model.getDate());
        holder.tv_invoice.setText(context.getResources().getString(R.string.invoice_no) + "   :  " + model.getOrder_id());
        if (model.getTotal_amount() != null && !model.getTotal_amount().isEmpty() && !model.getTotal_amount().equalsIgnoreCase("null")
                && !model.getTotal_amount().equalsIgnoreCase("0.00")) {
            holder.tv_amt.setText(context.getResources().getString(R.string.amount) + "         :  " + model.getTotal_amount());
        } else {
            holder.tv_amt.setText(context.getResources().getString(R.string.amount) + "         :  ");
        }
        holder.tv_address.setText(context.getResources().getString(R.string.address_new) + "         :  " + model.getAddress());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ShowOrder.class);
                intent.putExtra("getFirm", "Firm");
                intent.putExtra("getAmount", model.getTotal_amount());
                intent.putExtra("getDate", model.getDate());
                intent.putExtra("getInvalid", model.getInvalid());
                intent.putExtra("getOrderid", model.getOrder_id());
                intent.putExtra("getAddress", model.getAddress());
                intent.putExtra("getMobile", model.getMobile());
                intent.putExtra("tv_tobepaid", model.getStatus());
                context.startActivity(intent);
            }
        });

        holder.image_order.setImageResource(R.drawable.viewimg);

        if (model.getInvalid() != null && !model.getInvalid().isEmpty() && !model.getInvalid().equalsIgnoreCase("null")) {
            if (model.getInvalid().equalsIgnoreCase("0"))
                holder.tv_invalid.setText(context.getResources().getString(R.string.new_order));
            else if (model.getInvalid().equalsIgnoreCase("1"))
                holder.tv_invalid.setText(context.getResources().getString(R.string.order_repeated));
            else
                holder.tv_invalid.setText(context.getResources().getString(R.string.cancelled));

        } else {
            holder.tv_invalid.setVisibility(View.GONE);
        }

        holder.bt_repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbAdapter.open();
                dbAdapter.deleteCart();

                String phone = model.getMobile();
                String address = model.getAddress();

                AppUtils.writeStringToPref(context, "phone", phone);
                AppUtils.writeStringToPref(context, "address", address);

                add = address.split(",");
                add2 = add[3].split("/");
                AppUtils.writeStringToPref(context, "area", add[0]);
                AppUtils.writeStringToPref(context, "sector", add[1]);
                AppUtils.writeStringToPref(context, "apartment", add[2]);
                AppUtils.writeStringToPref(context, "block", add2[0]);
                AppUtils.writeStringToPref(context, "houseno", add2[1]);
                AppUtils.writeStringToPref(context, "floor", add[4]);
                AppUtils.writeStringToPref(context, "AddressAdding", "normal");

                Intent intent = new Intent(context, CartListActivity.class);
                intent.putExtra("getOrderid", model.getOrder_id());
                intent.putExtra("invalid", "1");
                intent.putExtra(CartListActivity.CANHITAPI, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface itemClickListner {
        void ClickonImage(OrderApiData orderImageListData);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image_order;
        Button bt_repeat;
        TextView tv_date, tv_invoice, tv_amt, tv_address, tv_invalid;

        public ViewHolder(View itemView) {
            super(itemView);
            image_order = itemView.findViewById(R.id.image_order);
            bt_repeat = itemView.findViewById(R.id.bt_repeat);

            tv_date = itemView.findViewById(R.id.tv_date);
            tv_invoice = itemView.findViewById(R.id.tv_invoice);
            tv_amt = itemView.findViewById(R.id.tv_amt);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_invalid = itemView.findViewById(R.id.tv_invalid);
        }
    }

}