package com.example.sheetalchauhan.orderbookingapplication.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Order;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.CustomerUpsyncAddress;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TotalDataActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tv_server_data, tv_phone_data, tv_toolbar, tv_upsync_customer_data,
            tv_upsync_order_data, tv_order_data, tv_phone_data_order;
    private Context context;
    private DBAdapter dbAdapter;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_data);
        context = this;
        dbAdapter = DBAdapter.getInstance(context);
        dbAdapter.open();
        setId();
        setValue();
        back.setOnClickListener(this);
    }


    /// changes made at dbadapter line 1114
    private void setValue() {
        tv_toolbar.setText(R.string.total_data);
        // downsync total customer from server
        String serverData = AppUtils.readStringFromPref(context, "total_customer");
        if (serverData != null && !serverData.isEmpty())
            tv_server_data.setText(serverData);
        else {
            tv_server_data.setTextColor(getResources().getColor(R.color.colorred));
            tv_server_data.setText(R.string.please_downsync);
        }

        // downsync total order from server
        String orderData = AppUtils.readStringFromPref(context, "total_order");
        if (orderData != null && !orderData.isEmpty())
            tv_order_data.setText("" + Integer.parseInt(orderData));
        else {
            tv_order_data.setTextColor(getResources().getColor(R.color.colorred));
            tv_order_data.setText(R.string.please_downsync);
        }
        tv_phone_data.setText("" + getCustomerFromLocal());

        // downsync total customer from local
        //String orderlockdata=AppUtils.readStringFromPref(context,"total_locked");
//        if (orderlockdata!=null && ! orderlockdata.isEmpty())
        //tv_phone_data.setText("" + getCustomerFromLocal());

        //.........................by himani............................
        //..........check setCOunt.............................
        try {

            String allPageCount = AppUtils.readStringFromPref(this, "allPageCount");
            String downsyncPageNo = AppUtils.readStringFromPref(this, "downsyncPageNo");

            Log.i("COUNT", allPageCount + "  " + downsyncPageNo);
            if (!allPageCount.equals(downsyncPageNo)) {
                int lock = 0;
                try {
                    HashMap<String, Integer> map = getOrderInfoNew();

                    lock = map.get("lockedCount");
                    if (lock > 0) {
                        tv_phone_data_order.setText("" + map.get("unlockedCount") + "     (  " + lock + " locked  )");
                    } else {
                        tv_phone_data_order.setText("" + map.get("unlockedCount"));
                    }
                } catch (Exception e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            } else {
                int lock = 0;
                try {
                    HashMap<String, Integer> map = getOrderInfo();
                    lock = map.get("lockedCount");
                    if (lock > 0) {
                        tv_phone_data_order.setText("" + map.get("unlockedCount") + "     (  " + lock + " locked  )");
                    } else {
                        tv_phone_data_order.setText("" + map.get("unlockedCount"));
                    }
                } catch (Exception e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            tv_phone_data_order.setText("" + getOrderFromLocal2());
            e.printStackTrace();

        }


        Log.i("local_order_all", "__" + getOrderFromLocal());
        Log.i("local_order_all3", "__" + getNewOrderFromLocal());
        Log.i("local_order_all4", "__" + getOrderFromLocal2());
        Log.i("local_order_unlock", "__" + getNewOrderFromLocalUnlock());

        tv_upsync_customer_data.setText("" + getNewCustomerFromLocal());
        tv_upsync_order_data.setText("" + getNewOrderFromLocal());
    }

    private void setId() {
        tv_upsync_customer_data = findViewById(R.id.tv_upsync_customer_data);
        tv_upsync_order_data = findViewById(R.id.tv_upsync_order_data);
        tv_order_data = findViewById(R.id.tv_order_data);
        tv_phone_data_order = findViewById(R.id.tv_phone_data_order);
        tv_server_data = findViewById(R.id.tv_server_data);
        tv_phone_data = findViewById(R.id.tv_phone_data);
        tv_toolbar = findViewById(R.id.tv_name);
        back = findViewById(R.id.back);
    }

    private int getCustomerFromLocal() {
        List<String> list = new ArrayList<>();
        list = dbAdapter.getallCustomerId();
        return list != null ? list.size() : 0;
    }

    private int getOrderFromLocal() {
        return dbAdapter.getOrderdummy() != null ? dbAdapter.getOrderdummy().size() : 0;
    }

    private int getOrderFromLocal2() {
        return dbAdapter.getOrderdummy2() != null ? dbAdapter.getOrderdummy2().size() : 0;
    }

    private int getNewCustomerFromLocal() {
        List<CustomerUpsyncAddress> cusotmerList = new ArrayList<>();
        cusotmerList = dbAdapter.getNewCustomer();
        return cusotmerList != null ? cusotmerList.size() : 0;
    }

    private int getNewOrderFromLocal() {
        List<Order> orderList = new ArrayList<>();
        orderList = dbAdapter.getOrderSyncData();
        return orderList != null ? orderList.size() : 0;
    }

    private int getNewOrderFromLocalUnlock() {
        return dbAdapter.getOrderSyncDataUnlock() != null ? dbAdapter.getOrderSyncDataUnlock().size() : 0;
    }

    //..................Written by himani..............................

    private HashMap<String, Integer> getOrderInfoNew() {
        HashMap<String, Integer> map = new HashMap<>();

        int lockedCount = 0;
        int unlockedCount = 0;
        if (dbAdapter.getOrderFullInfo().size() > 0) {
            for (Order order : dbAdapter.getOrderFullInfo()) {
                int lockedStatus = ((order.getLock_status() != null) || (order.getLock_status() != "")) ? Integer.parseInt(order.getLock_status()) : 0;
                if (lockedStatus == 1) {
                    lockedCount++;
                } else {
                    unlockedCount++;
                }
            }
        }
        map.put("lockedCount", lockedCount);
        map.put("unlockedCount", unlockedCount);
        return map;
    }

    private HashMap<String, Integer> getOrderInfo() {
        HashMap<String, Integer> map = new HashMap<>();

        int lockedCount = 0;
        int unlockedCount = 0;
        if (dbAdapter.getOrderFullInfoOldTable().size() > 0) {
            for (Order order : dbAdapter.getOrderFullInfoOldTable()) {
                int lockedStatus = ((order.getLock_status() != null) || (order.getLock_status() != "")) ? Integer.parseInt(order.getLock_status()) : 0;
                if (lockedStatus == 1) {
                    lockedCount++;
                } else {
                    unlockedCount++;
                }
            }
        }
        map.put("lockedCount", lockedCount);
        map.put("unlockedCount", unlockedCount);
        return map;
    }

    //........................End......................................


    @Override
    protected void onStop() {
        super.onStop();
        dbAdapter.close();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;

            default:
                finish();
        }
    }
}
