package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.app.Dialog;
import android.content.Context;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.firm.Firm;
import com.example.sheetalchauhan.orderbookingapplication.model.passcode.PasscodeRequestModel;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ResponsePojo;
import com.example.sheetalchauhan.orderbookingapplication.model.responsedata.ResponsePojoFirm;
import com.example.sheetalchauhan.orderbookingapplication.netwowk.NetworkStatus;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirmAdapter extends RecyclerView.Adapter<FirmAdapter.ViewHolder> {
    public String userid;
    Context context;
    View view1;
    List<Firm> addressListList;
    int mCheckedPosition;
    Firm firm;
    FirmAdapter.ViewHolder viewHolder1;
    String flag, format = null;
    boolean isSelected = false;
    String imeiNumber = "";
    Dialog dialog;
    String selectposition;
    String selectPosition = "";
    String isFirm = null;
    private ProgressBar progress_bar;

    public FirmAdapter(Context context1, List<Firm> addressListList) {
        context = context1;
        this.addressListList = addressListList;
        this.flag = flag;
        selectposition = AppUtils.readStringFromPref(context, "firmselected");
    }

    @Override
    public FirmAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.firm_layout, parent, false);
        viewHolder1 = new FirmAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final FirmAdapter.ViewHolder holder, final int position) {

        format = AppUtils.readStringFromPref(context, "format");
        firm = addressListList.get(position);
        isFirm = AppUtils.readStringFromPref(context, "firmcode");


        holder.addressid.setText(firm.getFirmcode());
        holder.name.setText(firm.getName());
        holder.firmaddress.setText(firm.getAddress());
        holder.firmmobile.setText(firm.getMobile());
        holder.ratio.setOnCheckedChangeListener(null);


        if (isFirm != null) {
            if (isFirm.equalsIgnoreCase(firm.getFirmcode())) {
                selectposition = "" + position;
                holder.ratio.setChecked(true);
                AppUtils.writeStringToPref(context, "firmname", holder.name.getText().toString());
                AppUtils.writeStringToPref(context, "firmaddress", holder.firmaddress.getText().toString());
                AppUtils.writeStringToPref(context, "firmmobile", holder.firmmobile.getText().toString());
//                AppUtils.writeStringToPref(context, "firmcode", holder.addressid.getText().toString());
                AppUtils.writeStringToPref(context, "firmselected", position + "");
            }

        }


        selectPosition = (AppUtils.readStringFromPref(context, "firmselected") == null) ? "" : AppUtils.readStringFromPref(context, "firmselected");
        if (selectPosition != "") {
            holder.ratio.setEnabled(false);
        } else {
            holder.ratio.setEnabled(true);
        }
//        if(selectPosition != "") {
        if (selectPosition.equals(position + "")) {
            holder.ratio.setChecked(true);
            holder.deselect.setVisibility(View.VISIBLE);
        } else {
            holder.ratio.setChecked(false);
            holder.deselect.setVisibility(View.GONE);
//            holder.main_layout.setEnabled(false);
        }
//        }


        holder.ratio.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                holder.ratio.setChecked(false);
                imeiNumber = deviceId();

                dialog = new Dialog(context);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.passcode_dialogbox);
                final EditText et_passcode = dialog.findViewById(R.id.et_passcode);
                Button bt_passcode = dialog.findViewById(R.id.bt_passcode);
                Button bt_cancel = dialog.findViewById(R.id.bt_cancel);
                progress_bar = dialog.findViewById(R.id.progress_bar);
                bt_passcode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (et_passcode.getText().toString().isEmpty()) {
                            Toast.makeText(context, "passcode can't left empty", Toast.LENGTH_SHORT).show();

                        } else {

                            PasscodeRequestModel model = new PasscodeRequestModel();
                            model.setDeviceId(imeiNumber);
                            model.setFirmCode(addressListList.get(position).getId());
                            model.setPassword(et_passcode.getText().toString().trim());


                            progress_bar.setVisibility(View.VISIBLE);

                            if (!NetworkStatus.getConnectivity(context)) {
                                isSelected = false;
                                Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                                progress_bar.setVisibility(View.GONE);
                            } else {

                                Call<ResponsePojoFirm> call = RetrofitHandler.getInstance().getApi().selectedNewFirm(model);

                                call.enqueue(new Callback<ResponsePojoFirm>() {
                                    @Override
                                    public void onResponse(Call<ResponsePojoFirm> call, Response<ResponsePojoFirm> response) {
                                        ResponsePojoFirm responseDto = response.body();
                                        if (response.isSuccessful()) {
                                            progress_bar.setVisibility(View.GONE);

                                            if (responseDto.isError() == false) {
                                                try {

                                                    if (responseDto.getMsg().get(0).contains("Firm selected successfully")) {
                                                        isSelected = true;

                                                        if (responseDto.getNext_invoice_no() != null) {
                                                            if (!responseDto.getNext_invoice_no().isEmpty() && !responseDto.getNext_invoice_no().equalsIgnoreCase("0")) {
                                                                AppUtils.writeStringToPref(context, "firm_next_invoice_id", "" + responseDto.getFirm_code() + responseDto.getNext_invoice_no());
                                                            } else {
                                                                AppUtils.writeStringToPref(context, "firm_next_invoice_id", "" + responseDto.getFirm_code() + "1");
                                                            }
                                                        } else {
                                                            AppUtils.writeStringToPref(context, "firm_next_invoice_id", "" + responseDto.getFirm_code() + "1");
                                                        }

                                                        SimpleDateFormat format = new SimpleDateFormat("HH");
                                                        String date = format.format(new Date());
                                                        if (Integer.parseInt(date) >= 10) {
                                                            try {
                                                                SimpleDateFormat format3 = new SimpleDateFormat("yyyyMMdd");
                                                                String dailyDate = format3.format(new Date());
                                                                AppUtils.writeStringToPref(context, "lastDayDate", dailyDate);
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                    } else {
                                                        isSelected = false;
                                                    }
                                                    Toast.makeText(context, "" + responseDto.getMsg().get(0), Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    if (responseDto.getMsg().contains("[Firm selected successfully.]")) {
                                                        isSelected = true;

                                                        if (responseDto.getNext_invoice_no() != null && !responseDto.getNext_invoice_no().isEmpty()) {
                                                            if (!responseDto.getNext_invoice_no().equalsIgnoreCase("0")) {
                                                                AppUtils.writeStringToPref(context, "firm_next_invoice_id", "" + responseDto.getNext_invoice_no());
                                                            } else {
                                                                AppUtils.writeStringToPref(context, "firm_next_invoice_id", "" +
                                                                        responseDto.getFirm_code() + "1");
                                                            }
                                                        }

                                                        SimpleDateFormat format = new SimpleDateFormat("HH");
                                                        String date = format.format(new Date());
                                                        if (Integer.parseInt(date) >= 10) {
                                                            try {
                                                                SimpleDateFormat format3 = new SimpleDateFormat("yyyyMMdd");
                                                                String dailyDate = format3.format(new Date());
                                                                AppUtils.writeStringToPref(context, "lastDayDate", dailyDate);

                                                            } catch (Exception e2) {
                                                                e2.printStackTrace();
                                                            }
                                                        }

                                                    } else {
                                                        isSelected = false;
                                                    }
                                                    Toast.makeText(context, "" + responseDto.getMsg().get(0), Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                isSelected = false;
                                                Toast.makeText(context, "" + responseDto.getMsg().get(0), Toast.LENGTH_SHORT).show();

                                            }
                                            dialog.dismiss();


                                            if (isSelected) {

                                                selectposition = "" + position;
                                                notifyDataSetChanged();
                                                holder.ratio.setChecked(true);

                                                if (format == null) {
                                                    AppUtils.writeStringToPref(context, "format", "first");
                                                }

                                                AppUtils.writeStringToPref(context, "firmname", holder.name.getText().toString());
                                                AppUtils.writeStringToPref(context, "firmaddress", holder.firmaddress.getText().toString());
                                                AppUtils.writeStringToPref(context, "firmmobile", holder.firmmobile.getText().toString());
                                                AppUtils.writeStringToPref(context, "firmcode", holder.addressid.getText().toString());
                                                AppUtils.writeStringToPref(context, "firm_invoiceId", holder.addressid.getText().toString());
//                                                AppUtils.writeStringToPref(context, "firmselected", firm.getId());
                                                AppUtils.writeStringToPref(context, "firmselected", position + "");

                                            } else {
                                                holder.ratio.setChecked(false);
                                            }


                                        } else {
                                            isSelected = false;
                                            holder.ratio.setChecked(false);
                                        }
                                    }

                                    @Override
                                    public void onFailure
                                            (Call<ResponsePojoFirm> call, Throwable t) {
                                        dialog.dismiss();
                                        isSelected = false;
                                        progress_bar.setVisibility(View.GONE);
                                        Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                                        holder.ratio.setChecked(false);
                                    }
                                });
                            }
                        }

                    }

                });
                // if button is clicked, close the custom dialog
                bt_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        holder.deselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.progressBar.setVisibility(View.VISIBLE);
                PasscodeRequestModel model = new PasscodeRequestModel();
                model.setDeviceId(imeiNumber);
                model.setFirmCode(addressListList.get(position).getId());

                if (!NetworkStatus.getConnectivity(context)) {
                    Toast.makeText(context, "No Internet Connection !", Toast.LENGTH_SHORT).show();
                    holder.progressBar.setVisibility(View.GONE);

                } else {
                    Call<ResponsePojo> call = RetrofitHandler.getInstance().getApi().sendDeSelectedFirm(model);
                    call.enqueue(new Callback<ResponsePojo>() {
                        @Override
                        public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                            ResponsePojo responseDto = response.body();
                            if (response.isSuccessful()) {
                                if (responseDto.isError() == false) {
                                    holder.progressBar.setVisibility(View.GONE);
                                    holder.deselect.setVisibility(View.GONE);
                                    Toast.makeText(context, "" + responseDto.getMsg().get(0), Toast.LENGTH_SHORT).show();
                                    AppUtils.writeStringToPref(context, "firmname", "");
                                    AppUtils.writeStringToPref(context, "firmaddress", "");
                                    AppUtils.writeStringToPref(context, "firmmobile", "");
                                    AppUtils.writeStringToPref(context, "firmcode", "");
//                                                AppUtils.writeStringToPref(context, "firmselected", firm.getId());
                                    AppUtils.writeStringToPref(context, "firmselected", "");
                                    notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponsePojo> call, Throwable t) {

                        }
                    });
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    private String deviceId() {
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, addressid, textView, firmaddress, firmmobile;
        public ImageView deleteadress, edit;
        public RadioButton ratio;
        public Button deselect;
        public LinearLayout main_layout;
        public ProgressBar progressBar;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.firmname);
            firmaddress = (TextView) v.findViewById(R.id.firmaddress);
            firmmobile = (TextView) v.findViewById(R.id.firmmobile);
            addressid = (TextView) v.findViewById(R.id.addressid);
            ratio = (RadioButton) v.findViewById(R.id.ratio);
            textView = (TextView) v.findViewById(R.id.textview);
            deselect = v.findViewById(R.id.deselect);
            main_layout = v.findViewById(R.id.main_layout);
            progressBar = v.findViewById(R.id.deSelectProgress);
        }
    }
}
