package com.example.sheetalchauhan.orderbookingapplication.model.item;

public class Customer {

    private int id;

    private String mobile;
    private String address;
    private String firm;
    private String flag;
    private String orderId;
    private String tags;
    private String type;
    private String name;
    private String cartage;
    private String comments;
    private String tags_;

    private String full_new_address;
    private String house;
    private String floor;
    private String block;
    private String apartment;
    private String setor;
    private String area;
    private String hindiAddress;
    private String calculation_weight;
    private String delivery_time;


    public String getHindiAddress() {
        return hindiAddress;
    }

    public void setHindiAddress(String hindiAddress) {
        this.hindiAddress = hindiAddress;
    }

    public String getTags_() {
        return tags_;
    }

    public void setTags_(String tags_) {
        this.tags_ = tags_;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCartage() {
        return cartage;
    }

    public void setCartage(String cartage) {
        this.cartage = cartage;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCalculation_weight() {
        return calculation_weight;
    }

    public void setCalculation_weight(String calculation_weight) {
        this.calculation_weight = calculation_weight;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getFull_new_address() {
        return full_new_address;
    }

    public void setFull_new_address(String full_new_address) {
        this.full_new_address = full_new_address;
    }
}



