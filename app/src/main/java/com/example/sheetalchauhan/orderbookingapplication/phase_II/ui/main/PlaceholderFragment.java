package com.example.sheetalchauhan.orderbookingapplication.phase_II.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.adapters.ApartmentAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.AreaAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.BlockAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.FloorAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.HouseNoAdapter;
import com.example.sheetalchauhan.orderbookingapplication.adapters.SectorAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class PlaceholderFragment extends Fragment {

    private static final String TITLE = "title";
    private static final String AREA = "area";
    private DBAdapter dbAdapter;
    private View root;
    private String title = null, area = null;
    private Context context;

    public static PlaceholderFragment newInstance(String title, String area) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AREA, area);
        bundle.putString(TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(TITLE);
            area = getArguments().getString(AREA);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_recent_address, container, false);
        dbAdapter = DBAdapter.getInstance(getActivity());
        RecyclerView rv_recyclerview = root.findViewById(R.id.rv_recyclerview);
        TextView tv_noData = root.findViewById(R.id.tv_noData);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        rv_recyclerview.setLayoutManager(layoutManager);
        setAdapter(title, rv_recyclerview, tv_noData);
        return root;
    }

    private Boolean showData(List<String> areaList, RecyclerView rv_recyclerview, TextView tv_noData) {
        Boolean isListHasData = false;
        if (areaList != null && areaList.size() > 0) {
            tv_noData.setVisibility(View.GONE);
            rv_recyclerview.setVisibility(View.VISIBLE);
            isListHasData = true;

        } else {
            tv_noData.setVisibility(View.VISIBLE);
            rv_recyclerview.setVisibility(View.GONE);
            isListHasData = false;
        }
        return isListHasData;

    }

    private void setAdapter(String title, RecyclerView rv_recyclerview, TextView tv_noData) {
        if (title == null)
            return;

        List<String> areaList = new ArrayList<>();

        if (title.equalsIgnoreCase("area")) {
            areaList = dbAdapter.getRecentAreaList();
            if (showData(areaList, rv_recyclerview, tv_noData)) {
                AreaAdapter areaAdapter = new AreaAdapter(getActivity(), areaList);
                rv_recyclerview.setAdapter(areaAdapter);
            }

        } else if (title.equalsIgnoreCase("sector")) {
            String areaString = AppUtils.readStringFromPref(context, "area");
            areaList = dbAdapter.getRecentSectorList(areaString);
            if (showData(areaList, rv_recyclerview, tv_noData)) {
                SectorAdapter sectorAdapter = new SectorAdapter(getActivity(), areaList);
                rv_recyclerview.setAdapter(sectorAdapter);
            }

        } else if (title.equalsIgnoreCase("apartment")) {
            String sectorString = AppUtils.readStringFromPref(context, "sector");
            areaList = dbAdapter.getRecentApartmentList(sectorString);
            if (showData(areaList, rv_recyclerview, tv_noData)) {
                ApartmentAdapter apartmentAdapter = new ApartmentAdapter(getActivity(), areaList);
                rv_recyclerview.setAdapter(apartmentAdapter);
            }

        } else if (title.equalsIgnoreCase("block")) {
            String apartmentString = AppUtils.readStringFromPref(context, "apartment");
            areaList = dbAdapter.getRecentBlockList(apartmentString);
            if (showData(areaList, rv_recyclerview, tv_noData)) {
                BlockAdapter blockAdapter = new BlockAdapter(getActivity(), areaList);
                rv_recyclerview.setAdapter(blockAdapter);
            }

        } else if (title.equalsIgnoreCase("house")) {
            String blockString = AppUtils.readStringFromPref(context, "block");
            areaList = dbAdapter.getRecentHousenoList(blockString);
            if (showData(areaList, rv_recyclerview, tv_noData)) {
                HouseNoAdapter houseAdapter = new HouseNoAdapter(getActivity(), areaList);
                rv_recyclerview.setAdapter(houseAdapter);
            }

        } else if (title.equalsIgnoreCase("floor")) {
            String houseString = AppUtils.readStringFromPref(context, "houseno");
            areaList = dbAdapter.getRecentFloorList(houseString);
            if (showData(areaList, rv_recyclerview, tv_noData)) {
                FloorAdapter floorAdapter = new FloorAdapter(getActivity(), areaList);
                rv_recyclerview.setAdapter(floorAdapter);
            }
        }
    }

}