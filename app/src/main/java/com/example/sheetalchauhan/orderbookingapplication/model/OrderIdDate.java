package com.example.sheetalchauhan.orderbookingapplication.model;

public class OrderIdDate {
    public String orderId;
    public String orderDate;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }
}
