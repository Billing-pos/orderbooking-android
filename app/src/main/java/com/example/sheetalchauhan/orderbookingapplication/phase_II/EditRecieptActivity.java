package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditRecieptActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.bt_submit)
    Button bt_submit;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.et_name)
    EditText et_name;
    String nameEt, phoneEt, ccommentEt, ocommentEt, ordercommentEt;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_reciept);
        ButterKnife.bind(this);
        context = this;
        tv_name.setText("Modify Receipt");
        setOnClick();
    }

    private void setOnClick() {
        back.setOnClickListener(this);
        bt_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_submit:
                getData();
                break;
        }

    }

    private void getData() {
        if (TextUtils.isEmpty(et_name.getText().toString())) {
            nameEt = et_name.getText().toString();
        }
    }
}
