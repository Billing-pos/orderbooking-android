package com.example.sheetalchauhan.orderbookingapplication.activities.ManageTag;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllTag extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_yellow)
    TextView tv_yellow;
    @BindView(R.id.tv_green)
    TextView tv_green;
    @BindView(R.id.tv_red)
    TextView tv_red;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_tab);
        ButterKnife.bind(this);

        tv_name.setText(R.string.manage_tagging);
        tv_green.setOnClickListener(this);
        tv_yellow.setOnClickListener(this);
        tv_red.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_green:
                seeParticularTag("green");
                break;

            case R.id.tv_yellow:
                seeParticularTag("yellow");
                break;

            case R.id.tv_red:
                seeParticularTag("red");
                break;

            case R.id.back:
                finish();
                break;
        }
    }

    private void seeParticularTag(String color) {
        Intent intent = new Intent(AllTag.this, SeeParticularTag.class);
        intent.putExtra("color", color);
        startActivity(intent);
    }
}