package com.example.sheetalchauhan.orderbookingapplication.phase_II;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.ui.main.PlaceholderFragment;
import com.google.android.material.tabs.TabLayout;

public class RecentAddress extends AppCompatActivity implements AreaFragment.OnFragmentInteractionListener {
    private int[] TAB_TITLES = new int[]{R.string.recent_area, R.string.all_area};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_address);

        TabLayout tabs = findViewById(R.id.tabs);
        TabLayout.Tab firstTab = tabs.newTab();
        firstTab.setText("Recent");
        tabs.addTab(firstTab);

        TabLayout.Tab secondTab = tabs.newTab();
        secondTab.setText("All");
        tabs.addTab(secondTab);

        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.container, new PlaceholderFragment());
                    ft.commit();

                } else {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.container, new AreaFragment());
                    ft.commit();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}