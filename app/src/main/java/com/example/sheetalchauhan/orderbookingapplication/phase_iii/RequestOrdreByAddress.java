package com.example.sheetalchauhan.orderbookingapplication.phase_iii;

public class RequestOrdreByAddress {
    private String appVersion;
    private String address;

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
