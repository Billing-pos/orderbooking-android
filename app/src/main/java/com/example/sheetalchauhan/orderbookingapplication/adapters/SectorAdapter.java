package com.example.sheetalchauhan.orderbookingapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.AddApartmentActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.List;

public class SectorAdapter extends RecyclerView.Adapter<SectorAdapter.ViewHolder> {
    private Context context;
    private View view1;
    private DBAdapter dbAdapter;
    private List<String> addressListList;
    private SectorAdapter.ViewHolder viewHolder1;
    private String hindiValue = "";

    public SectorAdapter(Context context, List<String> addressListList) {
        this.context = context;
        this.addressListList = addressListList;
        dbAdapter = DBAdapter.getInstance(context);
    }

    @Override
    public SectorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view1 = LayoutInflater.from(context).inflate(R.layout.area_layout, parent, false);
        viewHolder1 = new SectorAdapter.ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final SectorAdapter.ViewHolder holder, final int position) {
        if (addressListList == null)
            return;

        if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
            holder.name.setText(addressListList.get(position));
        } else {
            dbAdapter.open();
            hindiValue = dbAdapter.getHindiAddress(addressListList.get(position), "sector_district");
            if (hindiValue != null && !hindiValue.isEmpty()) {
                holder.name.setText(hindiValue);

            } else {
                holder.name.setText(addressListList.get(position));
            }
        }

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.writeStringToPref(context, "sector", addressListList.get(position));
                AppUtils.writeStringToPref(context, "sector2", holder.name.getText().toString());
                Intent intent = new Intent(context, AddApartmentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                context.startActivity(intent);
            }
        });

        dbAdapter.close();
    }

    @Override
    public int getItemCount() {
        return addressListList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;


        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.tv_area);

        }
    }


}
