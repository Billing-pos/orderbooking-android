package com.example.sheetalchauhan.orderbookingapplication.model.databasemodel;

public class ClientData {
    private String id;
    private String ClientName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }
}
