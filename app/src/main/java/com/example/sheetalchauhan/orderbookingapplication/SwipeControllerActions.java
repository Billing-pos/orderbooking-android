package com.example.sheetalchauhan.orderbookingapplication;

public abstract class SwipeControllerActions {

    public void onLeftClicked(int position) {
    }

    public void onRightClicked(int position) {
    }

}
