package com.example.sheetalchauhan.orderbookingapplication.model.firm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FirmData {
    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("msg")
    @Expose
    private List<String> listString;

    @SerializedName("data")
    @Expose
    private List<Firm> firmList;


    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<String> getListString() {
        return listString;
    }

    public void setListString(List<String> listString) {
        this.listString = listString;
    }

    public List<Firm> getFirmList() {
        return firmList;
    }

    public void setFirmList(List<Firm> firmList) {
        this.firmList = firmList;
    }
}
