package com.example.sheetalchauhan.orderbookingapplication.utility;

import com.example.sheetalchauhan.orderbookingapplication.R;

import java.util.HashMap;
import java.util.Map;

public class ItemShape {
    public Map<String, Integer> shape() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("mpo", R.drawable.round_shape);
        map.put("p ii", R.drawable.dual_line_bottom);
        map.put("mpmu", R.drawable.rectangle_shape);
        map.put("mpg", R.drawable.dual_round_shape);


//        if(map.get(drawable) != null){
//            return map.get(drawable);
//        } else {
//            return null;
//        }
        return map;
    }

    ;

    //Crash on main activity line number 140
    public enum Drawable {
        round_shape(R.drawable.round_shape), rectangle_shape(R.drawable.rectangle_shape), dual_round_shape(R.drawable.dual_round_shape);

        Drawable(int round_shape) {
//            R.drawable.round_shape,R.drawable.rectangle_shape;
        }
    }
}
