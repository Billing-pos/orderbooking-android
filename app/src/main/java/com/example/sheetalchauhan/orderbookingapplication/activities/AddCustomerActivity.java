package com.example.sheetalchauhan.orderbookingapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.MainActivity;
import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.AddAreaActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.addresses.AddPhoneActivity;
import com.example.sheetalchauhan.orderbookingapplication.adapters.BetterSearchAdapter;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Customer;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppClass;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCustomerActivity extends AppCompatActivity implements View.OnClickListener, BetterSearchAdapter.SearchAdapterListener {
    public List<Customer> customerList = new ArrayList<>();
    public List<Customer> customerList_temp = new ArrayList<>();
    public List<String> phoneList = null;
    public List<String> phoneList2 = new ArrayList<>();
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_phone)
    AutoCompleteTextView et_phone;
    @BindView(R.id.et_address)
    AutoCompleteTextView et_address;
    @BindView(R.id.bt_phone_search)
    Button bt_phone_search;
    @BindView(R.id.bt_address_search)
    Button bt_address_search;
    @BindView(R.id.bt_address_filter)
    Button bt_address_filter;
    @BindView(R.id.img_close1)
    ImageView img_close1;
    @BindView(R.id.tv_add_new_customer)
    TextView tv_add_new_customer;
    @BindView(R.id.tv_phone_error)
    TextView tv_phone_error;
    @BindView(R.id.tv_address_error)
    TextView tv_address_error;
    @BindView(R.id.img_close)
    ImageView img_close;
    String phone, address;
    DBAdapter dbAdapter;
    ProgressDialog progressDialog;
    List<Customer> myCustomerList = new ArrayList<>();
    BetterSearchAdapter searchAdapter;
    private Boolean canBackForOrder = false;
    private RecyclerView rview;

    // to validate phone number & telephone number
    public static boolean isValidMobile(String s) {
        Pattern p = Pattern.compile("(0/91)?[5-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    public static boolean isValidTelephone(String s) {
        Pattern p = Pattern.compile("[2-9][0-9]{7}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    @Override
    public void onBackPressed() {

        if (canBackForOrder) {
            finish();
        } else {

            et_address.setVisibility(View.VISIBLE);
            Intent intent = new Intent(AddCustomerActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        ButterKnife.bind(this);

        canBackForOrder = getIntent().getBooleanExtra("canback", false);

        tv_name.setText(R.string.check_customer);
        dbAdapter = DBAdapter.getInstance(this);
        back.setOnClickListener(this);
        bt_address_search.setOnClickListener(this);
        bt_phone_search.setOnClickListener(this);
        tv_add_new_customer.setOnClickListener(this);
        img_close.setOnClickListener(this);
        img_close1.setOnClickListener(this);
        bt_address_filter.setOnClickListener(this);
        dbAdapter.open();
        phoneList = new ArrayList<>();
        progressDialog = new ProgressDialog(AddCustomerActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        dbAdapter.deleteCart();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDataForBetterSearching();
            }
        }, 80);


        et_address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    et_address.setClickable(false);
                    img_close.setVisibility(View.VISIBLE);
                    et_address.setEnabled(false);
                }
            }
        });
        List<String> temp = new ArrayList<>();

        et_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (et_address.getText().length() > 0) {
                        rview.setVisibility(View.VISIBLE);
                    } else
                        rview.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                searchAdapter.getFilter().filter(et_address.getText().toString());

                if (et_address.getText().toString().isEmpty()) {
                    img_close.setVisibility(View.GONE);
                    tv_address_error.setVisibility(View.GONE);
                } else {
                    img_close.setVisibility(View.VISIBLE);
                    tv_address_error.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        et_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (et_phone.getText().toString().isEmpty()) {
                    img_close1.setVisibility(View.GONE);
                    tv_phone_error.setVisibility(View.GONE);

                } else {
                    img_close1.setVisibility(View.VISIBLE);
                    tv_phone_error.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

//        getDataForBetterSearching();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_address_search:
                address = et_address.getText().toString();
                if (address.isEmpty()) {
                    tv_phone_error.setVisibility(View.GONE);
                    tv_address_error.setVisibility(View.VISIBLE);
                    tv_address_error.setText("* Please enter address !");
                    et_address.requestFocus();
                    return;

                } else if (!isValidAddress(address)) {
                    tv_phone_error.setVisibility(View.GONE);
                    img_close.setVisibility(View.VISIBLE);
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "hide", "show");
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "area", address);
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "newaddress", "addPhone");
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "AddressAdding", "new");
                    Intent intent = new Intent(AddCustomerActivity.this, AddAreaActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    return;

                } else {
                    address = et_address.getText().toString().replaceAll("/ ", "/");
                    tv_phone_error.setVisibility(View.GONE);
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "address", address);
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "phone", "");
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "hide", "show");
                    Intent intent = new Intent(AddCustomerActivity.this, PhoneListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    if (AppClass.isOrderFirst)
                        finish();
                }
                break;

            case R.id.bt_phone_search:
                phone = et_phone.getText().toString();
                if (phone.isEmpty()) {
                    tv_phone_error.setVisibility(View.VISIBLE);
                    tv_address_error.setVisibility(View.GONE);
                    tv_phone_error.setText("* Please enter phone number");
                    et_phone.requestFocus();
                    return;

                } else if (!isValidMobile(phone) && !isValidTelephone(phone)) {
                    tv_address_error.setVisibility(View.GONE);
                    tv_phone_error.setVisibility(View.VISIBLE);
                    tv_phone_error.setText("* This is invalid number !");
                    et_phone.requestFocus();
                    return;

                } else {
                    tv_phone_error.setVisibility(View.GONE);
                    tv_address_error.setVisibility(View.GONE);
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "phone", phone);
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "address", "");
                    AppUtils.writeStringToPref(AddCustomerActivity.this, "hide", "show");
                    Intent intent1 = new Intent(AddCustomerActivity.this, AddressListActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent1);
                    if (AppClass.isOrderFirst)
                        finish();


                }
                break;

            case R.id.img_close:
                et_address.setText("");
                et_address.requestFocus();
                img_close.setVisibility(View.GONE);
                et_address.setEnabled(true);
                break;

            case R.id.img_close1:
                et_phone.setText("");
                et_phone.requestFocus();
                img_close1.setVisibility(View.GONE);
                et_phone.setEnabled(true);
                break;

            case R.id.back:
                onBackPressed();
                break;

            case R.id.tv_add_new_customer:
                Intent intent1 = new Intent(AddCustomerActivity.this, AddPhoneActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent1);
                break;

            case R.id.bt_address_filter:
                Intent intent2 = new Intent(AddCustomerActivity.this, AddAreaActivity.class);
                AppUtils.writeStringToPref(AddCustomerActivity.this, "newaddress", "addPhone");
                AppUtils.writeStringToPref(AddCustomerActivity.this, "hide", "hide");
                AppUtils.writeStringToPref(AddCustomerActivity.this, "phone", "");
                intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent2);
                break;
        }
    }

    // to validate existing address
    public boolean isValidAddress(String s) {
        boolean isValidAddress = false;

        for (int i = 0; i < customerList.size(); i++) {
            if (customerList.get(i).getAddress().contains(s)) {
                isValidAddress = true;
                break;
            } else {
                isValidAddress = false;
            }
        }
        return isValidAddress;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUtils.writeStringToPref(AddCustomerActivity.this, "area", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "phone", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "sector", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "apartment", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "block", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "houseno", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "floor", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "address", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "addressid", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "newaddress", "");
        AppUtils.writeStringToPref(AddCustomerActivity.this, "AddressAdding", "");

    }


    private void getDataForBetterSearching() {
        phoneList = dbAdapter.getCustomerListSize();


        ArrayAdapter adapter1 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, phoneList);
        et_phone.setAdapter(adapter1);
        et_phone.setThreshold(1);
        progressDialog.dismiss();


        myCustomerList = dbAdapter.getAllPhoneList1NewPhase3();

        for (int i = 0; i < myCustomerList.size(); i++) {
            customerList.add(myCustomerList.get(i));

            Customer customer = new Customer();
            customer.setAddress(myCustomerList.get(i).getAddress());

            customerList_temp.add(customer);
        }

        rview = findViewById(R.id.rview);
        searchAdapter = new BetterSearchAdapter(this, customerList, customerList_temp, this);
        rview.setAdapter(searchAdapter);
    }

    @Override
    public void onAddressSelected(String contact) {
        if (contact != null) {
            et_address.setText(contact);
            rview.setVisibility(View.GONE);
            int pos = et_address.getText().length();
            et_address.setSelection(pos);
        }
    }


}
