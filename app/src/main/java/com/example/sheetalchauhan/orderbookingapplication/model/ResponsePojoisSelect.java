package com.example.sheetalchauhan.orderbookingapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePojoisSelect {
    @SerializedName("false")
    @Expose
    private boolean error;

    @SerializedName("msg")
    @Expose
    private List<String> msg;

    @SerializedName("code")
    @Expose
    private String code;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<String> getMsg() {
        return msg;
    }

    public void setMsg(List<String> msg) {
        this.msg = msg;
    }

    public String getcode() {
        return code;
    }

    public void setcode(String code) {
        this.code = code;
    }

}
