package com.example.sheetalchauhan.orderbookingapplication.model;

import java.util.List;

public class ReceiptTypeResponse {

    private Boolean error;
    private String msg;
    private List<ReceiptTypeModel> data;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ReceiptTypeModel> getData() {
        return data;
    }

    public void setData(List<ReceiptTypeModel> data) {
        this.data = data;
    }
}
