package com.example.sheetalchauhan.orderbookingapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.activities.PhoneListActivity;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddNewPhoneActivity extends AppCompatActivity implements View.OnClickListener {
    public List<String> customerList = new ArrayList<>();
    AutoCompleteTextView et_address;
    ImageView img_close;
    String address;
    @BindView(R.id.tv_address_error)
    TextView tv_address_error;
    DBAdapter dbAdapter;
    ProgressDialog progressDialog;
    Button bt_address_search;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_phone);
        ButterKnife.bind(this);
        context = this;
        AppUtils.writeStringToPref(context, "flag", "1");
        tv_name.setText(R.string.search_add_new_phone);
        bt_address_search = findViewById(R.id.bt_address_search);
        img_close = findViewById(R.id.img_close);
        bt_address_search.setOnClickListener(this);
        img_close.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();
        progressDialog = new ProgressDialog(AddNewPhoneActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        }, 50);
        et_address = findViewById(R.id.et_address);
        img_close = findViewById(R.id.img_close);
        et_address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    //  et_address.setClickable(false);
                    img_close.setVisibility(View.VISIBLE);
                    //et_address.setEnabled(false);
                }
            }
        });

        et_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (et_address.getText().toString().isEmpty()) {
                    img_close.setVisibility(View.GONE);
                    //tv_address_error.setVisibility(View.GONE);
                } else {
                    img_close.setVisibility(View.VISIBLE);
                    // tv_address_error.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        if (getIntent().getBooleanExtra("addPhoneNew", false)) {
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_address_search:
                address = et_address.getText().toString();
                if (address.isEmpty()) {
                    tv_address_error.setVisibility(View.VISIBLE);
                    tv_address_error.setText(R.string.please_enter_address_);
                    et_address.requestFocus();
                    return;

                } else if (!isValidAddress(address)) {
                    img_close.setVisibility(View.VISIBLE);
                    tv_address_error.setVisibility(View.VISIBLE);
                    tv_address_error.setText(R.string.invalid_address);

                } else {
                    address = et_address.getText().toString().replaceAll("/ ", "/");
                    AppUtils.writeStringToPref(AddNewPhoneActivity.this, "address", address);
                    AppUtils.writeStringToPref(AddNewPhoneActivity.this, "address2", address);
                    AppUtils.writeStringToPref(AddNewPhoneActivity.this, "phone", "");
                    AppUtils.writeStringToPref(AddNewPhoneActivity.this, "hide", "show");
                    Intent intent = new Intent(AddNewPhoneActivity.this, PhoneListActivity.class);
                    intent.putExtra("addPhoneNew", true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;
            case R.id.img_close:
                et_address.setText("");
                et_address.requestFocus();
                img_close.setVisibility(View.GONE);
                et_address.setEnabled(true);
                break;

        }
    }


    public boolean isValidAddress(String s) {
        boolean isValidAddress = false;

        for (int i = 0; i < customerList.size(); i++) {
            if (customerList.contains(s)) {
                isValidAddress = true;
                break;
            } else {
                isValidAddress = false;
            }
        }
        return isValidAddress;
    }

    private void getData() {
        customerList = dbAdapter.getAllPhoneList1New();
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, customerList);
        et_address.setAdapter(adapter);
        et_address.setThreshold(1);
        progressDialog.dismiss();
    }
}
