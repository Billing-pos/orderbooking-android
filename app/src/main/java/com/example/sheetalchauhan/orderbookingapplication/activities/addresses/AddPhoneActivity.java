package com.example.sheetalchauhan.orderbookingapplication.activities.addresses;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddPhoneActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.bt_proceed)
    Button bt_proceed;
    String phonestr;

    public static boolean isValidTelephone(String s) {
        Pattern p = Pattern.compile("[2-9][0-9]{7}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    // to validate phone number
    public static boolean isValid(String s) {
        Pattern p = Pattern.compile("(0/91)?[5-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_phone);
        ButterKnife.bind(this);
        tv_name.setText(R.string.add_phone_no);
        back.setOnClickListener(this);
        bt_proceed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_proceed:
                phonestr = et_phone.getText().toString();
                if (phonestr.isEmpty()) {
                    et_phone.setError("Please Enter Phone No");
                    et_phone.requestFocus();
                    return;

                } else if (!isValid(phonestr)) {
                } else if (!isValidTelephone(phonestr) && !isValidTelephone(phonestr)) {
                    et_phone.setError("invalid mobile !");
                    et_phone.requestFocus();
                    return;

                } else {
                    AppUtils.writeStringToPref(AddPhoneActivity.this, "phone", phonestr);
                    Intent intent = new Intent(AddPhoneActivity.this, AddAreaActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }
                break;
        }

    }
}
