package com.example.sheetalchauhan.orderbookingapplication.utility;

public interface Constants {



    /*api constants*/


    int STATUS_SUCCESS = 200;
    int STATUS_FAIL = 500;
    int STATUS_DEV_ERROR = 400;


    String BASE_URL = "http://digital306india.com/api/";//local
//    String BASE_URL="http://kgsd.addleaddigital.net/";//LIVE

//   String BASE_URL="https://www.myhgm.com/api/";//live

    //String BASE_URL_PAY = "http://10.107.4.9:8052/";//local
    String BASE_URL_PAY = "https://www.myhgm.com/";//LIve
    String BASE_URL_PAYMENT = "https://www.happygrahak.com/";//local

    String METHOD_STATEMENT_SERVICE = "serviceslog";
    String METHOD_STATEMENT_REFERAL = "directreferrallog";
    String METHOD_STATEMENT_ORDER = "";
    String METHOD_STATEMENT_WALLET = "walletstatement";


    int ACTIVITY_VIDEO = 6;


    /*avennue*/
    String STR_COMMAND = "command";


    String STR_WALLET = "amount_wallet";

    String STR_ENC_VAL = "enc_val";

    String STR_OTDER_TYPE = "str_otder_type";
    String PARAMETER_SEP = "&";
    String PARAMETER_EQUALS = "=";
    String DELIVERY_NAME = "delivery_name";
    String DELIVERY_ADDRESS = "delivery_address";
    String DELIVERY_CITY = "delivery_city";
    String DELIVERY_STATE = "delivery_state";
    String DELIVERY_ZIP = "delivery_zip";
    String DELIVERY_COUNTRY = "delivery_country";
    String DELIVERY_TEL = "delivery_tel";
    String ACCOUNT_MANAGER = "accountmanager";


//    String MERCHANT_ID = "161988";
//    String ACCESS_CODE = "AVHK75FA46AS65KHSA";
    //public static final String ACCESS_CODE = "AVQU75FA45AQ05UQQA";

    //https://www.linkedin.com/pulse/why-necessary-integrate-sdk-facebook-app-install-anurag-golipkar
    String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";


    //  String RESPONSE_HANDLER_URL = "https://www.happypesa.com/secure/ccavResponseHandler.php";
    //  String PURCHASE_RESPONSE_HANDLER_URL = "https://www.happypesa.com/paymentPackage/ccavResponseHandler.php";
    String RESPONSE_HANDLER_URL = BASE_URL + "payment-response";

    String RECHARGE_RESPONSE_HANDLER_URL_MEMBER_PLAN = BASE_URL_PAYMENT + "membership-pay-response";

    String TRANS_STATUS = "trans_status";
    String METHOD_PAYMENT_UPDATE = "package_update";


    /*end ccAvenue*/

    /*DMT*/
    String PARAM_STB_IS_REG = "param_is_reg";
    String PARAM_STB_REF = "param_stb_ref";
    String PARAM_STB_NUMBER = "param_stb_number";

    String TOTAL_LM = "total_lm";
    String TOTAL_RM = "total_rm";
    String TOTAL_UL = "total_ul";
    String TOTAL_UR = "total_ur";


    String STB_DATA = "stb_data";
    String STD_FULL_DATA = "std_full_data";


    /*view pager pages*/
    int PAGER_HOME = 0;

    int PAGER_INVITE = 2;


    // view pager for hotel pages...
    int PAGER_SUMMARY = 0;
    int PAGER_PHOTO = 1;

    //view pager for all rooms..
    int PAGER_ALLROOM = 0;
    int PAGER_BREAKFAST = 1;

    //view pager for bus tab.....

    int PAGER_FIRST = 0;
    int PAGER_SECOND = 1;
    int PAGER_THIRD = 2;
    int PAGER_FORTH = 3;
    int PAGER_FIFTH = 4;
    int PAGER_SIXTH = 5;
    int PAGER_SEVENTH = 6;


    /*permission constants*/
    int ALL_PERMISSION = 99;


    String NETWORK_INTENT = "android.net.conn.CONNECTIVITY_CHANGE";
    String INTENT_RESULT = "result";
    String STR_AD_ID = "str_ad_id";
    String STR_AD_COST = "str_ad_cost";
    int LOG_SIGN_CODE = 1;
    int PASS_CODE = 2;


    /*Dto id*/
    String SIGNUP_DATA = "signup_data";

    /*app status*/
    String PREF_APP_STATUS = "pref_app_status";
    String PREF_STATUS_OTP = "pref_status_otp";
    String PREF_STATUS_LOGIN = "pref_status_login";


    String PREF_VERSION_NUM = "pref_version_num";
    String DIALOG_FRAG_TAG = "dialog_frag_tag";

    /*prefrences data*/
    String PREF_DATA = "pref_data";
    String PREF_USER_DATA = "pref_user_data";
    String PREF_TTACK_DATA = "pref_ttack_data";


    String PREF_MOBILE = "pref_mobile";

    String FIREBASE_KEY = "firebase_key";

    String REFERRAL_KEY = "referrer";
    String PREF_BANNER = "pref_banner";


    /*app strings*/
    String CONTACT_DEV = "Please Contact our support";
    String ENTER_VALID = "Please Fill Valid Detail";
    String EXCEPTION_MSG = "We are facing some issues. Please check back later."/*"Please try again later."*/;
    String EXCEPTION_MSG_TIMEOUT = "Poor network connection"/*"Please try again later."*/;
    String USER_ERROR = "User error"/*"Please check credentials."*/;
    String POST_ERROR = "Post error"/*"Please check credentials."*/;
    String CREDENTIAL_ERROR = "Please check credentials.";


    String PROG_WAIT = "Please wait ...";
    String NETWORK_ERROR = "Network not available";
    String ALL = "All";
    String STR_EXIT_MSG = "Double click to exit";

    String STR_INTENT_VALUE = "str_intent_value";
    String STR_INTENT_BODY = "str_intent_body";
    String STR_INTENT_HAPPY = "str_intent_happy";
    String TYPE = "type";
    String STR_INTENT_PACKAGE = "str_intent_package";
    String STR_INTENT_TRACKER = "str_intent_tracker";
    String STR_INTENT_COUNT = "str_intent_count";


    String TITLE = "title";


    /*String id, String source, String desc, String url, String appLogo*/
    String ID = "id";


    String RATE_US = "rate_us";
    int Recharge_constant = 5;
    String LANG_PREF = "langPref";


    String CLIENT_TOKEN = "ax4@ub^81mb0#@";
    String ACCEPT = "application/json";
    String DEVICE = "android";
    //PAYMENT GATEWAY
    String MERCHANT_ID = "161988";
    String ACCESS_CODE = "AVHK75FA46AS65KHSA";
    String STR_ACCESS_CODE = "access_code";
    String STR_MERCHANT_ID = "merchant_id";
    String STR_ORDER_ID = "order_id";
    String STR_CURRENCY = "currency";
    String CURRENCY = "INR";
    String STR_AMOUNT = "amount";
    String STR_REDIRECT_URL = "redirect_url";
    //    String STR_REDIRECT_URL=BASE_URL+"gateway/PHP/ccavResponseHandler.php";
    String STR_CANCEL_URL = "cancel_url";
    //    String STR_CANCEL_URL=BASE_URL+"gateway/PHP/ccavResponseHandler.php";
    String STR_RSA_KEY_URL = "rsa_key_url";
    //    String STR_RSA_KEY_URL=BASE_URL+"gateway/PHP/GetRSA.php";
    String RSA_URL = BASE_URL + "get-rsa-key";
}
