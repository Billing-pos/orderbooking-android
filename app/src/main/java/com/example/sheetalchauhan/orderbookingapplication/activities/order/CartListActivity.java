package com.example.sheetalchauhan.orderbookingapplication.activities.order;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheetalchauhan.orderbookingapplication.BuildConfig;
import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.SwipeController;
import com.example.sheetalchauhan.orderbookingapplication.activities.AEMScrybeDevice;
import com.example.sheetalchauhan.orderbookingapplication.activities.AddCustomerActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.ChangeReceiptFormat;
import com.example.sheetalchauhan.orderbookingapplication.activities.IAemScrybe;
import com.example.sheetalchauhan.orderbookingapplication.activities.ItemSwipeCallback;
import com.example.sheetalchauhan.orderbookingapplication.activities.PrintBluetoothActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.navigationsactivity.RecieptFormatActivity;
import com.example.sheetalchauhan.orderbookingapplication.activities.product.AddProductActivity;
import com.example.sheetalchauhan.orderbookingapplication.adapters.CartAdapter;
import com.example.sheetalchauhan.orderbookingapplication.breceiver.AlarmReceiver;
import com.example.sheetalchauhan.orderbookingapplication.breceiver.Constants;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.interfaces.RetrofitHandler;
import com.example.sheetalchauhan.orderbookingapplication.model.customer.CustomergetAddress;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Order;
import com.example.sheetalchauhan.orderbookingapplication.netwowk.NetworkStatus;
import com.example.sheetalchauhan.orderbookingapplication.phase_II.model.OrderImageListData;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.RequestUserPermissionModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.SuperUserResponse;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.UserActivityModel;
import com.example.sheetalchauhan.orderbookingapplication.phase_iii.model.ResponseSearchProduct;
import com.example.sheetalchauhan.orderbookingapplication.screenshot.ScreenshotUtil;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppClass;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;
import com.example.sheetalchauhan.orderbookingapplication.utility.HandleError;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartListActivity extends AppCompatActivity implements View.OnClickListener, IAemScrybe, CartAdapter.CartProductsAdapterListener {
    public static final String CANHITAPI = "ishitApi";
    @BindView(R.id.ll_firm)
    LinearLayout ll_firm;
    @BindView(R.id.ll_first_r)
    LinearLayout ll_first_r;
    @BindView(R.id.ll_full)
    LinearLayout ll_full;
    @BindView(R.id.ll_name_number)
    LinearLayout ll_name_number;
    @BindView(R.id.ll_receipt)
    LinearLayout ll_receipt;
    @BindView(R.id.ll_second_r)
    RelativeLayout ll_second_r;
    @BindView(R.id.ll_third_r)
    RelativeLayout ll_third_r;
    @BindView(R.id.ll_fourth_r)
    RelativeLayout ll_fourth_r;
    @BindView(R.id.ll_fullscreen)
    LinearLayout parentView2;
    @BindView(R.id.ll_notfullscreen)
    LinearLayout parentView;
    @BindView(R.id.ll_bottom_amount)
    LinearLayout ll_bottom_amount;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.piece)
    TextView piece;
    @BindView(R.id.tv_tobepaid)
    TextView tv_tobepaid;
    @BindView(R.id.tv_firm_name1)
    TextView tv_firm_name1;
    @BindView(R.id.tv_firm_name2)
    TextView tv_firm_name2;
    @BindView(R.id.tv_firm_name3)
    TextView tv_firm_name3;
    @BindView(R.id.tv_firm_name4)
    TextView tv_firm_name4;
    @BindView(R.id.tv_address1)
    TextView tv_address1;
    @BindView(R.id.tv_address2)
    TextView tv_address2;
    @BindView(R.id.tv_address3)
    TextView tv_address3;
    @BindView(R.id.tv_address4)
    TextView tv_address4;
    @BindView(R.id.tv_mobile1)
    TextView tv_mobile1;
    @BindView(R.id.tv_mobile2)
    TextView tv_mobile2;
    @BindView(R.id.tv_mobile3)
    TextView tv_mobile3;
    @BindView(R.id.tv_mobile4)
    TextView tv_mobile4;
    @BindView(R.id.tv_invoice1)
    TextView tv_invoice1;
    @BindView(R.id.tv_invoice2)
    TextView tv_invoice2;
    @BindView(R.id.tv_invoice3)
    TextView tv_invoice3;
    @BindView(R.id.tv_invoice4)
    TextView tv_invoice4;
    @BindView(R.id.ll_edit)
    LinearLayout ll_edit;
    @BindView(R.id.discount_amount)
    TextView discount_amount;
    @BindView(R.id.tv_discount)
    TextView tv_discount;
    @BindView(R.id.tv_retail_dis)
    TextView tv_retail_dis;
    @BindView(R.id.retail_amount)
    TextView retail_amount;
    @BindView(R.id.amount)
    TextView tvAmount;
    @BindView(R.id.addmore)
    TextView addmore;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.tv_customername)
    TextView tv_customername;
    @BindView(R.id.cartage_amount)
    TextView cartage_amount;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.bt_pay)
    Button bt_pay;
    @BindView(R.id.iv_firm)
    ImageView iv_firm;
    @BindView(R.id.qrCode)
    ImageView qrCode;
    @BindView(R.id.sp_receipt_type)
    Spinner sp_receipt_type;
    @BindView(R.id.et_usercomment_print)
    EditText et_usercomment_print;
    @BindView(R.id.et_ordercomment_print)
    EditText et_ordercomment_print;
    @BindView(R.id.et_customer_name)
    EditText et_customer_name;
    @BindView(R.id.et_second_phone)
    EditText et_second_phone;
    String formattedDate, phone, address, addressHindi, companyname, firmcode = "";
    DBAdapter dbAdapter;
    double amount = 0.0;
    String phonestr, orderid, r_format = null;
    List<Cart> cartList = new ArrayList<>();
    long rowId;
    int commentIdEt;
    Context context;
    String orderids, address1 = null, receitpt__type = "N";
    String add[];
    String add2[];
    String addHindi[];
    String add2Hindi[];
    ArrayList<String> printerList;
    CartAdapter cartAdapter;
    SwipeController swipeController = null;
    AEMScrybeDevice aemScrybeDevice;
    int commentId = -1;
    private int spnCommentId;
    private Bitmap bitmap = null;
    private List list = new ArrayList<>();
    private String invalid = "0";
    private String customComment = "", user_name = "", secondPhone = "", orderId = "";
    private int alarmCommentId = 0, editCommentId = -1;
    private String alarmCommentText = null, customerComment = "", tags_new = "";
    private Boolean isSpinnerSelected = false;
    private int commentid;
    private AppCompatCheckBox cb_newuser;
    private float amt_cartage = 0, amt_dis_cartage = 0, amt_dis_retail = 0, amt_total, to_be_paid = 0, paid_amount_already = 0;
    private float serverCartage = 0, server_dis_cartage = 0, server_dis_retail = 0, calculatoin_weight;
    private String timeET, dateET, todoET;
    private Button btndate;
    private Button btntime, bt_comment;
    private List<String> receipt_Type = new ArrayList<>();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        ButterKnife.bind(this);
        tv_name.setText(R.string.my_cart);
        context = this;
        dbAdapter = DBAdapter.getInstance(this);
        back.setOnClickListener(this);
        ll_receipt.setOnClickListener(this);
        bt_pay.setOnClickListener(this);
        Date c = Calendar.getInstance().getTime();

        if (getIntent().getStringExtra("invalid") != null) {
            invalid = getIntent().getStringExtra("invalid");
        }

        if (getIntent().getBooleanExtra(CartListActivity.CANHITAPI, false)) {
            String getOrderId = getIntent().getStringExtra("getOrderid");
            String tempOrderId = getOrderId;

            if (tempOrderId.contains("_")) {

                String[] temp = getOrderId.split("_");


                if (temp[1] != null && !temp[1].isEmpty()) {
                    tempOrderId = temp[0] + "_" + temp[1];
                }
            }

            getOrderId = tempOrderId;


            if (null != getOrderId && !getOrderId.isEmpty())
                hitApi(getOrderId);
            else
                Toast.makeText(context, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }


        phonestr = AppUtils.readStringFromPref(context, "phone");
        aemScrybeDevice = new AEMScrybeDevice(CartListActivity.this);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = df.format(c);
        phone = AppUtils.readStringFromPref(context, "phone");
        address = AppUtils.readStringFromPref(context, "address").replaceAll(",", ", ");
        address = AppUtils.readStringFromPref(context, "address").replaceAll(",  ", ", ");
        /// hindi address
        addressHindi = AppUtils.readStringFromPref(context, "address2").replaceAll(",", ", ");
        addressHindi = AppUtils.readStringFromPref(context, "address2").replaceAll(",  ", ", ");


        firmcode = AppUtils.readStringFromPref(context, "firmcode");

        if (address != null && !address.isEmpty()) {
            add = address.split(",");
            add2 = add[3].split("/");
            AppUtils.writeStringToPref(context, "area", add[0]);
            AppUtils.writeStringToPref(context, "sector", add[1]);
            AppUtils.writeStringToPref(context, "apartment", add[2]);
            AppUtils.writeStringToPref(context, "block", add2[0]);
            AppUtils.writeStringToPref(context, "houseno", add2[1]);
            AppUtils.writeStringToPref(context, "floor", add[4]);

            companyname = AppUtils.readStringFromPref(context, "companyname");


            if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                tv_customername.setText("Address : " + AppUtils.readStringFromPref(context, "area") + ", " + AppUtils.readStringFromPref(context, "sector") + ", " + AppUtils.readStringFromPref(context, "apartment") +
                        "\n                   " + AppUtils.readStringFromPref(context, "block") + "/" + AppUtils.readStringFromPref(context, "houseno") + ", " + AppUtils.readStringFromPref(context, "floor")
                        + "\n\nPhone :   " + phone);
            } else {

                ///hindi address///////
                addHindi = addressHindi.split(",");
                add2Hindi = addHindi[3].split("/");
                AppUtils.writeStringToPref(context, "area2", addHindi[0]);
                AppUtils.writeStringToPref(context, "sector2", addHindi[1]);
                AppUtils.writeStringToPref(context, "apartment2", addHindi[2]);
                AppUtils.writeStringToPref(context, "block2", add2Hindi[0]);
                AppUtils.writeStringToPref(context, "houseno2", add2Hindi[1]);
                AppUtils.writeStringToPref(context, "floor2", addHindi[4]);

                tv_customername.setText("Address : " + AppUtils.readStringFromPref(context, "area2") + ", " + AppUtils.readStringFromPref(context, "sector2") + ", " + AppUtils.readStringFromPref(context, "apartment2") +
                        "\n                   " + AppUtils.readStringFromPref(context, "block2") + "/" + AppUtils.readStringFromPref(context, "houseno2") + ", " + AppUtils.readStringFromPref(context, "floor2")
                        + "\n\nPhone :   " + phone);
            }

        }


        dbAdapter.open();
        cartList = dbAdapter.getAllCartItem(address);
        dbAdapter.close();
        cartAdapter = new CartAdapter(context, cartList, this);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerview.setAdapter(cartAdapter);


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemSwipeCallback(this, cartAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerview);

        r_format = AppUtils.readStringFromPref(context, "format");
        if (r_format == null || firmcode == null) {
            showReceipt2();
        } else {
            showReceipt();
        }

        addmore.setOnClickListener(this);
        iv_firm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CartListActivity.this, RecieptFormatActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        if (!AppUtils.readStringFromPref(CartListActivity.this, "AddressAdding").equalsIgnoreCase("normal")) {
            ll_firm.setVisibility(View.VISIBLE);
        } else {
            ll_firm.setVisibility(View.GONE);

        }


        String userName = "" + AppUtils.readStringFromPref(context, "userName");
        if (!userName.equalsIgnoreCase("")) {
            et_customer_name.setText(userName);
        }

        et_second_phone.requestFocus();

        String userType = AppUtils.readStringFromPref(context, "userType");
        if (userType.equalsIgnoreCase("Whole sale")) {
            piece.setVisibility(View.VISIBLE);
        } else {
            piece.setVisibility(View.GONE);
        }


        //// code for discount
        /// cartage discount
        setDiscountPrice();


        cb_newuser = findViewById(R.id.cb_newuser);

        String str_address = AppUtils.readStringFromPref(CartListActivity.this, "AddressAdding");
        if (str_address.equalsIgnoreCase("address")) {


            cb_newuser.setVisibility(View.VISIBLE);
            if (AppUtils.readStringFromPref(context, "isCheckedBox").equalsIgnoreCase("true"))
                cb_newuser.setChecked(true);
            else
                cb_newuser.setChecked(false);


        } else if (str_address.equalsIgnoreCase("new")) {
            cb_newuser.setVisibility(View.VISIBLE);
            if (AppUtils.readStringFromPref(context, "isCheckedBox").equalsIgnoreCase("true"))
                cb_newuser.setChecked(true);
            else
                cb_newuser.setChecked(false);

        } else {
            cb_newuser.setVisibility(View.GONE);
            cb_newuser.setChecked(false);
        }


        cb_newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb_newuser.isChecked())
                    AppUtils.writeStringToPref(context, "isCheckedBox", "true");
                else
                    AppUtils.writeStringToPref(context, "isCheckedBox", "false");
            }
        });


        findViewById(R.id.ll_edit2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (null == address || address.isEmpty()) {
                    Toast.makeText(context, getResources().getString(R.string.please_select_address), Toast.LENGTH_SHORT).show();

                } else if (amount == 0) {
                    Toast.makeText(context, getResources().getString(R.string.please_select_amount), Toast.LENGTH_SHORT).show();

                } else if (to_be_paid == 0) {
                    Toast.makeText(context, getResources().getString(R.string.already_paid), Toast.LENGTH_SHORT).show();

                } else {
                    openDialog(CartListActivity.this, 3, "User Paid Advance Payment for invoice " + orderids, "" + to_be_paid);
                }

            }
        });

        receipt_Type = dbAdapter.getReceiptType();

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, receipt_Type);
        sp_receipt_type.setAdapter(adapter);

        sp_receipt_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (receipt_Type != null && !receipt_Type.isEmpty()) {
                    try {
                        receitpt__type = receipt_Type.get(position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String qr_code = "TotalAmount : " + amt_total + "\nInvoiceNo : " + orderids;

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(qr_code, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            qrCode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

    }

    public void openDialog(final Context context, final int permissionId, final String statement, final String balance) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.layout_advance_payment);
        dialog.show();

        final EditText et_username = (EditText) dialog.findViewById(R.id.et_username);
        final EditText et_passwrod = (EditText) dialog.findViewById(R.id.et_passwrod);
        Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = et_username.getText().toString().trim();
                String pass = et_passwrod.getText().toString().trim();
                if (user.isEmpty()) {
                    Toast.makeText(context, "please enter username", Toast.LENGTH_SHORT).show();

                } else if (pass.isEmpty()) {
                    Toast.makeText(context, "please enter password", Toast.LENGTH_SHORT).show();

                } else {
                    if (!NetworkStatus.getConnectivity(context)) {
                        Toast.makeText(context, context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

                    } else {
                        RequestUserPermissionModel model = new RequestUserPermissionModel();
                        model.setUserName(user);
                        model.setPassword(pass);
                        model.setPermissionId(permissionId);

                        final ProgressDialog dialog1 = new ProgressDialog(context);
                        dialog1.setTitle("Wait Loading ...");
                        dialog1.show();

                        Call<SuperUserResponse> call = RetrofitHandler.getInstance().getApi().getUserPermission(model);
                        call.enqueue(new Callback<SuperUserResponse>() {
                            @Override
                            public void onResponse(Call<SuperUserResponse> call, Response<SuperUserResponse> response) {
                                SuperUserResponse responseDto = response.body();
                                if (response.isSuccessful()) {
                                    if (!responseDto.getError()) {
                                        sendUserActivity(dialog, context, statement, user, balance);

                                    } else {
                                        Toast.makeText(context, responseDto.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                dialog1.dismiss();
                            }

                            @Override
                            public void onFailure(Call<SuperUserResponse> call, Throwable t) {
                                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                                dialog1.dismiss();
                            }
                        });
                    }

                }
            }
        });
    }

    public void sendUserActivity(final Dialog dialog, final Context context, final String statement, final String userName, final String balance) {
        if (!NetworkStatus.getConnectivity(context)) {
            Toast.makeText(context, context.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

        } else {

            UserActivityModel model = new UserActivityModel();
            model.setUesrName(userName);
            model.setBalance(balance);
            model.setActivity(statement);

            final ProgressDialog dialog1 = new ProgressDialog(context);
            dialog1.show();
            dialog1.setTitle("Wait Loading ...");

            Call<SuperUserResponse> call = RetrofitHandler.getInstance().getApi().getUserActivity(model);
            call.enqueue(new Callback<SuperUserResponse>() {
                @Override
                public void onResponse(Call<SuperUserResponse> call, Response<SuperUserResponse> response) {
                    SuperUserResponse responseDto = response.body();
                    if (response.isSuccessful()) {
                        if (!responseDto.getError()) {
                            int tempAmount = ((int) to_be_paid) + AppUtils.readIntFromPref(context, "paid_amount_already");
                            AppUtils.writeIntToPref(context, "paid_amount_already", (int) tempAmount);
                            AppUtils.writeBoolToPref(context, "isAdvancePaid", true);
                            to_be_paid = 0;
                            tv_tobepaid.setText("Rs. " + Math.round(to_be_paid));
                            dialog.dismiss();

                        } else {
                            Toast.makeText(context, responseDto.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    dialog1.dismiss();
                }

                @Override
                public void onFailure(Call<SuperUserResponse> call, Throwable t) {
                    Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                    dialog1.dismiss();
                }
            });
        }

    }

    private void toalFinalAmount() {
        setDiscountPrice();

        double total_kg = 0.0;
        for (int i = 0; i < cartList.size(); i++) {
            total_kg = total_kg + Double.parseDouble(cartList.get(i).getQuantity().replaceAll("-", "."));
        }

        if (("" + total_kg) != null && total_kg > 23) {
            double kg = total_kg - 23;
            double finalValue = Math.round(kg * 100) / 100;

            if (kg > finalValue)
                finalValue = finalValue + 1;

            double avgAmtforOneKg = 0.0;

            if (AppUtils.readStringFromPref(context, "cartage") != null && !AppUtils.readStringFromPref(context, "cartage").isEmpty() && Integer.parseInt(AppUtils.readStringFromPref(context, "cartage")) > 0)
                avgAmtforOneKg = Integer.parseInt(AppUtils.readStringFromPref(context, "cartage")) / 20;

            double temp = 0;
            temp = (((int) finalValue) + 3) * avgAmtforOneKg;
//            cartage_ext_amt = (int) temp;
        }

        int t_amount = 0;
        String temp_amount = null;
        for (int i = 0; i < cartList.size(); i++) {
            try {
                temp_amount = cartList.get(i).getAmount();
                if (temp_amount != null && !temp_amount.equals("null")) {

                    Double tempQuantity = Double.parseDouble("" + cartList.get(i).getAmount());
                    int aa = removeDecimalPrice2("" + tempQuantity);
                    BigDecimal bigDecimal = new BigDecimal(String.valueOf(aa));
                    int netKg = bigDecimal.intValue();
                    t_amount = t_amount + netKg;

                } else {
                    Double temp_amt = Double.parseDouble(cartList.get(i).getPrice()) * Double.parseDouble(cartList.get(i).getQuantity());
                    String temp_amt1 = removeDecimalPrice("" + temp_amt);
                    t_amount = t_amount + Integer.parseInt(temp_amt1);
                }
            } catch (Exception e) {
                Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            }

        }

        amount = Double.valueOf("" + t_amount);
        amt_total = (float) (amount + amt_cartage - amt_dis_cartage - amt_dis_retail);
        tvAmount.setText("Rs. " + Math.round(amt_total));
        to_be_paid = Math.round(amt_total);
        tv_tobepaid.setText("Rs. " + Math.round(to_be_paid));

        try {
            int tempPrepaid = AppUtils.readIntFromPref(context, "paid_amount_already");
            if (tempPrepaid != 0) {
                amt_total = amt_total - tempPrepaid;
                to_be_paid = Math.round(amt_total);
                tv_tobepaid.setText("Rs. " + Math.round(to_be_paid));

            }

        } catch (Exception e) {
            to_be_paid = Math.round(amt_total);
            tv_tobepaid.setText("Rs. " + Math.round(to_be_paid));
        }


    }

    @Override
    protected void onRestart() {
        super.onRestart();


        phone = AppUtils.readStringFromPref(context, "phone");
        address = AppUtils.readStringFromPref(context, "address").replaceAll(",", ", ");
        address = AppUtils.readStringFromPref(context, "address").replaceAll(",  ", ", ");
        /// hindi address
        addressHindi = AppUtils.readStringFromPref(context, "address2").replaceAll(",", ", ");
        addressHindi = AppUtils.readStringFromPref(context, "address2").replaceAll(",  ", ", ");


        if (address != null && !address.isEmpty()) {
            add = address.split(",");
            add2 = add[3].split("/");
            AppUtils.writeStringToPref(context, "area", add[0]);
            AppUtils.writeStringToPref(context, "sector", add[1]);
            AppUtils.writeStringToPref(context, "apartment", add[2]);
            AppUtils.writeStringToPref(context, "block", add2[0]);
            AppUtils.writeStringToPref(context, "houseno", add2[1]);
            AppUtils.writeStringToPref(context, "floor", add[4]);

            companyname = AppUtils.readStringFromPref(context, "companyname");
            firmcode = AppUtils.readStringFromPref(context, "firmcode");


            if (AppUtils.readStringFromPref(context, "changeLanguage").equalsIgnoreCase("english")) {
                tv_customername.setText("Address : " + AppUtils.readStringFromPref(context, "area") + ", " + AppUtils.readStringFromPref(context, "sector") + ", " + AppUtils.readStringFromPref(context, "apartment") +
                        "\n                   " + AppUtils.readStringFromPref(context, "block") + "/" + AppUtils.readStringFromPref(context, "houseno") + ", " + AppUtils.readStringFromPref(context, "floor")
                        + "\n\nPhone :   " + phone);
            } else {

                ///hindi address///////
                addHindi = addressHindi.split(",");
                add2Hindi = addHindi[3].split("/");
                AppUtils.writeStringToPref(context, "area2", addHindi[0]);
                AppUtils.writeStringToPref(context, "sector2", addHindi[1]);
                AppUtils.writeStringToPref(context, "apartment2", addHindi[2]);
                AppUtils.writeStringToPref(context, "block2", add2Hindi[0]);
                AppUtils.writeStringToPref(context, "houseno2", add2Hindi[1]);
                AppUtils.writeStringToPref(context, "floor2", addHindi[4]);

                tv_customername.setText("Address : " + AppUtils.readStringFromPref(context, "area2") + ", " + AppUtils.readStringFromPref(context, "sector2") + ", " + AppUtils.readStringFromPref(context, "apartment2") +
                        "\n                   " + AppUtils.readStringFromPref(context, "block2") + "/" + AppUtils.readStringFromPref(context, "houseno2") + ", " + AppUtils.readStringFromPref(context, "floor2")
                        + "\n\nPhone :   " + phone);
            }
        }


    }

    private void setDiscountPrice() {

        String tempAddress = address.replace("F", "f");


        price.setText(R.string.price_);
        ///// cartage discount
        server_dis_cartage = (0 + Float.parseFloat(dbAdapter.getCartage_discount(tempAddress)));

        //// main cartage amount
        serverCartage = (0 + Float.parseFloat(dbAdapter.getCartageAmount(tempAddress)));


        ////// retail discount
        server_dis_retail = (0 + Float.parseFloat(dbAdapter.getRetailDiscount(tempAddress)));

        ////// calculatoin_weight discount
        calculatoin_weight = (0 + Float.parseFloat(dbAdapter.getCalculationWeight(tempAddress)));


        tv_discount.setText(getResources().getString(R.string.wholesale_discount) + " (" + server_dis_cartage + "%)");
        tv_retail_dis.setText(getResources().getString(R.string.retail_discount) + " (" + server_dis_retail + ")");

        try {
            float totalKg = 0;
            if (cartList.size() > 0) {
                for (Cart kg : cartList) {
                    totalKg = totalKg + Float.parseFloat(kg.getQuantity().replace("-", "."));
                }
            }

            // retail discount
            if (server_dis_retail > 0) {
                amt_dis_retail = totalKg * server_dis_retail;
                retail_amount.setText("Rs. " + Math.round(amt_dis_retail));

            } else {
                amt_dis_retail = 0;
                retail_amount.setText("Rs. " + Math.round(amt_dis_retail));
            }

            // new formula of cartage and calculation_weight

            if (calculatoin_weight > 0) {
                calculatoin_weight += 3;
            } else
                calculatoin_weight = 23;


            //////// main cartage amount of more than 23 kg and below as well
            if (totalKg > calculatoin_weight) {

                float cartAmtFor1Kg = serverCartage / (calculatoin_weight - 3);

//                float totaKgForDiscount = totalKg - 20;
//                float perKgAmt = serverCartage / 20;
//                float finalAmt = totaKgForDiscount * perKgAmt;
//

                amt_cartage = cartAmtFor1Kg * totalKg;
                cartage_amount.setText("Rs. " + Math.round(amt_cartage));

            } else {
                amt_cartage = serverCartage;
                cartage_amount.setText("Rs. " + Math.round(amt_cartage));
            }

            // cartage discount value
            if (amt_cartage > 0 && server_dis_cartage > 0) {

                amt_dis_cartage = amt_cartage * (server_dis_cartage / 100);
                discount_amount.setText("Rs. " + Math.round(amt_dis_cartage));

            } else {
                amt_dis_cartage = 0;
                discount_amount.setText("Rs. " + Math.round(amt_dis_cartage));
            }

        } catch (Exception e) {
            amt_dis_retail = 0;
            retail_amount.setText("Rs. " + Math.round(amt_dis_retail));

            amt_cartage = serverCartage;
            cartage_amount.setText("Rs. " + Math.round(amt_cartage));

            amt_dis_cartage = 0;
            discount_amount.setText("Rs. " + Math.round(amt_dis_cartage));
        }
    }

    public int removeDecimalPrice2(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);

        return Integer.parseInt("" + bdprice);
    }

    public String removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        return "" + bdprice;
    }

    private String getOrderIdWithTime(String orderId) {
        Calendar now = Calendar.getInstance();
        int year_temp = now.get(Calendar.YEAR);
        int year = Integer.parseInt(("" + year_temp).substring(2));
        int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
        int day = now.get(Calendar.DAY_OF_MONTH);
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        int second = now.get(Calendar.SECOND);
        int millis = now.get(Calendar.MILLISECOND);


        if (day <= 9 && month <= 9)
            orderids = orderId + "_0" + day + "0" + month + year + "_" + hour + minute;
        else if (day <= 9)
            orderids = orderId + "_0" + day + month + year + "_" + hour + minute;
        else if (month <= 9)
            orderids = orderId + "_" + day + "0" + month + year + "_" + hour + minute;
        else {
            String tempHour = "" + hour;
            String tempMinute = "" + minute;

            if (hour < 10) {
                tempHour = "0" + hour;
            }

            if (minute < 10) {
                tempMinute = "0" + minute;
            }
            orderids = orderId + "_" + day + month + year + "_" + tempHour + tempMinute;
        }

        return orderids;
    }

    private void setInvoiceNo(String orderId) {
        tv_invoice1.setText("Inv - " + orderids);
        tv_invoice2.setText("Inv - " + orderids);
        tv_invoice3.setText("Inv - " + orderids);
        tv_invoice4.setText("Inv - " + orderids);
    }

    private void setOrderId() {
        list = dbAdapter.LastOrder(firmcode);

        if (list != null && list.size() > 0) {

            String[] temp = list.get(list.size() - 1).toString().split("_");
            String tempOrderNo = temp[0];
            int localdOrderId = 0;

            try {
                int orderIdToInt = Integer.parseInt(temp[0]);
                localdOrderId = orderIdToInt + 1;
            } catch (Exception e) {
                e.printStackTrace();
            }

            orderids = firmcode + "" + localdOrderId;

            if (!dbAdapter.isOrderExist(orderids)) {
                // order id is exist in database
                localdOrderId++;
                orderids = firmcode + "" + localdOrderId;

                for (int i = 0; i < list.size(); i++) {

                    if (dbAdapter.isOrderExist(orderids)) {
                        setInvoiceNo(getOrderIdWithTime(orderids));
                        return;
                    } else {
                        localdOrderId++;
                        orderids = firmcode + "" + localdOrderId;
                    }
                }
            }

        } else {


            String firm_invoice = AppUtils.readStringFromPref(context, "firm_next_invoice_id");

            orderids = "" + firm_invoice;
        }

        setInvoiceNo(getOrderIdWithTime(orderids));
    }

    @Override
    protected void onResume() {
        super.onResume();
        toalFinalAmount();
        try {
            if (getIntent().getStringExtra("editCart") != null) {
                AppUtils.writeStringToPref(context, "editCart", getIntent().getStringExtra("editCart"));
            } else {

            }
            if (getIntent().getStringExtra("orderId") != null && !getIntent().getStringExtra("orderId").equalsIgnoreCase("")) {
                AppUtils.writeStringToPref(context, "orderId", getIntent().getStringExtra("orderId"));
                orderids = AppUtils.readStringFromPref(context, "orderId");
                Log.i("good", "if order id " + orderids);
            } else {
                Log.i("good", "else order id " + orderids);
            }
            if (getIntent().getStringExtra("secondPhone") != null) {
                AppUtils.writeStringToPref(context, "secondPhone", getIntent().getStringExtra("secondPhone"));
            }
            if (getIntent().getStringExtra("customComment") != null) {
                AppUtils.writeStringToPref(context, "customComment", getIntent().getStringExtra("customComment"));
            }
            if (getIntent().getStringExtra("commentId") != null) {
                AppUtils.writeStringToPref(context, "commentId", getIntent().getStringExtra("commentId"));
            }
            if (getIntent().getStringExtra("userName") != null) {
                AppUtils.writeStringToPref(context, "userName", getIntent().getStringExtra("userName"));
            }

            if (AppUtils.readStringFromPref(context, "editCart") != null && AppUtils.readStringFromPref(context, "editCart").equalsIgnoreCase("true")) {
                orderids = AppUtils.readStringFromPref(context, "orderId");
                setInvoiceNo(AppUtils.readStringFromPref(context, "orderId"));
                et_second_phone.setText(AppUtils.readStringFromPref(context, "secondPhone"));
                et_customer_name.setText("" + AppUtils.readStringFromPref(context, "userName"));

            } else {
/////////////////// set order id if user input and increase according to last.......////////////////
                if (AppUtils.readStringFromPref(context, "next_invoice") != null && !AppUtils.readStringFromPref(context, "next_invoice").isEmpty()) {

                    if (dbAdapter.isOrderExist(AppUtils.readStringFromPref(context, "next_invoice"))) {
                        orderids = AppUtils.readStringFromPref(context, "next_invoice");
                        setInvoiceNo(getOrderIdWithTime(orderids));
                    } else {
                        AppUtils.writeStringToPref(context, "next_invoice", "");
                        setOrderId();
                    }

                } else {
                    setOrderId();
                }

            }
            //...............................................................////.................................................................................................//
            r_format = AppUtils.readStringFromPref(context, "format");
            if (r_format == null || firmcode == null) {
                showReceipt2();
            } else {
                showReceipt();
            }

        } catch (Exception e) {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
        }


        if (AppUtils.readStringFromPref(context, "second_phone") != null) {
            et_second_phone.setText(AppUtils.readStringFromPref(context, "second_phone"));
        }

        if (AppUtils.readStringFromPref(context, "customerName") != null) {
            et_customer_name.setText(AppUtils.readStringFromPref(context, "customerName"));
        }
    }

    private void showReceipt2() {
        r_format = AppUtils.readStringFromPref(context, "format");

        if (r_format == null) {
            AppUtils.writeStringToPref(context, "format", "first");
            r_format = AppUtils.readStringFromPref(context, "format");
        }

        if (r_format.contains("first")) {
            ll_first_r.setVisibility(View.VISIBLE);
            ll_second_r.setVisibility(View.GONE);
            ll_third_r.setVisibility(View.GONE);
            ll_fourth_r.setVisibility(View.GONE);

        } else if (r_format.contains("second")) {
            ll_second_r.setVisibility(View.VISIBLE);
            ll_first_r.setVisibility(View.GONE);
            ll_third_r.setVisibility(View.GONE);
            ll_fourth_r.setVisibility(View.GONE);

        } else if (r_format.contains("third")) {
            ll_third_r.setVisibility(View.VISIBLE);
            ll_first_r.setVisibility(View.GONE);
            ll_second_r.setVisibility(View.GONE);
            ll_fourth_r.setVisibility(View.GONE);

        } else {
            ll_fourth_r.setVisibility(View.VISIBLE);
            ll_first_r.setVisibility(View.GONE);
            ll_second_r.setVisibility(View.GONE);
            ll_third_r.setVisibility(View.GONE);

        }
    }

    private void showReceipt() {
        AppUtils.writeStringToPref(this, "navigation_side", "");
        orderid = "" + System.currentTimeMillis();
        r_format = AppUtils.readStringFromPref(context, "format");

        if (r_format == null) {
            AppUtils.writeStringToPref(context, "format", "first");
        }

        if (r_format.contains("first")) {
            ll_first_r.setVisibility(View.VISIBLE);
            ll_second_r.setVisibility(View.GONE);
            ll_third_r.setVisibility(View.GONE);
            ll_fourth_r.setVisibility(View.GONE);
            tv_firm_name1.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmname"));
            tv_address1.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmaddress"));
            tv_mobile1.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmmobile"));

        } else if (r_format.contains("second")) {
            ll_second_r.setVisibility(View.VISIBLE);
            ll_first_r.setVisibility(View.GONE);
            ll_third_r.setVisibility(View.GONE);
            ll_fourth_r.setVisibility(View.GONE);
            tv_firm_name2.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmname"));
            tv_address2.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmaddress"));
            tv_mobile2.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmmobile"));

        } else if (r_format.contains("third")) {
            ll_third_r.setVisibility(View.VISIBLE);
            ll_first_r.setVisibility(View.GONE);
            ll_second_r.setVisibility(View.GONE);
            ll_fourth_r.setVisibility(View.GONE);
            tv_firm_name3.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmname"));
            tv_address3.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmaddress"));
            tv_mobile3.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmmobile"));

        } else {
            ll_fourth_r.setVisibility(View.VISIBLE);
            ll_first_r.setVisibility(View.GONE);
            ll_second_r.setVisibility(View.GONE);
            ll_third_r.setVisibility(View.GONE);
            tv_firm_name4.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmname"));
            tv_address4.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmaddress"));
            tv_mobile4.setText("" + AppUtils.readStringFromPref(CartListActivity.this, "firmmobile"));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_receipt:
                startActivity(new Intent(CartListActivity.this, ChangeReceiptFormat.class));
                break;
            case R.id.back:
                onBackPressed();
                break;

            case R.id.addmore:

                if (et_customer_name.getText().toString().isEmpty())
                    AppUtils.writeStringToPref(context, "customerName", "");
                else
                    AppUtils.writeStringToPref(context, "customerName", et_customer_name.getText().toString().trim());

                if (et_second_phone.getText().toString().isEmpty())
                    AppUtils.writeStringToPref(context, "second_phone", "");
                else
                    AppUtils.writeStringToPref(context, "second_phone", et_second_phone.getText().toString().trim());

                Intent intent = new Intent(CartListActivity.this, AddProductActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;

            case R.id.bt_pay:
                try {

                    if (et_customer_name.getText().toString().isEmpty())
                        AppUtils.writeStringToPref(context, "customerName", "");
                    else
                        AppUtils.writeStringToPref(context, "customerName", et_customer_name.getText().toString().trim());

                    if (et_second_phone.getText().toString().isEmpty())
                        AppUtils.writeStringToPref(context, "second_phone", "");
                    else
                        AppUtils.writeStringToPref(context, "second_phone", et_second_phone.getText().toString().trim());


                    if (address != null && !address.isEmpty()) {

                        et_second_phone.setCursorVisible(false);
                        et_customer_name.setCursorVisible(false);

                        if (cartAdapter.getItemCount() < 1) {
                            Toast.makeText(context, "Please add at least 1 item", Toast.LENGTH_SHORT).show();

                        } else {
                            /// blank comment

                            final List<String> commentList111 = dbAdapter.getComment();
                            if (commentList111.get(spnCommentId).equalsIgnoreCase("Other"))
                                et_ordercomment_print.setText(customComment);
                            else
                                et_ordercomment_print.setText(commentList111.get(spnCommentId));
                            et_usercomment_print.setText(customerComment);


                            if (et_second_phone.getText().toString().isEmpty() || isValidMobile(et_second_phone.getText().toString()) || isValidTelephone(et_second_phone.getText().toString())) {


                                final Dialog dialog = new Dialog(CartListActivity.this);
                                dialog.setContentView(R.layout.dailog_layout);
                                Button yes = (Button) dialog.findViewById(R.id.yes);
                                Button no = (Button) dialog.findViewById(R.id.no);
                                final LinearLayout ll_customAlarm = (LinearLayout) dialog.findViewById(R.id.ll_customAlarm);
                                final Switch aSwitch = (Switch) dialog.findViewById(R.id.sw_alarm);
                                final RelativeLayout rl_editView = dialog.findViewById(R.id.rl_editView);
                                final Spinner alarmComment_spinner = dialog.findViewById(R.id.spinner_alarm_comment);
                                btndate = dialog.findViewById(R.id.buttonDate);
                                btntime = dialog.findViewById(R.id.buttonTime);
                                bt_comment = dialog.findViewById(R.id.bt_comment);


                                final List<String> commentList = new ArrayList<>();
                                commentList.add(getResources().getString(R.string.other));
                                commentList.add(getResources().getString(R.string.kali_roti));
                                commentList.add(getResources().getString(R.string.lal_roti));
                                commentList.add(getResources().getString(R.string.kide_roti));
                                commentList.add(getResources().getString(R.string.sukhi_roti));
                                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, commentList);

                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                alarmComment_spinner.setAdapter(adapter);

                                alarmComment_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (!commentList.isEmpty()) {
                                            ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                                            alarmCommentId = position;
                                            if (position == 0) {
                                                alarmCommentText = "Other";
                                                ll_customAlarm.setVisibility(View.VISIBLE);
                                            } else {
                                                alarmCommentText = commentList.get(position);
                                                ll_customAlarm.setVisibility(View.GONE);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });


                                final EditText etCustomAlarm = dialog.findViewById(R.id.et_otherAlarmcomment);

                                btndate.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        showCalendar();
                                    }
                                });
                                bt_comment.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        phaseIICalled();
                                    }
                                });
                                btntime.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        showClocK();
                                    }
                                });

                                aSwitch.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (aSwitch.isChecked())
                                            rl_editView.setVisibility(View.VISIBLE);
                                        else
                                            rl_editView.setVisibility(View.GONE);
                                    }
                                });
                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        user_name = et_customer_name.getText().toString();
                                        secondPhone = et_second_phone.getText().toString();

                                        if (aSwitch.isChecked()) {
                                            if (btndate.getText().toString().equalsIgnoreCase("Select Date")) {
                                                Toast.makeText(context, "Add Date", Toast.LENGTH_SHORT).show();

                                            } else if (btntime.getText().toString().equalsIgnoreCase("Select Time")) {
                                                Toast.makeText(context, "Add Time", Toast.LENGTH_SHORT).show();

                                            } else if (alarmCommentId == 0 && etCustomAlarm.getText().toString().isEmpty()) {
                                                Toast.makeText(context, "Add Reminder Name", Toast.LENGTH_SHORT).show();

                                            } else {
                                                try {

                                                    showPrintComment();

                                                    if (alarmCommentId == 0)
                                                        alarmCommentText = etCustomAlarm.getText().toString();


                                                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                                                    if (!mBluetoothAdapter.isEnabled()) {
                                                        mBluetoothAdapter.enable();
                                                        Toast.makeText(CartListActivity.this, R.string.open_bluetooth, Toast.LENGTH_SHORT).show();

                                                    } else {
                                                        dbAdapter.open();
                                                        String text = alarmCommentText;
                                                        Log.i("alarm_db", "-" + orderids + ", " + dateET + ", " + timeET + ", text");
//                                                    Toast.makeText(context, "testing "+"-"+orderids+", "+dateET+", "+timeET+", text", Toast.LENGTH_SHORT).show();
                                                        long id = dbAdapter.insertAlarmDatabase(orderids, dateET, timeET, text);
                                                        if (id == -1) {
                                                            Toast.makeText(context, "Try Again", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Boolean isSet = setAlarm(id, text);

                                                            if (!isSet) {
                                                                dbAdapter.deleteAlarmData("" + id);
                                                            } else {
                                                                Toast.makeText(context, R.string.reminder_set, Toast.LENGTH_SHORT).show();
                                                                dbAdapter.close();


                                                                String temp = "";
                                                                temp = AppUtils.readStringFromPref(CartListActivity.this, "phone");
                                                                if (!temp.equalsIgnoreCase("")) {
                                                                } else {
                                                                    AppUtils.writeStringToPref(CartListActivity.this, "phone", "");
                                                                }

                                                                showReceipt();
                                                                String str_address = AppUtils.readStringFromPref(CartListActivity.this, "AddressAdding");

                                                                if (str_address.equalsIgnoreCase("address")) {
                                                                    insertAddressExistMobileUpdate();

                                                                } else if (str_address.equalsIgnoreCase("phone")) {
                                                                    insertNewPhone();

                                                                } else if (str_address.equalsIgnoreCase("new")) {
                                                                    insertNewAddress();

                                                                } else if (str_address.equalsIgnoreCase("normal")) {
                                                                    createOrder();

                                                                } else if (AppUtils.readStringFromPref(context, "editCart") != null && AppUtils.readStringFromPref(context, "editCart").equalsIgnoreCase("true")) {
                                                                    updateOrderPhaseII();
                                                                } else {
                                                                    Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    Toast.makeText(CartListActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        } else {

                                            try {
                                                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                                                if (!mBluetoothAdapter.isEnabled()) {
                                                    mBluetoothAdapter.enable();
                                                    Toast.makeText(CartListActivity.this, R.string.open_bluetooth, Toast.LENGTH_SHORT).show();

                                                } else {
                                                    String temp = "";
                                                    temp = AppUtils.readStringFromPref(CartListActivity.this, "phone");
                                                    if (!temp.equalsIgnoreCase("")) {
                                                    } else {
                                                        AppUtils.writeStringToPref(CartListActivity.this, "phone", "");
                                                    }

                                                    showReceipt();
                                                    String str_address = AppUtils.readStringFromPref(CartListActivity.this, "AddressAdding");
                                                    dialog.dismiss();
                                                    if (str_address.equalsIgnoreCase("address")) {
                                                        insertAddressExistMobileUpdate();

                                                    } else if (str_address.equalsIgnoreCase("phone")) {
                                                        insertNewPhone();

                                                    } else if (str_address.equalsIgnoreCase("new")) {
                                                        insertNewAddress();

                                                    } else if (str_address.equalsIgnoreCase("normal")) {
                                                        createOrder();

                                                    } else if (AppUtils.readStringFromPref(context, "editCart") != null && AppUtils.readStringFromPref(context, "editCart").equalsIgnoreCase("true")) {
                                                        updateOrderPhaseII();

                                                    } else {
                                                        Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            } catch (Exception e) {
                                                Toast.makeText(CartListActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                });

                                no.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();

                            } else {
                                et_second_phone.setError(getResources().getString(R.string.invalid_phone));
                            }
                        }
                    } else {
                        AppClass.isOrderFirst = true;

                        Intent intent2 = new Intent(this, AddCustomerActivity.class);
                        AppUtils.writeStringToPref(this, "flag", "1");
                        intent2.putExtra("canback", true);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent2);
                    }
                } catch (Exception e) {
                    Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private Boolean setAlarm(long id, String text) {
        dbAdapter.open();

        SQLiteDatabase db = dbAdapter.getReadableDatabase();
        Cursor cursor = DBAdapter.selectAlarm(db, "" + id);
        String dateET[] = new String[3];
        String timeET[] = new String[2];
        String date = null, time = null, name = null;

        if (cursor != null) {
            while (cursor.moveToNext()) {
                date = cursor.getString(2);
                time = cursor.getString(3);
                name = cursor.getString(1);
            }
        }

        int k = 0;
        for (String s : date.split("-")) {
            dateET[k++] = s;
        }
        k = 0;
        for (String s : time.split(":")) {
            timeET[k++] = s;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(dateET[2]), Integer.parseInt(dateET[1]) - 1, Integer.parseInt(dateET[0]), Integer.parseInt(timeET[0]), Integer.parseInt(timeET[1]));
        long mili = calendar.getTimeInMillis();
        calendar.setTimeInMillis(mili);

        Calendar calendarCurrent = Calendar.getInstance();
        long miliCurrent = calendarCurrent.getTimeInMillis();
        calendarCurrent.setTimeInMillis(miliCurrent);
        long diff = mili - miliCurrent;
        long currentTime = System.currentTimeMillis();
        long alarmTime = diff + currentTime;
        long temp = 20000;
        long finalTime = mili - temp;

        int alarmId = (int) id;

        Log.i("time_mili", "00 " + mili);
        Log.i("time_miliCurrent", "00 " + miliCurrent);
        Log.i("time_diff", "00 " + diff);
        Log.i("time_currentTime", "00 " + currentTime);
        Log.i("time_alarmTime", "00 " + alarmTime);
        Log.i("time_alarmfinalTime", "00 " + finalTime);

        if (diff < 59000) {
            Toast.makeText(context, R.string.unable_to_set_alarm, Toast.LENGTH_SHORT).show();
            return false;
        } else {

            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("intentType", Constants.ADD_INTENT);
            intent.putExtra("PendingId", alarmId);
            intent.putExtra("DATE", date);
            intent.putExtra("TIME", time);
            intent.putExtra("TODO", text);
            intent.putExtra("alarmId", alarmId);
            intent.putExtra("orderids", orderids);

            String isEdit = AppUtils.readStringFromPref(context, "editCart");

            if (isEdit != null && isEdit.equalsIgnoreCase("true")) {
                Log.i("good", "id=" + alarmId + " : " + orderids);
                PendingIntent alarmIntent = PendingIntent.getBroadcast(context, alarmId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, finalTime, HandleError.getSnoozingTime(context), alarmIntent);

            } else {
                Log.i("good", "id1=" + alarmId + " : " + orderids);
                PendingIntent alarmIntent = PendingIntent.getBroadcast(context, alarmId, intent, 0);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, finalTime, HandleError.getSnoozingTime(context), alarmIntent);
            }
            return true;
        }
    }

    private void showCalendar() {
        try {
            Calendar calendar = Calendar.getInstance();
            final int date = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    month = month + 1;
                    btndate.setText("" + dayOfMonth + "-" + month + "-" + year);
                    dateET = "" + dayOfMonth + "-" + month + "-" + year;
                }
            }, year, month, date);
            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showClocK() {
        try {
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR);
            int minute = calendar.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    btntime.setText("" + hourOfDay + ":" + minute);
                    timeET = "" + hourOfDay + ":" + minute;
                }
            }, hour, minute, true);
            timePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertAddressExistMobileUpdate() {
        String area = AppUtils.readStringFromPref(CartListActivity.this, "area");
        String phone = AppUtils.readStringFromPref(CartListActivity.this, "phone");
        String sector = AppUtils.readStringFromPref(CartListActivity.this, "sector");
        String apartment = AppUtils.readStringFromPref(CartListActivity.this, "apartment");
        String block = AppUtils.readStringFromPref(CartListActivity.this, "block");
        String house = AppUtils.readStringFromPref(CartListActivity.this, "houseno");
        String temp_floor = AppUtils.readStringFromPref(CartListActivity.this, "floor");
        String floor = temp_floor;

        String strAddress = area + "," + sector + "," + apartment + "," + block + "/" + house + "," + temp_floor;
        AppUtils.writeStringToPref(CartListActivity.this, "address", strAddress);
        AppUtils.writeStringToPref(CartListActivity.this, "companyname", "Firm");
        String rowIdd = String.valueOf(rowId);
        dbAdapter.open();

        CustomergetAddress customerAddress = new CustomergetAddress();
        customerAddress.setAddress(strAddress);
        customerAddress.setArea(area);
        customerAddress.setSector(sector);
        customerAddress.setApartment(apartment);
        customerAddress.setBlock(block);
        customerAddress.setHouseno(house);
        customerAddress.setFloor(floor);

        customerAddress.setKothi_flate(AppUtils.readStringFromPref(CartListActivity.this, "kothi"));
        customerAddress.setGali_no(AppUtils.readStringFromPref(CartListActivity.this, "gali"));
        customerAddress.setLift(AppUtils.readStringFromPref(CartListActivity.this, "lift"));
        customerAddress.setExtra(AppUtils.readStringFromPref(CartListActivity.this, "extra"));

        customerAddress.setMobile(phone);
        customerAddress.setMobile2("");
        customerAddress.setMobile3("");
        customerAddress.setMobile4("");
        customerAddress.setMobile5("");
        customerAddress.setFlag("1");
        customerAddress.setTags("green");
        customerAddress.setFirm(firmcode);
        customerAddress.setOrderid(orderid);
        customerAddress.setType(AppUtils.readStringFromPref(context, "userType"));


        List<CustomergetAddress> list = dbAdapter.getExistingNumber(phone, strAddress);
        try {
            if (list.size() > 0) {
                Toast.makeText(CartListActivity.this, "Address already exists ", Toast.LENGTH_SHORT).show();
                int tableid = list.get(0).getId();

                if (list.get(0).getmobile().contains(phone) || list.get(0).getmobile2().contains(phone) || list.get(0).getmobile3().contains(phone) ||
                        list.get(0).getmobile4().contains(phone) || list.get(0).getmobile5().contains(phone)) {
                    Toast.makeText(CartListActivity.this, "Address and mobile already exists !", Toast.LENGTH_SHORT).show();
                    createOrder();
                    AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                    AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                } else {

                    String mobile = "mobile";
                    if (list.get(0).getmobile2().isEmpty()) {
                        mobile = "mobile2";
                        Long rowid = dbAdapter.updateCustomerMobile("" + tableid, mobile, phone, "", orderid);
                        if (rowid == -1) {
                            Toast.makeText(CartListActivity.this, R.string.customer_table_not_update, Toast.LENGTH_SHORT).show();
                            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                        } else {
                            createOrder();
                            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                        }

                    } else if (list.get(0).getmobile3().isEmpty()) {
                        mobile = "mobile3";
                        Long rowid = dbAdapter.updateCustomerMobile("" + tableid, mobile, phone, "", orderid);
                        if (rowid == -1) {
                            Toast.makeText(CartListActivity.this, R.string.customer_table_not_update, Toast.LENGTH_SHORT).show();
                            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                        } else {
                            createOrder();
                            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                        }

                    } else if (list.get(0).getmobile4().isEmpty()) {
                        mobile = "mobile4";
                        Long rowid = dbAdapter.updateCustomerMobile("" + tableid, mobile, phone, "", orderid);
                        if (rowid == -1) {
                            Toast.makeText(CartListActivity.this, R.string.customer_table_not_update, Toast.LENGTH_SHORT).show();
                            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                        } else {
                            createOrder();
                            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                        }

                    } else if (list.get(0).getmobile5().isEmpty()) {
                        mobile = "mobile5";
                        Long rowid = dbAdapter.updateCustomerMobile("" + tableid, mobile, phone, "", orderid);
                        if (rowid == -1) {
                            Toast.makeText(CartListActivity.this, R.string.customer_table_not_update, Toast.LENGTH_SHORT).show();
                            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");

                        } else {
                            createOrder();
                            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                        }

                    } else if (!list.get(0).getmobile5().isEmpty() && !list.get(0).getmobile4().isEmpty() && !list.get(0).getmobile3().isEmpty()
                            && !list.get(0).getmobile2().isEmpty() && !list.get(0).getmobile().isEmpty()) {
                        Toast.makeText(this, "5 Mobile already registered with this address", Toast.LENGTH_SHORT).show();
                    } else {

                        long rowid = dbAdapter.insertCustomerDatatest(customerAddress);
                        createOrder();
                        AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                        AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");

                    }
                }

            } else {

                long rowid = dbAdapter.insertCustomerDatatest(customerAddress);
                if (rowid == -1) {
                    Toast.makeText(CartListActivity.this, "Address is not inserted !", Toast.LENGTH_SHORT).show();

                } else {
                    createOrder();
                    AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
                    AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
                    Toast.makeText(CartListActivity.this, "User Added Successfully !", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void insertNewAddress() {
        String area = AppUtils.readStringFromPref(CartListActivity.this, "area");
        String phone = AppUtils.readStringFromPref(CartListActivity.this, "phone");
        String sector = AppUtils.readStringFromPref(CartListActivity.this, "sector");
        String apartment = AppUtils.readStringFromPref(CartListActivity.this, "apartment");
        String block = AppUtils.readStringFromPref(CartListActivity.this, "block");
        String house = AppUtils.readStringFromPref(CartListActivity.this, "houseno");
        String floor = AppUtils.readStringFromPref(CartListActivity.this, "floor");

        String strAddress = area + "," + sector + "," + apartment + "," + block + "/" + house + "," + floor;
        AppUtils.writeStringToPref(CartListActivity.this, "address", strAddress);

        AppUtils.writeStringToPref(CartListActivity.this, "companyname", "Firm");
        String rowIdd = String.valueOf(rowId);
        dbAdapter.open();

        CustomergetAddress customerAddress = new CustomergetAddress();
        customerAddress.setAddress(strAddress);
        customerAddress.setArea(area);
        customerAddress.setSector(sector);
        customerAddress.setApartment(apartment);
        customerAddress.setBlock(block);
        customerAddress.setHouseno(house);
        customerAddress.setFloor(floor);

        customerAddress.setKothi_flate(AppUtils.readStringFromPref(CartListActivity.this, "kothi"));
        customerAddress.setGali_no(AppUtils.readStringFromPref(CartListActivity.this, "gali"));
        customerAddress.setLift(AppUtils.readStringFromPref(CartListActivity.this, "lift"));
        customerAddress.setExtra(AppUtils.readStringFromPref(CartListActivity.this, "extra"));

        customerAddress.setMobile(phone);
        customerAddress.setMobile2("");
        customerAddress.setMobile3("");
        customerAddress.setMobile4("");
        customerAddress.setMobile5("");
        customerAddress.setFlag("1");
        customerAddress.setTags("green");
        customerAddress.setFirm(firmcode);
        customerAddress.setOrderid(orderid);
        customerAddress.setType(AppUtils.readStringFromPref(context, "userType"));

        List<CustomergetAddress> list = dbAdapter.getExistingNumber(phone, strAddress);

        try {

            long rowid = dbAdapter.insertCustomerDatatest(customerAddress);

            createOrder();
            Toast.makeText(CartListActivity.this, R.string.customer_table_update, Toast.LENGTH_LONG).show();
            AppUtils.writeStringToPref(CartListActivity.this, "newaddress", "");
            Intent intent = new Intent(CartListActivity.this, PrintBluetoothActivity.class);
            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
            Bitmap bitmap;


//            if (et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
//                bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView);
//                et_customer_name.setVisibility(View.GONE);
//                et_second_phone.setVisibility(View.GONE);
//            }

            if (et_customer_name.getText().toString().isEmpty() && !et_second_phone.getText().toString().isEmpty()) {
                et_customer_name.setText("");
                et_customer_name.setHint("");
                bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
            } else if (!et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
                et_second_phone.setText("");
                et_second_phone.setHint("");
                bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
            } else {

                bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
            }

            bitmap.compress(Bitmap.CompressFormat.PNG, 10, _bs);
            intent.putExtra("byteArray", _bs.toByteArray());
            intent.putExtra("userName", user_name);
            intent.putExtra("secondPhone", secondPhone);
            intent.putExtra("commentId", spnCommentId);
            intent.putExtra("customComment", customComment);
            intent.putExtra("customerComment", customerComment);
            intent.putExtra("dis_cartage", amt_dis_cartage);
            intent.putExtra("to_be_paid", to_be_paid);
            intent.putExtra("dis_retail", amt_dis_retail);
            intent.putExtra("cartage_amt", amt_cartage);
            intent.putExtra("total_amt", amt_total);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();

        } catch (Exception e) {

        }
    }

    private void insertNewPhone() {
        String tableId = AppUtils.readStringFromPref(CartListActivity.this, "tableId");
        String columnName = AppUtils.readStringFromPref(CartListActivity.this, "columnName");
        String rowIdd = orderid;

        long rowId2 = dbAdapter.insertCustomerData2(tableId, columnName, phonestr, firmcode, orderids);
        if (rowId2 == -1) {
            Toast.makeText(CartListActivity.this, R.string.not_save, Toast.LENGTH_SHORT).show();

        } else {
            createOrder();
            AppUtils.writeStringToPref(CartListActivity.this, "AddressAdding", "");
        }
    }

    public void createOrder() {
        if (cb_newuser.isChecked() && isSampleProductExist()) {
            tags_new = "(S) + (Z)";

        } else if (cb_newuser.isChecked()) {
            tags_new = "(Z)";

        } else if (isSampleProductExist()) {
            tags_new = "(S)";

        } else if (!cb_newuser.isChecked() && !isSampleProductExist()) {
            tags_new = "";

        } else {
            tags_new = "";
        }


        Bitmap bitmap1;

//        if (et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
////                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView);
//
//            et_customer_name.setVisibility(View.GONE);
//            et_second_phone.setVisibility(View.GONE);
//
//        }


        if (et_customer_name.getText().toString().isEmpty() && !et_second_phone.getText().toString().isEmpty()) {
            et_customer_name.setText("");
            et_customer_name.setHint("");
            bitmap1 = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
        } else if (!et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
            et_second_phone.setText("");
            et_second_phone.setHint("");
            bitmap1 = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
        } else {
            bitmap1 = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
        }

        ByteArrayOutputStream bs1 = new ByteArrayOutputStream();
        bitmap1.compress(Bitmap.CompressFormat.PNG, 10, bs1);

        Bitmap bitmap2 = ScreenshotUtil.getScreenshotFromRecyclerView(recyclerview);
        ByteArrayOutputStream bs2 = new ByteArrayOutputStream();
        bitmap2.compress(Bitmap.CompressFormat.PNG, 10, bs2);

        Bitmap bitmap3 = ScreenshotUtil.getInstance().takeScreenshotForView(ll_bottom_amount);
        ByteArrayOutputStream bs3 = new ByteArrayOutputStream();
        bitmap3.compress(Bitmap.CompressFormat.PNG, 10, bs3);

        try {
            if (AppUtils.readStringFromPref(context, "editCart") != null && AppUtils.readStringFromPref(context, "editCart").equalsIgnoreCase("true")) {
                int comment_id = 0;
                String custom_comment = "";
                Order order = new Order();
                if (!isSpinnerSelected) {
                    if (AppUtils.readStringFromPref(context, "commentId") != null) {
                        comment_id = Integer.parseInt(AppUtils.readStringFromPref(context, "commentId"));
                    }
                    if (AppUtils.readStringFromPref(context, "customComment") != null) {
                        custom_comment = AppUtils.readStringFromPref(context, "customComment");
                    }
                } else {
                    comment_id = commentId;
                    custom_comment = customComment;
                    if (commentId != 0)
                        custom_comment = "";
                    else
                        custom_comment = customComment;
                }

                order.setComment_id("" + comment_id);
                order.setCustom_comment("" + custom_comment);
                order.setOrderId("" + AppUtils.readStringFromPref(context, "orderId"));
                order.setMobile2("" + et_second_phone.getText().toString());
                order.setTotal_amount("" + Math.round(amt_total));
                order.setReceipt_type(receitpt__type);
                long rowId = dbAdapter.updateOrderEditCart(order);

                if (rowId > -1) {
                    String companystr = orderids;
                    long row = dbAdapter.updateCustomerLastOrderForEdit(address, et_customer_name.getText().toString());

                    if (row == -1) {
                    } else {
                        if (cartList.size() > 0)
                            dbAdapter.deleteAfterOrder(AppUtils.readStringFromPref(context, "orderId"));

                        for (int j = 0; j < cartList.size(); j++) {
                            Cart cart = new Cart();
                            cart.setProductname(cartList.get(j).getProductname());
                            cart.setPrice(cartList.get(j).getPrice());
                            cart.setPiece(cartList.get(j).getPiece() != null ? cartList.get(j).getPiece() : "null");
                            cart.setProductid(cartList.get(j).getProductid());
                            cart.setQuantity(cartList.get(j).getQuantity());
                            cart.setAddress(address);
                            cart.setPhone(phone);
                            cart.setAmount(cartList.get(j).getAmount());
                            cart.setOrderid(AppUtils.readStringFromPref(context, "orderId"));
                            cart.setFlag("1");
                            Log.i("data_user_inser", cartList.get(j).getPiece() != null ? cartList.get(j).getPiece() : "null");

                            dbAdapter.insertAfterOrderCreate(cart);
                        }

                        // update image for imageorder
                        ByteArrayOutputStream bs = new ByteArrayOutputStream();
                        Bitmap newBitmap = combineImages(bitmap1, bitmap2, bitmap3);
                        newBitmap.compress(Bitmap.CompressFormat.PNG, 10, bs);

                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date date1 = new Date();
                            firmcode = AppUtils.readStringFromPref(CartListActivity.this, "firmcode");
                            OrderImageListData orderImage = new OrderImageListData();
                            orderImage.setReceipt_no(AppUtils.readStringFromPref(context, "orderId"));
                            orderImage.setImage(Base64.encodeToString(bs.toByteArray(), Base64.DEFAULT));
                            orderImage.setDate(dateFormat.format(date1));
                            orderImage.setAmount("" + amount);
                            dbAdapter.open();
                            rowId = dbAdapter.updateOrderImage(orderImage);

                            if (rowId == -1)
                                Log.i("orderImage", "not insrted");
//                                Toast.makeText(CartListActivity.this, "Order Image not updated Successfully", Toast.LENGTH_LONG).show();
                            else {
                                Intent intent = new Intent(CartListActivity.this, PrintBluetoothActivity.class);
                                ByteArrayOutputStream _bs = new ByteArrayOutputStream();

                                Bitmap bitmap;

//                                if (et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
//                                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView);
//                                    et_customer_name.setVisibility(View.GONE);
//                                    et_second_phone.setVisibility(View.GONE);
//                                }

                                if (et_customer_name.getText().toString().isEmpty() && !et_second_phone.getText().toString().isEmpty()) {
                                    et_customer_name.setText("");
                                    et_customer_name.setHint("");
                                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                                } else if (!et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
                                    et_second_phone.setText("");
                                    et_second_phone.setHint("");
                                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                                } else {
                                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                                }
                                bitmap.compress(Bitmap.CompressFormat.PNG, 10, _bs);
                                intent.putExtra("byteArray", _bs.toByteArray());
                                intent.putExtra("userName", user_name);
                                intent.putExtra("secondPhone", secondPhone);
                                intent.putExtra("commentId", spnCommentId);
                                intent.putExtra("customComment", customComment);
                                intent.putExtra("customerComment", customerComment);
                                intent.putExtra("dis_cartage", amt_dis_cartage);
                                intent.putExtra("to_be_paid", to_be_paid);
                                intent.putExtra("dis_retail", amt_dis_retail);
                                intent.putExtra("cartage_amt", amt_cartage);
                                intent.putExtra("total_amt", amt_total);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                                finish();
                            }

                        } catch (Exception e) {
//                            Toast.makeText(context, "Error while update image order ...", Toast.LENGTH_SHORT).show();
                        }
                    }
                }


            } else {
                // fresh order

                ByteArrayOutputStream bs = new ByteArrayOutputStream();
                Bitmap newBitmap = combineImages(bitmap1, bitmap2, bitmap3);
                newBitmap.compress(Bitmap.CompressFormat.PNG, 10, bs);
                sendImage(Base64.encodeToString(bs.toByteArray(), Base64.DEFAULT));

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date1 = new Date();
                firmcode = AppUtils.readStringFromPref(CartListActivity.this, "firmcode");
                Order order = new Order();
                order.setDate(dateFormat.format(date1));
                order.setPhone(phonestr);
                order.setAddress(address);
                order.setOrderId(orderids);
                order.setFirm(firmcode);
                order.setFlag("1");
                order.setMobile2(secondPhone);
                order.setReceipt_type(receitpt__type);
                if (commentId == -1)
                    order.setComment_id("");
                else
                    order.setComment_id("" + commentId);
                if (commentId != 0)
                    customComment = "";

                order.setCustom_comment(customComment);
                try {
                    order.setInvalid(invalid);
                } catch (Exception e) {
                    order.setInvalid("0");
                }
                order.setMobile2(et_second_phone.getText().toString());
                order.setLock_status("0");
                order.setTotal_amount("" + amt_total);

                if (to_be_paid == 0)
                    order.setStatus(2);
                else
                    order.setStatus(1);

                dbAdapter.open();
                rowId = dbAdapter.insertOrder(order);

                if (rowId == -1)
                    Toast.makeText(CartListActivity.this, R.string.order_not_created_success, Toast.LENGTH_LONG).show();

                else {
                    String companystr = orderids;
                    Toast.makeText(CartListActivity.this, R.string.order_created_success, Toast.LENGTH_LONG).show();

                    long row = dbAdapter.updateCustomerLastOrder(address, user_name, companystr, firmcode, tags_new, "");

                    if (row > -1) {
                        for (int j = 0; j < cartList.size(); j++) {
                            Cart cart = new Cart();
                            cart.setProductname(cartList.get(j).getProductname());
                            cart.setPrice(cartList.get(j).getPrice());
                            cart.setPiece(cartList.get(j).getPiece() != null ? cartList.get(j).getPiece() : "null");
                            cart.setProductid(cartList.get(j).getProductid());
                            cart.setQuantity(cartList.get(j).getQuantity());
                            cart.setAddress(address);
                            cart.setPhone(phone);
                            cart.setAmount(cartList.get(j).getAmount());
                            cart.setOrderid(companystr);
                            cart.setFlag("1");
                            dbAdapter.insertAfterOrderCreate(cart);
                        }
                        AppUtils.writeStringToPref(CartListActivity.this, "order_id", companystr);
                        Intent intent = new Intent(CartListActivity.this, PrintBluetoothActivity.class);
                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        Bitmap bitmap;

//                        if (et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
////                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView);
//
//                            et_customer_name.setVisibility(View.GONE);
//                            et_second_phone.setVisibility(View.GONE);
//
//                        }

                        if (et_customer_name.getText().toString().isEmpty() && !et_second_phone.getText().toString().isEmpty()) {
                            et_customer_name.setText("");
                            et_customer_name.setHint("");
                            bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                        } else if (!et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
                            et_second_phone.setText("");
                            et_second_phone.setHint("");
                            bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                        } else {
                            bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                        }
                        bitmap.compress(Bitmap.CompressFormat.PNG, 10, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        intent.putExtra("userName", user_name);
                        intent.putExtra("secondPhone", secondPhone);
                        intent.putExtra("commentId", spnCommentId);
                        intent.putExtra("customComment", customComment);
                        intent.putExtra("customerComment", customerComment);
                        intent.putExtra("dis_cartage", amt_dis_cartage);
                        intent.putExtra("to_be_paid", to_be_paid);
                        intent.putExtra("dis_retail", amt_dis_retail);
                        intent.putExtra("cartage_amt", amt_cartage);
                        intent.putExtra("total_amt", amt_total);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(context, "Error while create order ...", Toast.LENGTH_SHORT).show();
        }
    }

    public int removeDecimalPriceInt(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        return Integer.parseInt("" + bdprice);
    }

    public boolean isSampleProductExist() {
        Boolean isSampleExist = false;
        try {
            if (cartList != null) {

                for (Cart cart : cartList) {

                    Log.i("tags_produt", "-- " + cart.getProductid());
                    Log.i("tags_produt2", "-- " + cart.getProductname());

                    if (cart.getProductid().equalsIgnoreCase("Sample")) {
                        isSampleExist = true;
                        return isSampleExist;
                    }

                }
            }
        } catch (Exception e) {

        }
        return isSampleExist;

    }

    @Override
    public void onDiscoveryComplete(ArrayList<String> aemPrinterList) {
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    @Override
    public void onCartItemRemoved() {
        try {
            invalid = "0";

            if (cartAdapter.getItemCount() < 1) {

                amt_dis_retail = 0;
                retail_amount.setText("Rs. " + Math.round(amt_dis_retail));

                amt_cartage = serverCartage;
                cartage_amount.setText("Rs. " + Math.round(amt_cartage));

                amt_dis_cartage = 0;
                discount_amount.setText("Rs. " + Math.round(amt_dis_cartage));

                amt_total = 0;
                tvAmount.setText("Rs. " + amt_total);

                finish();

            } else
                toalFinalAmount();

        } catch (Exception e) {
            Toast.makeText(context, "Error while item remove ...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void hidePiece() {
        piece.setVisibility(View.GONE);
    }

    private void phaseIICalled() {
        final List<String> commentList = dbAdapter.getComment();
        final List<String> commentIdList = dbAdapter.getCommentId();


        for (int i = 0; i < commentList.size(); i++) {
            Log.i("comment_value1", "-" + commentList.get(i));
        }
        for (int i = 0; i < commentIdList.size(); i++) {
            Log.i("comment_value", "-" + commentIdList.get(i));
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, commentList);
        final String otherCommentEt, orderCommetnEt;

        final Dialog dialog = new Dialog(CartListActivity.this);
        dialog.setContentView(R.layout.layout_commentdailog);
        dialog.show();
        Button bt_submit = (Button) dialog.findViewById(R.id.bt_submit);
        final TextView tv_other = dialog.findViewById(R.id.tv_other);
        final EditText et_othercomment = dialog.findViewById(R.id.et_othercomment);
        final EditText et_ordercomment = dialog.findViewById(R.id.et_ordercomment);

        Spinner spinner_customercomment = dialog.findViewById(R.id.spinner_customercomment);
        dbAdapter.open();

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_customercomment.setAdapter(adapter);

        spinner_customercomment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!commentList.isEmpty()) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    isSpinnerSelected = true;
                    commentId = Integer.parseInt(commentIdList.get(position));
                    spnCommentId = Integer.parseInt(commentIdList.get(position));
                    if (commentList.get(position).equalsIgnoreCase("Other")) {
                        et_othercomment.setVisibility(View.VISIBLE);
                        tv_other.setVisibility(View.VISIBLE);
                    } else {
                        et_othercomment.setVisibility(View.GONE);
                        tv_other.setVisibility(View.GONE);
                    }
                    commentIdEt = Integer.parseInt(commentIdList.get(position));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customComment = et_othercomment.getText().toString();
                customerComment = et_ordercomment.getText().toString();

                et_ordercomment_print.setText(customComment);
                et_usercomment_print.setText(customerComment);


                dialog.dismiss();
            }
        });

    }

    public void sendImage(String image) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = new Date();
            firmcode = AppUtils.readStringFromPref(CartListActivity.this, "firmcode");
            OrderImageListData orderImage = new OrderImageListData();
            orderImage.setReceipt_no(orderids);
            orderImage.setFirm(firmcode);
            orderImage.setImage(image);
            orderImage.setDate(dateFormat.format(date1));
            orderImage.setAmount("" + amount);
            orderImage.setFlag("1");
            dbAdapter.open();
            rowId = dbAdapter.insertOrderImage(orderImage);

        } catch (Exception e) {
//            Toast.makeText(context, "Error while create image order ...", Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap combineImages(Bitmap bmp1, Bitmap bmp2, Bitmap bmp3) {
        Bitmap b3 = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight() + bmp2.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b3);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, bmp1.getHeight(), null);

        Bitmap b4 = Bitmap.createBitmap(b3.getWidth(), b3.getHeight() + bmp3.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas1 = new Canvas(b4);
        canvas1.drawBitmap(b3, new Matrix(), null);
        canvas1.drawBitmap(bmp3, 0, b3.getHeight(), null);

        return b4;
    }

    // checkwhen this is called
    private void updateOrderPhaseII() {
        int comment_id = 0;
        String custom_comment = "";
        Order order = new Order();
        if (!isSpinnerSelected) {
            if (AppUtils.readStringFromPref(context, "commentId") != null) {
                comment_id = Integer.parseInt(AppUtils.readStringFromPref(context, "commentId"));
            }
            if (AppUtils.readStringFromPref(context, "customComment") != null) {
                custom_comment = AppUtils.readStringFromPref(context, "customComment");
            }
        } else {
            comment_id = commentId;
            custom_comment = customComment;
            if (commentId != 0)
                custom_comment = "";
            else
                custom_comment = customComment;
        }
        order.setComment_id("" + comment_id);
        order.setCustom_comment("" + custom_comment);
        order.setOrderId("" + AppUtils.readStringFromPref(context, "orderId"));
        order.setMobile2("" + et_second_phone.getText().toString());
        long rowId = dbAdapter.updateOrderEditCart(order);

        if (rowId > -1) {
            String companystr = orderids;
            long row = dbAdapter.updateCustomerLastOrderForEdit(address, et_customer_name.getText().toString());

            if (row == -1) {
            } else {
                if (cartList.size() > 0)
                    dbAdapter.deleteAfterOrder(AppUtils.readStringFromPref(context, "orderId"));
                for (int j = 0; j < cartList.size(); j++) {
                    Cart cart = new Cart();
                    cart.setProductname(cartList.get(j).getProductname());
                    cart.setPrice(cartList.get(j).getPrice());
                    cart.setProductid(cartList.get(j).getProductid());
                    cart.setQuantity(cartList.get(j).getQuantity());
                    cart.setAddress(address);
                    cart.setPhone(phone);
                    cart.setAmount(cartList.get(j).getAmount());
                    cart.setOrderid(AppUtils.readStringFromPref(context, "orderId"));
                    cart.setFlag("1");
                    dbAdapter.insertAfterOrderCreate(cart);
                }

                Intent intent = new Intent(CartListActivity.this, PrintBluetoothActivity.class);
                ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                Bitmap bitmap;

//                if (et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
//
//                    et_customer_name.setVisibility(View.GONE);
//                    et_second_phone.setVisibility(View.GONE);
//                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
//                }

                if (et_customer_name.getText().toString().isEmpty() && !et_second_phone.getText().toString().isEmpty()) {
                    et_customer_name.setText("");
                    et_customer_name.setHint("");
                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                } else if (!et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
                    et_second_phone.setText("");
                    et_second_phone.setHint("");
                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                } else {
                    bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(parentView2);
                }

                bitmap.compress(Bitmap.CompressFormat.PNG, 10, _bs);
                intent.putExtra("byteArray", _bs.toByteArray());
                intent.putExtra("userName", user_name);
                intent.putExtra("secondPhone", secondPhone);
                intent.putExtra("commentId", spnCommentId);
                intent.putExtra("customComment", customComment);
                intent.putExtra("customerComment", customerComment);
                intent.putExtra("dis_cartage", amt_dis_cartage);
                intent.putExtra("to_be_paid", to_be_paid);
                intent.putExtra("dis_retail", amt_dis_retail);
                intent.putExtra("cartage_amt", amt_cartage);
                intent.putExtra("total_amt", amt_total);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                finish();
            }
        }

    }

    public boolean isValidMobile(String s) {
        Pattern p = Pattern.compile("(0/91)?[5-9][0-9]{9}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    public boolean isValidTelephone(String s) {
        Pattern p = Pattern.compile("[2-9][0-9]{7}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }


    private void hitApi(String getOrderid) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please Wait");
        progressDialog.show();


        Call<ResponseSearchProduct> call = RetrofitHandler.getInstance().getApi().getShowOrderData("" + BuildConfig.VERSION_CODE, "" + getOrderid);

        call.enqueue(new Callback<ResponseSearchProduct>() {
            @Override
            public void onResponse(Call<ResponseSearchProduct> call, final Response<ResponseSearchProduct> response) {
                ResponseSearchProduct responseDto = response.body();

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                if (response.isSuccessful() && responseDto.isError() == false) {

                    if (response.body().getData() != null && response.body().getData().size() > 0) {

                        for (int i = 0; i < response.body().getData().size(); i++) {
                            Cart model = new Cart();
                            model.setProductid(response.body().getData().get(i).getProduct_id());
                            model.setProductname(response.body().getData().get(i).getProduct_name());
                            model.setPrice(response.body().getData().get(i).getPrice());
                            model.setQuantity(response.body().getData().get(i).getQty());
                            model.setAddress(address);
                            model.setPhone(phone);
                            cartList.add(model);
                            dbAdapter.insertCart(model);

                        }
                        cartAdapter.notifyDataSetChanged();
                        toalFinalAmount();

//                        setAdapterData(getOrderid, userAddress);

                    } else {
                        Toast.makeText(context, responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, responseDto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSearchProduct> call, final Throwable t) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
//                HandleError.handleTimeoutError(context, t, "858");
            }
        });
    }

    private void showPrintComment(){

        if (et_customer_name.getText().toString().isEmpty() && et_second_phone.getText().toString().isEmpty()) {
            et_customer_name.setVisibility(View.GONE);
            et_second_phone.setVisibility(View.GONE);
            ll_name_number.setVisibility(View.GONE);
            Log.i("et_order_commetn", "-" + et_ordercomment_print.getText().toString() + "+");
        }

        if (et_ordercomment_print.getText().toString().isEmpty() && et_usercomment_print.getText().toString().isEmpty()) {
            Log.i("et_order_commetn", "first if");
            et_ordercomment_print.setVisibility(View.GONE);
            et_usercomment_print.setVisibility(View.GONE);

        } else if (!et_ordercomment_print.getText().toString().isEmpty()) {
            Log.i("et_order_commetn", "else if");
            et_ordercomment_print.setVisibility(View.VISIBLE);
            et_usercomment_print.setVisibility(View.GONE);

        } else if (!et_usercomment_print.getText().toString().isEmpty()) {
            Log.i("et_order_commetn", "second else if");
            et_usercomment_print.setVisibility(View.VISIBLE);
            et_ordercomment_print.setVisibility(View.GONE);

        } else {
            Log.i("et_order_commetn", "all else");
            et_ordercomment_print.setVisibility(View.VISIBLE);
            et_usercomment_print.setVisibility(View.VISIBLE);
        }
    }


}