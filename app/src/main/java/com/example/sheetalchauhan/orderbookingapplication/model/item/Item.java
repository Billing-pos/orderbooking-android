package com.example.sheetalchauhan.orderbookingapplication.model.item;

public class Item {
    private String itemName;
    private String itemMrp;
    private String itemMsp;
    private String itemWeight;
    private String qty;
    private String currentQty;
    private String date;
    private String orderid;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemMrp() {
        return itemMrp;
    }

    public void setItemMrp(String itemMrp) {
        this.itemMrp = itemMrp;
    }

    public String getItemMsp() {
        return itemMsp;
    }

    public void setItemMsp(String itemMsp) {
        this.itemMsp = itemMsp;
    }

    public String getItemWeight() {
        return itemWeight;
    }

    public void setItemWeight(String itemWeight) {
        this.itemWeight = itemWeight;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(String currentQty) {
        this.currentQty = currentQty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }
}
