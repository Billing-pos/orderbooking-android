package com.example.sheetalchauhan.orderbookingapplication.breceiver;

public class Constants {
    public static final int REQUEST_EDIT = 0;
    public static final int REQUEST_ADD = 1;
    public static final int RESULT_CANCEL = 2;
//    public static final String ADD_INTENT = "com.example.leanh.util.Constant.ADD_INTENT";
//    // / this use for cancel alarm time
//    public static final String OFF_INTENT = "com.example.leanh.util.Constant.OFF_INTENT";

    public static final String ADD_INTENT = "com.example.ADD_INTENT";
    // / this use for cancel alarm time
    public static final String OFF_INTENT = "com.example.OFF_INTENT";


}
