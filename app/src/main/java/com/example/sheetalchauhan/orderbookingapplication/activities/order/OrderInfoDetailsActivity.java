package com.example.sheetalchauhan.orderbookingapplication.activities.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sheetalchauhan.orderbookingapplication.R;
import com.example.sheetalchauhan.orderbookingapplication.databse.DBAdapter;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Cart;
import com.example.sheetalchauhan.orderbookingapplication.model.item.Order;
import com.example.sheetalchauhan.orderbookingapplication.utility.AppUtils;

import java.math.BigDecimal;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderInfoDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_tags)
    TextView tv_tags;
    @BindView(R.id.tv_phone)
    TextView tv_phone;
    @BindView(R.id.tv_cancel_order)
    TextView tv_cancel_order;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.bt_repeat_order)
    Button bt_repeat_order;
    @BindView(R.id.bt_cancel_order)
    Button bt_cancel_order;
    @BindView(R.id.tv_order)
    TextView tv_order;
    @BindView(R.id.ll_layout)
    LinearLayout ll_layout;
    String invoiceno, orderitems = "";
    DBAdapter dbAdapter;
    List<Cart> cartList;
    List<Order> orderList;
    long rowid;
    Context context;
    String[] add = null;
    String[] add2 = null;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_info_details);
        context = this;
        ButterKnife.bind(this);
        tv_name.setText(R.string.order_details);
        back.setOnClickListener(this);
        dbAdapter = DBAdapter.getInstance(this);
        invoiceno = AppUtils.readStringFromPref(OrderInfoDetailsActivity.this, "invoiceno");

        dbAdapter.open();

        if (getIntent().getStringExtra("through") != null) {

            if (getIntent().getStringExtra("through").equalsIgnoreCase("address")) {
                String orderAddress = AppUtils.readStringFromPref(this, "orderAddress").replaceAll("/ ", "/");
                orderList = dbAdapter.getOrderListThroughAddress(orderAddress);
                String addressOrderId = dbAdapter.getOrderList1(orderAddress);
                cartList = dbAdapter.getOrderProduct(addressOrderId);

            } else {
                orderList = dbAdapter.getOrderList(invoiceno);
                cartList = dbAdapter.getOrderProduct(invoiceno);
            }

        } else {
            orderList = dbAdapter.getOrderList(invoiceno);
            cartList = dbAdapter.getOrderProduct(invoiceno);
        }

        if (orderList.size() > 0) {
            tv_address.setText("" + orderList.get(0).getAddress());
            tv_phone.setText("" + orderList.get(0).getPhone());
            if (cartList.size() > 0) {
                for (int i = 0; i < cartList.size(); i++) {
                    if (orderitems.isEmpty()) {
                        orderitems = "-  " + cartList.get(i).getProductname() + "\n";
                    } else {
                        orderitems = orderitems + "-  " + cartList.get(i).getProductname() + "\n";
                    }
                }
                tv_order.setText("" + orderitems);
            }

            if (orderList.get(0).getInvalid().equalsIgnoreCase("2")) {
                tv_cancel_order.setVisibility(View.VISIBLE);
                bt_cancel_order.setVisibility(View.GONE);
            } else {
                tv_cancel_order.setVisibility(View.GONE);
                bt_cancel_order.setVisibility(View.VISIBLE);
            }

            bt_cancel_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long rowId = dbAdapter.cancelOrder(invoiceno);
                    if (rowId == -1) {
                    } else {
                        finish();
                    }
                }
            });

        } else {
            ll_layout.setVisibility(View.GONE);
            bt_cancel_order.setVisibility(View.GONE);
            Toast.makeText(OrderInfoDetailsActivity.this, R.string.no_orderfound, Toast.LENGTH_SHORT).show();
        }


        dbAdapter.close();
        bt_repeat_order.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.bt_repeat_order:
                if (orderList.size() > 0) {
                    dbAdapter.deleteCart();
                    String phone = orderList.get(0).getPhone();
                    String address = orderList.get(0).getAddress();

                    try {
                        String userTye = dbAdapter.getCustomerType(address);
                        if (userTye != null && !userTye.isEmpty()) {
                            AppUtils.writeStringToPref(this, "userType", userTye);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    if (cartList.size() > 0) {

                        AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "AddressAdding", "normal");
                        for (int i = 0; i < cartList.size(); i++) {
                            Cart cart = new Cart();
                            cart.setProductname(cartList.get(i).getProductname());
                            cart.setProductid(cartList.get(i).getProductid());
                            cart.setPrice(cartList.get(i).getPrice());
                            cart.setPiece(cartList.get(i).getPiece() != null ? cartList.get(i).getPiece() : "null");
                            Log.i("data_user_orderinfo", cart.getPiece() + "--" + cartList.get(i).getPiece() != null ? cartList.get(i).getPiece() : "null");
                            cart.setQuantity(cartList.get(i).getQuantity());

//                            Double tempMoney = 0.0;
//                            tempMoney = Double.parseDouble(cartList.get(i).getAmount());
                            cart.setAmount("" + cartList.get(i).getAmount());

                            cart.setPhone(phone);
                            cart.setAddress(address);
                            rowid = dbAdapter.insertCart(cart);
                        }
                        if (rowid > 0) {
                            AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "phone", phone);
                            AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "address", address);

                            add = address.split(",");
                            add2 = add[3].split("/");
                            AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "area", add[0]);
                            AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "sector", add[1]);
                            AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "apartment", add[2]);
                            AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "block", add2[0]);
                            AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "houseno", add2[1]);
                            AppUtils.writeStringToPref(OrderInfoDetailsActivity.this, "floor", add[4]);

                            Intent intent = new Intent(OrderInfoDetailsActivity.this, CartListActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        } else {

                        }
                    }

                }
                break;
        }
    }


    public String removeDecimalPrice(String Price) {
        BigDecimal bdprice = new BigDecimal(Price);
        bdprice = bdprice.setScale(0, BigDecimal.ROUND_UP);
        return "" + bdprice;
    }
}
