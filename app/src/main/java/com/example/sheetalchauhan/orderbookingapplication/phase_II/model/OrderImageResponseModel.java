package com.example.sheetalchauhan.orderbookingapplication.phase_II.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderImageResponseModel {
    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private List<OrderImageListData> orderImageListData;

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<OrderImageListData> getOrderImageListData() {
        return orderImageListData;
    }

    public void setOrderImageListData(List<OrderImageListData> orderImageListData) {
        this.orderImageListData = orderImageListData;
    }
}
